/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100406
 Source Host           : localhost:3306
 Source Schema         : capnuoc_hgg

 Target Server Type    : MySQL
 Target Server Version : 100406
 File Encoding         : 65001

 Date: 12/05/2021 08:04:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for LICH_SU_CUP_NUOC
-- ----------------------------
DROP TABLE IF EXISTS `LICH_SU_CUP_NUOC`;
CREATE TABLE `LICH_SU_CUP_NUOC`  (
  `ID_LICH_SU_CUP_NUOC` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_HOA_DON` bigint(20) NOT NULL,
  `ID_NGUOI_CAP_NHAT` bigint(20) NOT NULL,
  `NGAY_GIO_CAP_NHAT` datetime(0) NOT NULL,
  `CUP_NUOC` tinyint(1) NOT NULL,
  `ID_THANG` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_LICH_SU_CUP_NUOC`) USING BTREE,
  INDEX `LOG_CUP_NUOC`(`ID_HOA_DON`) USING BTREE,
  CONSTRAINT `LOG_CUP_NUOC` FOREIGN KEY (`ID_HOA_DON`) REFERENCES `HOA_DON` (`ID_HOA_DON`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_vietnamese_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
