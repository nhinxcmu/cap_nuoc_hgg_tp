CREATE DEFINER=`root`@`localhost` PROCEDURE `bangKeCatNuocKhachHangThang_Call`(IN idThang bigint, IN idKhuVuc bigint, IN idDoiTuongCupNuoc int, IN idSoGhi varchar(1000))
BEGIN

IF idDoiTuongCupNuoc = 0 THEN
		CALL bangKeCatNuocKhachHangThang_TatCaDoiTuong(idThang,idKhuVuc,idSoGhi);
	ELSE
		CALL bangKeCatNuocKhachHangThang_TheoDoiTuong(idThang,idKhuVuc,idDoiTuongCupNuoc,idSoGhi);
	END IF;

END