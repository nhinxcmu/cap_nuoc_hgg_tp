CREATE DEFINER=`root`@`localhost` PROCEDURE `lichSuCatNuoc_Call`(IN idThang bigint, IN idKhuVuc bigint, IN idSoGhi varchar(1000), IN trangThai int)
BEGIN

IF trangThai = -1 THEN
		CALL lichSuCatNuoc_All(idThang,idKhuVuc,idSoGhi);
	ELSE
		CALL lichSuCatNuoc_TheoTrangThai(idThang,idKhuVuc,idSoGhi,trangThai);
	END IF;

END