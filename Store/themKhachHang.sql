CREATE PROCEDURE `themKhachHang`(IN giaTri JSON, IN idThang bigint)
BEGIN

	DECLARE message varchar(128) CHARSET utf8;
	DECLARE idThongTinKhachHang bigint;
	DECLARE idKhachHang bigint;
	DECLARE idDoiTuong bigint;
	DECLARE idNganHang bigint;
	DECLARE idNguoiCapNhat bigint;
	DECLARE idSoGhi bigint;
	DECLARE maKhachHang varchar(10) CHARSET utf8;
	DECLARE thuTu int;
	DECLARE tenKhachHang varchar(250) CHARSET utf8;
	DECLARE ho varchar(100) CHARSET utf8;
	DECLARE ten varchar(150) CHARSET utf8;
	DECLARE diaChiSuDung varchar(250) CHARSET utf8;
	DECLARE diaChiThanhToan varchar(250) CHARSET utf8;
	DECLARE maSoThue varchar(50) CHARSET utf8;
	DECLARE soCmnd varchar(20) CHARSET utf8;
	DECLARE soDienThoai varchar(20) CHARSET utf8;
	DECLARE soHopDong varchar(50) CHARSET utf8;
	DECLARE thuPbvmt tinyint(1) DEFAULT 0;
	DECLARE doiThiCong tinyint(1) DEFAULT 0;
	DECLARE ngayDangKy date;
	DECLARE ngayLapDat date;
	DECLARE ngayLapHopDong date;
	DECLARE soTaiKhoan varchar(50) CHARSET utf8;
	DECLARE tenTaiKhoan varchar(150) CHARSET utf8;
	DECLARE sThuPbvmt varchar(10) CHARSET utf8;
	DECLARE sDoiThiCong varchar(10) CHARSET utf8;

	DECLARE thuTuMoi int;
	DECLARE idKhuVuc bigint;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		GET DIAGNOSTICS CONDITION 1
		@p1 = RETURNED_SQLSTATE, @p2 = MESSAGE_TEXT;

		ROLLBACK;

		SET message = LEFT(@p2, 128);

		INSERT INTO LOG(NAME_LOG, SQL_STATE, MESSAGE, DATE) 
		VALUES('PROCEDURE themKhachHang', @p1, @p2, NOW());

		COMMIT;

		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = message;
	END;

	START TRANSACTION;

		SET idDoiTuong = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.idDoiTuong'));
		SET idNganHang = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.idNganHang'));
		SET idNguoiCapNhat = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.idNguoiCapNhat'));
		SET idSoGhi = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.idSoGhi'));
		SET maKhachHang = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.maKhachHang'));
		SET thuTu = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.thuTu'));
		SET tenKhachHang = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.tenKhachHang'));
		SET ho = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.ho'));
		SET ten = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.ten'));
		SET diaChiSuDung = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.diaChiSuDung'));
		SET diaChiThanhToan = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.diaChiThanhToan'));
		SET maSoThue = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.maSoThue'));
		SET soCmnd = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.soCmnd'));
		SET soDienThoai = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.soDienThoai'));
		SET soHopDong = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.soHopDong'));
		SET sThuPbvmt = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.thuPbvmt'));
		SET sDoiThiCong = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.doiThiCong'));
		SET ngayDangKy = FROM_UNIXTIME(JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.ngayDangKy'))/1000);
		SET ngayLapDat = FROM_UNIXTIME(JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.ngayLapDat'))/1000);
		SET ngayLapHopDong = FROM_UNIXTIME(JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.ngayLapHopDong'))/1000);
		SET soTaiKhoan = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.soTaiKhoan'));
		SET tenTaiKhoan = JSON_UNQUOTE(JSON_EXTRACT(giaTri,'$.tenTaiKhoan'));
		IF sThuPbvmt = 'true' THEN
			SET thuPbvmt = 1;
		END IF;
		IF sDoiThiCong = 'true' THEN
			SET doiThiCong = 1;
		END IF;
		
		IF thuTu = 0 THEN
			select max(ttkh.THU_TU)+10 from THONG_TIN_KHACH_HANG ttkh
			inner join THONG_TIN_KHACH_HANG_THANG ttkht on ttkht.ID_THANG = idThang and ttkh.ID_THONG_TIN_KHACH_HANG = ttkht.ID_THONG_TIN_KHACH_HANG
			where ttkh.ID_SO_GHI = idSoGhi into thuTu ;
		END IF;
		
		SET thuTuMoi = thuTu;
		SELECT d.ID_KHU_VUC FROM SO_GHI AS sg INNER JOIN DUONG AS d ON sg.ID_DUONG = d.ID_DUONG WHERE sg.ID_SO_GHI = idSoGhi INTO idKhuVuc;
		-- điều chỉnh thứ tự mới
		IF MOD(thuTuMoi, 10) <> 0 THEN
			SET thuTu = (FLOOR(thuTuMoi / 10) * 10) + 10;
		END IF; 

		SELECT AUTO_INCREMENT FROM information_schema.`TABLES` WHERE TABLE_NAME = 'KHACH_HANG'
		INTO idKhachHang;
		-- thêm khách hàng
		INSERT INTO KHACH_HANG(HUY) VALUES(0);

		-- thêm hóa đơn
		INSERT INTO HOA_DON(ID_NGUOI_CAP_NHAT, ID_KHACH_HANG, ID_THANG, ID_TRANG_THAI_CHI_SO, ID_TRANG_THAI_HOA_DON, CHI_SO_CU, CHI_SO_MOI, TIEU_THU, TIEN_NUOC,
			THUE, PHI_BVMT, TONG_TIEN, BANG_CHU, TIEN_THANH_TOAN, TIEN_CON_LAI, GHI_CHU, NGAY_GIO_CAP_NHAT, HUY, NHAN_TIN, PHI_BVR, HOA_DON_DAU, HOA_DON_CUOI)
		VALUES(idNguoiCapNhat, idKhachHang, idThang, 3, 5, 1, 0, 0, 0, 0, 0, 0, '', 0, 0, '', NOW(), 0, 0, 0, 1, 1);

		SELECT AUTO_INCREMENT FROM information_schema.`TABLES` WHERE TABLE_NAME = 'THONG_TIN_KHACH_HANG'
		INTO idThongTinKhachHang;
		-- thêm thông tin khách hàng
		INSERT INTO THONG_TIN_KHACH_HANG(ID_KHACH_HANG, ID_NGUOI_CAP_NHAT, ID_THANG, ID_NGAN_HANG, ID_SO_GHI, ID_DOI_TUONG, MA_KHACH_HANG, THU_TU, TEN_KHACH_HANG,
			HO, TEN, DIA_CHI_SU_DUNG, DIA_CHI_THANH_TOAN, MA_SO_THUE, SO_CMND, SO_DIEN_THOAI, SO_HOP_DONG, THU_PBVMT, NGAY_DANG_KY, NGAY_LAP_DAT, NGAY_LAP_HOP_DONG,
			SO_TAI_KHOAN, TEN_TAI_KHOAN, NGAY_GIO_CAP_NHAT, DOI_THI_CONG)
		VALUES(idKhachHang, idNguoiCapNhat, idThang, idNganHang, idSoGhi, idDoiTuong, maKhachHang, thuTu, tenKhachHang,
			ho, ten, diaChiSuDung, diaChiThanhToan, maSoThue, soCmnd, soDienThoai, soHopDong, thuPbvmt, ngayDangKy, ngayLapDat, ngayLapHopDong, 
			soTaiKhoan, tenTaiKhoan, NOW(), doiThiCong);
		-- thêm thông tin khách hàng tháng
		INSERT INTO THONG_TIN_KHACH_HANG_THANG(ID_THONG_TIN_KHACH_HANG, ID_THANG) VALUES(idThongTinKhachHang, idThang);

		IF MOD(thuTuMoi, 10) <> 0 THEN
			BEGIN
				CALL themTTKHCanCapNhatThuTuTrongKhuVuc(idKhuVuc, idThang, idKhachHang, thuTuMoi, idNguoiCapNhat);

				CALL capNhatThuTuKhachHangTrongKhuVuc(idKhuVuc, idThang, idKhachHang, thuTu, thuTuMoi, idNguoiCapNhat);
			END;
		END IF;

		SELECT * FROM THONG_TIN_KHACH_HANG WHERE ID_THONG_TIN_KHACH_HANG = idThongTinKhachHang;

	COMMIT;

END