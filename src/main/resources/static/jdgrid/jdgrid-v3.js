(function($) {
	jQuery.fn.jdGrid=function(options){
		var defaults={
			columns:[],
			data:[],
			footer:{},
			height:'300px',
			class:'table table-bordered table-condensed table-hover',
			extclass:'',
			border:{'border':'1px solid #d2d6de'},
			shwfooter:false,
			decsym:',',
			thosym:'.',
			decnum:2,
			dateformat:'dd/mm/yyyy hh:MM:ss',
			onRowSelected:function(){},
			onCellCommit:function(){},
			onCellCommiting:function(){return true;},
			onRowDoubleClick:function(){}
		};
		var settings=$.extend({},defaults,options);
		return this.each(function(){
			var jdgrid={
				element:this,
				fillData:function(data){
					settings.data=data;
					$(this.element).find('.jdgrid-body-wrapper table tbody').remove();
					$(this.element).find('.jdgrid-body-wrapper table').append(genTableBody());
					regEvent(this.element);
					adjColums(this.element);
				},
				clearData:function(){
					clrData(this.element);
				},
				getData:function(){
					return settings.data;
				},
				addRow:function(row){
					settings.data.push(row);
					$(this.element).find('.jdgrid-body-wrapper table tbody').append(genTableRow(row));
					regEvent(this.element);
					adjColums(this.element);
				},
				removeRow:function(i){
					settings.data.splice(i,1);
					$(this.element).find('.jdgrid-body-wrapper table tbody tr:eq('+i+')').remove();
					adjColums(this.element);
				},
				refresh:function(){
					adjColums(this.element);
				},
				setFooter:function(footer){
					if(settings.shwfooter){
						$(this.element).find('.jdgrid-body-wrapper table tfoot tr,.jdgrid-footer-wrapper table tfoot tr').remove();
						$(this.element).find('.jdgrid-body-wrapper table tfoot,.jdgrid-footer-wrapper table tfoot').append(genFooterRow(footer));
						adjColums(this.element);
					}
				},
				getSelectedRow:function(){
					var i=$(this.element).find('.jdgrid-body-wrapper table tbody tr.actived').index();
					return i<0?null:settings.data[i];
				},
				getSelectedRowIndex:function(){
					return $(this.element).find('.jdgrid-body-wrapper table tbody tr.actived').index();
				},
				clearSelectedRow:function(){
					$(this.element).find('.jdgrid-body-wrapper table tbody tr.actived').removeClass('actived');
				},
				updateRow:function(row,i){
					if(i>=0){
						settings.data[i]=row;
						$(this.element).find('.jdgrid-body-wrapper table tbody tr:eq('+i+')').replaceWith(genTableRow(row));
						regEvent(this.element);
						adjColums(this.element);
					}
				},
				getCellValue:function(rowIndex,colName){
					return settings.data[rowIndex][colName];
				},
				setCellValue:function(rowIndex,colName,val){
					settings.data[rowIndex][colName]=val;
					row=settings.data[rowIndex];
					this.updateRow(row,rowIndex);
				}
			};
			$(this).data('jdgrid',jdgrid);
			
			// table header
			var tblBody=$('<table/>').addClass(settings.class).addClass(settings.extclass);
			var tblBodyHeader=$('<thead/>');
			var tblBodyHeaderTr=$('<tr/>');
			$.each(settings.columns,function(i,colm){
				if(colm.title!=undefined){
					var th=$('<th/>');
					
					if(colm.type=='checkbox' || colm.type=='checkbox_ex'){
						th.html('<input type="checkbox" child="chk-'+colm.name+'"/>');
					}else{
						th.html(colm.title);
					}
					
					if(colm.css!=undefined){
						th.css(colm.css);
					}
					tblBodyHeaderTr.append(th);
				}
			});
			tblBodyHeader.append(tblBodyHeaderTr);
			tblBody.append(tblBodyHeader);
			
			// table body
			var tblBodyBody=genTableBody();
			tblBody.append(tblBodyBody);
			
			// table footer
			var tblBodyFooter=$('<tfoot/>');
			tblBodyFooter.append(genFooterRow(settings.footer));
			tblBody.append(tblBodyFooter);
			
			var headWrp=$('<div/>').addClass('jdgrid-header-wrapper');
			var tblHead=$('<table/>').addClass(settings.class).addClass(settings.extclass).append(tblBodyHeader.clone());
			headWrp.append(tblHead);
			$(this).append(headWrp)
			
			var bodyWrp=$('<div/>').addClass('jdgrid-body-wrapper').height(settings.height);
			bodyWrp.append(tblBody);
			$(this).append(bodyWrp).css(settings.border);
			
			if(settings.shwfooter){
				var footWrp=$('<div/>').addClass('jdgrid-footer-wrapper');
				var tblFoot=$('<table/>').addClass(settings.class).addClass(settings.extclass).append(tblBodyFooter.clone());
				footWrp.append(tblFoot);
				$(this).append(footWrp);
			}
			
			tblBodyHeader.css({'visibility':'collapse'});
			tblBodyFooter.hide();
			
			regEvent(this);
			
			adjColums(this);
		});
		
		function debug(){
			console.log(settings);
		}
		
		function genTableBody(){
			var tblBodyBody=$('<tbody/>');
			$.each(settings.data,function(i,row){
				tblBodyBody.append(genTableRow(row));
			});
			return tblBodyBody;
		}
		
		function genTableRow(row){
			var tr=$('<tr/>');
			$.each(settings.columns,function(j,colm){
				var td=$('<td/>');
				switch(colm.type){
					case 'control':
						td.html(colm.content(row));
						break;
					case 'check':
						var checked=row[colm.name]||row[colm.name]==1?'checked="checked"':'';
						td.html('<input type="checkbox" disabled="disabled" '+checked+'/>');
						break;
					case 'checkbox':
						td.html('<input type="checkbox" value="'+row[colm.name]+'" class="chk-'+colm.name+'"/>');
						break;
					case 'checkbox_ex':
						if(row[colm.tienThanhToan] == 0)
							td.html('<input type="checkbox" tongtien="'+row[colm.tongTien]+'" value="'+row[colm.name]+'" class="chk-'+colm.name+'"/>');
						else
							td.html('<input type="checkbox" checked="checked" disabled="disabled" tongtien="'+row[colm.tongTien]+'" value="'+row[colm.name]+'"/>');
						break;
					case 'interval':
						td.html(timeConverter(row[colm.name]));
						break;
					case 'interval_date':
						td.html(timeConverter(row[colm.name]).substring(9));
						break;
					case 'interval_color':
						if(row[colm.name] !== null)
							tr.css({"background-color": "#c5f9bb"});
						td.html(timeConverter(row[colm.name]));
						break;
					default:
						td.html(colm.format?formatNum(row[colm.name],settings.decnum,settings.decsym,settings.thosym):row[colm.name]);
				}
				
				if(colm.css!=undefined){
					td.css(colm.css);
				}
				if(colm.editable){
					td.addClass('editable');
				}
				tr.append(td);
			});
			return tr;
		}
		
		function genFooterRow(footer){
			var tr=$('<tr/>');
			$.each(settings.columns,function(j,colm){
				var td=$('<td/>');
				if(footer[colm.name]!=undefined){
					td.html(colm.format?formatNum(footer[colm.name],settings.decnum,settings.decsym,settings.thosym):footer[colm.name]);
					if(colm.css!=undefined){
						td.css(colm.css);
					}
				}
				tr.append(td);
			});
			return tr;
		}
		
		function adjColums(dom){
			$(dom).find('.jdgrid-body-wrapper table thead tr:first th').each(function(i,td){
				$(dom).find('.jdgrid-header-wrapper table thead tr th:eq('+i+'),.jdgrid-footer-wrapper table tfoot tr td:eq('+i+')').outerWidth($(td).outerWidth());
			});
		}
		
		function clrData(dom){
			settings.data=[];
			$(dom).find('.jdgrid-body-wrapper table tbody tr').remove();
		}
		
		function regEvent(dom){
			$(dom).find('.jdgrid-body-wrapper table tbody tr').off('click').on('click',function(){
				//debug();
				$(dom).find('.jdgrid-body-wrapper table tbody tr').removeClass('actived');
				$(this).addClass('actived');
				settings.onRowSelected(settings.data[$(this).index()]);
			});
			
			$(dom).find('.jdgrid-body-wrapper table tbody tr').off('dblclick').on('dblclick',function(){
				settings.onRowDoubleClick(settings.data[$(this).index()]);
			});
			
			$(dom).find('.jdgrid-body-wrapper table tbody tr td.editable').off('dblclick').on('dblclick',function(){
				//debug();
				var row = $(this).parent().index();
				var col = $(this).index();
				var inpt=$('<input type="text"/>');
				inpt.css({'color':'#000'}).val(settings.data[row][settings.columns[col]['name']]).width($(this).width());
				$(this).html(inpt);
				adjColums(dom);
				inpt.select();
				inpt.keypress(function(e){
					if(e.which==13){
						var val=settings.columns[col].format?formatNum($(this).val(),settings.decnum,settings.decsym,settings.thosym):$(this).val();
						if(settings.onCellCommiting(val,row,col)){
							settings.data[row][settings.columns[col]['name']]=$(this).val();
							$(this).parent().html(val);
							adjColums(dom);
							settings.onCellCommit($(this).val(),row,col);
						}else{
							$(this).parent().html(settings.data[row][settings.columns[col]['name']]);
						}
					}
				});
			});
			
			$(dom).find('.jdgrid-header-wrapper table thead tr th input[type=checkbox]').off('change').on('change',function(){
				//console.log();
				if(this.checked) {
					tongTien = 0;
					$('.'+$(this).attr('child')).prop('checked',true).trigger('change');
			    }else{
			    	tongTien = 0;
			    	 $('#txt-tongtienthanhtoan').val(numberWithCommas(tongTien));
			    	$('.'+$(this).attr('child')).prop('checked',false);
			    }
			});
			
			var tongTien = 0;
			$(dom).find('.jdgrid-body-wrapper table tbody tr td input[type=checkbox]').off('change').on('change',function(){
				//console.log();
				if(this.checked) {
					tongTien = tongTien + Number($(this).attr('tongtien'));
			        $('#txt-tongtienthanhtoan').val(numberWithCommas(tongTien));
			    }else{
			    	tongTien = tongTien - Number($(this).attr('tongtien'));
			        $('#txt-tongtienthanhtoan').val(numberWithCommas(tongTien));
			    }
			});
		}
		
		function formatNum(n,c,d,t){
			var n = n, 
				c = isNaN(c = Math.abs(c)) ? 2 : c, 
				d = d == undefined ? "." : d, 
				t = t == undefined ? "," : t, 
				s = n < 0 ? "-" : "", 
				i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
				j = (j = i.length) > 3 ? j % 3 : 0;
			return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		}
		
		function milisec2Date(milisec){
			if(milisec === null) return '';
			var date=new Date(milisec);
			var dd=('0'+date.getDate()).slice(-2);
			var mm=('0'+(date.getMonth() + 1)).slice(-2);
			var yyyy=date.getFullYear();
			var hh=('0'+date.getHours()).slice(-2);
			var MM=('0'+date.getMinutes()).slice(-2);
			var ss=('0'+date.getSeconds()).slice(-2);
			return settings.dateformat.replace('dd',dd).replace('mm',mm).replace('yyyy',yyyy).replace('hh',hh).replace('MM',mm).replace('ss',ss);
		}
	};
	
})(jQuery);