var pref_url="/ke-hoach";

$(document).ready(function(){
	$('#grid-ds').jdGrid({
		columns:[
			{name:'tenThang',title:'Tháng'},
			{name:'tenDuong',title:'Phường'},
			{name:'chiSoCu',title:'Chỉ số cũ',format:true,css:{'text-align':'right'}},
			{name:'chiSoMoi',title:'Chỉ số mới',format:true,css:{'text-align':'right'}},
			{name:'sanXuat',title:'Sản xuất',format:true,css:{'text-align':'right'}},
			{name:'tieuThu',title:'Kế hoạch tiêu thụ',format:true,css:{'text-align':'right'}},
			{name:'doanhThu',title:'Kế hoạch doanh thu',format:true,css:{'text-align':'right'}},
			{name:'col2',title:'T.Tác',type:'control',content:function(obj){return '<a href="#" class="row-edit-1" rid="'+obj.idKeHoach+'" title="Sửa"><i class="fa fa-edit"></i></a>'},css:{'text-align':'center','width':'70px'}}
		],
		shwfooter:true,
		footer: {
			'chiSoCu':0,
			'chiSoMoi':0,
			'sanXuat':0,
			'tieuThu':0,
			'doanhThu':0
		},
		extclass:'tbl-bold-footer',
		decnum: 0
	});
	
	$('#btn-ok').click(function(){
		if(check()){
			luu();
		}
	});
	
	$('#btn-them').click(function(){
		themThangMoi();
	});
	
	$('#btn-cancel').click(function(){
		huy();
	});
	
	$('#cmb-donvi-filter').change(function(){
		layDanhSachKv();
	});
	
	$('#cmb-nam-filter').change(function(){
		layDanhSachThang();
	});
	
	$('#cmb-khuvuc-filter').change(function(){
		layDanhSach();
	});
	
	$('#cmb-thang-filter').change(function(){
		layDanhSach();
	});
	
	$('#btn-search').click(function(){
		layDanhSach();
	});
	
	$('#txt-chiSoCu, #txt-chiSoMoi').change(function(){
		$('#txt-sanXuat').val(Number($('#txt-chiSoMoi').val()) - Number($('#txt-chiSoCu').val()));
	});
	
	initForm();
});

function check(){
	if($('#txt-idKeHoach').val()===''){
		showError('Thông báo','Vui lòng chọn kế hoạch!');
		return false;
	}
	if($('#txt-chiSoCu').val()===''){
		showError('Thông báo','Vui lòng nhập chỉ số cũ!');
		return false;
	}
	if($('#txt-chiSoMoi').val()===''){
		showError('Thông báo','Vui lòng nhập chỉ số mới!');
		return false;
	}
	if($('#txt-sanXuat').val()===''){
		showError('Thông báo','Vui lòng nhập sản xuất!');
		return false;
	}
	if($('#txt-tieuThu').val()===''){
		showError('Thông báo','Vui lòng nhập kế hoạch tiêu thụ!');
		return false;
	}
	if($('#txt-doanhThu').val()===''){
		showError('Thông báo','Vui lòng nhập kế hoạch doanh thu!');
		return false;
	}
	return true;
}

function initForm(){
	$.ajax({
		url:pref_url+'/init-form',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			var donvi=$.map(data,function(obj){
				obj.id=obj.idDonVi;
				obj.text=obj.tenDonVi;
				return obj;
			});
			
			$('#cmb-donvi-filter').select2({
				data: donvi
			});
			
			if($('#cmb-donvi-filter').val()!=null)
				layDanhSachKv();
		},
		error:function(){
			showError('Thông báo','Khởi tạo dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDanhSachKv(){
	$.ajax({
		url:pref_url+'/lay-ds-khu-vuc',
		method:'get',
		dataType:'json',
		data:{id:$('#cmb-donvi-filter').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			var khuvuc=$.map(data,function(obj){
				obj.id=obj.idKhuVuc;
				obj.text=obj.tenKhuVuc;
				return obj;
			});
			
			$('#cmb-khuvuc-filter option').remove();
			$('#cmb-khuvuc-filter').select2({
				data: khuvuc
			});
			
			layDanhSachNam();
		},
		error:function(){
			showError('Thông báo','Khởi tạo dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDanhSachNam(){
	$.ajax({
		url:pref_url+'/lay-ds-nam',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#cmb-nam-filter').select2({
				data: data
			});
			if($('#cmb-nam-filter').val()!=null)
				layDanhSachThang();
		},
		error:function(){
			showError('Thông báo','Lấy danh sách năm không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDanhSachThang(){
	$.ajax({
		url:pref_url+'/lay-ds-thang-theo-nam',
		method:'get',
		dataType:'json',
		data:{nam:$('#cmb-nam-filter').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			var thang=$.map(data,function(obj){
				obj.id=obj.idThang;
				obj.text=obj.tenThang;
				return obj;
			});
			$('#cmb-thang-filter option').remove();
			$('#cmb-thang-filter').html('<option value="0">Tất cả</option>');
			$('#cmb-thang-filter').select2({
				data: thang
			});
			
			layDanhSach();
		},
		error:function(){
			showError('Thông báo','Lấy danh sách tháng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function themThangMoi(){
	$.ajax({
		url:pref_url+'/them-thang-moi',
		method:'post',
		dataType:'json',
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Thêm tháng mới thành công');
				layDanhSach();
				layDanhSachThang();
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Thêm tháng mới không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDanhSach(){
	if($('#cmb-khuvuc-filter').val()==null || $('#cmb-nam-filter').val()==null || $('#cmb-thang-filter').val()==null)
		return;
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{idKhuVuc:$('#cmb-khuvuc-filter').val(),nam:$('#cmb-nam-filter').val(),idThang:$('#cmb-thang-filter').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#grid-ds').data('jdgrid').fillData(data);
			
			// tính tổng
			var tongChiSoCu = 0;
			var tongChiSoMoi = 0;
			var tongSanXuat = 0;
			var tongTieuThu = 0;
			var tongDoanhThu = 0;
			$.each(data, function(i, item) {
				tongChiSoCu = tongChiSoCu + item.chiSoCu;
				tongChiSoMoi = tongChiSoMoi + item.chiSoMoi;
				tongSanXuat = tongSanXuat + item.sanXuat;
			    tongTieuThu = tongTieuThu + item.tieuThu;
			    tongDoanhThu = tongDoanhThu + item.doanhThu;
			});
			// end tính tổng
			
			$('#grid-ds').data('jdgrid').setFooter({
				'chiSoCu':tongChiSoCu,
				'chiSoMoi':tongChiSoMoi,
				'sanXuat':tongSanXuat,
				'tieuThu':tongTieuThu,
				'doanhThu':tongDoanhThu
			});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				layChiTiet($(this).attr('rid'));
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function luu(){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach();
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function huy(){
	$('#txt-idKeHoach').val('');
	$('#txt-idDuong').val('');
	$('#txt-tenDuong').val('');
	$('#txt-idThang').val('');
	$('#txt-tenThang').val('');
	$('#txt-chiSoCu').val('');
	$('#txt-chiSoMoi').val('');
	$('#txt-sanXuat').val('');
	$('#txt-tieuThu').val('');
	$('#txt-doanhThu').val('');
}

function layChiTiet(id){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#txt-idKeHoach').val(data.idKeHoach);
			$('#txt-idDuong').val(data.idDuong);
			$('#txt-tenDuong').val(data.tenDuong);
			$('#txt-idThang').val(data.idThang);
			$('#txt-tenThang').val(data.tenThang);
			$('#txt-chiSoCu').val(data.chiSoCu);
			$('#txt-chiSoMoi').val(data.chiSoMoi);
			$('#txt-sanXuat').val(data.sanXuat);
			$('#txt-tieuThu').val(data.tieuThu);
			$('#txt-doanhThu').val(data.doanhThu);
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}
