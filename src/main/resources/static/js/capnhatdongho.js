var pref_url="/cap-nhat-dong-ho";

$(document).ready(function(){
	$('#btn-ok').click(function(){
		if(check()){
			luu();
		}
	});
	
	$('#btn-file').click(function(){
		// window.location.href = "help/CapNhatDongHo.xlsx";
		window.location.href = pref_url+"/xuat-file-xlsx";
	});
});

function check(){
	if($('#fileData').val()===''){
		showError('Thông báo','Vui lòng chọn file Excel import!');
		return false;
	}
	return true;
}



function luu(){
	$.ajax({
		url:pref_url+'/xu-ly-import',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		enctype: 'multipart/form-data',
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Cập nhật thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Cập nhật không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}
