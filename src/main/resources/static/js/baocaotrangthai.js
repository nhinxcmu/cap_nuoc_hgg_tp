var pref_url="/bao-cao-trang-thai";

$('#view').click(function() {
	if(check()){
		var tree = $("#tree").fancytree("getTree"),
		selNodes = tree.getSelectedNodes();
		var nodes = new Array();
	    selNodes.forEach(function(node) {
	    	if(node.key!=-1) nodes.push(node.key);
	    });
		var urlp = pref_url+"/xem-bao-cao?trangthai="+$('#cmb-trangthai').val()+'&tentrangthai='+$('#cmb-trangthai option:selected').text()+'&idsoghi='+nodes.join();
		$.ajax({
	        url: urlp,
	        beforeSend: function(jqXHR, settings){
	        	$('#box-ds').showLoading();
			},
	        success: function(response){
				PDFObject.embed(response, "#pdfRenderer");
			},
			complete: function(jqXHR, textStatus){
				$('#box-ds').hideLoading();
			}
	    });
	}
 });
 
$(document).ready(function(){
	$('#cmb-trangthai').select2({});
	layDsTrangThai();
	
	//tree
	$("#tree").fancytree({
		checkbox: true,
		selectMode: 3,
		// extensions: ["dnd"],
		icon: false,
		source: {
	        url: pref_url+"/du-lieu-cay",
	        cache: false
	    }//,
//	    activate: function(event, data){
//	    	actNode=data.node;
//	    },
//	    dnd: {
//	        autoExpandMS: 400,
//	        focusOnClick: true,
//	        preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
//	        preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
//	        dragStart: function(node, data) {
//	          /** This function MUST be defined to enable dragging for the tree.
//	           *  Return false to cancel dragging of node.
//	           */
//	          return true;
//	        },
//	        dragEnter: function(node, data) {
//	          /** data.otherNode may be null for non-fancytree droppables.
//	           *  Return false to disallow dropping on node. In this case
//	           *  dragOver and dragLeave are not called.
//	           *  Return 'over', 'before, or 'after' to force a hitMode.
//	           *  Return ['before', 'after'] to restrict available hitModes.
//	           *  Any other return value will calc the hitMode from the cursor position.
//	           */
//	          // Prevent dropping a parent below another parent (only sort
//	          // nodes under the same parent)
//	/*           if(node.parent !== data.otherNode.parent){
//	            return false;
//	          }
//	          // Don't allow dropping *over* a node (would create a child)
//	          return ["before", "after"];
//	*/
//	           return true;
//	        },
//	        dragDrop: function(node, data) {
//	          /** This function MUST be defined to enable dropping of items on
//	           *  the tree.
//	           */
//	        	data.otherNode.moveTo(node, data.hitMode);
//	        }
//	      }
	});
	
	$('#moRongTatCa').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
			node.setExpanded();
		});
		return false;
	});
	
	$('#thuGonTatCa').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
			node.setExpanded(false);
		});
		return false;
	});
	
	$('#chk-all').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
	        node.setSelected(true);
	    });
		return false;
	});
	
	$('#chk-none').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
	        node.setSelected(false);
	    });
		return false;
	});
	
	$('#pdf').click(function() {
		if(check()){
			var tree = $("#tree").fancytree("getTree"),
			selNodes = tree.getSelectedNodes();
			var nodes = new Array();
		    selNodes.forEach(function(node) {
		    	if(node.key!=-1) nodes.push(node.key);
		    });
			var urlp = pref_url+"/xuat-file-pdf?trangthai="+$('#cmb-trangthai').val()+'&tentrangthai='+$('#cmb-trangthai option:selected').text()+'&idsoghi='+nodes.join();
	        $.ajax({
	            url: urlp,
	            beforeSend: function(jqXHR, settings){
	            	$('#box-ds').showLoading();
	    		},
	    		success: function(response){
	    			$(location).attr('href', urlp);
	    		},
	    		complete: function(jqXHR, textStatus){
	    			$('#box-ds').hideLoading();
	    		}
	        });
		}
    });
	
    $('#xls').click(function() {
    	if(check()){
    		var tree = $("#tree").fancytree("getTree"),
    		selNodes = tree.getSelectedNodes();
    		var nodes = new Array();
    	    selNodes.forEach(function(node) {
    	    	if(node.key!=-1) nodes.push(node.key);
    	    });
	    	var urlp = pref_url+"/xuat-file-xls?trangthai="+$('#cmb-trangthai').val()+'&tentrangthai='+$('#cmb-trangthai option:selected').text()+'&idsoghi='+nodes.join();
	        $.ajax({
	            url: urlp,
	            beforeSend: function(jqXHR, settings){
	            	$('#box-ds').showLoading();
	    		},
	    		success: function(response){
	    			$(location).attr('href', urlp);
	    		},
	    		complete: function(jqXHR, textStatus){
	    			$('#box-ds').hideLoading();
	    		}
	        });
    	}
    });
    
    $('#xlsx').click(function() {
    	if(check()){
    		var tree = $("#tree").fancytree("getTree"),
    		selNodes = tree.getSelectedNodes();
    		var nodes = new Array();
    	    selNodes.forEach(function(node) {
    	    	if(node.key!=-1) nodes.push(node.key);
    	    });
	    	var urlp = pref_url+"/xuat-file-xlsx?trangthai="+$('#cmb-trangthai').val()+'&tentrangthai='+$('#cmb-trangthai option:selected').text()+'&idsoghi='+nodes.join();
	        $.ajax({
	            url: urlp,
	            beforeSend: function(jqXHR, settings){
	            	$('#box-ds').showLoading();
	    		},
	    		success: function(response){
	    			$(location).attr('href', urlp);
	    		},
	    		complete: function(jqXHR, textStatus){
	    			$('#box-ds').hideLoading();
	    		}
	        });
    	}
    });
    $('#rtf').click(function() {
    	if(check()){
    		var tree = $("#tree").fancytree("getTree"),
    		selNodes = tree.getSelectedNodes();
    		var nodes = new Array();
    	    selNodes.forEach(function(node) {
    	    	if(node.key!=-1) nodes.push(node.key);
    	    });
	    	var urlp = pref_url+"/xuat-file-rtf?trangthai="+$('#cmb-trangthai').val()+'&tentrangthai='+$('#cmb-trangthai option:selected').text()+'&idsoghi='+nodes.join();
	        $.ajax({
	            url: urlp,
	            beforeSend: function(jqXHR, settings){
	            	$('#box-ds').showLoading();
	    		},
	    		success: function(response){
	    			$(location).attr('href', urlp);
	    		},
	    		complete: function(jqXHR, textStatus){
	    			$('#box-ds').hideLoading();
	    		}
	        });
    	}
    });
});

function check(){
	var tree = $("#tree").fancytree("getTree"),
	selNodes = tree.getSelectedNodes();
	var nodes = new Array();
    selNodes.forEach(function(node) {
    	if(node.key!=-1) nodes.push(node.key);
    });
	if(nodes.join()===''){
		showError('Thông báo','Vui lòng chọn tổ!');
		return false;
	}
	return true;
}

function layDsTrangThai(){
	$.ajax({
		url:pref_url+'/lay-ds-trang-thai',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			var trangthai=$.map(data,function(obj){
				obj.id=obj.idTrangThaiChiSo;
				obj.text=obj.tenTrangThaiChiSo;
				return obj;
			});
			
			$('#cmb-trangthai').select2({
				data: trangthai
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách trạng thái không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

