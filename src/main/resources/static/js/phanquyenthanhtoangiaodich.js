var pref_url="/phan-quyen-thanh-toan-giao-dich";

$(document).ready(function(){
	//tree
	$("#tree").fancytree({
		checkbox: true,
		selectMode: 3,
		// extensions: ["dnd"],
		icon: false,
		source: {
	        url: pref_url+"/du-lieu-cay",
	        cache: false
	    }//,
//	    activate: function(event, data){
//	    	actNode=data.node;
//	    },
//	    dnd: {
//	        autoExpandMS: 400,
//	        focusOnClick: true,
//	        preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
//	        preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
//	        dragStart: function(node, data) {
//	          /** This function MUST be defined to enable dragging for the tree.
//	           *  Return false to cancel dragging of node.
//	           */
//	          return true;
//	        },
//	        dragEnter: function(node, data) {
//	          /** data.otherNode may be null for non-fancytree droppables.
//	           *  Return false to disallow dropping on node. In this case
//	           *  dragOver and dragLeave are not called.
//	           *  Return 'over', 'before, or 'after' to force a hitMode.
//	           *  Return ['before', 'after'] to restrict available hitModes.
//	           *  Any other return value will calc the hitMode from the cursor position.
//	           */
//	          // Prevent dropping a parent below another parent (only sort
//	          // nodes under the same parent)
//	/*           if(node.parent !== data.otherNode.parent){
//	            return false;
//	          }
//	          // Don't allow dropping *over* a node (would create a child)
//	          return ["before", "after"];
//	*/
//	           return true;
//	        },
//	        dragDrop: function(node, data) {
//	          /** This function MUST be defined to enable dropping of items on
//	           *  the tree.
//	           */
//	        	data.otherNode.moveTo(node, data.hitMode);
//	        }
//	      }
	});
	
	$('#moRongTatCa').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
			node.setExpanded();
		});
		return false;
	});
	
	$('#thuGonTatCa').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
			node.setExpanded(false);
		});
		return false;
	});
	
	$('#chk-all').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
	        node.setSelected(true);
	    });
		return false;
	});
	
	$('#chk-none').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
	        node.setSelected(false);
	    });
		return false;
	});
	
	$('#grid-ds').jdGrid({
		columns:[
			{name:'tenNhanVienGhiThu',title:'Nhân viên thanh toán'},
			{name:'tenNguoiPhanCong',title:'Người phân quyền'},
			{name:'ngayGioCapNhat',title:'Ngày phân quyền',type:'interval'},
			{name:'col2',title:'T.Tác',type:'control',content:function(obj){return '<a href="#" class="row-edit-1" rid="'+obj.idLichGhiThu+'" title="Sửa"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;<a href="#" class="row-del-1 text-danger" rid="'+obj.idLichGhiThu+'" title="Xóa"><i class="fa fa-trash-o"></i></a>'},css:{'text-align':'center','width':'70px'}}
		],
		height:'445px'
	});
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#btn-ok').click(function(){
		if(check()){
			luu();
		}
	});
	
	$('#btn-cancel').click(function(){
		huy();
	});
	
	$('#btn-search').click(function(){
		layDanhSach(0);
	});
	
	$('#txt-keyword').keypress(function(e){
        if(e.which == 13){
            $('#btn-search').click();
        }
    });
	
	$('#btn-clear-search').click(function(){
		$('#txt-keyword').val('').focus();
		layDanhSach(0);
	});
	
	layDanhSach(0);
	layDsNhanVien();
});

function check(){
	var tree = $("#tree").fancytree("getTree"),
	selNodes = tree.getSelectedNodes();
	var nodes = new Array();
    selNodes.forEach(function(node) {
    	if(node.key!=-1) nodes.push(node.key);
    });
	if(nodes.join()===''){
		showError('Thông báo','Vui lòng chọn tổ!');
		return false;
	}
	return true;
}

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{thongTinCanTim:$.trim($('#txt-keyword').val()),page:p},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#grid-ds').data('jdgrid').fillData(data.content);
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				layChiTiet($(this).attr('rid'));
			});
			
			$('.row-del-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa('+$(this).attr('rid')+')');
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function luu(){
	var data=new FormData($('#frm-1')[0]);

	var tree = $("#tree").fancytree("getTree"),
	selNodes = tree.getSelectedNodes();
	var nodes = [];
    selNodes.forEach(function(node) {
    	if(node.key!=-1) nodes.push(node.key);
    });
    data.append('soGhiDtos',nodes);
    
    $.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:data,
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function huy(){
	layDsNhanVien();
	$('#txt-idlichghithu').val('');
	$('#chk-none').click();
	$('#thuGonTatCa').click();
}

function layChiTiet(id){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#cmb-nhanvien').val(data.idNhanVienGhiThu).trigger('change');
			$('#txt-idlichghithu').val(data.idLichGhiThu);
			$('#tree').fancytree('getTree').reload(data.cayDtos);
			$('#moRongTatCa').click();
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function xoa(id){
	$.ajax({
		url:pref_url+'/xoa',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDsNhanVien(){
	$.ajax({
		url:pref_url+'/lay-ds-nhan-vien',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-nhanvien option').remove();
			
			var nhanvien=$.map(data,function(obj){
				obj.id=obj.idNguoiDung;
				obj.text=obj.hoTen;
				return obj;
			});
			
			$('#cmb-nhanvien').select2({
				data: nhanvien
			});
			
		},
		error:function(){
			showError('Thông báo','Lấy danh sách nhân viên không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}
