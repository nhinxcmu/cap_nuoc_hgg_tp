var pref_url="/phat-hanh-hoa-don-lap-dat";

$(document).ready(function(){
	$('#btn-ok-1').click(function(){
		showConfirm('Xác nhận', 'Bạn chắc phát hành hóa đơn?', 'luu(true)');
	});
	
	$('#btn-ok-2').click(function(){
		showConfirm('Xác nhận', 'Bạn chắc ngưng phát hành hóa đơn?', 'luu(false)');
	});
	
	laySoLuong();
});



function luu(phathanh){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:{phatHanh:phathanh},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				if(phathanh)
					toastInfo('Phát hành hóa đơn thành công');
				else
					toastInfo('Ngưng phát hành hóa đơn thành công');
				laySoLuong();
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			if(phathanh)
				showError('Thông báo','Phát hành hóa đơn không thành công, vui lòng thử lại sau!');
			else
				showError('Thông báo','Ngưng phát hành hóa đơn không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function laySoLuong(){
	$.ajax({
		url:pref_url+'/lay-so-luong',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data) {
			if(data.resCode === undefined) {
				$('#txt-tongso').val(numberWithCommas(data.tongHoaDon));
				$('#txt-daphathanh').val(numberWithCommas(data.tongDaPhatHanh));
				$('#txt-khongphathanh').val(numberWithCommas(data.tongKhongPhatHanh));
				$('#txt-chuaphathanh').val(numberWithCommas(data.tongChuaPhatHanh));
			} else {
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lấy số lượng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}
