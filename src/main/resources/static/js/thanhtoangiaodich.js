var pref_url="/thanh-toan-giao-dich";
var idkhachhang = 0;
var load_mod = false;

$(document).ready(function(){
	$('#grid-ds-thanhtoan').jdGrid({
		columns:[
			{name:'maPhieuThanhToan',title:'Mã phiếu TT'},
			{name:'maKhachHang',title:'Mã KH'},
			{name:'maDongHo',title:'Mã đồng hồ'},
			{name:'tenKhachHang',title:'Tên KH'},
			{name:'diaChiSuDung',title:'Địa chỉ sử dụng'},
			{name:'diaChiThanhToan',title:'Địa chỉ thanh toán'},
			{name:'soDienThoai',title:'Điện thoại'},
			{name:'tienThanhToan',title:'Tiền TT',format:true,css:{'text-align':'right'}},
			{name:'tenNguoiCapNhat',title:'Người cập nhật'},
			{name:'ngayGioCapNhat',title:'Ngày cập nhật',type:'interval',css:{'text-align':'center'}},
			{name:'huyThanhToan',title:'Hủy',type:'check',css:{'text-align':'center'}},
			{name:'col2',title:'T.Tác',type:'control',content:function(obj){return obj.huyThanhToan===false?'<a href="#" class="row-print-1" rid="'+obj.idPhieuThanhToan+'" title="In phiếu thanh toán"><i class="fa fa-print"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="row-del-1 text-danger" rid="'+obj.idPhieuThanhToan+'" title="Hủy thanh toán"><i class="fa fa-ban"></i></a>':''},css:{'text-align':'center','width':'80px'}}
		],
		decnum: 0
	});
	
	$('#page-ds-thanhtoan').jdPage({
		onPageChanged:function(p){
			layDanhSachDaThanhToan(p-1);
		}
	});
	
	$('#grid-ds').jdGrid({
		columns:[
			{name:'thuTu',title:'Thứ tự'},
			{name:'maDongHo',title:'Mã đồng hồ'},
			{name:'maKhachHang',title:'Mã KH'},
			{name:'tenKhachHang',title:'Tên KH'},
			{name:'diaChiSuDung',title:'Địa chỉ'},
			{name:'soDienThoai',title:'Điện thoại'},
			{name:'tenDoiTuong',title:'Đối tượng'},
			{name:'col2',title:'T.Tác',type:'control',content:function(obj){return '<a href="#" data-dismiss="modal" class="row-edit-1" rid="'+obj.idThongTinKhachHang+'" title="Chọn"><i class="fa fa-check"></i></a>'},css:{'text-align':'center','width':'80px'}}
		]
	});
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#cmb-phuong-filter').change(function(){
		layDsTo();
	});
	
	$('#cmb-to-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#btn-search').click(function(){
		layDanhSach(0);
	});
	
	$('#txt-keyword').keypress(function(e){
        if(e.which == 13){
            $('#btn-search').click();
        }
    });
	
	$('#btn-clear-search').click(function(){
		$('#txt-keyword').val('').focus();
		layDanhSach(0);
	});
	
	$('#btn-search-thanhtoan').click(function(){
		layDanhSachDaThanhToan(0);
	});
	
	$('#txt-keyword-thanhtoan').keypress(function(e){
        if(e.which == 13){
            $('#btn-search-thanhtoan').click();
        }
    });
	
	$('#btn-clear-search-thanhtoan').click(function(){
		$('#txt-keyword-thanhtoan').val('').focus();
		layDanhSachDaThanhToan(0);
	});
	
	$('#modInfo').on('shown.bs.modal',function(e){
		if(!load_mod) {
			layDsPhuong();
			load_mod = true;
		}
	});
	
	$('#modThanhToan').on('shown.bs.modal',function(e){
		layDanhSachDaThanhToan(0);
	});
	
	$('#grid-hoadon').jdGrid({
		columns:[
			{name:'idHoaDon',tienThanhToan:'tienThanhToan',tongTien:'tongTien',title:'Check all',type:'checkbox_ex',css:{'text-align':'center'}},
			{name:'tenThang',title:'Tháng',css:{'text-align':'center'}},
			{name:'tongTien',title:'Tổng tiền',format:true,css:{'text-align':'right'}},
			{name:'tienNuoc',title:'Tiền nước',format:true,css:{'text-align':'right'}},
			{name:'thue',title:'Thuế GTGT',format:true,css:{'text-align':'right'}},
			{name:'phiBvmt',title:'Phí BVMT',format:true,css:{'text-align':'right'}},
			{name:'phiBvr',title:'Phí BVR',format:true,css:{'text-align':'right'}},
			{name:'chiSoCu',title:'CS cũ',format:true,css:{'text-align':'right'}},
			{name:'chiSoMoi',title:'CS mới',format:true,css:{'text-align':'right'}},
			{name:'tieuThu',title:'Tiêu thụ',format:true,css:{'text-align':'right'}}
		],
		decnum: 0
	});
	
	$('#grid-thanhtoan').jdGrid({
		columns:[
			{name:'tenThangHoaDon',title:'Tháng',css:{'text-align':'center'}},
			{name:'tienThanhToan',title:'Tiền thanh toán',format:true,css:{'text-align':'right'}},
			{name:'tenHinhThucThanhToan',title:'Hình thức thanh toán'},
			{name:'tenNguoiCapNhat',title:'Người cập nhật'},
			{name:'ngayGioCapNhat',title:'Ngày cập nhật',type:'interval',css:{'text-align':'center'}},
			{name:'huyThanhToan',title:'Hủy',type:'check',css:{'text-align':'center'}}
		],
		height:'300px',
		decnum: 0
	});
	
	$('#page-thanhtoan').jdPage({
		onPageChanged:function(p){
			layDanhSachThanhToan(idkhachhang,p-1);
		}
	});
	
	$('#btn-search-makh').click(function(){
		if($.trim($('#txt-makhachhang-filter').val()) === '')
			toastError('Vui lòng nhập mã khách hàng!');
		else
			timMaKhachHang();
	});
	
	$('#txt-makhachhang-filter').keypress(function(e){
        if(e.which == 13){
            $('#btn-search-makh').click();
        }
    });
	
	$('#btnThanhToan').click(function(){
		thanhToan();
	});
});

function layDanhSachDaThanhToan(p){
	$.ajax({
		url:pref_url+'/lay-ds-da-thanh-toan',
		method:'get',
		dataType:'json',
		data:{thongTinCanTim:$.trim($('#txt-keyword-thanhtoan').val()),page:p},
		beforeSend:function(){
			$('#list-thanhtoan').showLoading();
		},
		success:function(data){
			if(data.resCode === undefined){
				$('#grid-ds-thanhtoan').data('jdgrid').fillData(data.content);
				$('#page-ds-thanhtoan').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
				
				$('.row-print-1').click(function(e){
					e.preventDefault();
					inPhieuThanhToan($(this).attr('rid'));
				});
				
				$('.row-del-1').click(function(e){
					e.preventDefault();
					showConfirm('Xác nhận', 'Bạn chắc muốn hủy thanh toán?', 'huyThanhToan('+$(this).attr('rid')+')');
				});
			} else {
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lấy danh sách đã thanh toán không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#list-thanhtoan').hideLoading();
		}
	});
}

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{idDuong:$('#cmb-phuong-filter').val(),idSoGhi:$('#cmb-to-filter').val(),thongTinCanTim:$.trim($('#txt-keyword').val()),page:p},
		beforeSend:function(){
			$('#list-khachhang').showLoading();
		},
		success:function(data){
			$('#grid-ds').data('jdgrid').fillData(data.content);
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				layChiTiet($(this).attr('rid'));
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#list-khachhang').hideLoading();
		}
	});
}

function layDsPhuong(){
	$.ajax({
		url:pref_url+'/lay-ds-phuong',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#list-khachhang').showLoading();
		},
		success:function(data){
			var phuong=$.map(data,function(obj){
				obj.id=obj.idDuong;
				obj.text=obj.tenDuong;
				return obj;
			});
			
			$('#cmb-phuong-filter').select2({
				data: phuong
			});
			
			layDsTo();
		},
		error:function(){
			showError('Thông báo','Lấy danh sách phường không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#list-khachhang').hideLoading();
		}
	});
}

function layDsTo(){
	$.ajax({
		url:pref_url+'/lay-ds-to',
		method:'get',
		dataType:'json',
		data:{id:$('#cmb-phuong-filter').val()},
		beforeSend:function(){
			$('#list-khachhang').showLoading();
		},
		success:function(data){
			$('#cmb-to-filter').html('<option value="0">-- Tất cả các tổ --</option>');
			
			var to=$.map(data,function(obj){
				obj.id=obj.idSoGhi;
				obj.text=obj.tenSoGhi;
				return obj;
			});
			
			$('#cmb-to-filter').select2({
				data: to
			});
			
			layDanhSach(0);
		},
		error:function(){
			showError('Thông báo','Lấy danh sách tổ không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#list-khachhang').hideLoading();
		}
	});
}

function timMaKhachHang(){
	$.ajax({
		url:pref_url+'/tim-ma-khach-hang',
		method:'get',
		dataType:'json',
		data:{maKhachHang:$('#txt-makhachhang-filter').val()},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.idThongTinKhachHang === null)
				toastError('Không tìm thấy khách hàng nào!');
			else {
				idkhachhang = data.idThongTinKhachHang;
				$('#txt-thutu').val(data.thuTu);
				$('#txt-makhachhang').val(data.maKhachHang);
				$('#txt-madongho').val(data.maDongHo);
				$('#txt-tenkhachhang').val(data.tenKhachHang);
				$('#txt-tendoituong').val(data.tenDoiTuong);
				$('#txt-diachithanhtoan').val(data.diaChiThanhToan);
				$('#chk-thupbvmt').prop('checked',data.thuPbvmt);
				layDanhSachThanhToan(idkhachhang,0);
				layDanhSachHoaDon(idkhachhang);
			}
		},
		error:function(){
			showError('Thông báo','Tìm khách hàng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layDanhSachThanhToan(id,p){
	$.ajax({
		url:pref_url+'/lay-ds-thanh-toan',
		method:'get',
		dataType:'json',
		data:{id:id,page:p},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#grid-thanhtoan').data('jdgrid').fillData(data.content);
			$('#page-thanhtoan').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
		},
		error:function(){
			showError('Thông báo','Lấy danh sách thanh toán không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layDanhSachHoaDon(id){
	$.ajax({
		url:pref_url+'/lay-ds-hoa-don',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#grid-hoadon').data('jdgrid').fillData(data);
		},
		error:function(){
			showError('Thông báo','Lấy danh sách hóa đơn không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layChiTiet(id){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.idThongTinKhachHang === null)
				toastError('Không tìm thấy khách hàng nào!');
			else {
				idkhachhang = data.idThongTinKhachHang;
				$('#txt-makhachhang-filter').val(data.maKhachHang);
				$('#txt-thutu').val(data.thuTu);
				$('#txt-makhachhang').val(data.maKhachHang);
				$('#txt-madongho').val(data.maDongHo);
				$('#txt-tenkhachhang').val(data.tenKhachHang);
				$('#txt-tendoituong').val(data.tenDoiTuong);
				$('#txt-diachithanhtoan').val(data.diaChiThanhToan);
				$('#chk-thupbvmt').prop('checked',data.thuPbvmt);
				layDanhSachThanhToan(idkhachhang,0);
				layDanhSachHoaDon(idkhachhang);
			}
		},
		error:function(){
			showError('Thông báo','Tìm khách hàng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function thanhToan(){
	var selected = [];
	$('#grid-hoadon .jdgrid-body-wrapper input:checked').each(function() {
		if(!$(this).is(':disabled'))
			selected.push($(this).val());
	});
	
	if(selected.join()===''){
		showError('Thông báo','Vui lòng chọn hóa đơn!');
	} else {
		$.ajax({
			url:pref_url+'/thanh-toan-hoa-don-tai-giao-dich',
			method:'post',
			dataType:'json',
			data:{strDsHoaDon:selected.join()},
			beforeSend:function(){
				$('#box-ds').showLoading();
			},
			success:function(data){
				if(data.resCode>0){
					toastInfo('Thanh toán thành công');
					$('#txt-tongtienthanhtoan').val(numberWithCommas(0));
					layDanhSachThanhToan(idkhachhang,0);
					layDanhSachHoaDon(idkhachhang);
					// inPhieuThanhToan(data.resMessage);
				}else{
					showError('Thông báo',data.resMessage);
				}
			},
			error:function(){
				showError('Thông báo','Thanh toán không thành công, vui lòng thử lại sau!');
			},
			complete:function(){
				$('#box-ds').hideLoading();
			}
		});
	}
}

function inPhieuThanhToan(idphieuthanhtoan){
	$.ajax({
        url: pref_url+'/xem-bao-cao?id='+idphieuthanhtoan,
		method:'get',
        beforeSend: function(jqXHR, settings){
        	$('#box-ds').showLoading();
        	$('#box-ds-thanhtoan').showLoading();
		},
        success: function(response){
			printJS(response);
		},
		complete: function(jqXHR, textStatus){
			$('#box-ds').hideLoading();
			$('#box-ds-thanhtoan').hideLoading();
		}
    });
}

function huyThanhToan(id){
	$.ajax({
		url:pref_url+'/huy-thanh-toan',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds-thanhtoan').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Hủy thanh toán thành công');
				layDanhSachDaThanhToan(0);
				if(idkhachhang>0){
					layDanhSachThanhToan(idkhachhang,0);
					layDanhSachHoaDon(idkhachhang);
				}
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Hủy thanh toán không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds-thanhtoan').hideLoading();
		}
	});
}
