$(document).ready(function () {
	$(document).on('expanded.pushMenu', function () {
		setTimeout(function () {
			refreshGrid();
		}, 300);
	});

	$(document).on('collapsed.pushMenu', function () {
		setTimeout(function () {
			refreshGrid();
		}, 300);
	});

	$(window).resize(function () {
		refreshGrid();
	});

	genMenu();
	layThongTinCongTy();
	/*layThongTinCongTy();
	genMenu();
	laySoLuongChiTieu();
	laySoLuongCongNo();*/
});

function refreshGrid() {
	if ($('.jdgrid').length > 0) {
		$('.jdgrid').each(function (i, g) {
			if ($(g).data('jdgrid') != undefined)
				$(g).data('jdgrid').refresh();
		});
	}
}

(function ($) {
	$.fn.showLoading = function () {

		$(this).each(function () {
			var loading = $('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
			$(this).append(loading);
		});

		return $(this);
	}

	$.fn.hideLoading = function () {

		$(this).each(function () {
			$(this).find('.overlay').remove();
		});

		return $(this);
	}
}(jQuery));

function laySoLuongChiTieu() {
	$.ajax({
		url: 'authen/lay-so-luong-chi-tieu',
		dataType: 'text',
		beforeSend: function (jqXHR, settings) {
		},
		success: function (data, textStatus, jqXHR) {
			if (data == undefined) {
				console.log('laySoLuongChiTieu error: ' + data.errorMessage);
			} else if (data == "0") {
				$('#chitieu').text(data);
				$('#li-chitieu').css({ "display": "none" });
			} else {
				$('#chitieu').text(data);
				$('#li-chitieu').css({ "display": "block" });
			}
		},
		complete: function (jqXHR, textStatus) {

		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log('laySoLuongChiTieu error: ' + textStatus);
		}
	});
}

function laySoLuongCongNo() {
	$.ajax({
		url: 'authen/lay-so-luong-cong-no',
		dataType: 'text',
		beforeSend: function (jqXHR, settings) {
		},
		success: function (data, textStatus, jqXHR) {
			if (data == undefined) {
				console.log('laySoLuongCongNo error: ' + data.errorMessage);
			} else if (data == "0") {
				$('#congno').text(data);
				$('#li-congno').css({ "display": "none" });
			} else {
				$('#congno').text(data);
				$('#li-congno').css({ "display": "block" });
			}
		},
		complete: function (jqXHR, textStatus) {

		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log('laySoLuongCongNo error: ' + textStatus);
		}
	});
}

function layThongTinCongTy() {
	$.ajax({
		url: 'authen/lay-tt-cty',
		dataType: 'json',
		beforeSend: function (jqXHR, settings) {
		},
		success: function (data, textStatus, jqXHR) {
			// console.log(data);
			$('#spanTenCongTy').text(data[0].giaTri);
			$('#spanSoDienThoai').text(data[3].giaTri);
		},
		complete: function (jqXHR, textStatus) {
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log('layThongTinCongTy error: ' + textStatus);
		}
	});
}

function genMenu() {
	$.ajax({
		url: 'authen/main-menu',
		dataType: 'json',
		beforeSend: function (jqXHR, settings) {
		},
		success: function (data, textStatus, jqXHR) {
			var pathname = window.location.pathname.substring(1);
			$.each(data, function (i, item) {
				if (jQuery.isEmptyObject(item.menuDtos)) {
					var cls = pathname == item.url ? 'active' : '';
					$('.sidebar-menu').append('<li class="' + cls + '"><a href="' + item.url + '">' + item.icon + '<span>' + item.title + '</span></a></li>');
				} else {
					var found = false;
					var group = $('<li class="treeview"><a href="#">' + item.icon + '<span>' + item.title + '</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu"></ul></li>');
					$.each(item.menuDtos, function (ii, itm) {
						var cls = pathname == itm.url ? 'active' : '';
						if (pathname == itm.url) found = true;
						group.find('ul').append('<li class="' + cls + ' menu"><a href="' + itm.url + '"><i class="fa fa-circle-o"></i> ' + '<span>' + itm.title + '<span id="nhacViec' + itm.idMenu + '" class="nhac-viec"></span></span></a></li>');
					});
					if (found) group.addClass('active menu-open');
					$('.sidebar-menu').append(group);
				}
			});
			nhacViecNhanVien();
			nhacViecPhongKeHoach();
		},
		complete: function (jqXHR, textStatus) {
		},
		error: function (jqXHR, textStatus, errorThrown) {
			toastError('Khởi tao menu không thành công!');
		}
	});
}

function nhacViecNhanVien() {
	$.ajax({
		url: 'authen/nhac-viec-nhan-vien',
		dataType: 'json',
		beforeSend: function (jqXHR, settings) {
		},
		success: function (data, textStatus, jqXHR) {
			console.log(data);
			if (data.tuChoiDuyet > 0)
				$('#nhacViec54').addClass("show");
			$('#nhacViec54').html(data.tuChoiDuyet);

		},
		complete: function (jqXHR, textStatus) {
		},
		error: function (jqXHR, textStatus, errorThrown) {
			toastError('Nhắc việc nhân viên không thành công!');
		}
	});
}

function nhacViecPhongKeHoach() {
	$.ajax({
		url: 'authen/nhac-viec-phong-ke-hoach',
		dataType: 'json',
		beforeSend: function (jqXHR, settings) {
		},
		success: function (data, textStatus, jqXHR) {
			console.log(data);
			if (data.choDuyet > 0)
				$('#nhacViec55').addClass("show");
			$('#nhacViec55').html(data.choDuyet);
		},
		complete: function (jqXHR, textStatus) {
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log({ jqXHR, textStatus, errorThrown });
			toastError('Nhắc việc phòng kế hoạch không thành công!');
		}
	});
}

function toastInfo(msg) {
	var toast = $('<div></div>').addClass('toast toast-info').html(msg);
	$('body').append(toast);
	toast.fadeIn().delay(2000).fadeOut(function () {
		toast.remove();
	});
}

function toastError(msg) {
	var toast = $('<div></div>').addClass('toast toast-err').html(msg);
	$('body').append(toast);
	toast.fadeIn().delay(2000).fadeOut(function () {
		toast.remove();
	});
}

function showError(title, msg) {
	var modal = $('<div class="modal bs-example-modal-sm" tabindex="-1" role="dialog">'
		+ '<div class="modal-dialog modal-sm" role="document">'
		+ '	<div class="modal-content">'
		+ '		<div class="modal-header modal-err">'
		+ '			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
		+ '			<h4 class="modal-title">' + title + '</h4>'
		+ '		</div>'
		+ '		<div class="modal-body text-danger">' + msg + '</div>'
		+ '		<div class="modal-footer">'
		+ '	    	<button type="button" class="btn btn-danger btn-flat btn-sm" data-dismiss="modal">&nbsp;&nbsp;OK&nbsp;&nbsp;</button>'
		+ '  	</div>'
		+ '	</div>'
		+ '</div>'
		+ '</div>');
	$('body').append(modal);
	modal.on('hidden.bs.modal', function (e) { $(this).remove(); });
	modal.modal('show');
}

function showSuccess(title, msg) {
	var modal = $('<div class="modal bs-example-modal-sm" tabindex="-1" role="dialog">'
		+ '<div class="modal-dialog modal-sm" role="document">'
		+ '	<div class="modal-content">'
		+ '		<div class="modal-header modal-suc">'
		+ '			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
		+ '			<h4 class="modal-title">' + title + '</h4>'
		+ '		</div>'
		+ '		<div class="modal-body text-success" style="word-break: break-all;">' + msg + '</div>'
		+ '		<div class="modal-footer">'
		+ '	    	<button type="button" class="btn btn-success btn-flat btn-sm" data-dismiss="modal">&nbsp;&nbsp;OK&nbsp;&nbsp;</button>'
		+ '  	</div>'
		+ '	</div>'
		+ '</div>'
		+ '</div>');
	$('body').append(modal);
	modal.on('hidden.bs.modal', function (e) { $(this).remove(); });
	modal.modal('show');
}

function showConfirm(title, msg, func) {
	var yesClk = false;
	var modal = $('<div class="modal bs-example-modal-sm" tabindex="-1" role="dialog">'
		+ '<div class="modal-dialog modal-sm" role="document">'
		+ '	<div class="modal-content">'
		+ '		<div class="modal-header modal-cfrm">'
		+ '			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
		+ '			<h4 class="modal-title">' + title + '</h4>'
		+ '		</div>'
		+ '		<div class="modal-body text-warning">' + msg + '</div>'
		+ '		<div class="modal-footer">'
		+ '			<button type="button" class="btn btn-default btn-flat btn-sm" data-dismiss="modal">Không</button>'
		+ '			<button type="button" class="btn btn-warning btn-flat btn-sm" data-dismiss="modal" id="btn-yes">&nbsp;&nbsp;Có&nbsp;&nbsp;</button>'
		+ '		</div>'
		+ '	</div>'
		+ '</div>'
		+ '</div>');
	$('body').append(modal);
	modal.find('.btn-warning').click(function () { yesClk = true; });
	modal.on('hidden.bs.modal', function (e) { $(this).remove(); if (yesClk) eval(func); });
	modal.modal('show');
}

//Valid number input
$('.number,.money').on('keypress', function (e) {
	return e.metaKey || // cmd/ctrl
		e.which <= 0 || // arrow keys
		e.which == 8 || // delete key
		/[0-9]/.test(String.fromCharCode(e.which)); // numbers
});

//Format money number
$('.money').keyup(function (e) {
	$(this).val(function (index, value) {
		return value
			.replace(/\D/g, "")
			.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
			;
	});
});

// Format number
function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

// Remove format number
function numberWithoutCommas(x) {
	return x.toString().replace(/\./g, "");
}

// Enter->tab
$('.enter-tab').keypress(function (e) {
	if (e.which == 13) {
		$('#' + $(this).attr('next')).focus();
	}
});

// Milisec to dd/mm/yyyy
function milisecToDate(interval) {
	var date = new Date(interval);
	return ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear();
}

function timeConverter(UNIX_timestamp) {
	if (UNIX_timestamp === null)
		return '';
	var a = new Date(UNIX_timestamp);
	var year = a.getFullYear();
	var month = a.getMonth() + 1;
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	var sec = a.getSeconds();
	var time = ('0' + hour).slice(-2) + ':' + ('0' + min).slice(-2) + ':' + ('0' + sec).slice(-2) + ' ' + ('0' + date).slice(-2) + '/' + ('0' + month).slice(-2) + '/' + year;
	return time;
}

// Copy array data and struct
function copyArray(source) {
	var dest = [];
	var len = source.length;
	for (var i = 0; i < len; i++) {
		dest[i] = {};
		for (var prop in source[i]) {
			dest[i][prop] = source[i][prop];
		}
	}
	return dest;
}