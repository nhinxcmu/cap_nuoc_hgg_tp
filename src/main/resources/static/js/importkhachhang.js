var pref_url="/import-khach-hang";

$(document).ready(function(){
	$('#btn-ok').click(function(){
		if(check()){
			luu();
		}
	});
	
	$('#btn-file').click(function(){
		window.location.href = "help/ImportKhachHang.xlsx";
	});
});

function check(){
	if($('#fileData').val()===''){
		showError('Thông báo','Vui lòng chọn file Excel import!');
		return false;
	}
	return true;
}



function luu(){
	$.ajax({
		url:pref_url+'/xu-ly-import',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		enctype: 'multipart/form-data',
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Import dữ liệu thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Import dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}
