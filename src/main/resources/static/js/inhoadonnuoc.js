var pref_url="/in-hoa-don-nuoc";

$('#view').click(function() {
	if(check()){
		var tree = $("#tree").fancytree("getTree"),
		selNodes = tree.getSelectedNodes();
		var nodes = new Array();
	    selNodes.forEach(function(node) {
	    	if(node.key!=-1) nodes.push(node.key);
	    });
		var urlp = pref_url+'/xem-bao-cao?dsSoGhi='+nodes.join()+'&tustt='+$('#txt-tuso').val()+'&denstt='+$('#txt-denso').val();
		$.ajax({
	        url: urlp,
	        beforeSend: function(jqXHR, settings){
	        	$('#box-ds').showLoading();
			},
	        success: function(response){
				PDFObject.embed(response, "#pdfRenderer");
			},
			complete: function(jqXHR, textStatus){
				$('#box-ds').hideLoading();
			}
	    });
	}
 });
 
$(document).ready(function(){
	//tree
	$("#tree").fancytree({
		checkbox: true,
		selectMode: 3,
		// extensions: ["dnd"],
		icon: false,
		source: {
	        url: pref_url+"/du-lieu-cay",
	        cache: false
	    }//,
//	    activate: function(event, data){
//	    	actNode=data.node;
//	    },
//	    dnd: {
//	        autoExpandMS: 400,
//	        focusOnClick: true,
//	        preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
//	        preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
//	        dragStart: function(node, data) {
//	          /** This function MUST be defined to enable dragging for the tree.
//	           *  Return false to cancel dragging of node.
//	           */
//	          return true;
//	        },
//	        dragEnter: function(node, data) {
//	          /** data.otherNode may be null for non-fancytree droppables.
//	           *  Return false to disallow dropping on node. In this case
//	           *  dragOver and dragLeave are not called.
//	           *  Return 'over', 'before, or 'after' to force a hitMode.
//	           *  Return ['before', 'after'] to restrict available hitModes.
//	           *  Any other return value will calc the hitMode from the cursor position.
//	           */
//	          // Prevent dropping a parent below another parent (only sort
//	          // nodes under the same parent)
//	/*           if(node.parent !== data.otherNode.parent){
//	            return false;
//	          }
//	          // Don't allow dropping *over* a node (would create a child)
//	          return ["before", "after"];
//	*/
//	           return true;
//	        },
//	        dragDrop: function(node, data) {
//	          /** This function MUST be defined to enable dropping of items on
//	           *  the tree.
//	           */
//	        	data.otherNode.moveTo(node, data.hitMode);
//	        }
//	      }
	});
	
	$('#moRongTatCa').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
			node.setExpanded();
		});
		return false;
	});
	
	$('#thuGonTatCa').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
			node.setExpanded(false);
		});
		return false;
	});
	
	$('#chk-all').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
	        node.setSelected(true);
	    });
		return false;
	});
	
	$('#chk-none').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
	        node.setSelected(false);
	    });
		return false;
	});
	
	$('#pdf').click(function() {
		if(check()){
			var tree = $("#tree").fancytree("getTree"),
			selNodes = tree.getSelectedNodes();
			var nodes = new Array();
		    selNodes.forEach(function(node) {
		    	if(node.key!=-1) nodes.push(node.key);
		    });
			var urlp = pref_url+'/xuat-file-pdf?dsSoGhi='+nodes.join()+'&tustt='+$('#txt-tuso').val()+'&denstt='+$('#txt-denso').val();
	        $.ajax({
	            url: urlp,
	            beforeSend: function(jqXHR, settings){
	            	$('#box-ds').showLoading();
	    		},
	    		success: function(response){
	    			$(location).attr('href', urlp);
	    		},
	    		complete: function(jqXHR, textStatus){
	    			$('#box-ds').hideLoading();
	    		}
	        });
		}
    });
	
    $('#xls').click(function() {
    	if(check()){
    		var tree = $("#tree").fancytree("getTree"),
    		selNodes = tree.getSelectedNodes();
    		var nodes = new Array();
    	    selNodes.forEach(function(node) {
    	    	if(node.key!=-1) nodes.push(node.key);
    	    });
	    	var urlp = pref_url+'/xuat-file-xls?dsSoGhi='+nodes.join()+'&tustt='+$('#txt-tuso').val()+'&denstt='+$('#txt-denso').val();
	        $.ajax({
	            url: urlp,
	            beforeSend: function(jqXHR, settings){
	            	$('#box-ds').showLoading();
	    		},
	    		success: function(response){
	    			$(location).attr('href', urlp);
	    		},
	    		complete: function(jqXHR, textStatus){
	    			$('#box-ds').hideLoading();
	    		}
	        });
    	}
    });
    
    $('#xlsx').click(function() {
    	if(check()){
    		var tree = $("#tree").fancytree("getTree"),
    		selNodes = tree.getSelectedNodes();
    		var nodes = new Array();
    	    selNodes.forEach(function(node) {
    	    	if(node.key!=-1) nodes.push(node.key);
    	    });
	    	var urlp = pref_url+'/xuat-file-xlsx?dsSoGhi='+nodes.join()+'&tustt='+$('#txt-tuso').val()+'&denstt='+$('#txt-denso').val();
	        $.ajax({
	            url: urlp,
	            beforeSend: function(jqXHR, settings){
	            	$('#box-ds').showLoading();
	    		},
	    		success: function(response){
	    			$(location).attr('href', urlp);
	    		},
	    		complete: function(jqXHR, textStatus){
	    			$('#box-ds').hideLoading();
	    		}
	        });
    	}
    });
    $('#rtf').click(function() {
    	if(check()){
    		var tree = $("#tree").fancytree("getTree"),
    		selNodes = tree.getSelectedNodes();
    		var nodes = new Array();
    	    selNodes.forEach(function(node) {
    	    	if(node.key!=-1) nodes.push(node.key);
    	    });
	    	var urlp = pref_url+'/xuat-file-rtf?dsSoGhi='+nodes.join()+'&tustt='+$('#txt-tuso').val()+'&denstt='+$('#txt-denso').val();
	        $.ajax({
	            url: urlp,
	            beforeSend: function(jqXHR, settings){
	            	$('#box-ds').showLoading();
	    		},
	    		success: function(response){
	    			$(location).attr('href', urlp);
	    		},
	    		complete: function(jqXHR, textStatus){
	    			$('#box-ds').hideLoading();
	    		}
	        });
    	}
    });
});

function check(){
	var tree = $("#tree").fancytree("getTree"),
	selNodes = tree.getSelectedNodes();
	var nodes = new Array();
    selNodes.forEach(function(node) {
    	if(node.key!=-1) nodes.push(node.key);
    });
	if(nodes.join()===''){
		showError('Thông báo','Vui lòng chọn tổ!');
		return false;
	}
	return true;
}

