var pref_url = "/khach-hang";
var giatri_soghi;
var giatri_duong;
var maKhachHang;

$(document).ready(function () {
	$('#grid-ds').jdGrid({
		columns: [
			{ name: 'thuTu', title: 'TT' },
			// {name:'maDongHo',title:'Mã đồng hồ'},
			{ name: 'maKhachHang', title: 'Mã KH' },
			{ name: 'tenKhachHang', title: 'Tên KH' },
			{ name: 'soSeri', title: 'Số seri', css: { 'width': '100px' } },
			{ name: 'diaChiSuDung', title: 'Địa chỉ' },
			{ name: 'soDienThoai', title: 'Điện thoại' },
			{ name: 'tenDoiTuong', title: 'Đối tượng' },
			{ name: 'col2', title: 'T.Tác', type: 'control', content: function (obj) { return '<a data-toggle="modal" data-target="#modBanDo" href="#" class="row-map-marker-alt-1" rid="' + obj.idThongTinKhachHang + '" title="Vị trí đồng hồ nước"><i class="fas fa-map-marker"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="modal" data-target="#modLichSu" href="#" class="row-history-1" rid="' + obj.idKhachHang + '" title="Lịch sử sử dụng nước"><i class="fa fa-history"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="modal" data-target="#modLichSuTT" href="#" class="row-history-1tt" rid="' + obj.idKhachHang + '" title="Log cập nhật"><i class="fa fa-list"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="modal" maKhachHang="'+obj.maKhachHang+'" data-target="#modForm" href="#" class="row-clock-1" rid="' + obj.idKhachHang + '" title="Quản lý đồng hồ nước"><i class="fa fa-clock"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="modal" data-target="#modPrint" href="#" class="row-print-1" rid="' + obj.idThongTinKhachHang + '" ridThang="0" title="In hóa đơn nước"><i class="fa fa-print"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="row-edit-1" rid="' + obj.idThongTinKhachHang + '" title="Sửa"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="row-del-1 text-danger" rid="' + obj.idThongTinKhachHang + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' }, css: { 'text-align': 'center', 'width': '190px' } }
		],
		height: '645px'
	});

	$('#grid-detail').jdGrid({
		columns: [
			{ name: 'maDongHo', title: 'Mã đồng hồ' },
			{ name: 'chungLoai', title: 'Chủng loại' },
			{ name: 'soSeri', title: 'Số seri' },
			{ name: 'kichCo', title: 'Kích cỡ' },
			{ name: 'heSoNhan', title: 'Hệ số nhân' },
			{ name: 'chiSoCucDai', title: 'Chỉ số cực đại' },
			{ name: 'nhaSanXuat', title: 'Nhà sản xuất' },
			{ name: 'ngaySanXuat', title: 'Ngày sản xuất', type: 'interval' },
			{ name: 'ngayDuaVaoSuDung', title: 'Ngày đưa vào sử dụng', type: 'interval' },
			{ name: 'ngayHetHanBaoHanh', title: 'Ngày hết hạn bảo hành', type: 'interval' },
			{ name: 'ngayKiemDinh', title: 'Ngày kiểm định', type: 'interval' },
			{ name: 'ngaySuaChuaLanCuoi', title: 'Ngày sửa chữa lần cuối', type: 'interval' },
			{ name: 'ghiChuSuaChuaLanCuoi', title: 'Ghi chú sửa chữa lần cuối' },
			{ name: 'tenTrangThaiDongHo', title: 'Trạng thái' },
			{ name: 'thangSuDung', title: 'Tháng sử dụng' }
		],
		height: '120px'
	});

	$('#grid-lichsu').jdGrid({
		columns: [
			{ name: 'tenThang', title: 'Tháng', css: { 'text-align': 'center' } },
			{ name: 'chiSoCu', title: 'CS cũ', format: true, css: { 'text-align': 'right' } },
			{ name: 'chiSoMoi', title: 'CS mới', format: true, css: { 'text-align': 'right' } },
			{ name: 'tieuThu', title: 'Tiêu thụ', format: true, css: { 'text-align': 'right' } },
			{ name: 'tienNuoc', title: 'Tiền nước', format: true, css: { 'text-align': 'right' } },
			{ name: 'thue', title: 'Thuế GTGT', format: true, css: { 'text-align': 'right' } },
			{ name: 'phiBvmt', title: 'Phí BVMT', format: true, css: { 'text-align': 'right' } },
			{ name: 'phiBvr', title: 'Phí BVR', format: true, css: { 'text-align': 'right' } },
			{ name: 'tongTien', title: 'Tổng tiền', format: true, css: { 'text-align': 'right' } },
			{ name: 'col2', title: 'T.Tác', type: 'control', content: function (obj) { return '<a data-toggle="modal" data-target="#modPrint" href="#" class="row-print-1" rid="' + obj.idThongTinKhachHang + '" ridThang="' + obj.idThang + '" title="In hóa đơn nước"><i class="fa fa-print"></i></a>' }, css: { 'text-align': 'center', 'width': '30px' } }
		],
		height: '450px',
		decnum: 0
	});

	$('#grid-lichsutt').jdGrid({
		columns: [
			{ name: 'thuTu', title: 'TT' },
			// {name:'maDongHo',title:'Mã đồng hồ'},
			{ name: 'maKhachHang', title: 'Mã KH' },
			{ name: 'tenKhachHang', title: 'Tên KH' },
			{ name: 'diaChiSuDung', title: 'Địa chỉ' },
			{ name: 'soDienThoai', title: 'Điện thoại' },
			{ name: 'tenDoiTuong', title: 'Đối tượng' },
			{ name: 'ngayGioCapNhat', title: 'Ngày giờ cập nhật', type: 'interval' },
			{ name: 'col2', title: 'T.Tác', type: 'control', content: function (obj) { return '<a href="#" class="row-edit-2" rid="' + obj.idLichSuKhachHang + '" title="Xem chi tiết"><i class="fa fa-list"></i></a>' }, css: { 'text-align': 'center', 'width': '50px' } }
		],
		height: '450px',
		decnum: 0
	});

	$('.input-mask').inputmask();

	$('.chon-ngay').datepicker({
		autoclose: true,
		todayHighlight: true,
		format: 'dd/mm/yyyy',
		zIndexOffset: 10000
	});//.datepicker('setDate', new Date());

	$('#page-ds').jdPage({
		onPageChanged: function (p) {
			layDanhSach(p - 1);
		}
	});

	$('#page-detail').jdPage({
		onPageChanged: function (p) {
			layDanhSachDetail(p - 1);
		}
	});

	$('#page-lichsu').jdPage({
		onPageChanged: function (p) {
			layLichSu(p - 1);
		}
	});

	$('#page-lichsutt').jdPage({
		onPageChanged: function (p) {
			layLichSuTT(p - 1);
		}
	});

	$('#modPrint').on('shown.bs.modal', function (e) {
		var urlp = pref_url + '/xem-bao-cao?id=' + $(e.relatedTarget).attr('rid') + '&idThang=' + $(e.relatedTarget).attr('ridThang');
		$.ajax({
			url: urlp,
			beforeSend: function (jqXHR, settings) {
				$('#box-modPrint').showLoading();
			},
			success: function (response) {
				PDFObject.embed(response, "#pdfRenderer");
			},
			complete: function (jqXHR, textStatus) {
				$('#box-modPrint').hideLoading();
			}
		});
	});

	$('#btn-ok').click(function () {
		if (check()) {
			luu();
		}
	});

	$('#btn-cancel').click(function () {
		huy();
	});

	$('#cmb-khuvuc-filter').change(function () {
		layDsPhuong();
	});

	$('#cmb-phuong-filter').change(function () {
		layDsTo();
	});

	$('#cmb-khuvuc').change(function () {
		layDsPhuongTT();
	});

	$('#cmb-phuong').change(function () {
		layDsToTT();
	});

	$('#cmb-to-filter').change(function () {
		layDanhSach(0);
	});

	$('#btn-search').click(function () {
		layDanhSach(0);
	});

	$('#txt-keyword').keypress(function (e) {
		if (e.which == 13) {
			$('#btn-search').click();
		}
	});

	$('#btn-clear-search').click(function () {
		$('#txt-keyword').val('').focus();
		layDanhSach(0);
	});

	$('#txt-ho, #txt-ten').change(function () {
		$('#txt-tenkhachhang').val($('#txt-ho').val() + ' ' + $('#txt-ten').val());
	});

	$('#txt-diachisudung').change(function () {
		if ($.trim($('#txt-diachithanhtoan').val()) == '')
			$('#txt-diachithanhtoan').val($('#txt-diachisudung').val());
	});

	$('#saoChep').click(function () {
		$('#txt-diachithanhtoan').val($('#txt-diachisudung').val());
	});

	$('#modForm').on('shown.bs.modal', function (e) {
		layChiTietDongHo($(e.relatedTarget).attr('rid'));
		maKhachHang= $(e.relatedTarget).attr('maKhachHang');
		layDsTrangThaiDongHo();
		// layDanhSachDetail($(e.relatedTarget).attr('rid'),0);
	});

	$('#modLichSu').on('shown.bs.modal', function (e) {
		$('#txt-idkhachhanglichsu').val($(e.relatedTarget).attr('rid'));
		layLichSu(0);
	});

	$('#modLichSuTT').on('shown.bs.modal', function (e) {
		$('#txt-idkhachhanglichsutt').val($(e.relatedTarget).attr('rid'));
		layLichSuTT(0);
	});

	$('#modBanDo').on('shown.bs.modal', function (e) {
		var ridBanDo = $(e.relatedTarget).attr('rid');
		if (ridBanDo === '0') {
			$.ajax({
				url: pref_url + '/lay-ds-full',
				method: 'get',
				dataType: 'json',
				data: { idKhuVuc: $('#cmb-khuvuc-filter').val(), idDuong: $('#cmb-phuong-filter').val(), idSoGhi: $('#cmb-to-filter').val(), thongTinCanTim: $.trim($('#txt-keyword').val()) },
				success: function (data) {
					var map;
					var stt = 1;
					$.each(data, function (key, value) {
						if (value.viDo !== null && value.kinhDo !== null) {
							var myLatlng = new google.maps.LatLng(value.viDo, value.kinhDo);

							if (stt === 1) {
								var myOptions = {
									zoom: 17,
									center: myLatlng
								};
								map = new google.maps.Map(document.getElementById("box-bando"), myOptions);
							}

							new google.maps.Marker({
								position: myLatlng,
								map: map,
								title: value.tenKhachHang + " (" + value.maKhachHang + ")"
							});

							stt++;
						}
					});

					if (stt === 1) {
						$('#box-bando').html('');
						showError('Thông báo', 'Chưa cập nhật vị trí đồng hồ nước!');
					}
				},
				error: function () {
					showError('Thông báo', 'Lấy thông tin không thành công, vui lòng thử lại sau!');
				}
			});
		} else {
			$.ajax({
				url: pref_url + '/lay-ct',
				method: 'get',
				dataType: 'json',
				data: { id: ridBanDo },
				success: function (data) {
					if (data.viDo !== null && data.kinhDo !== null) {
						var myLatlng = new google.maps.LatLng(data.viDo, data.kinhDo);
						var myOptions = {
							zoom: 17,
							center: myLatlng
						};
						var map = new google.maps.Map(document.getElementById("box-bando"), myOptions);
						var marker = new google.maps.Marker({
							position: myLatlng,
							map: map,
							title: data.tenKhachHang + " (" + data.maKhachHang + ")"
						});
					} else {
						$('#box-bando').html('');
						showError('Thông báo', 'Chưa cập nhật vị trí đồng hồ nước!');
					}
				},
				error: function () {
					showError('Thông báo', 'Lấy dữ liệu không thành công, vui lòng thử lại sau!');
				}
			});
		}
	});

	$('#kiemTra').click(function () {
		if ($.trim($('#txt-madongho').val()) === '')
			showError('Thông báo', 'Vui lòng nhập mã đồng hồ!');
		else {
			kiemTraDongHo($('#txt-madongho').val(), $('#txt-idkhachhang-dh').val());
		}
	});

	$('#txt-madongho').keypress(function (e) {
		if (e.which == 13) {
			$('#kiemTra').click();
		}
	});

	$('#btn-cancel-2').click(function () {
		if ($.trim($('#txt-iddongho').val()) === '')
			showError('Thông báo', 'Không có đồng hồ để xóa!');
		else {
			showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoaDongHo(' + $('#txt-iddongho').val() + ',' + $('#txt-idkhachhang-dh').val() + ')');
		}
	});

	$('#btn-ok-2').click(function () {
		if ($.trim($('#txt-madongho').val()) === '')
			showError('Thông báo', 'Vui lòng nhập mã đồng hồ!');
		else {
			luuDongHo();
		}
	});

	$('#cmb-size').change(function () {
		layDanhSach(0);
	});

	$('#btn-export').click(function () {
		var urlp = pref_url + '/xuat-file-xlsx?idKhuVuc=' + $('#cmb-khuvuc-filter').val() + '&idDuong=' + $('#cmb-phuong-filter').val() + '&idSoGhi=' + $('#cmb-to-filter').val();
		$.ajax({
			url: urlp,
			method: 'get',
			beforeSend: function (jqXHR, settings) {
				$('#box-ds').showLoading();
			},
			success: function (response) {
				$(location).attr('href', urlp);
			},
			complete: function (jqXHR, textStatus) {
				$('#box-ds').hideLoading();
			}
		});
	});

	layDsKhuVuc();
	layDsKhuVucTT();
	layMaKhachHangMoi();
	layDsNganHang();
	layDsDoiTuong();
});

function check() {
	if ($('#txt-ho').val() === '') {
		showError('Thông báo', 'Vui lòng nhập họ khách hàng!');
		return false;
	}
	if ($('#txt-ten').val() === '') {
		showError('Thông báo', 'Vui lòng nhập tên khách hàng!');
		return false;
	}
	if ($('#txt-diachisudung').val() === '') {
		showError('Thông báo', 'Vui lòng nhập địa chỉ sử dụng!');
		return false;
	}
	if ($('#txt-diachithanhtoan').val() === '') {
		showError('Thông báo', 'Vui lòng nhập địa chỉ thanh toán!');
		return false;
	}
	return true;
}

function layDanhSach(p) {
	$.ajax({
		url: pref_url + '/lay-ds',
		method: 'get',
		dataType: 'json',
		data: { idKhuVuc: $('#cmb-khuvuc-filter').val(), idDuong: $('#cmb-phuong-filter').val(), idSoGhi: $('#cmb-to-filter').val(), thongTinCanTim: $.trim($('#txt-keyword').val()), page: p, size: $('#cmb-size').val() },
		beforeSend: function () {
			$('#box-ds').showLoading();
		},
		success: function (data) {
			$('#grid-ds').data('jdgrid').fillData(data.content);
			$('#page-ds').data('jdpage').setData({ 'totalPage': data.totalPages, 'currentPage': data.number + 1, 'itemOnPage': data.size, 'totalItem': data.totalElements });

			$('.row-edit-1').click(function (e) {
				e.preventDefault();
				layChiTiet($(this).attr('rid'));
			});

			$('.row-del-1').click(function (e) {
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa(' + $(this).attr('rid') + ')');
			});
		},
		error: function () {
			showError('Thông báo', 'Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-ds').hideLoading();
		}
	});
}

function layDanhSachDetail(id, p) {
	$.ajax({
		url: pref_url + '/lay-ds-detail',
		method: 'get',
		dataType: 'json',
		data: { id: id, page: p },
		beforeSend: function () {
			$('#box-form-2').showLoading();
		},
		success: function (data) {
			$('#grid-detail').data('jdgrid').fillData(data.content);
			$('#page-detail').data('jdpage').setData({ 'totalPage': data.totalPages, 'currentPage': data.number + 1, 'itemOnPage': data.size, 'totalItem': data.totalElements });
		},
		error: function () {
			showError('Thông báo', 'Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-form-2').hideLoading();
		}
	});
}

function luu() {
	$.ajax({
		url: pref_url + '/luu',
		method: 'post',
		dataType: 'json',
		data: new FormData($('#frm-1')[0]),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$('#box-frm').showLoading();
		},
		success: function (data) {
			if (data.resCode > 0) {
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
			} else {
				showError('Thông báo', data.resMessage);
			}
		},
		error: function () {
			showError('Thông báo', 'Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-frm').hideLoading();
		}
	});
}

function huy() {
	$('#frm-1')[0].reset();
	$('#txt-idthongtinkhachhang').val('');
	layDsPhuongTT();
	layMaKhachHangMoi();
	layDsNganHang();
	layDsDoiTuong();
	// $('.chon-ngay').datepicker('setDate', new Date());
}

function layChiTiet(id) {
	$.ajax({
		url: pref_url + '/lay-ct',
		method: 'get',
		dataType: 'json',
		data: { id: id },
		beforeSend: function () {
			$('#box-frm').showLoading();
		},
		success: function (data) {
			$('#txt-idthongtinkhachhang').val(data.idThongTinKhachHang);
			$('#txt-makhachhang').val(data.maKhachHang);
			$('#txt-thutu').val(data.thuTu);
			$('#txt-ho').val(data.ho);
			$('#txt-ten').val(data.ten);
			$('#txt-tenkhachhang').val(data.tenKhachHang);
			$('#txt-diachisudung').val(data.diaChiSuDung);
			$('#txt-diachithanhtoan').val(data.diaChiThanhToan);
			$('#txt-sodienthoai').val(data.soDienThoai);
			$('#txt-socmnd').val(data.soCmnd);
			$('#txt-masothue').val(data.maSoThue);
			$('#txt-sotaikhoan').val(data.soTaiKhoan);
			$('#txt-tentaikhoan').val(data.tenTaiKhoan);
			$('#cmb-nganhang').val(data.idNganHang).trigger('change');
			giatri_soghi = data.idSoGhi;
			giatri_duong = data.idDuong;
			$('#cmb-khuvuc').val(data.idKhuVuc).trigger('change');
			// $('#cmb-phuong').val(data.idDuong).trigger('change');
			// $('#cmb-to').val(data.idSoGhi).trigger('change');
			$('#txt-sohopdong').val(data.soHopDong);
			$('#txt-ngaylaphopdong').datepicker('setDate', data.ngayLapHopDong == null ? '' : new Date(data.ngayLapHopDong));
			$('#txt-ngaydangky').datepicker('setDate', data.ngayDangKy == null ? '' : new Date(data.ngayDangKy));
			$('#txt-ngaylapdat').datepicker('setDate', data.ngayLapDat == null ? '' : new Date(data.ngayLapDat));
			$('#cmb-doituong').val(data.idDoiTuong).trigger('change');
			$('#chk-thupbvmt').prop('checked', data.thuPbvmt);
			$('#chk-doithicong').prop('checked', data.doiThiCong);
		},
		error: function () {
			showError('Thông báo', 'Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-frm').hideLoading();
		}
	});
}

function layChiTietLS(id) {
	$.ajax({
		url: pref_url + '/lay-ct-lich-su-khach-hang',
		method: 'get',
		dataType: 'html',
		data: { idLichSuKhachHang: id },
		success: function (data) {
			showSuccess('Thông tin chi tiết', data);
		},
		error: function () {
			showError('Thông báo', 'Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		}
	});
}

function xoa(id) {
	$.ajax({
		url: pref_url + '/xoa',
		method: 'post',
		dataType: 'json',
		data: { id: id },
		beforeSend: function () {
			$('#box-ds').showLoading();
		},
		success: function (data) {
			if (data.resCode > 0) {
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
			} else {
				showError('Thông báo', data.resMessage);
			}
		},
		error: function () {
			showError('Thông báo', 'Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-ds').hideLoading();
		}
	});
}

function layDsKhuVuc() {
	$.ajax({
		url: pref_url + '/lay-ds-khu-vuc',
		method: 'get',
		dataType: 'json',
		beforeSend: function () {
			$('#box-form').showLoading();
		},
		success: function (data) {
			var khuvuc = $.map(data, function (obj) {
				obj.id = obj.idKhuVuc;
				obj.text = obj.tenKhuVuc;
				return obj;
			});

			$('#cmb-khuvuc-filter').select2({
				data: khuvuc
			});

			layKhuVucHienTai();
		},
		error: function () {
			showError('Thông báo', 'Lấy danh sách khu vực không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-form').hideLoading();
		}
	});
}

function layDsPhuong() {
	$.ajax({
		url: pref_url + '/lay-ds-phuong',
		method: 'get',
		dataType: 'json',
		data: { id: $('#cmb-khuvuc-filter').val() },
		beforeSend: function () {
			$('#box-form').showLoading();
		},
		success: function (data) {
			$('#cmb-phuong-filter').html('<option value="0">-- Tất cả các phường --</option>');

			var phuong = $.map(data, function (obj) {
				obj.id = obj.idDuong;
				obj.text = obj.tenDuong;
				return obj;
			});

			$('#cmb-phuong-filter').select2({
				data: phuong
			});

			layDsTo();
		},
		error: function () {
			showError('Thông báo', 'Lấy danh sách phường không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-form').hideLoading();
		}
	});
}

function layDsTo() {
	$.ajax({
		url: pref_url + '/lay-ds-to',
		method: 'get',
		dataType: 'json',
		data: { id: $('#cmb-phuong-filter').val() },
		beforeSend: function () {
			$('#box-form').showLoading();
		},
		success: function (data) {
			$('#cmb-to-filter').html('<option value="0">-- Tất cả các tổ --</option>');

			var to = $.map(data, function (obj) {
				obj.id = obj.idSoGhi;
				obj.text = obj.tenSoGhi;
				return obj;
			});

			$('#cmb-to-filter').select2({
				data: to
			});

			layDanhSach(0);
		},
		error: function () {
			showError('Thông báo', 'Lấy danh sách tổ không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-form').hideLoading();
		}
	});
}

function layMaKhachHangMoi() {
	$.ajax({
		url: pref_url + '/lay-ma-khach-hang-moi',
		method: 'get',
		dataType: 'html',
		success: function (data) {
			$('#txt-makhachhang').val(data);
		}
	});
}

function layDsNganHang() {
	$.ajax({
		url: pref_url + '/lay-ds-ngan-hang',
		method: 'get',
		dataType: 'json',
		beforeSend: function () {
			$('#box-form').showLoading();
		},
		success: function (data) {
			var nganhang = $.map(data, function (obj) {
				obj.id = obj.idNganHang;
				obj.text = obj.tenNganHang;
				return obj;
			});

			$('#cmb-nganhang').select2({
				data: nganhang
			});
		},
		error: function () {
			showError('Thông báo', 'Lấy danh sách ngân hàng không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-form').hideLoading();
		}
	});
}

function layDsDoiTuong() {
	$.ajax({
		url: pref_url + '/lay-ds-doi-tuong',
		method: 'get',
		dataType: 'json',
		beforeSend: function () {
			$('#box-form').showLoading();
		},
		success: function (data) {
			var doituong = $.map(data, function (obj) {
				obj.id = obj.idDoiTuong;
				obj.text = obj.tenDoiTuong;
				return obj;
			});

			$('#cmb-doituong').select2({
				data: doituong
			});
		},
		error: function () {
			showError('Thông báo', 'Lấy danh sách đối tượng không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-form').hideLoading();
		}
	});
}

function layDsKhuVucTT() {
	$.ajax({
		url: pref_url + '/lay-ds-khu-vuc',
		method: 'get',
		dataType: 'json',
		beforeSend: function () {
			$('#box-form').showLoading();
		},
		success: function (data) {
			var khuvuc = $.map(data, function (obj) {
				obj.id = obj.idKhuVuc;
				obj.text = obj.tenKhuVuc;
				return obj;
			});

			$('#cmb-khuvuc').select2({
				data: khuvuc
			});

			layDsPhuongTT();
		},
		error: function () {
			showError('Thông báo', 'Lấy danh sách khu vực không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-form').hideLoading();
		}
	});
}

function layDsPhuongTT() {
	$.ajax({
		url: pref_url + '/lay-ds-phuong',
		method: 'get',
		dataType: 'json',
		data: { id: $('#cmb-khuvuc').val() },
		beforeSend: function () {
			$('#box-form').showLoading();
		},
		success: function (data) {
			$('#cmb-phuong option').remove();

			var phuong = $.map(data, function (obj) {
				obj.id = obj.idDuong;
				obj.text = obj.tenDuong;
				return obj;
			});

			$('#cmb-phuong').select2({
				data: phuong
			});

			if ($('#cmb-phuong').val() != null && giatri_duong !== undefined) {
				$('#cmb-phuong').val(giatri_duong).trigger('change');
				giatri_duong = undefined;
			} else
				layDsToTT();
		},
		error: function () {
			showError('Thông báo', 'Lấy danh sách phường không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-form').hideLoading();
		}
	});
}

function layDsToTT() {
	$.ajax({
		url: pref_url + '/lay-ds-to',
		method: 'get',
		dataType: 'json',
		data: { id: $('#cmb-phuong').val() },
		beforeSend: function () {
			$('#box-form').showLoading();
		},
		success: function (data) {
			$('#cmb-to option').remove();

			var to = $.map(data, function (obj) {
				obj.id = obj.idSoGhi;
				obj.text = obj.tenSoGhi;
				return obj;
			});

			$('#cmb-to').select2({
				data: to
			});

			if ($('#cmb-to').val() != null && giatri_soghi !== undefined) {
				$('#cmb-to').val(giatri_soghi).trigger('change');
				giatri_soghi = undefined;
			}
		},
		error: function () {
			showError('Thông báo', 'Lấy danh sách tổ không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-form').hideLoading();
		}
	});
}

function layDsTrangThaiDongHo() {
	$.ajax({
		url: pref_url + '/lay-ds-trang-thai-dong-ho',
		method: 'get',
		dataType: 'json',
		beforeSend: function () {
			$('#box-form-2').showLoading();
		},
		success: function (data) {
			var trangthaidongho = $.map(data, function (obj) {
				obj.id = obj.idTrangThaiDongHo;
				obj.text = obj.tenTrangThaiDongHo;
				return obj;
			});

			$('#cmb-trangthaidongho').select2({
				data: trangthaidongho
			});
		},
		error: function () {
			showError('Thông báo', 'Lấy danh sách trạng thái đồng hồ không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-form-2').hideLoading();
		}
	});
}

function layChiTietDongHo(idkh) {
	$.ajax({
		url: pref_url + '/lay-ct-dong-ho',
		method: 'get',
		dataType: 'json',
		data: { id: idkh },
		beforeSend: function () {
			$('#box-frm-2').showLoading();
		},
		success: function (data) {
			if (data.maDongHo != null) {
				$('#txt-idkhachhang-dh').val(idkh);
				$('#txt-iddongho').val(data.idDongHo);
				$('#txt-madongho').val(data.maDongHo);
				$('#txt-chungloai').val(data.chungLoai);
				$('#txt-soseri').val(data.soSeri);
				$('#txt-kichco').val(data.kichCo);
				$('#txt-hesonhan').val(data.heSoNhan);
				$('#txt-chisocucdai').val(data.chiSoCucDai);
				$('#txt-nhasanxuat').val(data.nhaSanXuat);
				$('#txt-ngaysanxuat').datepicker('setDate', data.ngaySanXuat == null ? '' : new Date(data.ngaySanXuat));
				$('#txt-ngayduavaosudung').datepicker('setDate', data.ngayDuaVaoSuDung == null ? '' : new Date(data.ngayDuaVaoSuDung));
				$('#txt-ngayhethanbaohanh').datepicker('setDate', data.ngayHetHanBaoHanh == null ? '' : new Date(data.ngayHetHanBaoHanh));
				$('#txt-ngaykiemdinh').datepicker('setDate', data.ngayKiemDinh == null ? '' : new Date(data.ngayKiemDinh));
				$('#txt-ngaysuachualancuoi').datepicker('setDate', data.ngaySuaChuaLanCuoi == null ? '' : new Date(data.ngaySuaChuaLanCuoi));
				$('#txt-ghichusuachualancuoi').val(data.ghiChuSuaChuaLanCuoi);
				$('#cmb-trangthaidongho').val(data.idTrangThaiDongHo).trigger('change');
			} else {
				$('#frm-2')[0].reset();
				$('#txt-idkhachhang-dh').val(idkh);
				$('#txt-iddongho').val('');
				layDsTrangThaiDongHo();
			}
		},
		error: function () {
			showError('Thông báo', 'Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-frm-2').hideLoading();
		}
	});
}

function kiemTraDongHo(madongho, idkh) {
	$.ajax({
		url: pref_url + '/kiem-tra-dong-ho',
		method: 'get',
		dataType: 'json',
		data: { maDongHo: madongho, idKhachHang: idkh },
		beforeSend: function () {
			$('#box-frm-2').showLoading();
		},
		success: function (data) {
			if (data.resCode === undefined) {
				toastInfo('Có thể sử dụng mã đồng hồ này');
				if (data.maDongHo != null) {
					$('#txt-idkhachhang-dh').val(idkh);
					$('#txt-iddongho').val(data.idDongHo);
					$('#txt-madongho').val(data.maDongHo);
					$('#txt-chungloai').val(data.chungLoai);
					$('#txt-soseri').val(data.soSeri);
					$('#txt-kichco').val(data.kichCo);
					$('#txt-hesonhan').val(data.heSoNhan);
					$('#txt-chisocucdai').val(data.chiSoCucDai);
					$('#txt-nhasanxuat').val(data.nhaSanXuat);
					$('#txt-ngaysanxuat').datepicker('setDate', data.ngaySanXuat == null ? '' : new Date(data.ngaySanXuat));
					$('#txt-ngayduavaosudung').datepicker('setDate', data.ngayDuaVaoSuDung == null ? '' : new Date(data.ngayDuaVaoSuDung));
					$('#txt-ngayhethanbaohanh').datepicker('setDate', data.ngayHetHanBaoHanh == null ? '' : new Date(data.ngayHetHanBaoHanh));
					$('#txt-ngaykiemdinh').datepicker('setDate', data.ngayKiemDinh == null ? '' : new Date(data.ngayKiemDinh));
					$('#txt-ngaysuachualancuoi').datepicker('setDate', data.ngaySuaChuaLanCuoi == null ? '' : new Date(data.ngaySuaChuaLanCuoi));
					$('#txt-ghichusuachualancuoi').val(data.ghiChuSuaChuaLanCuoi);
					$('#cmb-trangthaidongho').val(data.idTrangThaiDongHo).trigger('change');
				} else {
					$('#frm-2')[0].reset();
					$('#txt-idkhachhang-dh').val(idkh);
					$('#txt-madongho').val(madongho);
					$('#txt-iddongho').val('');
					layDsTrangThaiDongHo();
				}
			} else {
				$('#frm-2')[0].reset();
				$('#txt-idkhachhang-dh').val(idkh);
				$('#txt-madongho').val(madongho);
				$('#txt-iddongho').val('');
				layDsTrangThaiDongHo();
				showError('Thông báo', data.resMessage);
			}
		},
		error: function () {
			showError('Thông báo', 'Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-frm-2').hideLoading();
		}
	});
}

function xoaDongHo(idDongHo, idKhachHang) {
	$.ajax({
		url: pref_url + '/xoa-dong-ho',
		method: 'post',
		dataType: 'json',
		data: { idDongHo: idDongHo, idKhachHang: idKhachHang },
		beforeSend: function () {
			$('#box-form-2').showLoading();
		},
		success: function (data) {
			if (data.resCode > 0) {
				toastInfo('Xóa dữ liệu thành công');
				$('#frm-2')[0].reset();
				$('#txt-idkhachhang-dh').val(idKhachHang);
				$('#txt-iddongho').val('');
				layDsTrangThaiDongHo();
				layDanhSach(0);
			} else {
				showError('Thông báo', data.resMessage);
			}
		},
		error: function () {
			showError('Thông báo', 'Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-form-2').hideLoading();
		}
	});
}

function luuDongHo() {
	$.ajax({
		url: pref_url + '/luu-dong-ho',
		method: 'post',
		dataType: 'json',
		data: new FormData($('#frm-2')[0]),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$('#box-frm-2').showLoading();
		},
		success: function (data) {
			if (data.resCode > 0) {
				toastInfo('Lưu dữ liệu thành công');
				layChiTietDongHo($('#txt-idkhachhang-dh').val());
				layDanhSach(0);
			} else {
				showError('Thông báo', data.resMessage);
			}
		},
		error: function () {
			showError('Thông báo', 'Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-frm-2').hideLoading();
		}
	});
}

function layLichSu(p) {
	$.ajax({
		url: pref_url + '/lay-lich-su',
		method: 'get',
		dataType: 'json',
		data: { idKhachHang: $('#txt-idkhachhanglichsu').val(), page: p },
		beforeSend: function () {
			$('#box-lichsu').showLoading();
		},
		success: function (data) {
			$('#grid-lichsu').data('jdgrid').fillData(data.content);
			$('#page-lichsu').data('jdpage').setData({ 'totalPage': data.totalPages, 'currentPage': data.number + 1, 'itemOnPage': data.size, 'totalItem': data.totalElements });
		},
		error: function () {
			showError('Thông báo', 'Lấy lịch sử không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-lichsu').hideLoading();
		}
	});
}

function layLichSuTT(p) {
	$.ajax({
		url: pref_url + '/lay-ds-lich-su-khach-hang',
		method: 'get',
		dataType: 'json',
		data: { idKhachHang: $('#txt-idkhachhanglichsutt').val(), page: p },
		beforeSend: function () {
			$('#box-lichsutt').showLoading();
		},
		success: function (data) {
			$('#grid-lichsutt').data('jdgrid').fillData(data.content);
			$('#page-lichsutt').data('jdpage').setData({ 'totalPage': data.totalPages, 'currentPage': data.number + 1, 'itemOnPage': data.size, 'totalItem': data.totalElements });

			$('.row-edit-2').click(function (e) {
				e.preventDefault();
				layChiTietLS($(this).attr('rid'));
			});
		},
		error: function () {
			showError('Thông báo', 'Lấy log cập nhật không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-lichsutt').hideLoading();
		}
	});
}

function layKhuVucHienTai() {
	$.ajax({
		url: pref_url + '/lay-khu-vuc-hien-tai',
		method: 'get',
		dataType: 'json',
		beforeSend: function () {
			$('#box-form').showLoading();
		},
		success: function (data) {
			$('#cmb-khuvuc-filter').val(data).trigger('change');
		},
		error: function () {
			showError('Thông báo', 'Lấy khu vực hiện tại không thành công, vui lòng thử lại sau!');
		},
		complete: function () {
			$('#box-form').hideLoading();
		}
	});
}
$('#txt-soseri').keyup(e=>{
	$('#txt-madongho').val(maKhachHang+"_"+e.target.value)
});
