var pref_url="/lich-su-cat-nuoc";

$('#view').click(function() {
	    var urlp = pref_url+"/xem-bao-cao?trangThai="+$('#cmb-trang-thai').val();
		$.ajax({
	        url: urlp,
	        beforeSend: function(jqXHR, settings){
	        	$('#box-ds').showLoading();
			},
	        success: function(response){
				PDFObject.embed(response, "#pdfRenderer");
			},
			complete: function(jqXHR, textStatus){
				$('#box-ds').hideLoading();
			}
	    });
 });
 
$(document).ready(function(){
	
	$('#pdf').click(function() {
		    var urlp = pref_url+"/xuat-file-pdf?trangThai="+$('#cmb-trang-thai').val();
			$.ajax({
		        url: urlp,
		        beforeSend: function(jqXHR, settings){
		        	$('#box-ds').showLoading();
				},
				success: function(response){
	    			$(location).attr('href', urlp);
				},
				complete: function(jqXHR, textStatus){
					$('#box-ds').hideLoading();
				}
		    });
    });
	
    $('#xls').click(function() {
    	
		    var urlp = pref_url+"/xuat-file-xls?trangThai="+$('#cmb-trang-thai').val();
			$.ajax({
		        url: urlp,
		        beforeSend: function(jqXHR, settings){
		        	$('#box-ds').showLoading();
				},
				success: function(response){
	    			$(location).attr('href', urlp);
				},
				complete: function(jqXHR, textStatus){
					$('#box-ds').hideLoading();
				}
		    });
    });
    
    $('#xlsx').click(function() {
    	
		    var urlp = pref_url+"/xuat-file-xlsx?trangThai="+$('#cmb-trang-thai').val();
			$.ajax({
		        url: urlp,
		        beforeSend: function(jqXHR, settings){
		        	$('#box-ds').showLoading();
				},
				success: function(response){
	    			$(location).attr('href', urlp);
				},
				complete: function(jqXHR, textStatus){
					$('#box-ds').hideLoading();
				}
		    });
    });
    $('#rtf').click(function() {
		    var urlp = pref_url+"/xuat-file-rtf?trangThai="+$('#cmb-trang-thai').val();
			$.ajax({
		        url: urlp,
		        beforeSend: function(jqXHR, settings){
		        	$('#box-ds').showLoading();
				},
				success: function(response){
	    			$(location).attr('href', urlp);
				},
				complete: function(jqXHR, textStatus){
					$('#box-ds').hideLoading();
				}
		    });
    });	
});

//function check(){
//	var tree = $("#tree").fancytree("getTree"),
//	selNodes = tree.getSelectedNodes();
//	var nodes = new Array();
//    selNodes.forEach(function(node) {
//    	if(node.key!=-1) nodes.push(node.key);
//    });
//	if(nodes.join()===''){
//		showError('Thông báo','Vui lòng chọn tổ!');
//		return false;
//	}
//	return true;
//}