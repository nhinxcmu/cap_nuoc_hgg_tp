var pref_url="/ttgd-tuyen";

$(document).ready(function(){
	$('#grid-ds-thanhtoan').jdGrid({
		columns:[
			{name:'maPhieuThanhToan',title:'Mã phiếu TT'},
			{name:'maKhachHang',title:'Mã KH'},
			{name:'maDongHo',title:'Mã đồng hồ'},
			{name:'tenKhachHang',title:'Tên KH'},
			{name:'diaChiSuDung',title:'Địa chỉ sử dụng'},
			{name:'diaChiThanhToan',title:'Địa chỉ thanh toán'},
			{name:'soDienThoai',title:'Điện thoại'},
			{name:'tienThanhToan',title:'Tiền TT',format:true,css:{'text-align':'right'}},
			{name:'tenNguoiCapNhat',title:'Người cập nhật'},
			{name:'ngayGioCapNhat',title:'Ngày cập nhật',type:'interval',css:{'text-align':'center'}},
			{name:'huyThanhToan',title:'Hủy',type:'check',css:{'text-align':'center'}},
			{name:'col2',title:'T.Tác',type:'control',content:function(obj){return obj.huyThanhToan===false?'<a href="#" class="row-del-1 text-danger" rid="'+obj.idPhieuThanhToan+'" title="Hủy thanh toán"><i class="fa fa-ban"></i></a>':''},css:{'text-align':'center','width':'80px'}}
		],
		decnum: 0
	});
	
	$('#page-ds-thanhtoan').jdPage({
		onPageChanged:function(p){
			layDanhSachDaThanhToan(p-1);
		}
	});
	
	$('#btn-search-thanhtoan').click(function(){
		layDanhSachDaThanhToan(0);
	});
	
	$('#txt-keyword-thanhtoan').keypress(function(e){
        if(e.which == 13){
            $('#btn-search-thanhtoan').click();
        }
    });
	
	$('#btn-clear-search-thanhtoan').click(function(){
		$('#txt-keyword-thanhtoan').val('').focus();
		layDanhSachDaThanhToan(0);
	});
	
	$('#modThanhToan').on('shown.bs.modal',function(e){
		layDanhSachDaThanhToan(0);
	});
	
	$('#btn-ok-1').click(function(){
		showConfirm('Xác nhận', 'Bạn chắc thanh toán giao dịch cho khu vực?', 'luu(false)');
	});
	
	$('#btn-ok-2').click(function(){
		showConfirm('Xác nhận', 'Bạn chắc thanh toán giao dịch cho TẤT CẢ CÁC KHU VỰC?', 'luu(true)');
	});
	
	laySoLuong();
});

function layDanhSachDaThanhToan(p){
	$.ajax({
		url:pref_url+'/lay-ds-da-thanh-toan',
		method:'get',
		dataType:'json',
		data:{thongTinCanTim:$.trim($('#txt-keyword-thanhtoan').val()),page:p},
		beforeSend:function(){
			$('#list-thanhtoan').showLoading();
		},
		success:function(data){
			if(data.resCode === undefined){
				$('#grid-ds-thanhtoan').data('jdgrid').fillData(data.content);
				$('#page-ds-thanhtoan').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
				
				$('.row-del-1').click(function(e){
					e.preventDefault();
					showConfirm('Xác nhận', 'Bạn chắc muốn hủy thanh toán?', 'huyThanhToan('+$(this).attr('rid')+')');
				});
			} else {
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lấy danh sách đã thanh toán không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#list-thanhtoan').hideLoading();
		}
	});
}

function huyThanhToan(id){
	$.ajax({
		url:pref_url+'/huy-thanh-toan',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds-thanhtoan').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Hủy thanh toán thành công');
				layDanhSachDaThanhToan(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Hủy thanh toán không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds-thanhtoan').hideLoading();
		}
	});
}

function luu(tatCa){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:{tatCa:tatCa},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Thanh toán giao dịch thành công');
				laySoLuong();
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Thanh toán giao dịch cho khu vực không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function laySoLuong(){
	$.ajax({
		url:pref_url+'/lay-so-luong',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data) {
			if(data.resCode === undefined) {
				$('#txt-tongso').val(numberWithCommas(data.tongHoaDon));
				$('#txt-dathanhtoan').val(numberWithCommas(data.tongDaThanhToan));
				$('#txt-chuathanhtoan').val(numberWithCommas(data.tongChuaThanhToan));
			} else {
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lấy số lượng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}
