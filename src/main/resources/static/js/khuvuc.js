var pref_url="/khu-vuc";

$(document).ready(function(){
	$('#grid-ds').jdGrid({
		columns:[
			{name:'tenKhuVuc',title:'Tên khu vực'},
			{name:'soDienThoai',title:'Số điện thoại'},
			{name:'tenThuNgan',title:'Tên thu ngân'},
			{name:'hanThanhToan',title:'Hạn thanh toán',type:'interval_date'},
			{name:'ngayBatDau',title:'Ngày BĐ',css:{'width':'100px','text-align':'center'}},
			{name:'ngayKetThuc',title:'Ngày KT',css:{'width':'100px','text-align':'center'}},
			{name:'ngayIn',title:'Ngày in',css:{'width':'100px','text-align':'center'}},
			{name:'suDungHddt',title:'Sử dụng HĐĐT',type:'check',css:{'text-align':'center'}},
			{name:'tenDonVi',title:'Tên đơn vị'},
			{name:'col2',title:'T.Tác',type:'control',content:function(obj){return '<a href="#" class="row-edit-1" rid="'+obj.idKhuVuc+'" title="Sửa"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;<a href="#" class="row-del-1 text-danger" rid="'+obj.idKhuVuc+'" title="Xóa"><i class="fa fa-trash-o"></i></a>'},css:{'text-align':'center','width':'70px'}}
		]
	});
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#btn-ok').click(function(){
		if(check()){
			luu();
		}
	});
	
	$('#btn-cancel').click(function(){
		huy();
	});
	
	$('#cmb-donvi-filter').change(function(){
		layDanhSach(0);
	});
	
	initForm();
	
	$('.input-mask').inputmask();
	
	$('.chon-ngay').datepicker({
		autoclose: true,
		todayHighlight:true,
		format:'dd/mm/yyyy',
		zIndexOffset:10000
    });
});

function check(){
	if($('#txt-tenKhuVuc').val()===''){
		showError('Thông báo','Vui lòng nhập tên khu vực!');
		return false;
	}
	var bd=$('#txt-ngayBatDau').val();
	if(bd===''||isNaN(bd)){
		showError('Thông báo','Vui lòng nhập ngày bắt đầu!');
		return false;
	}
	var kt=$('#txt-ngayKetThuc').val();
	if(kt===''||isNaN(kt)){
		showError('Thông báo','Vui lòng nhập ngày kết thúc!');
		return false;
	}
	var ngayin=$('#txt-ngayIn').val();
	if(ngayin===''||isNaN(ngayin)){
		showError('Thông báo','Vui lòng nhập ngày in!');
		return false;
	}
	if($('#cmb-donvi').val()==null){
		showError('Thông báo','Vui lòng chọn đơn vị!');
		return false;
	}
	return true;
}

function initForm(){
	$.ajax({
		url:pref_url+'/init-form',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-frm').showLoading();
			$('#box-ds').showLoading();
		},
		success:function(data){
			var donvi=$.map(data,function(obj){
				obj.id=obj.idDonVi;
				obj.text=obj.tenDonVi;
				return obj;
			});
			
			$('#cmb-donvi,#cmb-donvi-filter').select2({
				data: donvi
			});
			
			if($('#cmb-donvi-filter').val()!=null)
				layDanhSach(0);
		},
		error:function(){
			showError('Thông báo','Khởi tạo dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
			$('#box-ds').hideLoading();
		}
	});
}

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{page:p,idDonVi:$('#cmb-donvi-filter').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#grid-ds').data('jdgrid').fillData(data.content);
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				layChiTiet($(this).attr('rid'));
			});
			
			$('.row-del-1').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn xóa?', 'xoa('+$(this).attr('rid')+')');
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function luu(){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:new FormData($('#frm-1')[0]),
		processData: false,
        contentType: false,
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				huy();
				toastInfo('Lưu dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function huy(){
	$('#txt-idKhuVuc').val('');
	$('#cmb-donvi').val($("#cmb-donvi option:first").val()).trigger('change');
	$('#txt-tenKhuVuc').val('').focus();
	$('#txt-ngayBatDau').val('');
	$('#txt-ngayKetThuc').val('');
	$('#txt-ngayIn').val('');
	$('#chk-suDungHddt').prop('checked', true);
	$('#txt-soDienThoai').val('');
	$('#txt-tenThuNgan').val('');
	$('#txt-hanThanhToan').val('');
}

function layChiTiet(id){
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#txt-idKhuVuc').val(data.idKhuVuc);
			$('#cmb-donvi').val(data.idDonVi).trigger('change');
			$('#txt-tenKhuVuc').val(data.tenKhuVuc);
			$('#txt-ngayBatDau').val(data.ngayBatDau);
			$('#txt-ngayKetThuc').val(data.ngayKetThuc);
			$('#txt-ngayIn').val(data.ngayIn);
			$('#chk-suDungHddt').prop('checked', data.suDungHddt);
			$('#txt-soDienThoai').val(data.soDienThoai);
			$('#txt-tenThuNgan').val(data.tenThuNgan);
			$('#txt-hanThanhToan').datepicker('setDate', data.hanThanhToan==null?'':new Date(data.hanThanhToan));
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
			$('#box-ds').hideLoading();
		}
	});
}

function xoa(id){
	$.ajax({
		url:pref_url+'/xoa',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				toastInfo('Xóa dữ liệu thành công');
				layDanhSach(0);
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Xóa dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}