var prefURL = '/phieu-sua-seri';
var stringFormat = new Intl.NumberFormat('de-DE', {});
var now = new Date(Date.now());
var nowSring = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " + "23:59:59";
console.log(nowSring);
app.controller('PhieuSuaSeriController', function PhieuSuaSeriController($scope, $http) {
    $scope.searchForm = {
        page: 0,
        pageSize: 20,
        tuNgay: new Date(Date.parse('2021-01-01')),
        denNgay: new Date(nowSring),
        trangThai: 1,
        idKhuVuc: -1,
        idDuong: -1,
        idSoGhi: -1,
        searchString: "",
        pageKh: 0,
        pageSizeKh: 20,
        thongTinCanTim: ""
    };
    $scope.phieuSuaSeri = {}
    $scope.layDs = function (f) {
        $('#box-ds').showLoading();
        $http({
            url: prefURL + '/lay-ds', method: 'POST',
            data: JSON.stringify($scope.searchForm),
        }).then(function successCallBack(response) {
            $('#box-ds').hideLoading();
            $scope.phieuSuaSerisPageable = response.data;
            $(function () {
                try {
                    $('#page-ds').data('jdpage').setData({
                        'totalPage': $scope.phieuSuaSerisPageable.totalPages,
                        'currentPage': $scope.phieuSuaSerisPageable.number + 1,
                        'itemOnPage': $scope.phieuSuaSerisPageable.size,
                        'totalItem': $scope.phieuSuaSerisPageable.totalElements
                    });

                } catch (ex) {
                    console.log(ex);
                }
            });
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $('#box-ds').hideLoading();
            $.notify("Thất bại", "error");
        });
    }

    $('#page-ds').jdPage({
        onPageChanged: function (p) {
            $scope.searchForm.page = p - 1;
            $scope.layDs();
        }
    });
    $scope.setPhieuSuaSeri = function (phieuSuaSeri) {
        $scope.phieuSuaSeri = phieuSuaSeri != null ? phieuSuaSeri : {};
        $scope.phieuSuaSeri.ngayLap = new Date($scope.phieuSuaSeri.ngayLap);
    };
    $scope.luu = function () {
        if (window.confirm("Bạn muốn lưu thông tin phiếu sửa seri?")) {
            $("#box-form").showLoading();
            $http({
                url: prefURL + '/luu',
                method: 'POST',
                data: JSON.stringify($scope.phieuSuaSeri),
            }).then(function successCallBack(response) {
                $("#box-form").hideLoading();
                if (response.data.resCode > 0) {
                    $scope.layDs();
                    $.notify("Thành công", "success");
                } else
                    $.notify(response.data.resMessage, "error");
            }, function errorCallback() {
                $("#box-form").hideLoading();
                $.notify("Thất bại", "error");
            });
        }
    }

    $scope.layDsPhuong = function (f) {
        $http({
            url: prefURL + '/lay-ds-phuong', method: 'POST',
            data: JSON.stringify($scope.searchForm),
        }).then(function successCallBack(response) {
            $scope.phuongs = [{ idDuong: -1, tenDuong: "Tất cả đường" }];
            $scope.phuongs = $scope.phuongs.concat(response.data);
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.layDsTo = function (f) {
        $http({
            url: prefURL + '/lay-ds-to?idDuong=' + $scope.searchForm.idDuong,
            method: 'POST',
        }).then(function successCallBack(response) {
            $scope.tos = [{ idSoGhi: -1, tenSoGhi: "Tất cả tổ" }];
            $scope.tos = $scope.tos.concat(response.data);
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.hideModal = function (modal) {
        $(modal).modal('hide');
    }
    $scope.currencyFormat = function (number) {
        return stringFormat.format(number);
    }
    $scope.getTrangThai = function (trangThai) {
        switch (trangThai) {
            case 1: return "Khởi tạo";
            case 2: return "Đã gửi duyệt";
            case 3: return "Đã từ chối duyệt";
            case 4: return "Đã duyệt";
        }
    }
    $scope.layDsKhuVuc = function (f) {
        $http({
            url: prefURL + '/lay-ds-khu-vuc', method: 'GET',
        }).then(function successCallBack(response) {
            $scope.khuVucs = [{ idKhuVuc: -1, tenKhuVuc: "Tất cả khu vực" }];
            $scope.khuVucs = $scope.khuVucs.concat(response.data);
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.layDsPhuong = function (f) {
        $http({
            url: prefURL + '/lay-ds-phuong', method: 'POST',
            data: JSON.stringify($scope.searchForm),
        }).then(function successCallBack(response) {
            $scope.phuongs = [{ idDuong: -1, tenDuong: "Tất cả đường" }];
            $scope.phuongs = $scope.phuongs.concat(response.data);
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.layDsTo = function (f) {
        $http({
            url: prefURL + '/lay-ds-to?idDuong=' + $scope.searchForm.idDuong,
            method: 'POST',
        }).then(function successCallBack(response) {
            $scope.tos = [{ idSoGhi: -1, tenSoGhi: "Tất cả tổ" }];
            $scope.tos = $scope.tos.concat(response.data);
            $scope.searchForm.idSoGhi = -1;
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $('#kh-page-ds').jdPage({
        onPageChanged: function (p) {
            $scope.searchForm.pageKh = p - 1;
            $scope.layDsKhachHang();
        }
    });
    $scope.khachHangs = [];
    $scope.layDsKhachHang = function () {
        $('#box-ds-kh').showLoading();
        $http({
            url: '/khach-hang/lay-ds?idKhuVuc=' + $scope.searchForm.idKhuVuc + '&idDuong=' + $scope.searchForm.idDuong + '&idSoGhi=' + $scope.searchForm.idSoGhi + "&thongTinCanTim=" + $scope.searchForm.searchString + "&page=" + $scope.searchForm.pageKh + "&size=" + $scope.searchForm.pageSizeKh,
            method: 'get',

        }).then(function successCallBack(response) {
            $('#box-ds-kh').hideLoading();
            var data = response.data;
            $scope.khachHangs = data.content;
            console.log($scope.khachHangs);
            try {
                $('#kh-page-ds').data('jdpage').setData({
                    'totalPage': data.totalPages,
                    'currentPage': data.number + 1,
                    'itemOnPage': data.size,
                    'totalItem': data.totalElements
                });

            } catch (ex) {
                console.log(ex);
            }
        }, function errorCallback() {
            $('#box-ds').hideLoading();
            showError('Thông báo', 'Lấy danh sách không thành công, vui lòng thử lại sau!');
        });
    }
    $scope.chonThongTinKhachHang = function (idThongTinKhachHang) {
        var thongTinKhachHang = $scope.khachHangs.filter(n => n.idThongTinKhachHang == idThongTinKhachHang)[0];
        $scope.phieuSuaSeri.maKhachHang = thongTinKhachHang.maKhachHang;
        $scope.phieuSuaSeri.idDongHo = thongTinKhachHang.idDongHo;
        $scope.phieuSuaSeri.soSeri = thongTinKhachHang.soSeri;
        $scope.phieuSuaSeri.soSeriCu = thongTinKhachHang.soSeri;
        $scope.phieuSuaSeri.maKhachhang = thongTinKhachHang.maKhachHang;
        $scope.phieuSuaSeri.tenKhachHang = thongTinKhachHang.tenKhachHang;
        $scope.phieuSuaSeri.idKhachHang = thongTinKhachHang.idKhachHang;
        // $scope.phieuSuaSeri.idThongTinKhachHang=thongTinKhachHang.idThongTinKhachHang;
        // $scope.phieuSuaSeri.soSeri=thongTinKhachHang.soSeri;
        // $scope.phieuSuaSeri.soSeri=thongTinKhachHang.soSeri;
    }
    $scope.guiDuyet = function (phieuSuaSeri) {
        if (window.confirm("Bạn muốn gửi duyệt phiếu sửa số seri?")) {
            $("#box-form").showLoading();
            $http({
                url: prefURL + '/gui-duyet',
                method: 'POST',
                data: JSON.stringify(phieuSuaSeri),
            }).then(function successCallBack(response) {
                $("#box-form").hideLoading();
                if (response.data.resCode > 0) {
                    $scope.layDs();
                    $.notify("Thành công", "success");
                } else
                    $.notify(response.data.resMessage, "error");
            }, function errorCallback() {
                $("#box-form").hideLoading();
                $.notify("Thất bại", "error");
            });
        }
    }

    $scope.xoa = function (phieuSuaSeri) {
        if (window.confirm("Bạn muốn xóa duyệt phiếu sửa số seri?")) {
            $("#box-form").showLoading();
            $http({
                url: prefURL + '/xoa',
                method: 'POST',
                data: JSON.stringify(phieuSuaSeri),
            }).then(function successCallBack(response) {
                $("#box-form").hideLoading();
                if (response.data.resCode > 0) {
                    $scope.layDs();
                    $.notify("Thành công", "success");
                } else
                    $.notify(response.data.resMessage, "error");
            }, function errorCallback() {
                $("#box-form").hideLoading();
                $.notify("Thất bại", "error");
            });
        }
    }
    



function check() {
    if ($('#fileData').val() === '') {
        showError('Thông báo', 'Vui lòng chọn file Excel import!');
        return false;
    }
    return true;
}

$(document).ready(function () {
    $('#btn-ok').click(function () {
        if (check()) {
            luu();
        }
    });
});

$('#btn-file').click(function(){
    window.location.href = "help/ImportSeri.xlsx";
});
function luu() {
    $.ajax({
        url: '/phieu-sua-seri/xu-ly',
        method: 'post',
        dataType: 'json',
        data: new FormData($('#frm-1')[0]),
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        beforeSend: function () {
            $('#box-ds').showLoading();
        },
        success: function (data) {
            if (data.resCode > 0) {
                toastInfo('Import dữ liệu thành công');
                $scope.layDs();
            } else {
                showError('Thông báo', data.resMessage);
            }
        },
        error: function () {
            showError('Thông báo', 'Import dữ liệu không thành công, vui lòng thử lại sau!');
        },
        complete: function () {
            $('#box-ds').hideLoading();
        }
    });
}
});