var prefURL = '/phieu-bao-hong';
var stringFormat = new Intl.NumberFormat('de-DE', {});
var now = new Date(Date.now());
var nowSring = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " + "23:59:59";
console.log(nowSring);
app.controller('PhieuBaoHongController', function PhieuBaoHongController($scope, $http) {
    $scope.searchForm = {
        page: 0,
        pageSize: 20,
        tuNgay: new Date(Date.parse('2021-01-01')),
        denNgay: new Date(nowSring),
        trangThai: 1,
        idKhuVuc: -1,
        idDuong: -1,
        idSoGhi: -1,
        searchString: "",
        pageKh: 0,
        pageSizeKh: 20,
        thongTinCanTim: ""
    };
    $scope.phieuBaoHong = {}
    $scope.layDs = function (f) {
        $('#box-ds').showLoading();
        $http({
            url: prefURL + '/lay-ds', method: 'POST',
            data: JSON.stringify($scope.searchForm),
        }).then(function successCallBack(response) {
            $('#box-ds').hideLoading();
            $scope.phieuBaoHongsPageable = response.data;
            $(function () {
                try {
                    $('#page-ds').data('jdpage').setData({
                        'totalPage': $scope.phieuBaoHongsPageable.totalPages,
                        'currentPage': $scope.phieuBaoHongsPageable.number + 1,
                        'itemOnPage': $scope.phieuBaoHongsPageable.size,
                        'totalItem': $scope.phieuBaoHongsPageable.totalElements
                    });

                } catch (ex) {
                    console.log(ex);
                }
            });
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $('#box-ds').hideLoading();
            $.notify("Thất bại", "error");
        });
    }

    $('#page-ds').jdPage({
        onPageChanged: function (p) {
            $scope.searchForm.page = p - 1;
            $scope.layDs();
        }
    });
    $scope.setPhieuBaoHong = function (phieuBaoHong) {
        $scope.phieuBaoHong = phieuBaoHong != null ? phieuBaoHong : {};
        $scope.phieuBaoHong.ngayLap = new Date($scope.phieuBaoHong.ngayLap);
    };
    $scope.luu = function () {
        if (window.confirm("Bạn muốn lưu thông tin phiếu sửa seri?")) {
            $("#box-form").showLoading();
            $http({
                url: prefURL + '/luu?gui=0',
                method: 'POST',
                data: JSON.stringify($scope.phieuBaoHong),
            }).then(function successCallBack(response) {
                $("#box-form").hideLoading();
                if (response.data.resCode > 0) {
                    $scope.layDs();
                    $.notify("Thành công", "success");
                } else
                    $.notify(response.data.resMessage, "error");
            }, function errorCallback() {
                $("#box-form").hideLoading();
                $.notify("Thất bại", "error");
            });
        }
    }

    $scope.layDsPhuong = function (f) {
        $http({
            url: prefURL + '/lay-ds-phuong', method: 'POST',
            data: JSON.stringify($scope.searchForm),
        }).then(function successCallBack(response) {
            $scope.phuongs = [{ idDuong: -1, tenDuong: "Tất cả đường" }];
            $scope.phuongs = $scope.phuongs.concat(response.data);
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.layDsTo = function (f) {
        $http({
            url: prefURL + '/lay-ds-to?idDuong=' + $scope.searchForm.idDuong,
            method: 'POST',
        }).then(function successCallBack(response) {
            $scope.tos = [{ idSoGhi: -1, tenSoGhi: "Tất cả tổ" }];
            $scope.tos = $scope.tos.concat(response.data);
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.hideModal = function (modal) {
        $(modal).modal('hide');
    }
    $scope.currencyFormat = function (number) {
        return stringFormat.format(number);
    }
    $scope.getTrangThai = function (trangThai) {
        switch (trangThai) {
            case 1: return "Khởi tạo";
            case 2: return "Đã gửi duyệt";
            case 3: return "Đã từ chối duyệt";
            case 4: return "Đã duyệt";
        }
    }
    $scope.layDsKhuVuc = function (f) {
        $http({
            url: prefURL + '/lay-ds-khu-vuc', method: 'GET',
        }).then(function successCallBack(response) {
            $scope.khuVucs = [{ idKhuVuc: -1, tenKhuVuc: "Tất cả khu vực" }];
            $scope.khuVucs = $scope.khuVucs.concat(response.data);
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $('#kh-page-ds').jdPage({
        onPageChanged: function (p) {
            $scope.searchForm.pageKh = p - 1;
            $scope.layDsKhachHang();
        }
    });
    $scope.khachHangs = [];
    $scope.layDsKhachHang = function () {
        $('#box-ds-kh').showLoading();
        $http({
            url: '/khach-hang/lay-ds?idKhuVuc=' + $scope.searchForm.idKhuVuc + '&idDuong=' + $scope.searchForm.idDuong + '&idSoGhi=' + $scope.searchForm.idSoGhi + "&thongTinCanTim=" + $scope.searchForm.searchString + "&page=" + $scope.searchForm.pageKh + "&size=" + $scope.searchForm.pageSizeKh,
            method: 'get',

        }).then(function successCallBack(response) {
            $('#box-ds-kh').hideLoading();
            var data = response.data;
            $scope.khachHangs = data.content;
            console.log($scope.khachHangs);
            try {
                $('#kh-page-ds').data('jdpage').setData({
                    'totalPage': data.totalPages,
                    'currentPage': data.number + 1,
                    'itemOnPage': data.size,
                    'totalItem': data.totalElements
                });

            } catch (ex) {
                console.log(ex);
            }
        }, function errorCallback() {
            $('#box-ds').hideLoading();
            showError('Thông báo', 'Lấy danh sách không thành công, vui lòng thử lại sau!');
        });
    }
    $scope.chonThongTinKhachHang = function (idThongTinKhachHang) {
        var thongTinKhachHang = $scope.khachHangs.filter(n => n.idThongTinKhachHang == idThongTinKhachHang)[0];
        $scope.phieuBaoHong.maKhachHang = thongTinKhachHang.maKhachHang;
        $scope.phieuBaoHong.idDongHo = thongTinKhachHang.idDongHo;
        $scope.phieuBaoHong.maKhachhang = thongTinKhachHang.maKhachHang;
        $scope.phieuBaoHong.tenKhachHang = thongTinKhachHang.tenKhachHang;
        $scope.phieuBaoHong.idKhachHang = thongTinKhachHang.idKhachHang;

    }
    $scope.guiDuyet = function (phieuBaoHong) {
        if (window.confirm("Bạn muốn gửi duyệt phiếu sửa số seri?")) {
            $("#box-form").showLoading();
            $http({
                url: prefURL + '/gui-duyet',
                method: 'POST',
                data: JSON.stringify(phieuBaoHong),
            }).then(function successCallBack(response) {
                $("#box-form").hideLoading();
                if (response.data.resCode > 0) {
                    $scope.layDs();
                    $.notify("Thành công", "success");
                } else
                    $.notify(response.data.resMessage, "error");
            }, function errorCallback() {
                $("#box-form").hideLoading();
                $.notify("Thất bại", "error");
            });
        }
    }

    $scope.xoa = function (phieuBaoHong) {
        if (window.confirm("Bạn muốn xóa duyệt phiếu sửa số seri?")) {
            $("#box-form").showLoading();
            $http({
                url: prefURL + '/xoa',
                method: 'POST',
                data: JSON.stringify(phieuBaoHong),
            }).then(function successCallBack(response) {
                $("#box-form").hideLoading();
                if (response.data.resCode > 0) {
                    $scope.layDs();
                    $.notify("Thành công", "success");
                } else
                    $.notify(response.data.resMessage, "error");
            }, function errorCallback() {
                $("#box-form").hideLoading();
                $.notify("Thất bại", "error");
            });
        }
    }
    $scope.layDsTinhTrang = function (f) {
        $http({
            url: prefURL + '/lay-ds-tinh-trang',
            method: 'GET',
        }).then(function successCallBack(response) {
            $scope.tinhTrangs = response.data;
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.selectFile = function (e) {
        if (e.target.files.length > 0)
            getBase64(e.target.files[0]).then(base64Image => {
                $scope.phieuBaoHong.base64Image = base64Image;
            });
    };

    function getBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result.split(",")[1]);
            reader.onerror = error => reject(error);
        });
    }




    function check() {
        if ($('#fileData').val() === '') {
            showError('Thông báo', 'Vui lòng chọn file Excel import!');
            return false;
        }
        return true;
    }

    $(document).ready(function () {
        $('#btn-ok').click(function () {
            if (check()) {
                luu();
            }
        });
    });

    $('#btn-file').click(function () {
        window.location.href = "help/ImportSeri.xlsx";
    });
    function luu() {
        $.ajax({
            url: '/phieu-sua-seri/xu-ly',
            method: 'post',
            dataType: 'json',
            data: new FormData($('#frm-1')[0]),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $('#box-ds').showLoading();
            },
            success: function (data) {
                if (data.resCode > 0) {
                    toastInfo('Import dữ liệu thành công');
                    $scope.layDs();
                } else {
                    showError('Thông báo', data.resMessage);
                }
            },
            error: function () {
                showError('Thông báo', 'Import dữ liệu không thành công, vui lòng thử lại sau!');
            },
            complete: function () {
                $('#box-ds').hideLoading();
            }
        });
    }
});