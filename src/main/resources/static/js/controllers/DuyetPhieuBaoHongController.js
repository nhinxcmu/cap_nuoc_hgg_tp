var prefURL = '/duyet-phieu-bao-hong';
var stringFormat = new Intl.NumberFormat('de-DE', {});
var now = new Date(Date.now());
var nowSring = now.getFullYear()+"-"+(now.getMonth()+1)+"-"+now.getDate()+" " +"23:59:59";
console.log(nowSring);
app.controller('DuyetPhieuBaoHongController', function DuyetPhieuBaoHongController($scope, $http) {
    $scope.searchForm = {
        page: 0,
        pageSize: 20,
        tuNgay: new Date(Date.parse('2021-01-01')),
        denNgay: new Date(nowSring),
        trangThai: 2,
        idKhuVuc: -1,
        idDuong: -1,
        idSoGhi: -1,
        searchString: "",
        pageKh: 0,
        pageSizeKh: 20,
        thongTinCanTim: ""
    };
    $scope.phieuBaoHong = {}
    $scope.layDs = function (f) {
        $('#box-ds').showLoading();
        $http({
            url: prefURL + '/lay-ds', method: 'POST',
            data: JSON.stringify($scope.searchForm),
        }).then(function successCallBack(response) {
            $('#box-ds').hideLoading();
            $scope.phieuBaoHongsPageable = response.data;
            $(function () {
                try {
                    $('#page-ds').data('jdpage').setData({
                        'totalPage': $scope.phieuBaoHongsPageable.totalPages,
                        'currentPage': $scope.phieuBaoHongsPageable.number + 1,
                        'itemOnPage': $scope.phieuBaoHongsPageable.size,
                        'totalItem': $scope.phieuBaoHongsPageable.totalElements
                    });

                } catch (ex) {
                    console.log(ex);
                }
            });
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $('#box-ds').hideLoading();
            $.notify("Thất bại", "error");
        });
    }

    $('#page-ds').jdPage({
        onPageChanged: function (p) {
            $scope.searchForm.page = p - 1;
            $scope.layDs();
        }
    });
    $scope.setPhieuBaoHong = function (phieuBaoHong) {
        $scope.phieuBaoHong = phieuBaoHong != null ? phieuBaoHong : {};
        $scope.phieuBaoHong.ngayLap = new Date($scope.phieuBaoHong.ngayLap);
    };
    

    $scope.layDsPhuong = function (f) {
        $http({
            url: prefURL + '/lay-ds-phuong', method: 'POST',
            data: JSON.stringify($scope.searchForm),
        }).then(function successCallBack(response) {
            $scope.phuongs = [{ idDuong: -1, tenDuong: "Tất cả đường" }];
            $scope.phuongs = $scope.phuongs.concat(response.data);
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.layDsTo = function (f) {
        $http({
            url: prefURL + '/lay-ds-to?idDuong=' + $scope.searchForm.idDuong,
            method: 'POST',
        }).then(function successCallBack(response) {
            $scope.tos = [{ idSoGhi: -1, tenSoGhi: "Tất cả tổ" }];
            $scope.tos = $scope.tos.concat(response.data);
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.hideModal = function (modal) {
        $(modal).modal('hide');
    }
    $scope.currencyFormat = function (number) {
        return stringFormat.format(number);
    }
    $scope.getTrangThai = function (trangThai) {
        switch (trangThai) {
            case 1: return "Khởi tạo";
            case 2: return "Đã gửi duyệt";
            case 3: return "Đã duyệt";
        }
    }
    $scope.layDsKhuVuc = function (f) {
        $http({
            url: prefURL + '/lay-ds-khu-vuc', method: 'GET',
        }).then(function successCallBack(response) {
            $scope.khuVucs = [{ idKhuVuc: -1, tenKhuVuc: "Tất cả khu vực" }];
            $scope.khuVucs = $scope.khuVucs.concat(response.data);
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.layDsPhuong = function (f) {
        $http({
            url: prefURL + '/lay-ds-phuong', method: 'POST',
            data: JSON.stringify($scope.searchForm),
        }).then(function successCallBack(response) {
            $scope.phuongs = [{ idDuong: -1, tenDuong: "Tất cả đường" }];
            $scope.phuongs = $scope.phuongs.concat(response.data);
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.layDsTo = function (f) {
        $http({
            url: prefURL + '/lay-ds-to?idDuong=' + $scope.searchForm.idDuong,
            method: 'POST',
        }).then(function successCallBack(response) {
            $scope.tos = [{ idSoGhi: -1, tenSoGhi: "Tất cả tổ" }];
            $scope.tos = $scope.tos.concat(response.data);
            $scope.searchForm.idSoGhi = -1;
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.layDsTinhTrang = function (f) {
        $http({
            url: prefURL + '/lay-ds-tinh-trang',
            method: 'GET',
        }).then(function successCallBack(response) {
           $scope.tinhTrangs=response.data;
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $('#kh-page-ds').jdPage({
        onPageChanged: function (p) {
            $scope.searchForm.pageKh = p - 1;
            $scope.layDsKhachHang();
        }
    });
    $scope.duyet = function(phieuBaoHong){
        if (window.confirm("Bạn muốn duyệt phiếu báo hỏng?")) {
            $("#box-form").showLoading();
            $http({
                url: prefURL + '/duyet',
                method: 'POST',
                data: JSON.stringify(phieuBaoHong),
            }).then(function successCallBack(response) {
                $("#box-form").hideLoading();
                if (response.data.resCode > 0) {
                    $scope.layDs();
                    $.notify("Thành công", "success");
                } else
                    $.notify(response.data.resMessage, "error");
            }, function errorCallback() {
                $("#box-form").hideLoading();
                $.notify("Thất bại", "error");
            });
        }
    }
});