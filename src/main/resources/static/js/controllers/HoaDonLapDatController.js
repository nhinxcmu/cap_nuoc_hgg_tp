var prefURL = '/hoa-don-lap-dat';
var page = 0;
var stringFormat =new Intl.NumberFormat('de-DE', {});
app.controller('HoaDonLapDatController', function HoaDonLapDatController($scope, $http) {
    $scope.thueSuat = 10;
    $scope.searchForm = {
        searchString: "",
        page: 0,
        pageSize: 20,
        idDuong: -1,
        idSoGhi: -1,
        idTrangThaiHoaDon: -1,
    };
    $scope.tos = [{ idSoGhi: -1, tenSoGhi: "Tất cả tổ" }];
    $scope.layDs = function (f) {
        $('#box-ds').showLoading();
        $http({
            url: prefURL + '/lay-ds', method: 'POST',
            data: JSON.stringify($scope.searchForm),
        }).then(function successCallBack(response) {
            $('#box-ds').hideLoading();
            $scope.hoaDonsPageable = response.data;
            $(function () {
                try {
                    $('#page-ds').data('jdpage').setData({
                        'totalPage': $scope.hoaDonsPageable.totalPages,
                        'currentPage': $scope.hoaDonsPageable.number + 1,
                        'itemOnPage': $scope.hoaDonsPageable.size,
                        'totalItem': $scope.hoaDonsPageable.totalElements
                    });

                } catch (ex) {
                    console.log(ex);
                }
            });
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $('#box-ds').hideLoading();
            $.notify("Thất bại", "error");
        });
    }

    $('#page-ds').jdPage({
        onPageChanged: function (p) {
            $scope.searchForm.page = p - 1;
            $scope.layDs();
        }
    });
    $scope.setHoaDon = function (hoaDon) {
        $scope.hoaDon = hoaDon != null ? hoaDon : {};
    };
    $scope.luu = function () {
        if (window.confirm("Bạn muốn lưu thông tin hóa đơn?")) {
            $("#box-form").showLoading();
            $http({
                url: prefURL + '/luu',
                method: 'POST',
                data: JSON.stringify($scope.hoaDon),
            }).then(function successCallBack(response) {
                $("#box-form").hideLoading();
                if (response.data.resCode > 0) {
                    $scope.layDs();
                    $.notify("Thành công", "success");
                } else
                    $.notify(response.data.resMessage, "error");
            }, function errorCallback() {
                $("#box-form").hideLoading();
                $.notify("Thất bại", "error");
            });
        }

    }
    $scope.tienDichVuChange = function (hoaDon) {
        hoaDon.tienThue = hoaDon.tienDichVu * $scope.thueSuat / 100;
        hoaDon.tongTien = hoaDon.tienThue + hoaDon.tienDichVu;
    }
    $scope.layDsPhuong = function (f) {
        $http({
            url: prefURL + '/lay-ds-phuong', method: 'POST',
            data: JSON.stringify($scope.searchForm),
        }).then(function successCallBack(response) {
            $scope.phuongs = [{ idDuong: -1, tenDuong: "Tất cả đường" }];
            $scope.phuongs = $scope.phuongs.concat(response.data);
            // $scope.searchForm.idDuong = response.data[0].idDuong;
            // $scope.layDsTo();
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.layDsTo = function (f) {
        $http({
            url: prefURL + '/lay-ds-to?idDuong=' + $scope.searchForm.idDuong,
            method: 'POST',
        }).then(function successCallBack(response) {
            $scope.tos = [{ idSoGhi: -1, tenSoGhi: "Tất cả tổ" }];
            $scope.tos = $scope.tos.concat(response.data);
            $scope.searchForm.idSoGhi = -1;
            if (f != null)
                f(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    }
    $scope.phatHanhHoaDonDienTu = function (hoaDon) {
        if (window.confirm("Bạn muốn phát hành hóa đơn cho hợp đồng này?")) {
            $('#box-ds').showLoading();
            $http({
                url: '/hoa-don-lap-dat/phat-hanh-hddt',
                method: 'POST',
                data: JSON.stringify(hoaDon),
            }).then(function successCallBack(response) {
                $scope.layDs();
                if (response.data.resCode > 0) {
                    $.notify("Thành công", "success");
                } else
                    $.notify(response.data.resMessage, "error");

                $('#box-ds').hideLoading();
            }, function errorCallback() {
                $.notify("Thất bại", "error");
                $('#box-ds').hideLoading();
            });
        }
    }
    $scope.layDsHddt = function (hoaDon) {
        $('#HDDT .box').showLoading();
        $http({
            url: '/hoa-don-lap-dat/lay-ds-hddt',
            method: 'POST',
            data: JSON.stringify(hoaDon),
        }).then(function successCallBack(response) {
            $scope.hoaDon.hddts = response.data;
            $('#HDDT .box').hideLoading();
        }, function errorCallback() {
            $.notify("Thất bại", "error");
            $('#HDDT .box').hideLoading();
        });
    }

    $scope.layDsTrangThaiHoaDon = function () {
        $http({
            url: '/hoa-don-lap-dat/lay-ds-tt-hoa-don',
            method: 'GET',
        }).then(function successCallBack(response) {
            $scope.trangThaiHoaDons = [{ idTrangThaiHoaDon: -1, tenTrangThaiHoaDon: "Tất cả trạng thái" }];
            $scope.trangThaiHoaDons =  $scope.trangThaiHoaDons.concat(response.data);
        }, function errorCallback() {
            $.notify("Thất bại", "error");
        });
    };

    $scope.hideModal = function (modal) {
        $(modal).modal('hide');
    }
    $scope.ngungPhatHanh = function (hoaDonDienTu) {
        if (window.confirm("Bạn muốn hủy hóa đơn này?")) {
            $('#HDDT .box').showLoading();
            $http({
                url: '/hoa-don-lap-dat/ngung-phat-hanh-hddt',
                method: 'POST',
                data: JSON.stringify(hoaDonDienTu),
            }).then(function successCallBack(response) {
                
                $scope.layDs();
                if (response.data.resCode > 0) {
                    $.notify("Thành công", "success");
                    $scope.layDsHddt($scope.hoaDon);
                } else
                    $.notify("Thất bại", "error");
                $('#HDDT .box').hideLoading();
            }, function errorCallback() {
                $.notify("Thất bại", "error");
                $('#HDDT .box').hideLoading();
            });
        }
    }
    $scope.currencyFormat = function(number){
        return stringFormat.format(number);
    }

});