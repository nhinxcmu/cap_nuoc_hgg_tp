var pref_url="/chuyen-du-lieu";

$(document).ready(function(){
	$('#btn-ok-1').click(function(){
		showConfirm('Xác nhận', 'Bạn chắc chuyển tất cả khách hàng?', 'luu(true)');
	});
	
	$('#btn-ok-2').click(function(){
		showConfirm('Xác nhận', 'Bạn chắc chuyển khách hàng chưa chuyển?', 'luu(false)');
	});
	
	laySoLuong();
});



function luu(all){
	$.ajax({
		url:pref_url+'/luu',
		method:'post',
		dataType:'json',
		data:{chuyenTatCa:all},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				if(all)
					toastInfo('Chuyển tất cả khách hàng thành công');
				else
					toastInfo('Chuyển khách hàng chưa chuyển thành công');
				laySoLuong();
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			if(all)
				showError('Thông báo','Chuyển tất cả khách hàng không thành công, vui lòng thử lại sau!');
			else
				showError('Thông báo','Chuyển khách hàng chưa chuyển không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function laySoLuong(){
	$.ajax({
		url:pref_url+'/lay-so-luong',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#txt-tongso').val(numberWithCommas(data.tongKhachHang));
			$('#txt-dachuyen').val(numberWithCommas(data.tongDaChuyen));
			$('#txt-chuachuyen').val(numberWithCommas(data.tongChuaChuyen));
		},
		error:function(){
			showError('Thông báo','Lấy số lượng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}
