var pref_url="/ket-chuyen-du-lieu";

$(document).ready(function(){
	$('#grid-ds').jdGrid({
		columns:[
			{name:'tenThang',title:'Tháng làm việc',css:{'text-align':'center'}},
			{name:'tenTrangThaiThang',title:'Trạng thái'},
			{name:'col2',title:'T.Tác',type:'control',content:function(obj){return obj.idTrangThaiThang==1?'<a href="#" class="row-ketchuyen" rid="'+obj.idThang+'" title="Kết chuyển"><i class="fa fa-check-circle"></i></a>':'<a href="#" class="row-khoiphuc" rid="'+obj.idThang+'" title="Khôi phục"><i class="fa fa-ban"></i></a>'},css:{'text-align':'center','width':'70px'}}
		],
		height:'397px',
		decnum: 0
	});
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#btn-ketchuyen').click(function(){
		ketChuyenTatCa();
	});
	
	layDanhSach();
});

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{id:$('#cmb-nhanvien').val(),page:p},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#grid-ds').data('jdgrid').fillData(data.content);
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-ketchuyen').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn kết chuyển?', 'ketChuyen('+$(this).attr('rid')+')');
			});
			
			$('.row-khoiphuc').click(function(e){
				e.preventDefault();
				showConfirm('Xác nhận', 'Bạn chắc muốn khôi phục?', 'khoiPhuc('+$(this).attr('rid')+')');
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function ketChuyen(idThang){
	$.ajax({
		url:pref_url+'/ket-chuyen',
		method:'post',
		dataType:'json',
		data:{idThang:idThang},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layDanhSach(0);
				$('#span-thang').text(idThang.substring(4) + "/" + idThang.substring(0,4));
				toastInfo('Kết chuyển thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Kết chuyển không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function ketChuyenTatCa(){
	$.ajax({
		url:pref_url+'/ket-chuyen-tat-ca',
		method:'post',
		dataType:'json',
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layDanhSach(0);
				toastInfo('Kết chuyển thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Kết chuyển không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function khoiPhuc(idThang){
	$.ajax({
		url:pref_url+'/khoi-phuc',
		method:'post',
		dataType:'json',
		data:{idThang:idThang},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layDanhSach(0);
				toastInfo('Khôi phục thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Khôi phục không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}
