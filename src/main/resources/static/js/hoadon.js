var pref_url="/hoa-don";
var index;
var ds_full=[];

$(document).ready(function(){
	$('#grid-ds').jdGrid({
		columns:[
			{name:'thuTu',title:'Thứ tự'},
			{name:'maKhachHang',title:'Mã KH'},
			{name:'tenKhachHang',title:'Tên KH'},
			{name:'diaChiSuDung',title:'Địa chỉ'},
			{name:'chiSoCu',title:'CS cũ',format:true,css:{'text-align':'right'}},
			{name:'chiSoMoi',title:'CS mới',format:true,css:{'text-align':'right'}},
			{name:'tieuThu',title:'Tiêu thụ',format:true,css:{'text-align':'right'}},
			{name:'tienNuoc',title:'Tiền nước',format:true,css:{'text-align':'right'}},
			{name:'thue',title:'Thuế GTGT',format:true,css:{'text-align':'right'}},
			{name:'phiBvmt',title:'Phí BVMT',format:true,css:{'text-align':'right'}},
			{name:'phiBvr',title:'Phí BVR',format:true,css:{'text-align':'right'}},
			{name:'tongTien',title:'Tổng tiền',format:true,css:{'text-align':'right'}},
			{name:'tenTrangThaiChiSo',title:'T.thái CS'},
			{name:'tenTrangThaiHoaDon',title:'T.thái HĐ'},
			{name:'ngayThanhToan',title:'Ngày TT',type:'interval_color',css:{'text-align':'center'}},
			{name:'col2',title:'Đã in',type:'control',content:function(obj){return '<a data-toggle="modal" data-target="#modLichSu" href="#" rid="'+obj.idHoaDon+'" title="Số lần in">'+obj.soLanIn+'</a>'},css:{'text-align':'center','width':'50px'}},
			{name:'col2',title:'T.Tác',type:'control',content:function(obj){return ((obj.hoaDonCuoi==1&&obj.hoaDonDau==0)?'<a href="#" class="row-delete-1" rid="'+obj.idHoaDon+'" title="Hủy"><i class="fa fa-trash-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;':'')+'<a href="#" class="row-del-1 text-danger" rid="'+obj.idHoaDon+'" title="Đổi trạng thái in"><i class="fa fa-exchange"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="modal" data-target="#modForm" href="#" class="row-edit-1" rid="'+obj.idHoaDon+'" title="Sửa"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="modal" data-target="#modPrint" href="#" class="row-print-1" rid="'+obj.idThongTinKhachHang+'" title="In hóa đơn nước"><i class="fa fa-print"></i></a>'},css:{'text-align':'center','width':'100px'}}
		],
		height:'497px',
		decnum: 0
	});
	
	$('#grid-lichsu').jdGrid({
		columns:[
			{name:'tenNguoiDung',title:'Tên đăng nhập'},
			{name:'hoTen',title:'Họ tên'},
			{name:'ngayGioIn',title:'Ngày in',type:'interval',css:{'text-align':'center'}}
		],
		height:'450px',
		decnum: 0
	});
	
	$('#popover-content').jdGrid({
		columns:[
			{name:'tieuThu',title:'Tiêu thụ',format:true,css:{'text-align':'right'}},
			{name:'donGia',title:'Đơn giá',format:true,css:{'text-align':'right'}},
			{name:'thanhTien',title:'Tiền nước',format:true,css:{'text-align':'right'}},
			{name:'phiBvmt',title:'Phí BVMT',format:true,css:{'text-align':'right'}},
			{name:'phiBvr',title:'Phí BVR',format:true,css:{'text-align':'right'}}
		],
		height:'120px',
		decnum: 1
	});
	
	$('#modLichSu').on('shown.bs.modal',function(e){
		layLichSu($(e.relatedTarget).attr('rid'));
	});
	
	$('#modForm').on('shown.bs.modal',function(e){
		layChiTiet($(e.relatedTarget).attr('rid'));
	});
	
	$('#modPrint').on('shown.bs.modal',function(e){
		var urlp = pref_url+'/xem-bao-cao?id='+$(e.relatedTarget).attr('rid');
		$.ajax({
	        url: urlp,
	        beforeSend: function(jqXHR, settings){
	        	$('#box-modPrint').showLoading();
			},
	        success: function(response){
				PDFObject.embed(response, "#pdfRenderer");
			},
			complete: function(jqXHR, textStatus){
				$('#box-modPrint').hideLoading();
			}
	    });
	});
	
	$('html').click(function(e) {
		$("[data-toggle=popover]").popover('hide');
	});
	
	$("[data-toggle=popover]").popover({
	    html: true,
	    trigger: 'manual',
		content: function() {
			return $('#popover-content').html();
	    }
	}).click(function(e) {
	    $(this).popover('toggle');
	    e.stopPropagation();
	});
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#btn-khongxai').click(function(){
		if(check()){
			luuKhongXai();
		}
	});
	
	$('#btn-chuaghi').click(function(){
		if(check()){
			luuChuaGhiChiSo();
		}
	});
	
	$('#luuChiSoCu').click(function(){
		if(check()){
			if($('#txt-chisocu').val()===''){
				showError('Thông báo','Vui lòng nhập chỉ số cũ!');
				$('#txt-chisocu').focus();
				return false;
			}
			else if($('#txt-chisomoi').val()!=='' && Number($('#txt-chisomoi').val()) > 0 && Number($('#txt-chisomoi').val()) < Number($('#txt-chisocu').val())){
				showError('Thông báo','Chỉ số cũ không được lớn hơn chỉ số mới!');
				$('#txt-chisomoi').focus();
				return false;
			}
			else
				luuChiSoCu();
		}
	});
	
	$('#luuChiSoMoi').click(function(){
		if(check()){
			if($('#txt-chisomoi').val()===''){
				showError('Thông báo','Vui lòng nhập chỉ số mới!');
				$('#txt-chisomoi').focus();
				return false;
			}
			else if($('#txt-chisocu').val()!=='' && Number($('#txt-chisomoi').val()) > 0 && Number($('#txt-chisomoi').val()) < Number($('#txt-chisocu').val())){
				showError('Thông báo','Chỉ số mới không được nhỏ hơn chỉ số cũ!');
				$('#txt-chisomoi').focus();
				return false;
			}
			else {
				var delta = Number($('#txt-chisomoi').val()) - Number($('#txt-chisocu').val()) - Number($('#txt-mucsudung').val());
				if(delta > 0)
					showConfirm('Xác nhận', 'Mức sử dụng vượt '+delta+'m3, bạn có muốn tiếp tục?', 'luuChiSoMoi()');
				else
					luuChiSoMoi();
			}
		}
	});
	
	$('#khoanTieuThu').click(function(){
		if(check()){
			if($('#txt-khoantieuthu').val()===''){
				showError('Thông báo','Vui lòng nhập tiêu thụ!');
				$('#txt-khoantieuthu').focus();
				return false;
			}
			else {
				var delta = Number($('#txt-tieuthu').val()) - Number($('#txt-mucsudung').val());
				if(delta > 0)
					showConfirm('Xác nhận', 'Mức sử dụng vượt '+delta+'m3, bạn có muốn tiếp tục?', 'khoanTieuThu()');
				else
					khoanTieuThu();
			}
		}
	});
	
//	$('#btn-cancel').click(function(){
//		huy();
//	});
	
	$('#cmb-khuvuc-filter').change(function(){
		layDsPhuong();
	});
	
	$('#cmb-phuong-filter').change(function(){
		layDsTo();
	});
	
	$('#cmb-to-filter, #cmb-ttchiso-filter, #cmb-tthoadon-filter, #cmb-ttthanhtoan-filter').change(function(){
		layDanhSach(0);
	});
	
	$('#btn-search').click(function(){
		layDanhSach(0);
	});
	
	$('#txt-keyword').keypress(function(e){
        if(e.which == 13){
            $('#btn-search').click();
        }
    });
	
	$('#btn-clear-search').click(function(){
		$('#txt-keyword').val('').focus();
		layDanhSach(0);
	});
	
	$('#txt-chisocu, #txt-chisomoi').change(function(){
		$('#txt-tieuthu').val(Number($('#txt-chisomoi').val()) - Number($('#txt-chisocu').val()));
	});
	
	$('#btn-themhd').click(function(){
		showConfirm('Xác nhận', 'Bạn chắc muốn thêm hóa đơn mới?', 'themHoaDon('+$(this).attr('rid')+')');
	});
	
	$('#cmb-doi-tuong-cup-nuoc').select2({});
	
	layDsKhuVuc();
	layDsTTChiSo();
	layDsTTHoaDon();
	layDsDoiTuongCupNuoc();
	
	$("#modForm").on("hidden.bs.modal", function () {
		$('#cmb-doi-tuong-cup-nuoc').val(0).trigger("change");
		$('#cmb-doi-tuong-cup-nuoc').select2({disabled: false, width: '100%'});
	});
});

function check(){
	if($('#txt-idhoadon').val()===''){
		showError('Thông báo','Vui lòng chọn hóa đơn!');
		return false;
	}
	return true;
}

function layDanhSach(p){
	if(p==0) layDsFull();
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{daThanhToan:$('#cmb-ttthanhtoan-filter').val(),idTrangThaiChiSo:$('#cmb-ttchiso-filter').val(),idTrangThaiHoaDon:$('#cmb-tthoadon-filter').val(),idKhuVuc:$('#cmb-khuvuc-filter').val(),idDuong:$('#cmb-phuong-filter').val(),idSoGhi:$('#cmb-to-filter').val(),thongTinCanTim:$.trim($('#txt-keyword').val()),page:p},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#grid-ds').data('jdgrid').fillData(data.content);
			$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
			
			$('.row-edit-1').click(function(e){
				e.preventDefault();
				// layChiTiet($(this).attr('rid'));
				index = $('.row-edit-1').index($(this));
			});
			
			$('.row-delete-1').click(function(e){
				showConfirm('Xác nhận', 'Bạn chắc muốn hủy hóa đơn?', 'huyHoaDon('+$(this).attr('rid')+')');
				e.preventDefault();
			});
			
			$('.row-del-1').click(function(e){
				showConfirm('Xác nhận', 'Bạn chắc muốn đổi trạng thái in?', 'luuDaIn('+$(this).attr('rid')+')');
				e.preventDefault();
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDSGiaNuoc(id){
	$.ajax({
		url:pref_url+'/lay-ds-gia-nuoc',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#popover-content').data('jdgrid').fillData(data);
		},
		error:function(){
			showError('Thông báo','Lấy danh sách giá nước không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function huy(){
	$('#frm-1')[0].reset();
	$('#txt-idhoadon').val('');
	$("#div-lydo").css({"display":"none"});
	$('#btn-ok').attr('disabled',false);
	$('#btn-ok').html('<i class="fa fa-check-circle"></i> Phát hành');
	$('#btn-ok').off('click');
	$('#btn-cupnuoc').html('<i class="fa fa-ban"></i> Cắt nước');
}

function layChiTiet(id){
	$('#btn-prev').off('click').on('click', function () {
		layKhachHangTruoc(id, true);
	});
	
	$('#btn-next').off('click').on('click', function () {
		layKhachHangTruoc(id, false);
	});
	
	layDSGiaNuoc(id);
	
	$.ajax({
		url:pref_url+'/lay-ct',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#grid-ds').data('jdgrid').setCellValue(index,'chiSoCu',data.chiSoCu);
			$('#grid-ds').data('jdgrid').setCellValue(index,'chiSoMoi',data.chiSoMoi);
			$('#grid-ds').data('jdgrid').setCellValue(index,'tieuThu',data.tieuThu);
			$('#grid-ds').data('jdgrid').setCellValue(index,'tienNuoc',data.tienNuoc);
			$('#grid-ds').data('jdgrid').setCellValue(index,'thue',data.thue);
			$('#grid-ds').data('jdgrid').setCellValue(index,'phiBvmt',data.phiBvmt);
			$('#grid-ds').data('jdgrid').setCellValue(index,'phiBvr',data.phiBvr);
			$('#grid-ds').data('jdgrid').setCellValue(index,'tongTien',data.tongTien);
			$('#grid-ds').data('jdgrid').setCellValue(index,'tenTrangThaiChiSo',data.tenTrangThaiChiSo);
			$('#grid-ds').data('jdgrid').setCellValue(index,'tenTrangThaiHoaDon',data.tenTrangThaiHoaDon);
			
			if(data.hoaDonCuoi == 1) {
				$('#btn-themhd').attr('disabled',false);
				$('#btn-themhd').attr('rid',id);
			} else {
				$('#btn-themhd').attr('disabled',true);
				$('#btn-themhd').attr('rid',0);
			}
			
			if(data.idTrangThaiHoaDon == 6) {
				$('#btn-print').attr('disabled',false);
				$('#btn-print').attr('rid',data.idThongTinKhachHang);
			} else {
				$('#btn-print').attr('disabled',true);
				$('#btn-print').attr('rid',0);
			}
			
			$('#txt-idhoadon').val(data.idHoaDon);
			$('#txt-mucsudung').val(data.mucSuDung);
			$('#txt-chisocu').val(data.chiSoCu);
			$('#txt-chisomoi').val(data.chiSoMoi);
			$('#txt-tieuthu').val(data.tieuThu);
			$('#txt-ghichu').val(data.ghiChu);
			$('#txt-tiennuoc').val(numberWithCommas(data.tienNuoc));
			$('#txt-thue').val(numberWithCommas(data.thue));
			$('#txt-phibvmt').val(numberWithCommas(data.phiBvmt));
			$('#txt-phibvr').val(numberWithCommas(data.phiBvr));
			$('#txt-tongTien').val(numberWithCommas(data.tongTien));
			$('#txt-tentrangthaichiso').val(data.tenTrangThaiChiSo);
			$('#txt-tentrangthaihoadon').val(data.tenTrangThaiHoaDon);
			$('#txt-makhachhang').val(data.maKhachHang);
			$('#txt-thutu').val(data.thuTu);
			$('#txt-tenkhachhang').val(data.tenKhachHang);
			$('#txt-diachisudung').val(data.diaChiSuDung);
			$('#txt-tendoituong').val(data.tenDoiTuong);
			$('#chk-thupbvmt').prop('checked', data.thuPbvmt);
			$("#div-lydo").css({"display":"none"});
			
			if(data.idTrangThaiChiSo == 7) {
				$("#div-lydo").css({"display":"block"});
				$('#txt-lydo').attr('readonly',true);
				$('#txt-lydo').val(data.lyDoCupNuoc);
				$('#cmb-doi-tuong-cup-nuoc').select2({disabled: true});
				$('#cmb-doi-tuong-cup-nuoc').val(data.idDoiTuongCupNuoc);
				$('#cmb-doi-tuong-cup-nuoc').trigger('change');
				// $('#cupNuoc').attr('disabled',true);
				$('#huyCupNuoc').attr('disabled',true);
				$('#btn-cupnuoc').html('<i class="fa fa-ban"></i> Mở nước');
				$('#btn-cupnuoc').off('click').on('click', function () {
					if(check()){
						luuMoNuoc();
					}
				});
				
				$('#btn-khongxai').attr('disabled',true);
				$('#btn-chuaghi').attr('disabled',true);
				$('#btn-ok').attr('disabled',true);
				$('#luuChiSoCu').attr('disabled',true);
				$('#luuChiSoMoi').attr('disabled',true);
				$('#khoanTieuThu').attr('disabled',true);
			} else if(data.idTrangThaiHoaDon == 6) {
				$('#btn-ok').attr('disabled',false);
				$('#btn-ok').html('<i class="fa fa-check-circle"></i> Ngưng phát hành');
				$('#btn-ok').off('click').on('click', function () {
					if(check()){
						ngungPhatHanh();
					}
				});
				
				$('#btn-khongxai').attr('disabled',true);
				$('#btn-chuaghi').attr('disabled',true);

				// $('#btn-cupnuoc').attr('disabled',true);
				$('#btn-cupnuoc').off('click').on('click', function () {
					$("#div-lydo").css({"display":"block"});
//					$("#txt-lydo").focus();
				});
				$('#cupNuoc').off('click').on('click', function () {
					if(check()){
//						if($('#txt-lydo').val()===''){
//							showError('Thông báo','Vui lòng nhập lý do cúp nước!');
//							$('#txt-lydo').focus();
//							return false;
//						}
						if($('#cmb-doi-tuong-cup-nuoc').val()==0){
							showError('Thông báo','Vui lòng chọn đối tượng cắt nước!');
							$('#cmb-doi-tuong-cup-nuoc').focus();
							return false;
						}
						else
							luuCupNuoc();
					}
				});

				$('#luuChiSoCu').attr('disabled',true);
				$('#luuChiSoMoi').attr('disabled',true);
				$('#khoanTieuThu').attr('disabled',true);
			} else if(data.idTrangThaiHoaDon == 5 && data.idTrangThaiChiSo != 3 && data.idTrangThaiChiSo != 7 && data.idTrangThaiChiSo != 10 && Number($('#txt-tiennuoc').val().replace(/\./g,'')) > 0) {
				$('#btn-ok').attr('disabled',false);
				$('#btn-ok').html('<i class="fa fa-check-circle"></i> Phát hành');
				$('#btn-ok').off('click').on('click', function () {
					if(check()){
						phatHanh();
					}
				});
				
				$("#div-lydo").css({"display":"none"});
				$('#txt-lydo').attr('readonly',false);
				$('#txt-lydo').val('');
				$('#cupNuoc').attr('disabled',false);
				$('#huyCupNuoc').attr('disabled',false);
				$('#btn-cupnuoc').html('<i class="fa fa-ban"></i> Cắt nước');
				$('#btn-cupnuoc').off('click').on('click', function () {
					$("#div-lydo").css({"display":"block"});
//					$("#txt-lydo").focus();
				});
				
				$('#huyCupNuoc').off('click').on('click', function () {
					$("#div-lydo").css({"display":"none"});
				});
				
				$('#cupNuoc').off('click').on('click', function () {
					if(check()){
//						if($('#txt-lydo').val()===''){
//							showError('Thông báo','Vui lòng nhập lý do cúp nước!');
//							$('#txt-lydo').focus();
//							return false;
//						}
						if($('#cmb-doi-tuong-cup-nuoc').val()==0){
							showError('Thông báo','Vui lòng chọn đối tượng cắt nước!');
							$('#cmb-doi-tuong-cup-nuoc').focus();
							return false;
						}
						else
							luuCupNuoc();
					}
				});
				
				$('#btn-khongxai').attr('disabled',false);
				$('#btn-chuaghi').attr('disabled',false);
				$('#btn-cupnuoc').attr('disabled',false);
				$('#luuChiSoCu').attr('disabled',false);
				$('#luuChiSoMoi').attr('disabled',false);
				$('#khoanTieuThu').attr('disabled',false);
			} else {
				$('#btn-ok').attr('disabled',true);
				$('#btn-ok').html('<i class="fa fa-check-circle"></i> Không phát hành');
				$('#btn-ok').off('click');
				
				$("#div-lydo").css({"display":"none"});
				$('#txt-lydo').attr('readonly',false);
				$('#txt-lydo').val('');
				$('#cupNuoc').attr('disabled',false);
				$('#huyCupNuoc').attr('disabled',false);
				$('#btn-cupnuoc').html('<i class="fa fa-ban"></i> Cắt nước');
				$('#btn-cupnuoc').off('click').on('click', function () {
					$("#div-lydo").css({"display":"block"});
//					$("#txt-lydo").focus();
					$('#cmb-doi-tuong-cup-nuoc').select2({disabled: false});
				});
				
				$('#huyCupNuoc').off('click').on('click', function () {
					$("#div-lydo").css({"display":"none"});
				});
				
				$('#cupNuoc').off('click').on('click', function () {
					if(check()){
//						if($('#txt-lydo').val()===''){
//							showError('Thông báo','Vui lòng nhập lý do cúp nước!');
//							$('#txt-lydo').focus();
//							return false;
//						}
						if($('#cmb-doi-tuong-cup-nuoc').val()==0){
							showError('Thông báo','Vui lòng chọn đối tượng cắt nước!');
							$('#cmb-doi-tuong-cup-nuoc').focus();
							return false;
						}
						else
							luuCupNuoc();
					}
				});
				
				$('#btn-khongxai').attr('disabled',false);
				$('#btn-chuaghi').attr('disabled',false);
				$('#luuChiSoCu').attr('disabled',false);
				$('#luuChiSoMoi').attr('disabled',false);
				$('#khoanTieuThu').attr('disabled',false);
			}
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layLichSu(id){
	$.ajax({
		url:pref_url+'/lay-lich-su',
		method:'get',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			$('#grid-lichsu').data('jdgrid').fillData(data);
		},
		error:function(){
			showError('Thông báo','Lấy dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layDsDoiTuongCupNuoc(){
	$.ajax({
		url:pref_url+'/lay-ds-doi-tuong-cup-nuoc',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-doi-tuong-cup-nuoc').select2({ width: '100%' }).html('<option value="0">-- Chọn đối tượng cắt nước --</option>');
			$.each(data, function(i, item) {
			    $('#cmb-doi-tuong-cup-nuoc').append($('<option>', { 
			        value: item.idDoiTuongCupNuoc,
			        text : item.tenDoiTuongCupNuoc 
			    }));
			});
				$('#cmb-doi-tuong-cup-nuoc').select2({ width: '100%' }).trigger('change');
		},
		error:function(){
			showError('Thông báo','Lấy danh sách đối tượng cắt nước không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layDsKhuVuc(){
	$.ajax({
		url:pref_url+'/lay-ds-khu-vuc',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			var khuvuc=$.map(data,function(obj){
				obj.id=obj.idKhuVuc;
				obj.text=obj.tenKhuVuc;
				return obj;
			});
			
			$('#cmb-khuvuc-filter').select2({
				data: khuvuc
			});
			
			layKhuVucHienTai();
		},
		error:function(){
			showError('Thông báo','Lấy danh sách khu vực không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layDsPhuong(){
	$.ajax({
		url:pref_url+'/lay-ds-phuong',
		method:'get',
		dataType:'json',
		data:{id:$('#cmb-khuvuc-filter').val()},
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-phuong-filter').html('<option value="0">-- Tất cả các phường --</option>');
			
			var phuong=$.map(data,function(obj){
				obj.id=obj.idDuong;
				obj.text=obj.tenDuong;
				return obj;
			});
			
			$('#cmb-phuong-filter').select2({
				data: phuong
			});
			
			layDsTo();
		},
		error:function(){
			showError('Thông báo','Lấy danh sách phường không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layDsTo(){
	$.ajax({
		url:pref_url+'/lay-ds-to',
		method:'get',
		dataType:'json',
		data:{id:$('#cmb-phuong-filter').val()},
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-to-filter').html('<option value="0">-- Tất cả các tổ --</option>');
			
			var to=$.map(data,function(obj){
				obj.id=obj.idSoGhi;
				obj.text=obj.tenSoGhi;
				return obj;
			});
			
			$('#cmb-to-filter').select2({
				data: to
			});
			
			layDanhSach(0);
		},
		error:function(){
			showError('Thông báo','Lấy danh sách tổ không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function luuMoNuoc(){
	$.ajax({
		url:pref_url+'/luu-mo-nuoc',
		method:'post',
		dataType:'json',
		data:{id:$('#txt-idhoadon').val()},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layChiTiet($('#txt-idhoadon').val());
				toastInfo('Mở nước thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Mở nước không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function luuCupNuoc(){
	$.ajax({
		url:pref_url+'/luu-cup-nuoc',
		method:'post',
		dataType:'json',
		data:{id:$('#txt-idhoadon').val(),lyDoCup:$('#txt-lydo').val(),idDoiTuong:$('#cmb-doi-tuong-cup-nuoc').val()},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layChiTiet($('#txt-idhoadon').val());
				toastInfo('Cắt nước thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Cắt nước không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function luuChiSoCu(){
	$.ajax({
		url:pref_url+'/luu-chi-so-cu',
		method:'post',
		dataType:'json',
		data:{id:$('#txt-idhoadon').val(),chiSoCu:$('#txt-chisocu').val()},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layChiTiet($('#txt-idhoadon').val());
				toastInfo('Lưu chỉ số cũ thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu chỉ số cũ không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function luuChiSoMoi(){
	$.ajax({
		url:pref_url+'/luu-chi-so-moi',
		method:'post',
		dataType:'json',
		data:{id:$('#txt-idhoadon').val(),chiSoMoi:$('#txt-chisomoi').val()},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layChiTiet($('#txt-idhoadon').val());
				toastInfo('Lưu chỉ số mới thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu chỉ số mới không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function khoanTieuThu(){
	$.ajax({
		url:pref_url+'/khoan-tieu-thu',
		method:'post',
		dataType:'json',
		data:{id:$('#txt-idhoadon').val(),tieuThu:$('#txt-tieuthu').val()},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layChiTiet($('#txt-idhoadon').val());
				toastInfo('Khoán tiêu thụ thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Khoán tiêu thụ không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function luuKhongXai(){
	$.ajax({
		url:pref_url+'/luu-khong-xai',
		method:'post',
		dataType:'json',
		data:{id:$('#txt-idhoadon').val()},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layChiTiet($('#txt-idhoadon').val());
				toastInfo('Lưu không sử dụng thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu không sử dụng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function luuChuaGhiChiSo(){
	$.ajax({
		url:pref_url+'/luu-chua-ghi-chi-so',
		method:'post',
		dataType:'json',
		data:{id:$('#txt-idhoadon').val()},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layChiTiet($('#txt-idhoadon').val());
				toastInfo('Lưu chưa ghi chỉ số thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lưu chưa ghi chỉ số không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function phatHanh(){
	$.ajax({
		url:pref_url+'/phat-hanh',
		method:'post',
		dataType:'json',
		data:{id:$('#txt-idhoadon').val()},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layChiTiet($('#txt-idhoadon').val());
				toastInfo('Phát hành hóa đơn thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Phát hành hóa đơn không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function ngungPhatHanh(){
	$.ajax({
		url:pref_url+'/ngung-phat-hanh',
		method:'post',
		dataType:'json',
		data:{id:$('#txt-idhoadon').val()},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layChiTiet($('#txt-idhoadon').val());
				toastInfo('Ngưng phát hành hóa đơn thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Ngưng phát hành hóa đơn không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layDsTTChiSo(){
	$.ajax({
		url:pref_url+'/lay-ds-tt-chi-so',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			var ttchiso=$.map(data,function(obj){
				obj.id=obj.idTrangThaiChiSo;
				obj.text=obj.tenTrangThaiChiSo;
				return obj;
			});
			
			$('#cmb-ttchiso-filter').select2({
				data: ttchiso
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách trạng thái chỉ số không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layDsTTHoaDon(){
	$.ajax({
		url:pref_url+'/lay-ds-tt-hoa-don',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			var tthoadon=$.map(data,function(obj){
				obj.id=obj.idTrangThaiHoaDon;
				obj.text=obj.tenTrangThaiHoaDon;
				return obj;
			});
			
			$('#cmb-tthoadon-filter').select2({
				data: tthoadon
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách trạng thái hóa đơn không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function layKhachHangTruoc(id, khachHangTruoc){
	var index = ds_full.indexOf(Number(id));
	if(khachHangTruoc){
		if(index==0)
			toastError('Không có khách hàng trước');
		else
			layChiTiet(ds_full[index-1]);
	}else{
		if(index==ds_full.length-1)
			toastError('Không có khách hàng sau');
		else
			layChiTiet(ds_full[index+1]);
	}
}

function layDsFull(){
	$.ajax({
		url:pref_url+'/lay-ds-full',
		method:'get',
		dataType:'json',
		data:{daThanhToan:$('#cmb-ttthanhtoan-filter').val(),idTrangThaiChiSo:$('#cmb-ttchiso-filter').val(),idTrangThaiHoaDon:$('#cmb-tthoadon-filter').val(),idDuong:$('#cmb-phuong-filter').val(),idSoGhi:$('#cmb-to-filter').val(),thongTinCanTim:$.trim($('#txt-keyword').val())},
		success:function(data){
			ds_full=data;
		},
		error:function(){
			showError('Thông báo','Đã có lỗi xảy ra, vui lòng thử lại sau!');
		}
	});
}

function layKhuVucHienTai(){
	$.ajax({
		url:pref_url+'/lay-khu-vuc-hien-tai',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			$('#cmb-khuvuc-filter').val(data).trigger('change');
		},
		error:function(){
			showError('Thông báo','Lấy khu vực hiện tại không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function huyHoaDon(id){
	$.ajax({
		url:pref_url+'/huy-hoa-don',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layDanhSach(0);
				toastInfo('Hủy hóa đơn thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Hủy hóa đơn không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function luuDaIn(id){
	$.ajax({
		url:pref_url+'/luu-da-in',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layDanhSach(0);
				toastInfo('Đổi trạng thái in thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Đổi trạng thái in không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function themHoaDon(id){
	$.ajax({
		url:pref_url+'/them-hoa-don',
		method:'post',
		dataType:'json',
		data:{id:id},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layChiTiet(data.resMessage);
				layDanhSach(0);
				toastInfo('Thêm hóa đơn thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Thêm hóa đơn không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}
