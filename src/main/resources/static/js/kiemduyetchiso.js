var pref_url="/kiem-duyet-chi-so";

$(document).ready(function(){
	$('#grid-ds').jdGrid({
		columns:[
			{name:'tenNhanVienGhiThu',title:'Nhân viên'},
			{name:'tongKhachHang',title:'Tổng khách hàng',format:true,css:{'text-align':'right'}},
			{name:'tongCoTheDuyet',title:'Tổng có thể duyệt',format:true,css:{'text-align':'right'}},
			{name:'tongDaDuyet',title:'Tổng đã duyệt',format:true,css:{'text-align':'right'}},
			{name:'tongChuaGhi',title:'Tổng chưa ghi',format:true,css:{'text-align':'right'}},
			{name:'tongBinhThuong',title:'Tổng bình thường',format:true,css:{'text-align':'right'}},
			{name:'tongKhongXai',title:'Tổng không sử dụng',format:true,css:{'text-align':'right'}},
			{name:'tongKhoanTieuThu',title:'Tổng khoán tiêu thụ',format:true,css:{'text-align':'right'}},
			{name:'tongCupNuoc',title:'Tổng cúp nước',format:true,css:{'text-align':'right'}}
		],
		height:'497px',
		decnum: 0
	});
	
	$('#modInfo').on('shown.bs.modal',function(e){
		if($('#grid-ds-cs').data('jdgrid') === undefined) {
			$('#grid-ds-cs').jdGrid({
				columns:[
					{name:'thuTu',title:'Thứ tự'},
					{name:'maKhachHang',title:'Mã KH'},
					{name:'tenKhachHang',title:'Tên KH'},
					{name:'diaChiSuDung',title:'Địa chỉ'},
					{name:'chiSoCu',title:'CS cũ',format:true,css:{'text-align':'right'}},
					{name:'chiSoMoi',title:'CS mới',format:true,css:{'text-align':'right'}},
					{name:'tieuThu',title:'Tiêu thụ',format:true,css:{'text-align':'right'}},
					{name:'tenTrangThaiChiSo',title:'Trạng thái'},
					{name:'tenNguoiCapNhat',title:'Nhân viên ghi'},
					{name:'ngayGioCapNhat',title:'Ngày giờ ghi',type:'interval'},
					{name:'duyetChiSo',title:'Duyệt',type:'check',css:{'text-align':'center'}},
					{name:'tenNguoiDuyet',title:'Nhân viên duyệt'},
					{name:'ngayGioDuyet',title:'Ngày giờ duyệt',type:'interval'},
					{name:'tenDoiTuong',title:'Đối tượng'},
					{name:'tenSoGhi',title:'Tổ'},
				],
				decnum: 0
			});
			
			$('#page-ds-cs').jdPage({
				onPageChanged:function(p){
					layDanhSachChiSo($(e.relatedTarget).attr('rnv'),$(e.relatedTarget).attr('rid'),p-1);
				}
			});
		}
		
		layDanhSachChiSo($(e.relatedTarget).attr('rnv'),$(e.relatedTarget).attr('rid'),0);
	});
	
	$('#modForm').on('shown.bs.modal',function(e){
		if($('#grid-ds-kd').data('jdgrid') === undefined) {
			$('#grid-ds-kd').jdGrid({
				columns:[
					{name:'idChiSo',title:'Check all',type:'checkbox',css:{'text-align':'center'}},
					{name:'thuTu',title:'Thứ tự'},
					{name:'maKhachHang',title:'Mã KH'},
					{name:'tenKhachHang',title:'Tên KH'},
					{name:'diaChiSuDung',title:'Địa chỉ'},
					{name:'chiSoCu',title:'CS cũ',format:true,css:{'text-align':'right'}},
					{name:'chiSoMoi',title:'CS mới',format:true,css:{'text-align':'right'}},
					{name:'tieuThu',title:'Tiêu thụ',format:true,css:{'text-align':'right'}},
					{name:'tenTrangThaiChiSo',title:'Trạng thái'},
					{name:'tenNguoiCapNhat',title:'Nhân viên ghi'},
					{name:'ngayGioCapNhat',title:'Ngày giờ ghi',type:'interval'},
					{name:'tenDoiTuong',title:'Đối tượng'},
					{name:'tenSoGhi',title:'Tổ'},
				],
				decnum: 0
			});
		}
		
		layDanhSachKiemDuyet($(e.relatedTarget).attr('rnv'),$(e.relatedTarget).attr('rid'));
	});
	
	$('#page-ds').jdPage({
		onPageChanged:function(p){
			layDanhSach(p-1);
		}
	});
	
	$('#cmb-nhanvien').change(function(){
		layDanhSach(0);
	});
	
	layDsNhanVien();
});

function layDanhSach(p){
	$.ajax({
		url:pref_url+'/lay-ds',
		method:'get',
		dataType:'json',
		data:{id:$('#cmb-nhanvien').val(),page:p},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			if(data.resCode === undefined){
				if($('#cmb-nhanvien').val() == '0'){
					$('#grid-ds').empty();
					$('#grid-ds').data('jdgrid').clearData();
					$('#grid-ds').jdGrid({
						columns:[
							{name:'tenNhanVienGhiThu',title:'Nhân viên'},
							{name:'tongKhachHang',title:'Tổng khách hàng',format:true,css:{'text-align':'right'}},
							{name:'tongCoTheDuyet',title:'Tổng có thể duyệt',format:true,css:{'text-align':'right'}},
							{name:'tongDaDuyet',title:'Tổng đã duyệt',format:true,css:{'text-align':'right'}},
							{name:'tongChuaGhi',title:'Tổng chưa ghi',format:true,css:{'text-align':'right'}},
							{name:'tongBinhThuong',title:'Tổng bình thường',format:true,css:{'text-align':'right'}},
							{name:'tongKhongXai',title:'Tổng không sử dụng',format:true,css:{'text-align':'right'}},
							{name:'tongKhoanTieuThu',title:'Tổng khoán tiêu thụ',format:true,css:{'text-align':'right'}},
							{name:'tongCupNuoc',title:'Tổng cúp nước',format:true,css:{'text-align':'right'}},
							{name:'col2',title:'T.Tác',type:'control',content:function(obj){return '<a href="#" class="row-check" rnv="'+obj.idNhanVienGhiThu+'" rid="0" title="Kiểm duyệt nhân viên"><i class="fa fa-check-circle"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="modal" data-target="#modForm" href="#" class="row-edit" rnv="'+obj.idNhanVienGhiThu+'" rid="0" title="Kiểm duyệt khách hàng"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="modal" data-target="#modInfo" href="#" class="row-info" rnv="'+obj.idNhanVienGhiThu+'" rid="0" title="Chi tiết"><i class="fa fa-info"></i></a>'},css:{'text-align':'center','width':'100px'}}
						],
						height:'497px',
						decnum: 0
					});
					$('#grid-ds').data('jdgrid').fillData(data.content);
					$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
					
					$('.row-check').click(function(e){
						e.preventDefault();
						showConfirm('Xác nhận', 'Bạn chắc muốn duyệt chỉ số?', 'kiemDuyetChiSo('+$(this).attr('rnv')+','+$(this).attr('rid')+')');
					});
				} else {
					$('#grid-ds').empty();
					$('#grid-ds').data('jdgrid').clearData();
					$('#grid-ds').jdGrid({
						columns:[
							{name:'tenDuong',title:'Phường'},
							{name:'tenSoGhi',title:'Tổ'},
							{name:'tongKhachHang',title:'Tổng khách hàng',format:true,css:{'text-align':'right'}},
							{name:'tongCoTheDuyet',title:'Tổng có thể duyệt',format:true,css:{'text-align':'right'}},
							{name:'tongDaDuyet',title:'Tổng đã duyệt',format:true,css:{'text-align':'right'}},
							{name:'tongChuaGhi',title:'Tổng chưa ghi',format:true,css:{'text-align':'right'}},
							{name:'tongBinhThuong',title:'Tổng bình thường',format:true,css:{'text-align':'right'}},
							{name:'tongKhongXai',title:'Tổng không sử dụng',format:true,css:{'text-align':'right'}},
							{name:'tongKhoanTieuThu',title:'Tổng khoán tiêu thụ',format:true,css:{'text-align':'right'}},
							{name:'tongCupNuoc',title:'Tổng cúp nước',format:true,css:{'text-align':'right'}},
							{name:'col2',title:'T.Tác',type:'control',content:function(obj){return '<a href="#" class="row-check" rnv="'+$('#cmb-nhanvien').val()+'" rid="'+obj.idSoGhi+'" title="Kiểm duyệt tổ"><i class="fa fa-check-circle"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="modal" data-target="#modForm" href="#" class="row-edit" rnv="'+$('#cmb-nhanvien').val()+'" rid="'+obj.idSoGhi+'" title="Kiểm duyệt khách hàng"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="modal" data-target="#modInfo" href="#" class="row-info" rnv="'+$('#cmb-nhanvien').val()+'" rid="'+obj.idSoGhi+'" title="Chi tiết"><i class="fa fa-info"></i></a>'},css:{'text-align':'center','width':'100px'}}
						],
						height:'497px',
						decnum: 0
					});
					$('#grid-ds').data('jdgrid').fillData(data.content);
					$('#page-ds').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
					
					$('.row-check').click(function(e){
						e.preventDefault();
						showConfirm('Xác nhận', 'Bạn chắc muốn duyệt chỉ số?', 'kiemDuyetChiSo('+$(this).attr('rnv')+','+$(this).attr('rid')+')');
					});
				}
			} else {
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Lấy danh sách không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDsNhanVien(){
	$.ajax({
		url:pref_url+'/lay-ds-nhan-vien',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-form').showLoading();
		},
		success:function(data){
			var nhanvien=$.map(data,function(obj){
				obj.id=obj.idNguoiDung;
				obj.text=obj.hoTen;
				return obj;
			});
			
			$('#cmb-nhanvien').select2({
				data: nhanvien
			});
			
			layDanhSach(0);
		},
		error:function(){
			showError('Thông báo','Lấy danh sách nhân viên không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-form').hideLoading();
		}
	});
}

function kiemDuyetChiSo(idNguoiDung,idSoGhi){
	$.ajax({
		url:pref_url+'/kiem-duyet-chi-so',
		method:'post',
		dataType:'json',
		data:{idNguoiDung:idNguoiDung,idSoGhi:idSoGhi},
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			if(data.resCode>0){
				layDanhSach(0);
				toastInfo('Duyệt chỉ số thành công');
			}else{
				showError('Thông báo',data.resMessage);
			}
		},
		error:function(){
			showError('Thông báo','Duyệt chỉ số không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}

function layDanhSachChiSo(nhanvien,soghi,p){
	$.ajax({
		url:pref_url+'/lay-ds-chi-so',
		method:'get',
		dataType:'json',
		data:{idSoGhi:soghi,idNguoiDung:nhanvien,page:p},
		beforeSend:function(){
			$('#box-modInfo').showLoading();
		},
		success:function(data){
			$('#grid-ds-cs').data('jdgrid').fillData(data.content);
			$('#page-ds-cs').data('jdpage').setData({'totalPage':data.totalPages,'currentPage':data.number+1,'itemOnPage':data.size,'totalItem':data.totalElements});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách chỉ số không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-modInfo').hideLoading();
		}
	});
}

function layDanhSachKiemDuyet(nhanvien,soghi){
	$.ajax({
		url:pref_url+'/lay-ds-kiem-duyet',
		method:'get',
		dataType:'json',
		data:{idSoGhi:soghi,idNguoiDung:nhanvien},
		beforeSend:function(){
			$('#box-modForm').showLoading();
		},
		success:function(data){
			$('#grid-ds-kd').data('jdgrid').fillData(data);
			$('#btn-duyet').off('click').on('click', function () {
				duyetKhachHang(nhanvien,soghi);
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách có thể kiểm duyệt không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-modForm').hideLoading();
		}
	});
}

function duyetKhachHang(nhanvien,soghi){
	var selected = [];
	$('#grid-ds-kd .jdgrid-body-wrapper input:checked').each(function() {
	    selected.push($(this).val());
	});
	
	if(selected.join()===''){
		showError('Thông báo','Vui lòng chọn khách hàng!');
	} else {
		$.ajax({
			url:pref_url+'/kiem-duyet-chi-so-khach-hang',
			method:'post',
			dataType:'json',
			data:{strDsChiSo:selected.join()},
			beforeSend:function(){
				$('#box-frm').showLoading();
			},
			success:function(data){
				if(data.resCode>0){
					layDanhSachKiemDuyet(nhanvien,soghi);
					layDanhSach(0);
					toastInfo('Duyệt chỉ số thành công');
				}else{
					showError('Thông báo',data.resMessage);
				}
			},
			error:function(){
				showError('Thông báo','Duyệt chỉ số không thành công, vui lòng thử lại sau!');
			},
			complete:function(){
				$('#box-frm').hideLoading();
			}
		});
	}
}
