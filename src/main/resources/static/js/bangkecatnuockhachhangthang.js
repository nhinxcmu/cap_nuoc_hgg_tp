var pref_url="/bang-ke-cat-nuoc-khach-hang-thang";

$('#view').click(function() {
	var urlp = pref_url+"/xem-bao-cao?idDoiTuongCupNuoc="+$('#cmb-doi-tuong-cn').val()+'&tenDoiTuongCupNuoc='+$('#cmb-doi-tuong-cn option:selected').text();
	$.ajax({
        url: urlp,
        beforeSend: function(jqXHR, settings){
        	$('#box-ds').showLoading();
		},
        success: function(response){
			PDFObject.embed(response, "#pdfRenderer");
		},
		complete: function(jqXHR, textStatus){
			$('#box-ds').hideLoading();
		}
    });
 });
 
$(document).ready(function(){
	$('#cmb-doi-tuong-cn').select2({});
	layDsDoiTuongCupNuoc();
	$('#pdf').click(function() {
		var urlp = pref_url+"/xuat-file-pdf?idDoiTuongCupNuoc="+$('#cmb-doi-tuong-cn').val()+'&tenDoiTuongCupNuoc='+$('#cmb-doi-tuong-cn option:selected').text();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
	
    $('#xls').click(function() {
    	var urlp = pref_url+"/xuat-file-xls?idDoiTuongCupNuoc="+$('#cmb-doi-tuong-cn').val()+'&tenDoiTuongCupNuoc='+$('#cmb-doi-tuong-cn option:selected').text();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
    
    $('#xlsx').click(function() {
    	var urlp = pref_url+"/xuat-file-xlsx?idDoiTuongCupNuoc="+$('#cmb-doi-tuong-cn').val()+'&tenDoiTuongCupNuoc='+$('#cmb-doi-tuong-cn option:selected').text();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
    $('#rtf').click(function() {
    	var urlp = pref_url+"/xuat-file-rtf?idDoiTuongCupNuoc="+$('#cmb-doi-tuong-cn').val()+'&tenDoiTuongCupNuoc='+$('#cmb-doi-tuong-cn option:selected').text();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
});

function layDsDoiTuongCupNuoc(){
	$.ajax({
		url:pref_url+'/lay-ds-doi-tuong-cup-nuoc',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			var doituongcupnuoc=$.map(data,function(obj){
				obj.id=obj.idDoiTuongCupNuoc;
				obj.text=obj.tenDoiTuongCupNuoc;
				return obj;
			});
			
			$('#cmb-doi-tuong-cn').select2({
				data: doituongcupnuoc
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách đối tượng cúp nước không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}
