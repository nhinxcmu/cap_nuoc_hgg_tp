var pref_url="/tong-hop-cat-nuoc-theo-tuyen";

$('#view').click(function() {
	var urlp = pref_url+'/xem-bao-cao?idKhuVuc';
	$.ajax({
        url: urlp,
        beforeSend: function(jqXHR, settings){
        	$('#box-ds').showLoading();
		},
        success: function(response){
			PDFObject.embed(response, "#pdfRenderer");
		},
		complete: function(jqXHR, textStatus){
			$('#box-ds').hideLoading();
		}
    });
//	}
 });
 
$(document).ready(function(){	
	$('#pdf').click(function() {
		var urlp = pref_url+'/xuat-file-pdf?idKhuVuc';
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
	
    $('#xls').click(function() {
    	var urlp = pref_url+'/xuat-file-xls?idKhuVuc';
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
    
    $('#xlsx').click(function() {
    	var urlp = pref_url+'/xuat-file-xlsx?idKhuVuc';
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
    $('#rtf').click(function() {
    	var urlp = pref_url+'/xuat-file-rtf?idKhuVuc';
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
});
