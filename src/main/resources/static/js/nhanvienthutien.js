var pref_url="/nhan-vien-thu-tien";

$('#view').click(function() {
	var urlp = pref_url+"/xem-bao-cao?idNguoiDung="+$('#cmb-nguoidung').val()+'&hoTen='+$('#cmb-nguoidung option:selected').text()+'&tungay='+$('#txt-tungay').val()+'&denngay='+$('#txt-denngay').val()+"&idthang="+$('#cmb-thang').val();
	$.ajax({
        url: urlp,
        beforeSend: function(jqXHR, settings){
        	$('#box-ds').showLoading();
		},
        success: function(response){
			PDFObject.embed(response, "#pdfRenderer");
		},
		complete: function(jqXHR, textStatus){
			$('#box-ds').hideLoading();
		}
    });
 });
 
$(document).ready(function(){
	$('.input-mask').inputmask();
	
	$('.chon-ngay').datepicker({
		autoclose: true,
		todayHighlight:true,
		format:'dd/mm/yyyy',
		zIndexOffset:10000
    }).datepicker('setDate', new Date());
	
	$('#cmb-nguoidung').select2({});
	layDsNguoiDung();
	
	$('#cmb-thang').select2({});
	layDsThang();
	
	$('#pdf').click(function() {
		var urlp = pref_url+"/xuat-file-pdf?idNguoiDung="+$('#cmb-nguoidung').val()+'&hoTen='+$('#cmb-nguoidung option:selected').text()+'&tungay='+$('#txt-tungay').val()+'&denngay='+$('#txt-denngay').val()+"&idthang="+$('#cmb-thang').val();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
	
    $('#xls').click(function() {
    	var urlp = pref_url+"/xuat-file-xls?idNguoiDung="+$('#cmb-nguoidung').val()+'&hoTen='+$('#cmb-nguoidung option:selected').text()+'&tungay='+$('#txt-tungay').val()+'&denngay='+$('#txt-denngay').val()+"&idthang="+$('#cmb-thang').val();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
    
    $('#xlsx').click(function() {
    	var urlp = pref_url+"/xuat-file-xlsx?idNguoiDung="+$('#cmb-nguoidung').val()+'&hoTen='+$('#cmb-nguoidung option:selected').text()+'&tungay='+$('#txt-tungay').val()+'&denngay='+$('#txt-denngay').val()+"&idthang="+$('#cmb-thang').val();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
    $('#rtf').click(function() {
    	var urlp = pref_url+"/xuat-file-rtf?idNguoiDung="+$('#cmb-nguoidung').val()+'&hoTen='+$('#cmb-nguoidung option:selected').text()+'&tungay='+$('#txt-tungay').val()+'&denngay='+$('#txt-denngay').val()+"&idthang="+$('#cmb-thang').val();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
});

function layDsNguoiDung(){
	$.ajax({
		url:pref_url+'/lay-ds-nguoi-dung',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			var nguoidung=$.map(data,function(obj){
				obj.id=obj.idNguoiDung;
				obj.text=obj.hoTen;
				return obj;
			});
			
			$('#cmb-nguoidung').select2({
				data: nguoidung
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách nhân viên không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDsThang(){
	$.ajax({
		url:pref_url+'/lay-ds-thang',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			var thang=$.map(data,function(obj){
				obj.id=obj.idThang;
				obj.text=obj.tenThang;
				return obj;
			});
			
			$('#cmb-thang').select2({
				data: thang
			});
		},
		error:function(){
			showError('Thông báo','Lấy danh sách tháng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

