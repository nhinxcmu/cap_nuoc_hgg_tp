var pref_url="/tin-nhan";

$(document).ready(function(){
	$('#grid-hoadon').jdGrid({
		columns:[
			{name:'idHoaDon',title:'Check all',type:'checkbox',css:{'text-align':'center'}},
			{name:'thuTu',title:'Thứ tự'},
			{name:'maKhachHang',title:'Mã KH'},
			{name:'tenKhachHang',title:'Tên KH'},
			{name:'soDienThoai',title:'Số điện thoại'},
			{name:'diaChiSuDung',title:'Địa chỉ'},
			// {name:'chiSoCu',title:'CS cũ',format:true,css:{'text-align':'right'}},
			// {name:'chiSoMoi',title:'CS mới',format:true,css:{'text-align':'right'}},
			{name:'tieuThu',title:'Tiêu thụ',format:true,css:{'text-align':'right'}},
			// {name:'tienNuoc',title:'Tiền nước',format:true,css:{'text-align':'right'}},
			// {name:'thue',title:'Thuế GTGT',format:true,css:{'text-align':'right'}},
			// {name:'phiBvmt',title:'Phí BVMT',format:true,css:{'text-align':'right'}},
			// {name:'phiBvr',title:'Phí BVR',format:true,css:{'text-align':'right'}},
			{name:'tongTien',title:'Tổng tiền',format:true,css:{'text-align':'right'}},
			{name:'soLuongTinNhan',title:'Tin đã gửi',format:true,css:{'text-align':'right'}},
			{name:'col2',title:'T.Tác',type:'control',content:function(obj){return $('#cmb-tinnhan-filter').val()==='0'?'':'<a data-toggle="modal" data-target="#modInfo" href="#" class="row-info-1" rid="'+obj.idHoaDon+'" title="Xem tin nhắn"><i class="fa fa-info"></i></a>'},css:{'text-align':'center','width':'70px'}}
		],
		decnum: 0
	});
	
	$('#modInfo').on('shown.bs.modal',function(e){
		if($('#grid-ds-tn').data('jdgrid') === undefined) {
			$('#grid-ds-tn').jdGrid({
				columns:[
					{name:'soDienThoai',title:'Số điện thoại'},
					{name:'noiDung',title:'Nội dung'},
					{name:'thoiGianGui',title:'Thời gian gửi',type:'interval'},
					{name:'loi',title:'Lỗi',type:'check',css:{'text-align':'center'}},
				],
				decnum: 0
			});
		}
		
		layDanhSachTinNhan($(e.relatedTarget).attr('rid'));
	});
	
	$('#cmb-tinnhan-filter').change(function(){
		$('#btn-search').click();
	});
	
	$('#btn-search').click(function(){
		layDanhSachHoaDon();
	});
	
	$('#txt-keyword').keypress(function(e){
        if(e.which == 13){
            $('#btn-search').click();
        }
    });
	
	$('#btn-send').click(function(){
		guiTinNhan();
	});
	
	$('#cmb-tinnhan-filter').select2({});
	
	layDanhSachHoaDon();
});

function layDanhSachHoaDon(){
	$.ajax({
		url:pref_url+'/lay-ds-hoa-don',
		method:'get',
		dataType:'json',
		data:{thongTinCanTim:$('#txt-keyword').val(),nhanTin:$('#cmb-tinnhan-filter').val()},
		beforeSend:function(){
			$('#box-ds').showLoading();
		},
		success:function(data){
			$('#grid-hoadon').data('jdgrid').fillData(data);
			var so_hd = data.length;
			var so_tn = 0;
			for (i = 0; i < so_hd; i++) {
				so_tn += data[i].soLuongTinNhan;
			}
			
			$('#sum-row').html('Tổng cộng <b>'+so_hd+'</b> hóa đơn, <b>'+so_tn+'</b> tin đã gửi.');
			
		},
		error:function(){
			showError('Thông báo','Lấy danh sách khách hàng không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-ds').hideLoading();
		}
	});
}

function layDanhSachTinNhan(idHoaDon){
	$.ajax({
		url:pref_url+'/lay-ds-tin-nhan',
		method:'get',
		dataType:'json',
		data:{idHoaDon:idHoaDon},
		beforeSend:function(){
			$('#box-modal-info').showLoading();
		},
		success:function(data){
			$('#grid-ds-tn').data('jdgrid').fillData(data);
		},
		error:function(){
			showError('Thông báo','Lấy danh sách tin nhắn không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-modal-info').hideLoading();
		}
	});
}

function guiTinNhan(){
	var selected = [];
	$('#grid-hoadon .jdgrid-body-wrapper input:checked').each(function() {
		if(!$(this).is(':disabled'))
			selected.push($(this).val());
	});
	
	if(selected.join()===''){
		showError('Thông báo','Vui lòng chọn khách hàng!');
	} else {
		$.ajax({
			url:pref_url+'/gui-tin-nhan',
			method:'post',
			dataType:'json',
			data:{strDsHoaDon:selected.join()},
			beforeSend:function(){
				$('#box-ds').showLoading();
			},
			success:function(data){
				if(data.resCode>0){
					toastInfo('Gửi tin nhắn thành công '+data.resMessage+' khách hàng');
					layDanhSachHoaDon();
				}else{
					showError('Thông báo',data.resMessage);
				}
			},
			error:function(){
				showError('Thông báo','Gửi tin nhắn không thành công, vui lòng thử lại sau!');
			},
			complete:function(){
				$('#box-ds').hideLoading();
			}
		});
	}
}
