var pref_url="/lich-su-thay-doi-so-seri";

$('#view').click(function() {	
	if(check()){
		var tree = $("#tree").fancytree("getTree"),
		selNodes = tree.getSelectedNodes();
		var nodes = new Array();
	    selNodes.forEach(function(node) {
	    	if(node.key!=-1) nodes.push(node.key);
	    });
	    var urlp = pref_url+"/xem-bao-cao?idSoGhi="+nodes.join();
		$.ajax({
	        url: urlp,
	        beforeSend: function(jqXHR, settings){
	        	$('#box-ds').showLoading();
			},
	        success: function(response){
				PDFObject.embed(response, "#pdfRenderer");
			},
			complete: function(jqXHR, textStatus){
				$('#box-ds').hideLoading();
			}
	    });
	}
 });
 
$(document).ready(function(){
	
	$("#tree").fancytree({
		checkbox: true,
		selectMode: 3,
		// extensions: ["dnd"],
		icon: false,
		source: {
	        url: pref_url+"/du-lieu-cay",
	        cache: false
	    }
	});
	
	$('#moRongTatCa').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
			node.setExpanded();
		});
		return false;
	});
	
	$('#thuGonTatCa').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
			node.setExpanded(false);
		});
		return false;
	});
	
	$('#chk-all').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
	        node.setSelected(true);
	    });
		return false;
	});
	
	$('#chk-none').click(function() {
		$("#tree").fancytree("getTree").visit(function(node){
	        node.setSelected(false);
	    });
		return false;
	});
	
	$('#pdf').click(function() {
		if(check()){
			var tree = $("#tree").fancytree("getTree"),
			selNodes = tree.getSelectedNodes();
			var nodes = new Array();
		    selNodes.forEach(function(node) {
		    	if(node.key!=-1) nodes.push(node.key);
		    });
		    var urlp = pref_url+"/xuat-file-pdf?idSoGhi="+nodes.join();
			$.ajax({
		        url: urlp,
		        beforeSend: function(jqXHR, settings){
		        	$('#box-ds').showLoading();
				},
				success: function(response){
	    			$(location).attr('href', urlp);
				},
				complete: function(jqXHR, textStatus){
					$('#box-ds').hideLoading();
				}
		    });
		}
    });
	
    $('#xls').click(function() {
    	if(check()){
			var tree = $("#tree").fancytree("getTree"),
			selNodes = tree.getSelectedNodes();
			var nodes = new Array();
		    selNodes.forEach(function(node) {
		    	if(node.key!=-1) nodes.push(node.key);
		    });
		    var urlp = pref_url+"/xuat-file-xls?idSoGhi="+nodes.join();
			$.ajax({
		        url: urlp,
		        beforeSend: function(jqXHR, settings){
		        	$('#box-ds').showLoading();
				},
				success: function(response){
	    			$(location).attr('href', urlp);
				},
				complete: function(jqXHR, textStatus){
					$('#box-ds').hideLoading();
				}
		    });
		}
    });
    
    $('#xlsx').click(function() {
    	if(check()){
			var tree = $("#tree").fancytree("getTree"),
			selNodes = tree.getSelectedNodes();
			var nodes = new Array();
		    selNodes.forEach(function(node) {
		    	if(node.key!=-1) nodes.push(node.key);
		    });
		    var urlp = pref_url+"/xuat-file-xlsx?idSoGhi="+nodes.join();
			$.ajax({
		        url: urlp,
		        beforeSend: function(jqXHR, settings){
		        	$('#box-ds').showLoading();
				},
				success: function(response){
	    			$(location).attr('href', urlp);
				},
				complete: function(jqXHR, textStatus){
					$('#box-ds').hideLoading();
				}
		    });
		}
    });
    $('#rtf').click(function() {
    	if(check()){
			var tree = $("#tree").fancytree("getTree"),
			selNodes = tree.getSelectedNodes();
			var nodes = new Array();
		    selNodes.forEach(function(node) {
		    	if(node.key!=-1) nodes.push(node.key);
		    });
		    var urlp = pref_url+"/xuat-file-rtf?idSoGhi="+nodes.join();
			$.ajax({
		        url: urlp,
		        beforeSend: function(jqXHR, settings){
		        	$('#box-ds').showLoading();
				},
				success: function(response){
	    			$(location).attr('href', urlp);
				},
				complete: function(jqXHR, textStatus){
					$('#box-ds').hideLoading();
				}
		    });
		}
    });	
});

function check(){
	var tree = $("#tree").fancytree("getTree"),
	selNodes = tree.getSelectedNodes();
	var nodes = new Array();
    selNodes.forEach(function(node) {
    	if(node.key!=-1) nodes.push(node.key);
    });
	if(nodes.join()===''){
		showError('Thông báo','Vui lòng chọn tổ!');
		return false;
	}
	return true;
}