var pref_url="/bao-cao-san-luong-vlg";

$('#view').click(function() {
	var urlp = pref_url+'/xem-bao-cao?tuthang='+$('#cmb-tuthang').val()+'&denthang='+$('#cmb-denthang').val();
	$.ajax({
        url: urlp,
        beforeSend: function(jqXHR, settings){
        	$('#box-ds').showLoading();
		},
        success: function(response){
			PDFObject.embed(response, "#pdfRenderer");
		},
		complete: function(jqXHR, textStatus){
			$('#box-ds').hideLoading();
		}
    });
 });
 
$(document).ready(function(){
	initForm();
	
	$('#pdf').click(function() {
		var urlp = pref_url+'/xuat-file-pdf?tuthang='+$('#cmb-tuthang').val()+'&denthang='+$('#cmb-denthang').val();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
	
    $('#xls').click(function() {
    	var urlp = pref_url+'/xuat-file-xls?tuthang='+$('#cmb-tuthang').val()+'&denthang='+$('#cmb-denthang').val();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
    
    $('#xlsx').click(function() {
    	var urlp = pref_url+'/xuat-file-xlsx?tuthang='+$('#cmb-tuthang').val()+'&denthang='+$('#cmb-denthang').val();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
    $('#rtf').click(function() {
    	var urlp = pref_url+'/xuat-file-rtf?tuthang='+$('#cmb-tuthang').val()+'&denthang='+$('#cmb-denthang').val();
        $.ajax({
            url: urlp,
            beforeSend: function(jqXHR, settings){
            	$('#box-ds').showLoading();
    		},
    		success: function(response){
    			$(location).attr('href', urlp);
    		},
    		complete: function(jqXHR, textStatus){
    			$('#box-ds').hideLoading();
    		}
        });
    });
});

function initForm(){
	$.ajax({
		url:pref_url+'/lay-ds-thang',
		method:'get',
		dataType:'json',
		beforeSend:function(){
			$('#box-frm').showLoading();
		},
		success:function(data){
			var thangs=$.map(data,function(obj){
				obj.id=obj.idThang;
				obj.text=obj.tenThang;
				return obj;
			});
			
			$('#cmb-tuthang').select2({
				data: thangs
			});
			
			$('#cmb-denthang').select2({
				data: thangs
			});
		},
		error:function(){
			showError('Thông báo','Khởi tạo dữ liệu không thành công, vui lòng thử lại sau!');
		},
		complete:function(){
			$('#box-frm').hideLoading();
		}
	});
}
