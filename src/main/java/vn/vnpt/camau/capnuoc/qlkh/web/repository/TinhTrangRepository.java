package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.TinhTrang;

public interface TinhTrangRepository extends Repository<TinhTrang, Long> {

	List<TinhTrang> findAll();
}
