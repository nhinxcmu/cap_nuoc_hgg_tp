package vn.vnpt.camau.capnuoc.qlkh.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.NguoiDungRepository;

@Service
public class UserDetailsServiceImp implements UserDetailsService {

	@Autowired
	private NguoiDungRepository nguoiDungRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		NguoiDung nguoiDungCanKiemTra = nguoiDungRepository.findByTenNguoiDungAndVaiTro_Menus_Menu_IdMenuIsNotNull(username);
		if (nguoiDungCanKiemTra == null) {
			throw new UsernameNotFoundException("Tên người dùng hoặc mật khẩu không đúng.");
		}
		UserDetailsImp userDetails = new UserDetailsImp(nguoiDungCanKiemTra, nguoiDungCanKiemTra.getVaiTro().getMenus());
		return userDetails;
	}
	public UserDetails loadByUserId(Long userId) throws UsernameNotFoundException {
		NguoiDung nguoiDungCanKiemTra = nguoiDungRepository.findByIdNguoiDungAndVaiTro_Menus_Menu_IdMenuIsNotNull(userId);
		if (nguoiDungCanKiemTra == null) {
			throw new UsernameNotFoundException("Không tìm thấy người dùng có ID "+userId+" hoặc người dùng không có menu");
		}
		UserDetailsImp userDetails = new UserDetailsImp(nguoiDungCanKiemTra, nguoiDungCanKiemTra.getVaiTro().getMenus());
		return userDetails;
	}

}
