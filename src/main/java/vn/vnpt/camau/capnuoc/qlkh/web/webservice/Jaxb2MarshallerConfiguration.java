package vn.vnpt.camau.capnuoc.qlkh.web.webservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;

@Configuration
public class Jaxb2MarshallerConfiguration {

	@Autowired
	private ThamSoService thamSoService;

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		// this package must match the package in the <generatePackage> specified in
		// pom.xml
		marshaller.setContextPath("vn.vnpt.camau.capnuoc.qlkh.web.wsdl");
		
		return marshaller;
	}
	
	@Bean
	public PublishServiceClient publishServiceClient(Jaxb2Marshaller marshaller) {
		PublishServiceClient client = new PublishServiceClient();
		
		client.setDefaultUri(thamSoService.layUriPublishService());
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		
		return client;
	}
	
	@Bean
	public BusinessServiceClient businessServiceClient(Jaxb2Marshaller marshaller) {
		BusinessServiceClient client = new BusinessServiceClient();
		
		client.setDefaultUri(thamSoService.layUriBusinessService());
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		
		return client;
	}
	
	@Bean
	public PortalServiceClient portalServiceClient(Jaxb2Marshaller marshaller) {
		PortalServiceClient client = new PortalServiceClient();
		
		client.setDefaultUri(thamSoService.layUriPortalService());
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		
		return client;
	}

}
