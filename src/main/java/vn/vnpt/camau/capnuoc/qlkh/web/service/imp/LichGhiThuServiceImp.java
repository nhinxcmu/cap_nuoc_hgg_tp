package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.LichGhiThuConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.SoGhiConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.CayDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.LichGhiThuDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Duong;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhuVuc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichGhiThu;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.SoGhi;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DuongRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.KhuVucRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.LichGhiThuRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.SoGhiRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.LichGhiThuService;

@Service
public class LichGhiThuServiceImp implements LichGhiThuService {

	@Autowired
	private LichGhiThuRepository repository;
	@Autowired
	private LichGhiThuConverter converter;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private KhuVucRepository khuVucRepository;
	@Autowired
	private DuongRepository duongRepository;
	@Autowired
	private SoGhiRepository soGhiRepository;
	@Autowired
	private SoGhiConverter soGhiConverter;
	@Autowired
	private ThangRepository thangRepository;
	private Set<SoGhi> soGhis;

	@Override
	public Page<LichGhiThuDto> layDsLichGhiThu(long idVaiTro, String thongTinCanTim, Pageable pageable) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		long idDonViLamViec = Utilities.layIdDonViLamViec(httpSession);
		Page<LichGhiThu> dsLichGhiThuCanLay;
		// if (idVaiTro == 0) {
		// if (thongTinCanTim.equals("")) {
		// dsLichGhiThuCanLay = repository
		// .findByThangs_IdThangAndNguoiDuocPhanCong_DonVi_IdDonViOrderByIdLichGhiThuDesc(idThangLamViec,
		// idDonViLamViec,
		// pageable);
		// } else {
		// dsLichGhiThuCanLay = repository
		// .findByThangs_IdThangAndNguoiDuocPhanCong_DonVi_IdDonViAndNguoiDuocPhanCong_HoTenContainsOrderByIdLichGhiThuDesc(
		// idThangLamViec, idDonViLamViec, thongTinCanTim, pageable);
		// }
		// } else {
		if (thongTinCanTim.equals("")) {
			dsLichGhiThuCanLay = repository
					.findByThangs_IdThangAndNguoiDuocPhanCong_DonVi_IdDonViAndNguoiDuocPhanCong_VaiTro_IdVaiTroOrderByIdLichGhiThuDesc(
							idThangLamViec, idDonViLamViec, idVaiTro, pageable);
		} else {
			dsLichGhiThuCanLay = repository
					.findByThangs_IdThangAndNguoiDuocPhanCong_DonVi_IdDonViAndNguoiDuocPhanCong_VaiTro_IdVaiTroAndNguoiDuocPhanCong_HoTenContainsOrderByIdLichGhiThuDesc(
							idThangLamViec, idDonViLamViec, idVaiTro, thongTinCanTim, pageable);
		}
		// }
		Page<LichGhiThuDto> dsLichGhiThuDaChuyenDoi = dsLichGhiThuCanLay.map(converter::convertToDto);
		return dsLichGhiThuDaChuyenDoi;
	}

	@Override
	public LichGhiThuDto layLichGhiThu(long id) {
		LichGhiThu lichGhiThuCanLay = repository.findByIdLichGhiThu(id);
		Long idDonVi = Utilities.layIdDonViLamViec(httpSession);
		this.soGhis = lichGhiThuCanLay.getSoGhis();
		List<CayDto> dsCay = layDsCay(idDonVi);
		LichGhiThuDto lichGhiThuDaChuyenDoi = converter.convertToDto(lichGhiThuCanLay);
		lichGhiThuDaChuyenDoi.setCayDtos(dsCay);
		return lichGhiThuDaChuyenDoi;
	}

	private List<CayDto> layDsCay(Long idDonVi) {
		List<CayDto> dsCayKhuVuc = new ArrayList<CayDto>();
		List<KhuVuc> dsKhuVuc = khuVucRepository.findByDonVi_IdDonViOrderByTenKhuVuc(idDonVi);
		for (KhuVuc khuVuc : dsKhuVuc) {
			CayDto cayKhuVuc = taoCayKhuVuc(khuVuc);
			dsCayKhuVuc.add(cayKhuVuc);
		}
		return dsCayKhuVuc;
	}

	private CayDto taoCayKhuVuc(KhuVuc khuVuc) {
		List<CayDto> dsCayDuong = taoDsCayDuong(khuVuc.getIdKhuVuc());
		if (dsCayDuong.size() == 0) {
			return new CayDto();
		}
		CayDto cayKhuVuc = new CayDto(-1, khuVuc.getTenKhuVuc(), true, null, dsCayDuong);
		return cayKhuVuc;
	}

	private List<CayDto> taoDsCayDuong(Long idKhuVuc) {
		List<CayDto> dsCayDuong = new ArrayList<CayDto>();
		List<Duong> dsDuong = duongRepository.findByKhuVuc_IdKhuVucOrderByTenDuong(idKhuVuc);
		for (Duong duong : dsDuong) {
			CayDto cayDuong = taoCayDuong(duong);
			dsCayDuong.add(cayDuong);
		}
		return dsCayDuong;
	}

	private CayDto taoCayDuong(Duong duong) {
		List<CayDto> dsCaySoGhi = taoDsCaySoGhi(duong.getIdDuong());
		if (dsCaySoGhi.size() == 0) {
			return new CayDto();
		}
		CayDto cayDuong = new CayDto(-1, duong.getTenDuong(), true, null, dsCaySoGhi);
		return cayDuong;
	}

	private List<CayDto> taoDsCaySoGhi(Long idDuong) {
		List<CayDto> dsCaySoGhi = new ArrayList<CayDto>();
		List<SoGhi> dsSoGhi = soGhiRepository.findByDuong_IdDuongOrderByTenSoGhi(idDuong);
		for (SoGhi soGhi : dsSoGhi) {
			CayDto caySoGhi = taoCaySoGhi(soGhi);
			dsCaySoGhi.add(caySoGhi);
		}
		return dsCaySoGhi;
	}

	private CayDto taoCaySoGhi(SoGhi soGhi) {
		boolean select = false;
		if (soGhis != null) {
			select = soGhis.stream().filter(s -> s.getIdSoGhi() == soGhi.getIdSoGhi()).count() > 0;
		}
		CayDto caySoGhi = new CayDto(soGhi.getIdSoGhi(), soGhi.getTenSoGhi(), null, select, null);
		return caySoGhi;
	}

	@Override
	public List<CayDto> layDsCaySoGhi() {
		Long idDonVi = Utilities.layIdDonViLamViec(httpSession);
		this.soGhis = null;
		List<CayDto> dsCay = layDsCay(idDonVi);
		return dsCay;
	}

	@Override
	public LichGhiThuDto luuLichGhiThu(LichGhiThuDto dto) throws Exception {
		LichGhiThu lichGhiThuCanLuu;
		if (dto.getIdLichGhiThu() == null) {
			lichGhiThuCanLuu = themLichGhiThu(dto);
		} else {
			lichGhiThuCanLuu = capNhatLichGhiThu(dto);
		}
		LichGhiThuDto lichGhiThuDaChuyenDoi = converter.convertToDto(lichGhiThuCanLuu);
		return lichGhiThuDaChuyenDoi;
	}

	private LichGhiThu themLichGhiThu(LichGhiThuDto dto) throws Exception {
		// kiểm tra lịch ghi thu đã tồn tại
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		if (laLichGhiThuDaTonTai(dto.getIdNhanVienGhiThu(), idThangLamViec)) {
			throw new Exception("Lịch ghi thu đã tồn tại.");
		}
		// thêm lịch ghi thu
		LichGhiThu lichGhiThuDaLuu = luuLichGhiThu2(dto);
		// trả lịch ghi thu
		return lichGhiThuDaLuu;
	}

	private boolean laLichGhiThuDaTonTai(Long idNhanVienGhiThu, long idThang) {
		Long soLuongLichGhiThuCanKiemTra = repository
				.countByNguoiDuocPhanCong_IdNguoiDungAndThang_IdThang(idNhanVienGhiThu, idThang);
		return soLuongLichGhiThuCanKiemTra > 0;
	}

	private LichGhiThu luuLichGhiThu2(LichGhiThuDto dto) throws Exception {
		// thiết lập thông tin cập nhật
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		dto.setIdNguoiPhanCong(idNguoiDungHienHanh);
		dto.setNgayGioCapNhat(new Date());
		// chuyển đổi
		LichGhiThu lichGhiThuCanLuu = converter.convertToEntity(dto);
		// thiết lập tháng
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		Thang thangThemLichGhiThu = new Thang();
		thangThemLichGhiThu.setIdThang(idThangLamViec);
		Set<Thang> dsThangSuDungLichGhiThu = new HashSet<Thang>();
		dsThangSuDungLichGhiThu.add(thangThemLichGhiThu);
		lichGhiThuCanLuu.setThang(thangThemLichGhiThu);
		lichGhiThuCanLuu.setThangs(dsThangSuDungLichGhiThu);
		// thiết lập sổ ghi
		Set<SoGhi> dsSoGhi = dto.getSoGhiDtos().stream().map(soGhiConverter::convertToEntity)
				.collect(Collectors.toSet());
		lichGhiThuCanLuu.setSoGhis(dsSoGhi);
		try {
			repository.save(lichGhiThuCanLuu);
			return lichGhiThuCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu lịch ghi thu.");
		}
	}

	private LichGhiThu capNhatLichGhiThu(LichGhiThuDto dto) throws Exception {
		// kiểm tra lịch ghi thu
		kiemTraLichGhiThu(dto);
		// cập nhật lịch ghi thu
		LichGhiThu lichGhiThuDaCapNhat = luuLichGhiThu2(dto);
		// trả thông lịch ghi thu
		return lichGhiThuDaCapNhat;
	}

	private void kiemTraLichGhiThu(LichGhiThuDto dto) throws Exception {
		// kiểm tra lịch ghi thu cần cập nhật
		LichGhiThu lichGhiThuCanKiemTra = repository.findOne(dto.getIdLichGhiThu());
		if (lichGhiThuCanKiemTra == null) {
			throw new Exception("Không tìm thấy lịch ghi thu.");
		}
		// kiểm tra lịch ghi thu đã tồn tại
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		if (laLichGhiThuDaTonTai(dto, idThangLamViec)) {
			throw new Exception("Lịch ghi thu đã tồn tại.");
		}
		// kiểm tra lịch ghi thu tháng làm việc
		long idThangCanKiemTra = lichGhiThuCanKiemTra.getThang().getIdThang();
		if (idThangCanKiemTra != idThangLamViec) {
			dto.setIdLichGhiThu(null);
		}
	}

	private boolean laLichGhiThuDaTonTai(LichGhiThuDto dto, long idThang) {
		LichGhiThu lichGhiThuCanKiemTra = repository
				.findByNguoiDuocPhanCong_IdNguoiDungAndThang_IdThang(dto.getIdNhanVienGhiThu(), idThang);
		if (lichGhiThuCanKiemTra != null) {
			if (lichGhiThuCanKiemTra.getIdLichGhiThu() != dto.getIdLichGhiThu()) {
				return true;
			}
		}
		return false;
	}

	@Override
	@Transactional
	public void xoaLichGhiThu(long id) throws Exception {
		// kiểm tra lịch ghi thu
		kiemTraLichGhiThu(id);
		// xóa lịch ghi thu
		try {
			repository.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể xóa lịch ghi thu.");
		}
	}

	private void kiemTraLichGhiThu(long id) throws Exception {
		// lấy số lượng lịch ghi thu tháng
		Long soLuongThang = thangRepository.countByLichGhiThuThangs_IdLichGhiThu(id);
		if (soLuongThang == 0) {
			throw new Exception("Không tìm thấy lịch ghi thu.");
		}
		if (soLuongThang > 1) {
			throw new Exception("Lịch ghi đã áp dụng cho nhiều tháng.");
		}
	}

	@Override
	public Page<LichGhiThuDto> layDsLichGhiThuTheoMenuThanhToanGiaoDich(String thongTinCanTim, Pageable pageable) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		long idDonViLamViec = Utilities.layIdDonViLamViec(httpSession);
		long idMenu = Utilities.ID_MENU_THANH_TOAN_GIAO_DICH;
		Page<LichGhiThu> dsLichGhiThuCanLay;
		if (thongTinCanTim.equals("")) {
			dsLichGhiThuCanLay = repository
					.findByThangs_IdThangAndNguoiDuocPhanCong_DonVi_IdDonViAndNguoiDuocPhanCong_VaiTro_Menus_IdMenuOrderByIdLichGhiThuDesc(
							idThangLamViec, idDonViLamViec, idMenu, pageable);
		} else {
			dsLichGhiThuCanLay = repository
					.findByThangs_IdThangAndNguoiDuocPhanCong_DonVi_IdDonViAndNguoiDuocPhanCong_VaiTro_Menus_IdMenuAndNguoiDuocPhanCong_HoTenContainsOrderByIdLichGhiThuDesc(
							idThangLamViec, idDonViLamViec, idMenu, thongTinCanTim, pageable);
		}
		Page<LichGhiThuDto> dsLichGhiThuDaChuyenDoi = dsLichGhiThuCanLay.map(converter::convertToDto);
		return dsLichGhiThuDaChuyenDoi;
	}

}
