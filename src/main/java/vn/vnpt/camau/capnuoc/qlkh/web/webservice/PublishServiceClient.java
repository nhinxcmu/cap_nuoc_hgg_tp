package vn.vnpt.camau.capnuoc.qlkh.web.webservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.ImportAndPublishInv;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.ImportAndPublishInvResponse;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.UpdateCus;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.UpdateCusResponse;

public class PublishServiceClient extends WebServiceGatewaySupport {

	@Autowired
	private ThamSoService thamSoService;

	public UpdateCusResponse updateCus(String xmlCusData, Integer convert) {
		UpdateCus request = new UpdateCus();
		
		request.setXMLCusData(xmlCusData);
		request.setUsername(thamSoService.layUsername());
		request.setPass(thamSoService.layPassword());
		request.setConvert(convert);
		
		String soapAction = Utilities.NAMESPACE + "UpdateCus";
		UpdateCusResponse response = (UpdateCusResponse) getWebServiceTemplate()
				.marshalSendAndReceive(thamSoService.layUriPublishService(), request, new SoapActionCallback(soapAction));
		
		return response;
	}
	
	public ImportAndPublishInvResponse importAndPublishInv(String xmlInvData, int convert) {
		ImportAndPublishInv request = new ImportAndPublishInv();
		
		request.setAccount(thamSoService.layAccount());
		request.setACpass(thamSoService.layACpass());
		request.setXmlInvData(xmlInvData);
		request.setUsername(thamSoService.layUsername());
		request.setPass((thamSoService.layPassword())); // set pass cho CMU, BKN
		request.setPassword((thamSoService.layPassword())); // set pass cho TBH
		request.setPattern(thamSoService.layPattern());
		request.setSerial(thamSoService.laySerial());
		request.setConvert(convert);
		
		String soapAction = Utilities.NAMESPACE + "ImportAndPublishInv";
		ImportAndPublishInvResponse response = (ImportAndPublishInvResponse) getWebServiceTemplate()
				.marshalSendAndReceive(thamSoService.layUriPublishService(), request, new SoapActionCallback(soapAction));
		
		return response;
	}
	public ImportAndPublishInvResponse importAndPublishInvLapDat(String xmlInvData, int convert) {
		ImportAndPublishInv request = new ImportAndPublishInv();
		
		request.setAccount(thamSoService.layAccount());
		request.setACpass(thamSoService.layACpass());
		request.setXmlInvData(xmlInvData);
		request.setUsername(thamSoService.layUsername());
		request.setPass((thamSoService.layPassword())); // set pass cho CMU, BKN
		request.setPassword((thamSoService.layPassword())); // set pass cho TBH
		request.setPattern(thamSoService.layPatternLapDat());
		request.setSerial(thamSoService.laySerialLapDat());
		request.setConvert(convert);
		
		String soapAction = Utilities.NAMESPACE + "ImportAndPublishInv";
		ImportAndPublishInvResponse response = (ImportAndPublishInvResponse) getWebServiceTemplate()
				.marshalSendAndReceive(thamSoService.layUriPublishService(), request, new SoapActionCallback(soapAction));
		
		return response;
	}
	
}
