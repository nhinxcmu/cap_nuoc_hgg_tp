package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class GiaNuocDto {

	@Getter
	@Setter
	@NotNull
	private Long idGiaNuoc;
	@Getter
	@Setter
	@Mapping("doiTuong.idDoiTuong")
	private Long idDoiTuong;
	@Getter
	@Setter
	@Mapping("doiTuong.tenDoiTuong")
	private String tenDoiTuong;
	@Getter
	@Setter
	@Mapping("huyen.idHuyen")
	private Long idHuyen;
	@Getter
	@Setter
	@Mapping("huyen.tenHuyen")
	private String tenHuyen;
	@Getter
	@Setter
	@Mapping("nguoiDung.idNguoiDung")
	private Long idNguoiCapNhat;
	@Getter
	@Setter
	@Mapping("nguoiDung.tenNguoiDung")
	private String tenNguoiCapNhat;
	@Getter
	@Setter
	@NotNull
	@Size(max = 250)
	private String tenGiaNuoc;
	@Getter
	@Setter
	@NotNull
	@Size(max = 10)
	private Date ngayApDung;
	@Getter
	@Setter
	@NotNull
	private boolean phanTram;
	@Getter
	@Setter
	@NotNull
	@Size(max = 19)
	private Date ngayGioCapNhat;
	@Getter
	@Setter
	private List<DuongDto> duongDtos = new ArrayList<DuongDto>(0);
	@Getter
	@Setter
	private List<ChiTietGiaNuocDto> chiTietGiaNuocDtos = new ArrayList<ChiTietGiaNuocDto>(0);
	
}
