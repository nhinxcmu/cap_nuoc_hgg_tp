package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.beans.PropertyEditorSupport;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.CayDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.LichGhiThuDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.NguoiDungDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoGhiDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.VaiTroDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.LichGhiThuService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NguoiDungService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.VaiTroService;

@Controller
@RequestMapping("/lich-ghi-thu")
public class LichGhiThuController {
	private ModelAttr modelAttr = new ModelAttr("Lịch ghi thu", "lichghithu",
			new String[] { "fancytree-2.23.0/jquery.fancytree-all-deps.min.js",
					"fancytree-2.23.0/jquery.fancytree.dnd.js", "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js",
					"bower_components/select2/dist/js/select2.min.js", "js/lichghithu.js" },
			new String[] { "fancytree-2.23.0/skin-win8/ui.fancytree.min.css",
					"bower_components/select2/dist/css/select2.min.css", "jdgrid/jdgrid.css" });
	@Autowired
	private LichGhiThuService lichGhiThuServ;
	@Autowired
	private VaiTroService vaiTroServ;
	@Autowired
	private NguoiDungService nguoiDungServ;

	@InitBinder("lichGhiThuDto")
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Set.class, "soGhiDtos", new CustomSoGhiEditor());
	}

	private class CustomSoGhiEditor extends PropertyEditorSupport {

		private Set<SoGhiDto> set = new HashSet<SoGhiDto>();

		@Override
		public void setValue(Object value) {
			set = new HashSet<SoGhiDto>();
			String[] a = (String[]) value;
			for (int i = 0; i < a.length; i++) {
				SoGhiDto d = new SoGhiDto();
				d.setIdSoGhi(Long.valueOf(a[i]));
				set.add(d);
			}
		}

		@Override
		public Set<SoGhiDto> getValue() {
			return set;
		}

		@Override
		public String getAsText() {
			// TODO Auto-generated method stub
			return super.getAsText();
		}

		@Override
		public void setAsText(String text) throws IllegalArgumentException {
			set = new HashSet<SoGhiDto>();
			String[] a =text.split(",");
			for (String s:a) {
				SoGhiDto d = new SoGhiDto();
				d.setIdSoGhi(Long.valueOf(s));
				set.add(d);
			}
		}
	}

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/du-lieu-cay")
	public @ResponseBody List<CayDto> layDsCaySoGhi() {
		return lichGhiThuServ.layDsCaySoGhi();
	}

	@GetMapping("/lay-ds-vai-tro")
	public @ResponseBody List<VaiTroDto> layDsVaiTroGhiThu() {
		return vaiTroServ.layDsVaiTroGhiThu();
	}

	@GetMapping("/lay-ds-nhan-vien")
	public @ResponseBody List<NguoiDungDto> layDsNhanVienGhiThu(long idVaiTro) {
		return nguoiDungServ.layDsNhanVienGhiThu(idVaiTro);
	}

	@GetMapping("/lay-ds")
	public @ResponseBody Page<LichGhiThuDto> layDsLichGhiThu(long idVaiTro, String thongTinCanTim, Pageable page) {
		return lichGhiThuServ.layDsLichGhiThu(idVaiTro, thongTinCanTim, page);
	}

	@GetMapping("/lay-ct")
	public @ResponseBody LichGhiThuDto layLichGhiThu(long id) {
		return lichGhiThuServ.layLichGhiThu(id);
	}

	@PostMapping("/luu")
	public @ResponseBody String luu(LichGhiThuDto dto) {
		try {
			lichGhiThuServ.luuLichGhiThu(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/xoa")
	public @ResponseBody String xoa(long id) {
		try {
			lichGhiThuServ.xoaLichGhiThu(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
