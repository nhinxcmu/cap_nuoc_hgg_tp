package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.KeHoachDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Duong;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KeHoach;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;

@Component
public class KeHoachConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public KeHoachDto convertToDto(KeHoach entity) {
		if (entity == null) {
			return new KeHoachDto();
		}
		KeHoachDto dto = mapper.map(entity, KeHoachDto.class);
		return dto;
	}

	public KeHoach convertToEntity(KeHoachDto dto) {
		if (dto == null) {
			return new KeHoach();
		}
		KeHoach entity = mapper.map(dto, KeHoach.class);
		setThang(dto, entity);
		setDuong(dto, entity);
		return entity;
	}

	private void setThang(KeHoachDto dto, KeHoach entity) {
		if (dto.getIdThang() != null) {
			Thang thang = new Thang();
			thang.setIdThang(dto.getIdThang());
			entity.setThang(thang);
		}
	}

	private void setDuong(KeHoachDto dto, KeHoach entity) {
		if (dto.getIdDuong() != null) {
			Duong duong = new Duong();
			duong.setIdDuong(dto.getIdDuong());
			entity.setDuong(duong);
		}
	}

}
