package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class LichSuInDto {

	@Getter
	@Setter
	private Long idLichSuIn;
	@Getter
	@Setter
	@Mapping("nguoiDung.idNguoiDung")
	private Long idNguoiDung;
	@Getter
	@Setter
	@Mapping("nguoiDung.tenNguoiDung")
	private String tenNguoiDung;
	@Getter
	@Setter
	@Mapping("nguoiDung.hoTen")
	private String hoTen;
	@Getter
	@Setter
	private Date ngayGioIn;
}
