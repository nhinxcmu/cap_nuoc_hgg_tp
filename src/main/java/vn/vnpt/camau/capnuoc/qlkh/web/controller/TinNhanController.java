package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDeNhanTinDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.TinNhanDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HoaDonService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.TinNhanService;

@Controller
@RequestMapping("/tin-nhan")
public class TinNhanController {
	private ModelAttr modelAttr = new ModelAttr("Nhắn tin thông báo tiền nước", "tinnhan",
			new String[] { "jdgrid/jdgrid-v3.js", "bower_components/select2/dist/js/select2.min.js", "js/tinnhan.js" },
			new String[] { "jdgrid/jdgrid.css", "bower_components/select2/dist/css/select2.min.css" });
	@Autowired
	private HoaDonService hoaDonServ;
	@Autowired
	private TinNhanService tinNhanServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/lay-ds-hoa-don")
	public @ResponseBody List<HoaDonDeNhanTinDto> layDsHoaDonDeNhanTin(String thongTinCanTim, int nhanTin) {
		return hoaDonServ.layDsHoaDonDeNhanTin(thongTinCanTim, nhanTin);
	}

	@GetMapping("/lay-ds-tin-nhan")
	public @ResponseBody List<TinNhanDto> layDsTinNhan(long idHoaDon) {
		return tinNhanServ.layDsTinNhan(idHoaDon);
	}

	@PostMapping("/gui-tin-nhan")
	public @ResponseBody String themTinNhan(String strDsHoaDon) {
		try {
			List<Long> dsIdHoaDon = new ArrayList<Long>();
			for (String s : strDsHoaDon.split(","))
				dsIdHoaDon.add(Long.parseLong(s));
			String id = Integer.toString(tinNhanServ.themTinNhan(dsIdHoaDon));
			return new Response(1, id).toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
