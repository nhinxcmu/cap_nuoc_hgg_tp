package vn.vnpt.camau.capnuoc.qlkh.web.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.ThangConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.BienNhanThuTienNuocMobileDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ChiSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.NguoiDungDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.PhieuBaoHongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.PhieuSuaSeriDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.TinhTrangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.SearchForm;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ChiSoService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DuongService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HoaDonService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhachHangService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NguoiDungService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.PhieuBaoHongService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.PhieuSuaSeriService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.PhieuThanhToanService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.SoGhiService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThangService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.TinhTrangService;

@RestController
@RequestMapping("/mobile-api")
public class MobileController {
	@Autowired
	private NguoiDungService nguoiDungServ;
	@Autowired
	private DuongService duongServ;
	@Autowired
	private SoGhiService soGhiServ;
	@Autowired
	private ChiSoService chiSoServ;
	@Autowired
	private HoaDonService hoaDonServ;
	@Autowired
	private KhachHangService khachHangServ;
	@Autowired
	private PhieuThanhToanService phieuThanhToanServ;
	@Autowired
	private ThangService thangServ;
	@Autowired
	private ThangConverter thangConv;
	@Autowired
	private PhieuSuaSeriService phieuSuaSeriServ;
	@Autowired
	private PhieuBaoHongService phieuBaoHongServ;
	@Autowired
	private TinhTrangService tinhTrangServ;

	@RequestMapping("/dang-nhap")
	public @ResponseBody NguoiDungDto dangNhap() {
		return nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung());
	}

	@RequestMapping(value = "/init-data", produces = "application/json;charset=UTF-8")
	public @ResponseBody String initData() {
		NguoiDungDto nddto = nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung());
		long idNguoiDung = nddto.getIdNguoiDung();
		ObjectMapper mapper = new ObjectMapper();
		long idDuong = 0;
		try {
			String result = "{\"dsDuong\":" + mapper.writeValueAsString(duongServ.layDsDuongTheoNguoiDung(idNguoiDung))
					+ ",\"dsSoGhi\":"
					+ mapper.writeValueAsString(soGhiServ.layDsSoGhiTheoNguoiDung(idDuong, idNguoiDung))
					+ ",\"dsChiSo\":" + mapper.writeValueAsString(chiSoServ.layDsChiSoTheoNguoiDung(idNguoiDung))
					+ ",\"thang\":" + mapper.writeValueAsString(
							thangConv.convertToDto(thangServ.layThangPhanCongGhiThuMoiNhat(idNguoiDung)))
					+ "}";

			return result;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/dong-bo", produces = "application/json;charset=UTF-8")
	public @ResponseBody String dongBo(@RequestBody String strDsChiSo, boolean boQua) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
		List<ChiSoDto> dsChiSo = new ArrayList<>();
		try {
			dsChiSo = mapper.readValue(strDsChiSo, new TypeReference<List<ChiSoDto>>() {
			});
			chiSoServ.dongBoChiSo(dsChiSo, boQua);
			return new Response(1, "Đồng bộ thành công!").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/lay-ds-duong", produces = "application/json;charset=UTF-8")
	public @ResponseBody String layDsDuong(long idNguoiDung) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return "{\"dsDuong\":" + mapper.writeValueAsString(duongServ.layDsDuongTheoNguoiDung(idNguoiDung)) + "}";
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/lay-ds-so-ghi", produces = "application/json;charset=UTF-8")
	public @ResponseBody String layDsSoGhi(long idNguoiDung, long idDuong) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return "{\"dsSoGhi\":" + mapper.writeValueAsString(soGhiServ.layDsSoGhiTheoNguoiDung(idDuong, idNguoiDung))
					+ "}";
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/lay-ds-khach-hang", produces = "application/json;charset=UTF-8")
	public @ResponseBody String layDsKhachHang(long idSoGhi, String thongTinCanTim, long idNguoiDung) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return "{\"dsKhachHang\":"
					+ mapper.writeValueAsString(khachHangServ.layDsKhachHang(idSoGhi, thongTinCanTim, idNguoiDung))
					+ "}";
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/lay-ds-hoa-don", produces = "application/json;charset=UTF-8")
	public @ResponseBody String layDsHoaDon(long idThongTinKhachHang, long idNguoiDung) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			List<HoaDonDto> dsHoaDon = hoaDonServ.layDsHoaDon(idThongTinKhachHang, idNguoiDung);
			return "{\"dsHoaDon\":" + mapper.writeValueAsString(dsHoaDon) + "}";
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/thanh-toan", produces = "application/json;charset=UTF-8")
	public @ResponseBody String thanhToan(@RequestParam("dsIdHoaDon") List<Long> dsIdHoaDon, long idNguoiDung) {
		try {
			hoaDonServ.thanhToanHoaDonTaiNha(dsIdHoaDon, idNguoiDung);
			return new Response(1, "Thanh toán thành công!").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/thanh-toan-in", produces = "application/json;charset=UTF-8")
	public @ResponseBody String thanhToanIn(@RequestParam("dsIdHoaDon") List<Long> dsIdHoaDon, long idNguoiDung,
			boolean xem) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			long idPhieuThanhToan = hoaDonServ.thanhToanHoaDonTaiNha(dsIdHoaDon, idNguoiDung);
			List<BienNhanThuTienNuocMobileDto> ls = hoaDonServ.layDSBienNhanThuTienNuocMobileDto(idPhieuThanhToan, xem);
			if (ls == null)
				return new Response(-1, "Không thể in").toString();

			return mapper.writeValueAsString(ls);
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/in-bien-nhan")
	public @ResponseBody String inBienNhan(long idPhieuThanhToan, boolean xem) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			List<BienNhanThuTienNuocMobileDto> ls = hoaDonServ.layDSBienNhanThuTienNuocMobileDto(idPhieuThanhToan, xem);
			if (ls == null)
				return new Response(-1, "Không thể in").toString();

			return mapper.writeValueAsString(ls);
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/lay-ds-phieu-tt", produces = "application/json;charset=UTF-8")
	public @ResponseBody String layDsPhieuThanhToan(long idNguoiDung) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return "{\"dsPhieuThanhToan\":"
					+ mapper.writeValueAsString(phieuThanhToanServ.layDsPhieuThanhToanTheoNguoiDung(idNguoiDung, ""))
					+ "}";
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/huy-phieu-tt", produces = "application/json;charset=UTF-8")
	public @ResponseBody String huyPhieuThanhToan(long id, long idNguoiDung) {
		try {
			phieuThanhToanServ.huyPhieuThanhToan(id, idNguoiDung);
			return new Response(1, "Hủy thanh toán thành công!").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/cap-nhat-toa-do", produces = "application/json;charset=UTF-8")
	public @ResponseBody String capNhatToaDo(long idThongTinKhachHang, Double viDo, Double kinhDo) {
		try {
			khachHangServ.capNhatToaDo(idThongTinKhachHang, viDo, kinhDo);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			// e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/lay-khach-hang", produces = "application/json;charset=UTF-8")
	public @ResponseBody String layKhachHang(long idThongTinKhachHang) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(khachHangServ.layKhachHang(idThongTinKhachHang));
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/luu-chi-so-moi", produces = "application/json;charset=UTF-8")
	public @ResponseBody String luuChiSoMoi(String maKhachHang, int chiSoMoi) {
		try {
			HoaDonDto dto = hoaDonServ.luuChiSoMoi(maKhachHang, chiSoMoi);
			return new Response(1, dto.getIdHoaDon() + "").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/phat-hanh", produces = "application/json;charset=UTF-8")
	public @ResponseBody String phatHanhHoaDon(long id) {
		try {
			hoaDonServ.phatHanhHoaDon(id);
			return new Response(1, hoaDonServ.layHoaDon(id).getIdThongTinKhachHang() + "").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping(value = "/ngung-phat-hanh", produces = "application/json;charset=UTF-8")
	public @ResponseBody String ngungPhatHanhHoaDon(long id) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			hoaDonServ.ngungPhatHanhHoaDon(id);
			return new Response(1, mapper.writeValueAsString(hoaDonServ.layHoaDon(id))).toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@RequestMapping("/lay-ds-phieu-sua-seri")
	public @ResponseBody List<PhieuSuaSeriDto> layDs(@RequestBody SearchForm searchForm) {
		return phieuSuaSeriServ.layDs(Utilities.layIdNguoiDung(), searchForm.trangThai, searchForm.tuNgay,
				searchForm.denNgay);
	}

	@RequestMapping(value = "/luu-phieu-sua-seri")
	@ResponseBody
	public Response luu(String maKhachHang, String soSeriMoi, String lyDoSua, Boolean gui) {
		try {
			PhieuSuaSeriDto phieuSuaSeri = phieuSuaSeriServ.luu(maKhachHang, soSeriMoi, lyDoSua);
			if (gui) {
				guiDuyet(phieuSuaSeri.getIdPhieuSuaSeri());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

	@RequestMapping(value = "/sua-phieu-sua-seri")
	@ResponseBody
	public Response luu(Long idPhieuSuaSeri, String soSeriMoi, String lyDoSua) {
		try {
			phieuSuaSeriServ.luu(idPhieuSuaSeri, soSeriMoi, lyDoSua);

		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

	@RequestMapping(value = "/xoa-phieu-sua-seri")
	@ResponseBody
	public Response xoa(Long idPhieuSuaSeri) {
		try {
			phieuSuaSeriServ.xoa(new PhieuSuaSeriDto(idPhieuSuaSeri));
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

	@RequestMapping(value = "/gui-duyet-phieu-sua-seri")
	@ResponseBody
	public Response guiDuyet(Long idPhieuSuaSeri) {
		try {
			phieuSuaSeriServ.guiDuyet(new PhieuSuaSeriDto(idPhieuSuaSeri));
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

	@RequestMapping(value = "/tu-choi-duyet-phieu-sua-seri")
	@ResponseBody
	public Response tuChoiDuyet(Long idPhieuSuaSeri, String lyDoTuChoi) {
		try {
			PhieuSuaSeriDto phieu = new PhieuSuaSeriDto(idPhieuSuaSeri);
			phieu.setLyDoTuChoi(lyDoTuChoi);
			phieuSuaSeriServ.tuChoiDuyet(phieu);
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

	@RequestMapping(value = "/duyet-phieu-sua-seri")
	@ResponseBody
	public Response duyet(Long idPhieuSuaSeri) {
		try {
			phieuSuaSeriServ.duyet(new PhieuSuaSeriDto(idPhieuSuaSeri));
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

	@RequestMapping(value = "/lay-phieu-sua-seri")
	@ResponseBody
	public PhieuSuaSeriDto layPhieuSuaSeri(Long idPhieuSuaSeri) {
		return phieuSuaSeriServ.layPhieuSuaSeri(idPhieuSuaSeri);
	}

	// ---------------------------PHIẾU BÁO HỎNG

	@RequestMapping("/lay-ds-phieu-bao-hong")
	public @ResponseBody List<PhieuBaoHongDto> layDsPhieuBaoHong(@RequestBody SearchForm searchForm) {
		return phieuBaoHongServ.layDs(Utilities.layIdNguoiDung(), searchForm.trangThai, searchForm.tuNgay,
				searchForm.denNgay);
	}

	@RequestMapping(value = "/luu-phieu-bao-hong")
	@ResponseBody
	public Response luu(String maKhachHang, Long idTinhTrang, String moTa, Boolean gui,
			@RequestBody ImageBody base64Image) {
		try {
			PhieuBaoHongDto phieuBaoHong = phieuBaoHongServ.luu(maKhachHang, idTinhTrang, moTa,
					base64Image.getBase64Image());
			if (gui) {
				guiDuyetPhieuBaoHong(phieuBaoHong.getIdPhieuBaoHong());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

	@RequestMapping(value = "/sua-phieu-bao-hong")
	@ResponseBody
	public Response luu(Long idPhieuBaoHong, Long idTinhTrang, String moTa ,@RequestBody ImageBody base64Image) {
		try {
			phieuBaoHongServ.luu(idPhieuBaoHong, idTinhTrang, moTa, base64Image.getBase64Image());

		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

	@RequestMapping(value = "/xoa-phieu-bao-hong")
	@ResponseBody
	public Response xoaPhieuBaoHong(Long idPhieuBaoHong) {
		try {
			phieuBaoHongServ.xoa(new PhieuBaoHongDto(idPhieuBaoHong));
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

	@RequestMapping(value = "/gui-duyet-phieu-bao-hong")
	@ResponseBody
	public Response guiDuyetPhieuBaoHong(Long idPhieuBaoHong) {
		try {
			phieuBaoHongServ.guiDuyet(new PhieuBaoHongDto(idPhieuBaoHong));
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

	@RequestMapping(value = "/duyet-phieu-bao-hong")
	@ResponseBody
	public Response duyetPhieuBaoHong(Long idPhieuBaoHong) {
		try {
			phieuBaoHongServ.duyet(new PhieuBaoHongDto(idPhieuBaoHong));
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

	@RequestMapping(value = "/lay-phieu-bao-hong")
	@ResponseBody
	public PhieuBaoHongDto layPhieuBaoHong(Long idPhieuBaoHong) {
		return phieuBaoHongServ.layPhieuBaoHong(idPhieuBaoHong);
	}

	@RequestMapping(value = "/lay-ds-tinh-trang")
	@ResponseBody
	public List<TinhTrangDto> layDsTinhTrang() {
		return tinhTrangServ.layDs();
	}

}

class ImageBody {
	private String base64Image;

	public String getBase64Image() {
		return base64Image;
	}

	public void setBase64Image(String base64Image) {
		this.base64Image = base64Image;
	}
}
