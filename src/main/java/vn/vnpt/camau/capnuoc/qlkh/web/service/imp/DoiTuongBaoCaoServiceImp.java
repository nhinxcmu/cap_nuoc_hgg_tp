package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.DoiTuongBaoCaoConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DoiTuongBaoCaoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuongBaoCao;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DoiTuongBaoCaoRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DoiTuongBaoCaoService;

@Service
public class DoiTuongBaoCaoServiceImp implements DoiTuongBaoCaoService {

	@Autowired
	private DoiTuongBaoCaoConverter converter;
	@Autowired
	private DoiTuongBaoCaoRepository repository;

	@Override
	public Page<DoiTuongBaoCaoDto> layDsDoiTuongBaoCao(Pageable pageable) {
		Page<DoiTuongBaoCao> dsDoiTuongBaoCaoCanLay = repository.findByOrderByIdDoiTuongBaoCaoDesc(pageable);
		Page<DoiTuongBaoCaoDto> dsDoiTuongBaoCaoDaChuyenDoi = dsDoiTuongBaoCaoCanLay.map(converter::convertToDto);
		return dsDoiTuongBaoCaoDaChuyenDoi;
	}

	@Override
	public List<DoiTuongBaoCaoDto> layDsDoiTuongBaoCao() {
		Pageable pageable = null;
		List<DoiTuongBaoCaoDto> dsDoiTuongBaoCaoCanLay = layDsDoiTuongBaoCao(pageable).getContent().stream().sorted()
				.collect(Collectors.toList());
		return dsDoiTuongBaoCaoCanLay;
	}

	@Override
	public DoiTuongBaoCaoDto layDoiTuongBaoCao(long id) {
		DoiTuongBaoCao doiTuongBaoCaoCanLay = repository.findOne(id);
		DoiTuongBaoCaoDto doiTuongBaoCaoDaChuyenDoi = converter.convertToDto(doiTuongBaoCaoCanLay);
		return doiTuongBaoCaoDaChuyenDoi;
	}

	@Override
	public DoiTuongBaoCaoDto luuDoiTuongBaoCao(DoiTuongBaoCaoDto dto) throws Exception {
		if (dto.getIdDoiTuongBaoCao() != null) {
			kiemTraDoiTuongBaoCao(dto.getIdDoiTuongBaoCao());
		}
		try {
			DoiTuongBaoCao doiTuongBaoCaoCanLuu = converter.convertToEntity(dto);
			repository.save(doiTuongBaoCaoCanLuu);
			DoiTuongBaoCaoDto doiTuongBaoCaoDaChuyenDoi = converter.convertToDto(doiTuongBaoCaoCanLuu);
			return doiTuongBaoCaoDaChuyenDoi;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu đối tượng báo cáo.");
		}
	}

	private void kiemTraDoiTuongBaoCao(Long idDoiTuongBaoCao) throws Exception {
		DoiTuongBaoCao doiTuongBaoCaoCanKiemTra = repository.findOne(idDoiTuongBaoCao);
		if (doiTuongBaoCaoCanKiemTra == null) {
			throw new Exception("Không tìm thấy đối tượng báo cáo.");
		}
	}

	@Override
	public void xoaDoiTuongBaoCao(long id) throws Exception {
		kiemTraDoiTuongBaoCao(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa đối tượng báo cáo.");
			}
			throw new Exception(e.getMessage());
		}
	}

}
