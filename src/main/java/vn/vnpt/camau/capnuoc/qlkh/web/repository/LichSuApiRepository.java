package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuApi;

public interface LichSuApiRepository extends CrudRepository<LichSuApi, Long> {
	
}
