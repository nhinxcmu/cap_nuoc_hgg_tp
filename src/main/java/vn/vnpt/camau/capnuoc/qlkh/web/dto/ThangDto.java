package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import lombok.Getter;
import lombok.Setter;

public class ThangDto {

	@Getter
	@Setter
	private long idThang;
	@Getter
	@Setter
	private String tenThang;

}
