package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.KhuVucThangConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhuVuc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhuVucThang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiThang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.KhuVucRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.KhuVucThangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucThangService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThangService;

@Service
public class KhuVucThangServiceImp implements KhuVucThangService {

	@Autowired
	private KhuVucThangRepository repository;
	@Autowired
	private KhuVucThangConverter converter;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private ThangService thangService;
	@Autowired
	private KhuVucRepository khuVucRepository;

	@Override
	public Page<KhuVucThangDto> layDsKhuVucThangTheoKhuVucLamViec(Pageable pageable) {
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		Page<KhuVucThang> dsKhuVucThangCanLay = repository
				.findByKhuVuc_IdKhuVucOrderByThang_IdThangDesc(idKhuVucLamViec, pageable);
		Page<KhuVucThangDto> dsKhuVucThangDaChuyenDoi = dsKhuVucThangCanLay.map(converter::convertToDto);
		return dsKhuVucThangDaChuyenDoi;
	}

	@Override
	public void ketChuyenKhuVucThang(long idThang) throws Exception {
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		// khu vực tháng
		KhuVucThang khuVucThang = layKhuVucThang(idThang, idKhuVucLamViec);
		if (laKhuVucThangDaKetChuyen(khuVucThang)) {
			throw new Exception("Khu vực tháng đã kết chuyển.");
		}
		// kiểm tra ngày giờ kết chuyển của tháng trước
		// TODO
		if (laKhuVucThangVuaKetChuyen(Utilities.layThangTruoc(idThang), idKhuVucLamViec)) {
			throw new Exception("Khu vực tháng vừa kết chuyển.");
		}
		// gọi store kết chuyển
		try {
			repository.ketChuyenDuLieu(idThang, idKhuVucLamViec, idNguoiDungHienHanh);
			ThangDto thangLamViec = thangService.layThang(idThang);
			httpSession.setAttribute("thang", thangLamViec);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể kết chuyển dữ liệu.");
		}
	}

	@Override
	@Transactional
	public void ketChuyenTatCa() throws Exception {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();

		try {
			List<KhuVuc> khuVucs = khuVucRepository.findByOrderByTenKhuVuc();
			for (KhuVuc khuVuc : khuVucs) {
				long idKhuVuc = khuVuc.getIdKhuVuc();
				// khu vực tháng
				KhuVucThang khuVucThang = layKhuVucThang(idThangLamViec, idKhuVuc);
				if (laKhuVucThangDaKetChuyen(khuVucThang)) {
					continue;
				}
				// kiểm tra ngày giờ kết chuyển của tháng trước
				if (laKhuVucThangVuaKetChuyen(Utilities.layThangTruoc(idThangLamViec), idKhuVuc)) {
					continue;
				}
				// gọi store kết chuyển
				repository.ketChuyenDuLieu(idThangLamViec, idKhuVuc, idNguoiDungHienHanh);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể kết chuyển dữ liệu.");
		}
	}

	private KhuVucThang layKhuVucThang(long idThang, long idKhuVuc) throws Exception {
		KhuVucThang khuVucThang = repository.findByThang_IdThangAndKhuVuc_IdKhuVuc(idThang, idKhuVuc);
		if (khuVucThang == null) {
			throw new Exception("Không tìm thấy khu vực tháng.");
		}
		return khuVucThang;
	}

	private boolean laKhuVucThangDaKetChuyen(KhuVucThang khuVucThang) {
		long idTrangThaiThang = khuVucThang.getTrangThaiThang().getIdTrangThaiThang();
		return idTrangThaiThang == Utilities.ID_TRANG_THAI_DA_KET_CHUYEN;
	}

	private boolean laKhuVucThangVuaKetChuyen(long idThang, long idKhuVuc) {
		KhuVucThang khuVucThang = repository.findByThang_IdThangAndKhuVuc_IdKhuVuc(idThang, idKhuVuc);
		if (khuVucThang == null || khuVucThang.getNgayGioKetChuyen() == null) {
			return false;
		}
		return (new Date()).getTime() - khuVucThang.getNgayGioKetChuyen().getTime() < 3600000; // thời gian cập nhật
																								// cách nhau 1 giờ
	}

	@Override
	public void khoiPhucKhuVucThangDangLamViec(long idThang) throws Exception {
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		// lấy khu vực tháng
		KhuVucThang khuVucThang = layKhuVucThang(idThang, idKhuVucLamViec);
		if (laKhuVucThangDangLamViec(khuVucThang)) {
			throw new Exception("Khu vực tháng đang làm việc.");
		}
		TrangThaiThang trangThaiThang = new TrangThaiThang();
		trangThaiThang.setIdTrangThaiThang(Utilities.ID_TRANG_THAI_DANG_LAM_VIEC);
		khuVucThang.setTrangThaiThang(trangThaiThang);
		// cập nhật khu vực tháng
		try {
			repository.save(khuVucThang);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể khôi phục đang làm việc.");
		}
	}

	private boolean laKhuVucThangDangLamViec(KhuVucThang khuVucThang) {
		long idTrangThaiThang = khuVucThang.getTrangThaiThang().getIdTrangThaiThang();
		return idTrangThaiThang == Utilities.ID_TRANG_THAI_DANG_LAM_VIEC;
	}

	@Override
	public KhuVucThangDto layKhuVucThangLamViec() {
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		KhuVucThang khuVucThangCanLay = repository.findByThang_IdThangAndKhuVuc_IdKhuVuc(idThangLamViec,
				idKhuVucLamViec);
		KhuVucThangDto khuVucThangDaChuyenDoi = converter.convertToDto(khuVucThangCanLay);
		return khuVucThangDaChuyenDoi;
	}

}
