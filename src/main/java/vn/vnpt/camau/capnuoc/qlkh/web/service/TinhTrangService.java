package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.TinhTrangDto;

public interface TinhTrangService {
    List<TinhTrangDto> layDs();
}
