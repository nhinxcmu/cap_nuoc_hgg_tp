package vn.vnpt.camau.capnuoc.qlkh.web.webservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.ConvertForStoreFkey;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.ConvertForStoreFkeyResponse;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.DownloadInvPDFFkey;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.DownloadInvPDFFkeyNoPay;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.DownloadInvPDFFkeyNoPayResponse;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.DownloadInvPDFFkeyResponse;

public class PortalServiceClient extends WebServiceGatewaySupport {

	@Autowired
	private ThamSoService thamSoService;

	public ConvertForStoreFkeyResponse convertForStoreFkey(String fkey) {
		ConvertForStoreFkey request = new ConvertForStoreFkey();
		
		request.setUserName(thamSoService.layUsername());
		request.setUserPass(thamSoService.layPassword());
		request.setFkey(fkey);
		
		String soapAction = Utilities.NAMESPACE + "convertForStoreFkey";
		ConvertForStoreFkeyResponse response = (ConvertForStoreFkeyResponse) getWebServiceTemplate()
				.marshalSendAndReceive(thamSoService.layUriPortalService(), request, new SoapActionCallback(soapAction));
		
		return response;
	}
	public DownloadInvPDFFkeyResponse downloadInvPDFFkey (String fkey){
		DownloadInvPDFFkey request = new DownloadInvPDFFkey();
		request.setUserName(thamSoService.layUsername());
		request.setUserPass(thamSoService.layPassword());
		request.setFkey(fkey);
		String soapAction = Utilities.NAMESPACE + "downloadInvPDFFkey";
		DownloadInvPDFFkeyResponse response = (DownloadInvPDFFkeyResponse) getWebServiceTemplate()
				.marshalSendAndReceive(thamSoService.layUriPortalService(), request, new SoapActionCallback(soapAction));
		return response;
	}
	public DownloadInvPDFFkeyNoPayResponse downloadInvPDFNoPayFkey (String fkey){
		DownloadInvPDFFkeyNoPay request = new DownloadInvPDFFkeyNoPay();
		request.setUserName(thamSoService.layUsername());
		request.setUserPass(thamSoService.layPassword());
		request.setFkey(fkey);
		String soapAction = Utilities.NAMESPACE + "downloadInvPDFFkeyNoPay";
		DownloadInvPDFFkeyNoPayResponse response = (DownloadInvPDFFkeyNoPayResponse) getWebServiceTemplate()
				.marshalSendAndReceive(thamSoService.layUriPortalService(), request, new SoapActionCallback(soapAction));
		return response;
	}
	
}
