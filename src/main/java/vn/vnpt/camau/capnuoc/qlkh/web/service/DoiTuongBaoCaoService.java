package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.DoiTuongBaoCaoDto;

public interface DoiTuongBaoCaoService {

	Page<DoiTuongBaoCaoDto> layDsDoiTuongBaoCao(Pageable pageable);
	
	List<DoiTuongBaoCaoDto> layDsDoiTuongBaoCao();
	
	DoiTuongBaoCaoDto layDoiTuongBaoCao(long id);
	
	DoiTuongBaoCaoDto luuDoiTuongBaoCao(DoiTuongBaoCaoDto dto) throws Exception;
	
	void xoaDoiTuongBaoCao(long id) throws Exception;

}
