package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDienTuLapDatDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDonDienTuLapDat;


@Component
public class HoaDonDienTuLapDatConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public HoaDonDienTuLapDatDto convertToDto(HoaDonDienTuLapDat entity) {
		if (entity == null) {
			return new HoaDonDienTuLapDatDto();
		}
		HoaDonDienTuLapDatDto dto = mapper.map(entity, HoaDonDienTuLapDatDto.class);
		return dto;
	}
	public HoaDonDienTuLapDat convertToEntity(HoaDonDienTuLapDatDto dto) {
		HoaDonDienTuLapDat entity = mapper.map(dto, HoaDonDienTuLapDat.class);
		return entity;
	}
}
