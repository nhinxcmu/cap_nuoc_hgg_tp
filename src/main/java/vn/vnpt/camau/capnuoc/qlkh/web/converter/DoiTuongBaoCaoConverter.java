package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.DoiTuongBaoCaoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuongBaoCao;

@Component
public class DoiTuongBaoCaoConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public DoiTuongBaoCaoDto convertToDto(DoiTuongBaoCao entity) {
		if (entity == null) {
			return new DoiTuongBaoCaoDto();
		}
		DoiTuongBaoCaoDto dto = mapper.map(entity, DoiTuongBaoCaoDto.class);
		return dto;
	}

	public DoiTuongBaoCao convertToEntity(DoiTuongBaoCaoDto dto) {
		if (dto == null) {
			return new DoiTuongBaoCao();
		}
		DoiTuongBaoCao entity = mapper.map(dto, DoiTuongBaoCao.class);
		return entity;
	}

}
