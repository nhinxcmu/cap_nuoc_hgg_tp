package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.DoiTuongDto;

public interface DoiTuongService {

	Page<DoiTuongDto> layDsDoiTuong(Pageable pageable);
	
	List<DoiTuongDto> layDsDoiTuong();
	
	DoiTuongDto layDoiTuong(long id);
	
	DoiTuongDto luuDoiTuong(DoiTuongDto dto) throws Exception;
	
	void xoaDoiTuong(long id) throws Exception;

}
