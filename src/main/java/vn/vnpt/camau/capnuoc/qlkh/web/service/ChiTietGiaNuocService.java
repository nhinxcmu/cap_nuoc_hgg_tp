package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.math.BigDecimal;
import java.util.List;

public interface ChiTietGiaNuocService {

	List<BigDecimal> layDsMucGia(long idSoGhi) throws Exception;
	
}
