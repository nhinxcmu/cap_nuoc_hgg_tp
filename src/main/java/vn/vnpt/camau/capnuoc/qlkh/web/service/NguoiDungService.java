package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.NguoiDungDto;

public interface NguoiDungService {

	boolean laNguoiDungThuocXiNghiep();

	boolean laNguoiDungThuocXiNghiep(long idNguoiDung);
	
	List<NguoiDungDto> layDsNguoiDungThuocXiNghiep();

	Page<NguoiDungDto> layDsNguoiDung(long idDonVi, String thongTinCanTim, Pageable pageable);
	
	List<NguoiDungDto> layDsNguoiDung(long idDonVi);
	
	NguoiDungDto layNguoiDung(long id);
	
	NguoiDungDto luuNguoiDung(NguoiDungDto dto) throws Exception;
	
	void xoaNguoiDung(long id) throws Exception;
	
	void datLaiMatKhauCuaNguoiDung(long id) throws Exception;
	
	NguoiDungDto khoaNguoiDung(long id) throws Exception;
	
	NguoiDungDto moKhoaNguoiDung(long id) throws Exception;
	
	NguoiDungDto capNhatThongTinNguoiDung(NguoiDungDto dto) throws Exception;
	
	void doiMatKhauCuaNguoiDung(String matKhauCu, String matKhauMoi) throws Exception;
	
	List<NguoiDungDto> layDsNhanVienGhiThu(long idVaiTro);
	
	List<NguoiDungDto> layDsNhanVienGhiThu();
	
	List<NguoiDungDto> layDsNhanVienThanhToanGiaoDich();
	
}
