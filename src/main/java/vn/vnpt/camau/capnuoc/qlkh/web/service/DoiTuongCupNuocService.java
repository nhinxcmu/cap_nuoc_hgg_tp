package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.DoiTuongCupNuocDto;

public interface DoiTuongCupNuocService {

	List<DoiTuongCupNuocDto> layDsDoiTuongCupNuoc();
}
