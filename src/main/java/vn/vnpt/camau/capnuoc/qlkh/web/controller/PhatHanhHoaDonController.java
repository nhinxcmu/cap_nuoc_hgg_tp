package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThamSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HoaDonService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.SoLieuService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;

@Controller
@RequestMapping("/phat-hanh-hoa-don")
public class PhatHanhHoaDonController {
	private ModelAttr modelAttr = new ModelAttr("Phát hành hóa đơn", "phathanhhoadon",
			new String[] { "js/phathanhhoadon.js" }, new String[] {});
	@Autowired
	private SoLieuService soLieuServ;
	@Autowired
	private HoaDonService hoaDonServ;
	@Autowired
	private ThamSoService thamSoServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/lay-so-luong")
	public @ResponseBody String thongKePhatHanhHoaDon() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(soLieuServ.thongKePhatHanhHoaDon());
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/luu")
	public @ResponseBody String chuyenDuLieuChiSo(boolean phatHanh) {
		ThamSoDto thamSo = thamSoServ.layThamSo(30);
		try {
			if (thamSo.getGiaTri().equals("False")) {
				thamSo.setGiaTri("True");
				thamSoServ.luuThamSo(thamSo);

				if (phatHanh)
					hoaDonServ.phatHanhHoaDonHangLoat();
				else
					hoaDonServ.ngungPhatHanhHoaDonHangLoat();

				thamSo.setGiaTri("False");
				thamSoServ.luuThamSo(thamSo);

				return new Response(1, "Success").toString();
			} else
				return new Response(-1, "Hệ thống đang bận").toString();
		} catch (Exception e) {
			thamSo.setGiaTri("False");
			try {
				thamSoServ.luuThamSo(thamSo);
			} catch (Exception e1) {
				return new Response(-1, e.getMessage()).toString();
			}
			return new Response(-1, e.getMessage()).toString();
		}

	}
}
