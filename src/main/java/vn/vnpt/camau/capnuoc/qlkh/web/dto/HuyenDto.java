package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

public class HuyenDto implements Comparable<HuyenDto> {

	@Getter
	@Setter
	@NotNull
	private Long idHuyen;
	@Getter
	@Setter
	@NotNull
	@Size(max = 250)
	private String tenHuyen;
	
	@Override
	public int compareTo(HuyenDto object) {
		return this.tenHuyen.compareTo(object.getTenHuyen());
	}
}
