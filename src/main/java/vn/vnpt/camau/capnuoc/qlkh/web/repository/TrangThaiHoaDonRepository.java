package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiHoaDon;

public interface TrangThaiHoaDonRepository extends Repository<TrangThaiHoaDon, Long> {

	List<TrangThaiHoaDon> findAll();
}
