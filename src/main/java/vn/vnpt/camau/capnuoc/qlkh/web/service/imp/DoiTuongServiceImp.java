package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.DoiTuongConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DoiTuongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuong;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DoiTuongRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DoiTuongService;

@Service
public class DoiTuongServiceImp implements DoiTuongService {

	@Autowired
	private DoiTuongConverter converter;
	@Autowired
	private DoiTuongRepository repository;

	@Override
	public Page<DoiTuongDto> layDsDoiTuong(Pageable pageable) {
		Page<DoiTuong> dsDoiTuongCanLay = repository.findByOrderByIdDoiTuongDesc(pageable);
		Page<DoiTuongDto> dsDoiTuongDaChuyenDoi = dsDoiTuongCanLay.map(converter::convertToDto);
		return dsDoiTuongDaChuyenDoi;
	}

	@Override
	public List<DoiTuongDto> layDsDoiTuong() {
		Pageable pageable = null;
		List<DoiTuongDto> dsDoiTuongCanLay = layDsDoiTuong(pageable).getContent().stream().sorted()
				.collect(Collectors.toList());
		return dsDoiTuongCanLay;
	}

	@Override
	public DoiTuongDto layDoiTuong(long id) {
		DoiTuong doiTuongCanLay = repository.findOne(id);
		DoiTuongDto doiTuongDaChuyenDoi = converter.convertToDto(doiTuongCanLay);
		return doiTuongDaChuyenDoi;
	}

	@Override
	public DoiTuongDto luuDoiTuong(DoiTuongDto dto) throws Exception {
		if (dto.getIdDoiTuong() == null) {
			if (laMaDoiTuongDaTonTai(dto.getIdDoiTuong(), dto.getMaDoiTuong())) {
				throw new Exception("Mã đối tượng đã tồn tại.");
			}
		} else {
			kiemTraDoiTuong(dto.getIdDoiTuong());
		}
		try {
			DoiTuong doiTuongCanLuu = converter.convertToEntity(dto);
			repository.save(doiTuongCanLuu);
			DoiTuongDto doiTuongDaChuyenDoi = converter.convertToDto(doiTuongCanLuu);
			return doiTuongDaChuyenDoi;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu đối tượng.");
		}
	}

	private boolean laMaDoiTuongDaTonTai(Long idDoiTuong, String maDoiTuong) {
		DoiTuong doiTuongCanKiemTra = repository.findByMaDoiTuong(maDoiTuong);
		if (doiTuongCanKiemTra == null) {
			return false;
		}
		return true;
	}

	private void kiemTraDoiTuong(Long idDoiTuong) throws Exception {
		DoiTuong doiTuongCanKiemTra = repository.findOne(idDoiTuong);
		if (doiTuongCanKiemTra == null) {
			throw new Exception("Không tìm thấy đối tượng.");
		}
	}

	@Override
	public void xoaDoiTuong(long id) throws Exception {
		kiemTraDoiTuong(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa đối tượng.");
			}
			throw new Exception(e.getMessage());
		}
	}

}
