package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.TinNhan;

public interface TinNhanRepository extends CrudRepository<TinNhan, Long> {

	List<TinNhan> findByHoaDon_IdHoaDonOrderByIdTinNhanDesc(Long idHoaDon);
}
