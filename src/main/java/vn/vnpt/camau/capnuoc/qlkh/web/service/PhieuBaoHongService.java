package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.PhieuBaoHongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.FileBean;

public interface PhieuBaoHongService {

    Page<PhieuBaoHongDto> layDs(Long idNguoiDung, int trangThai, Date tuNgay, Date denNgay, Pageable pageable);

    List<PhieuBaoHongDto> layDs(Long idNguoiDung, int trangThai, Date tuNgay, Date denNgay);

    PhieuBaoHongDto luu(PhieuBaoHongDto phieuBaoHong) throws Exception;

    PhieuBaoHongDto luu(String maKhachHang, Long idTinhTrang, String moTa, String hinhAnh) throws Exception;

    PhieuBaoHongDto luu(Long idPhieuBaoHong, Long idTinhTrang, String moTa, String hinhAnh) throws Exception;

    PhieuBaoHongDto guiDuyet(PhieuBaoHongDto phieuBaoHong) throws Exception;

    // PhieuBaoHongDto tuChoiDuyet(PhieuBaoHongDto phieuBaoHong) throws Exception;

    PhieuBaoHongDto duyet(PhieuBaoHongDto phieuBaoHong) throws Exception;

    int xoa(PhieuBaoHongDto phieuBaoHong) throws Exception;

    String importChiSo(FileBean fileBean);

    // String nhacViecNhanVien();

    String nhacViecPhongKeHoach();

    PhieuBaoHongDto layPhieuBaoHong(Long idPhieuBaoHong);
}