package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.KetQuaHddt;

public interface KetQuaHddtRepository extends CrudRepository<KetQuaHddt, Long> {

}
