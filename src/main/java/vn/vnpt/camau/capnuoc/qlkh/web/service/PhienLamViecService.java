package vn.vnpt.camau.capnuoc.qlkh.web.service;

public interface PhienLamViecService {

	void thietLapPhienLamViec(long idNguoiDung);
	
	void thietLapPhienLamViec(long idDonViLamViec, long idKhucVucLamViec, long idThangLamViec);
}
