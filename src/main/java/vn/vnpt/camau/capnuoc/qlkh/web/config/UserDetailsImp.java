package vn.vnpt.camau.capnuoc.qlkh.web.config;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Menu;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;

@SuppressWarnings("serial")
@AllArgsConstructor
public class UserDetailsImp implements UserDetails {

	private NguoiDung nguoiDung;
	private Set<Menu> menus = new HashSet<Menu>();

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		for (Menu menu : menus) {
			authorities.add(new SimpleGrantedAuthority(menu.getUrl()));
		}
		return authorities;
	}

	public NguoiDung getNguoiDung(){
		return nguoiDung;
	}

	@Override
	public String getPassword() {
		return nguoiDung.getMatKhau();
	}

	@Override
	public String getUsername() {
		return nguoiDung.getTenNguoiDung();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return !nguoiDung.isKhoa();
	}

	public Long getId() {
		return nguoiDung.getIdNguoiDung();
	}

	public String getFullname() {
		return nguoiDung.getHoTen();
	}

}
