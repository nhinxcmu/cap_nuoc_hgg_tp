package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoGhiDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Duong;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.SoGhi;

@Component
public class SoGhiConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public SoGhiDto convertToDto(SoGhi entity) {
		if (entity == null) {
			return new SoGhiDto();
		}
		SoGhiDto dto = mapper.map(entity, SoGhiDto.class);
		return dto;
	}
	
	public SoGhi convertToEntity(SoGhiDto dto) {
		if (dto == null) {
			return new SoGhi();
		}
		SoGhi entity = mapper.map(dto, SoGhi.class);
		setDuong(dto, entity);
		return entity;
	}

	private void setDuong(SoGhiDto dto, SoGhi entity) {
		if (dto.getIdDuong() != null) {
			Duong duong = new Duong();
			duong.setIdDuong(dto.getIdDuong());
			entity.setDuong(duong);
		}
	}

}
