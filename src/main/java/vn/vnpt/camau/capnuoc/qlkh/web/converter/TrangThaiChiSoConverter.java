package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.TrangThaiChiSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiChiSo;

@Component
public class TrangThaiChiSoConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public TrangThaiChiSoDto convertToDto(TrangThaiChiSo entity) {
		if (entity == null) {
			return new TrangThaiChiSoDto();
		}
		TrangThaiChiSoDto dto = mapper.map(entity, TrangThaiChiSoDto.class);
		return dto;
	}

}
