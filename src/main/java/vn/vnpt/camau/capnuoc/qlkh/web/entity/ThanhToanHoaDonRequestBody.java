package vn.vnpt.camau.capnuoc.qlkh.web.entity;

public class ThanhToanHoaDonRequestBody {
    private String[] idHoaDons;

    public String[] getIdHoaDons() {
        return idHoaDons;
    }

    public void setIdHoaDons(String[] idHoaDons) {
        this.idHoaDons = idHoaDons;
    }
}
