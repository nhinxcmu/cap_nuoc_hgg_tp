package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.KhuVucConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhuVuc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhuVucThang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhuVucThangId;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiThang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DuongRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.KhuVucRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.KhuVucThangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucService;

@Service
public class KhuVucServiceImp implements KhuVucService {

	@Autowired
	private KhuVucConverter converter;
	@Autowired
	private KhuVucRepository repository;
	@Autowired
	private KhuVucThangRepository khuVucThangRepository;
	@Autowired
	private ThangRepository thangRepository;
	@Autowired
	private DuongRepository duongRepository;
	@Autowired
	private HttpSession httpSession;

	@Override
	public KhuVucDto layKhuVucLamViec(long idDonVi) {
		KhuVuc khuVucCanLay = repository.findTopByDonVi_IdDonViOrderByTenKhuVuc(idDonVi);
		KhuVucDto khuVucDaChuyenDoi = converter.convertToDto(khuVucCanLay);
		return khuVucDaChuyenDoi;
	}

	@Override
	public List<KhuVucDto> layDsKhuVucLamViec(long idDonVi) {
		List<KhuVuc> dsKhuVucCanLay = repository.findByDonVi_IdDonViOrderByTenKhuVuc(idDonVi);
		List<KhuVucDto> dsKhuVucDaChuyenDoi = dsKhuVucCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsKhuVucDaChuyenDoi;
	}

	@Override
	public Page<KhuVucDto> layDsKhuVuc(long idDonVi, Pageable pageable) {
		Page<KhuVuc> dsKhuVucCanLay;
		if (idDonVi == 0) {
			dsKhuVucCanLay = repository.findByOrderByIdKhuVucDesc(pageable);
		} else {
			dsKhuVucCanLay = repository.findByDonVi_IdDonViOrderByIdKhuVucDesc(idDonVi, pageable);
		}
		Page<KhuVucDto> dsKhuVucDaChuyenDoi = dsKhuVucCanLay.map(converter::convertToDto);
		return dsKhuVucDaChuyenDoi;
	}

	@Override
	public List<KhuVucDto> layDsKhuVuc(long idDonVi) {
		Pageable pageable = null;
		List<KhuVucDto> dsKhuVucCanLay = layDsKhuVuc(idDonVi, pageable).getContent().stream().sorted().collect(Collectors.toList());
		return dsKhuVucCanLay;
	}

	@Override
	public KhuVucDto layKhuVuc(long id) {
		KhuVuc khuVucCanLay = repository.findByIdKhuVuc(id);
		KhuVucDto khuVucDaChuyenDoi = converter.convertToDto(khuVucCanLay);
		return khuVucDaChuyenDoi;
	}

	@Override
	@Transactional
	public KhuVucDto luuKhuVuc(KhuVucDto dto) throws Exception {
		kiemTraKhuVuc(dto.getIdKhuVuc());
		KhuVuc khuVucCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(khuVucCanLuu);
			if (dto.getIdKhuVuc() == null) {
				themKhuVucThang(khuVucCanLuu);
			} else {
				capNhatKhuVucThang(khuVucCanLuu);
			}
			KhuVucDto khuVucDaChuyenDoi = converter.convertToDto(khuVucCanLuu);
			return khuVucDaChuyenDoi;			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu khu vực.");
		}
	}

	private void kiemTraKhuVuc(Long idKhuVuc) throws Exception {
		if (idKhuVuc != null) {
			KhuVuc khuVucCanKiemTra = repository.findOne(idKhuVuc);
			if (khuVucCanKiemTra == null) {
				throw new Exception("Không tìm thấy khu vực.");
			}
		}
	}

	private void themKhuVucThang(KhuVuc khuVuc) {
		KhuVucThang khuVucThangCanThem = new KhuVucThang();
		KhuVucThangId id = new KhuVucThangId();
		id.setIdKhuVuc(khuVuc.getIdKhuVuc());
		Thang thangMoiNhat = layThangMoiNhat();
		id.setIdThang(thangMoiNhat.getIdThang());
		khuVucThangCanThem.setId(id);
		khuVucThangCanThem.setKhuVuc(khuVuc);
		khuVucThangCanThem.setThang(thangMoiNhat);
		TrangThaiThang trangThaiThangDangLamViec = taoTrangThaiThangDangLamViec();
		khuVucThangCanThem.setTrangThaiThang(trangThaiThangDangLamViec);
		String tuNgay = layTuNgay(khuVuc.getNgayBatDau(), thangMoiNhat.getIdThang());
		khuVucThangCanThem.setTuNgay(tuNgay);
		String denNgay = layDenNgay(khuVuc.getNgayKetThuc(), thangMoiNhat.getTenThang());
		khuVucThangCanThem.setDenNgay(denNgay);
		khuVucThangRepository.save(khuVucThangCanThem);
	}

	private Thang layThangMoiNhat() {
		Thang thangCanLay = thangRepository.findTopByOrderByIdThangDesc();
		return thangCanLay;
	}

	private TrangThaiThang taoTrangThaiThangDangLamViec() {
		TrangThaiThang trangThaiThang = new TrangThaiThang();
		trangThaiThang.setIdTrangThaiThang(Utilities.ID_TRANG_THAI_DANG_LAM_VIEC);
		return trangThaiThang;
	}

	private String layTuNgay(String ngayBatDau, long idThang) {
		long thangTruoc = Utilities.layThangTruoc(idThang);
		String thangHienThi = Utilities.layThangHienThi(thangTruoc);
		return ngayBatDau + "/" + thangHienThi;
	}

	private String layDenNgay(String ngayKetThuc, String tenThang) {
		return ngayKetThuc + "/" + tenThang;
	}

	private void capNhatKhuVucThang(KhuVuc khuVuc) throws Exception {
		KhuVucThang khuVucThangCanCapNhat = khuVucThangRepository.findTopByKhuVuc_IdKhuVucOrderByThang_IdThangDesc(khuVuc.getIdKhuVuc());
		if (khuVucThangCanCapNhat == null) {
			throw new Exception("Không tìm thấy khu vực tháng.");
		}
		Thang thangHienHanhCuaKhuVuc = khuVucThangCanCapNhat.getThang();
		String tuNgay = layTuNgay(khuVuc.getNgayBatDau(), thangHienHanhCuaKhuVuc.getIdThang());
		khuVucThangCanCapNhat.setTuNgay(tuNgay);
		String denNgay = layDenNgay(khuVuc.getNgayKetThuc(), thangHienHanhCuaKhuVuc.getTenThang());
		khuVucThangCanCapNhat.setDenNgay(denNgay);
		khuVucThangRepository.save(khuVucThangCanCapNhat);
	}

	@Override
	@Transactional
	public void xoaKhuVuc(long id) throws Exception {
		kiemTraKhuVuc(id);
		kiemTraDuong(id);
		try {
			khuVucThangRepository.deleteByKhuVuc_IdKhuVuc(id);
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa khu vực.");
			}
			throw new Exception(e.getMessage());
		}
	}

	private void kiemTraDuong(long id) throws Exception {
		long soLuongDuongTheoKhuVuc = duongRepository.countByKhuVuc_IdKhuVuc(id);
		if (soLuongDuongTheoKhuVuc > 0) {
			throw new Exception("Khu vực đã thêm phường nên không thể xóa.");
		}
	}

	@Override
	public boolean laKhuVucSuDungHDDT() {
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		return repository.findOne(idKhuVucLamViec).isSuDungHddt();
	}

	@Override
	public boolean laKhuVucSuDungHDDT(Long idKhuVuc) {
		return repository.findOne(idKhuVuc).isSuDungHddt();
	}

}
