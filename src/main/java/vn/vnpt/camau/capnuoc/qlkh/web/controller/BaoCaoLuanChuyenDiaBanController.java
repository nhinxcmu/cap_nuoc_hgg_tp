package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.NguoiDungDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.CallReportService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NguoiDungService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;

@Controller
@RequestMapping("/bao-cao-luan-chuyen-dia-ban")
public class BaoCaoLuanChuyenDiaBanController {
	private ModelAttr modelAttr = new ModelAttr("Báo cáo luân chuyển địa bàn", "baocaoluanchuyendiaban",
			new String[] { "bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect.js",
					"js/baocaoluanchuyendiaban.js", "js/pdfobject.js" },
			new String[] { "bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css" });
	@Autowired
	DataSource dataSource;
	@Autowired
	private CallReportService callReportServ;
	@Autowired
	private NguoiDungService nguoiDungServ;
	@Autowired
	private ThamSoService thamSoServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/lay-ds-nhan-vien")
	public @ResponseBody List<NguoiDungDto> layDsNhanVienGhiThu() {
		return nguoiDungServ.layDsNhanVienGhiThu();
	}

	@RequestMapping(value = "/xem-bao-cao", method = RequestMethod.GET)
	public @ResponseBody String xemBaoCao(HttpSession httpSession, String nhanvien) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/baocaoluanchuyendiaban.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("baocaoluanchuyendiaban", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String fileName = reportFile.getPath();
		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		if (nhanvien.equals(""))
			return "";
		long thang1 = Utilities.layThangLamViec(httpSession).getIdThang();
		long thang2 = Utilities.layThangTruoc(thang1);
		long thang3 = Utilities.layThangTruoc(thang2);
		long thang4 = Utilities.layThangTruoc(thang3);
		long thang5 = Utilities.layThangTruoc(thang4);
		long thang6 = Utilities.layThangTruoc(thang5);
		long thang7 = Utilities.layThangTruoc(thang6);
		long thang8 = Utilities.layThangTruoc(thang7);
		long thang9 = Utilities.layThangTruoc(thang8);
		long thang10 = Utilities.layThangTruoc(thang9);
		long thang11 = Utilities.layThangTruoc(thang10);
		long thang12 = Utilities.layThangTruoc(thang11);
		for (String s : nhanvien.split(",")) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("idnhanvien", Long.valueOf(s));
			parameters.put("tennhanvien", nguoiDungServ.layNguoiDung(Long.valueOf(s)).getHoTen());
			parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
			parameters.put("thang1", thang1 % 100 + "/" + thang1 / 100);
			parameters.put("thang2", thang2 % 100 + "/" + thang2 / 100);
			parameters.put("thang3", thang3 % 100 + "/" + thang3 / 100);
			parameters.put("thang4", thang4 % 100 + "/" + thang4 / 100);
			parameters.put("thang5", thang5 % 100 + "/" + thang5 / 100);
			parameters.put("thang6", thang6 % 100 + "/" + thang6 / 100);
			parameters.put("thang7", thang7 % 100 + "/" + thang7 / 100);
			parameters.put("thang8", thang8 % 100 + "/" + thang8 / 100);
			parameters.put("thang9", thang9 % 100 + "/" + thang9 / 100);
			parameters.put("thang10", thang10 % 100 + "/" + thang10 / 100);
			parameters.put("thang11", thang11 % 100 + "/" + thang11 / 100);
			parameters.put("thang12", thang12 % 100 + "/" + thang12 / 100);
			parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
			parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
			parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
			Connection conn = dataSource.getConnection();
			JasperPrint print = JasperFillManager.fillReport(fileName, parameters, conn);
			conn.close();
			jasperPrintList.add(print);
		}
		return callReportServ.viewListReport(jasperPrintList, reportFile, httpSession);
	}

	@RequestMapping(value = "/xuat-file-pdf", method = RequestMethod.GET)
	public @ResponseBody void xuatFilePDF(HttpServletResponse response, HttpSession httpSession, String nhanvien)
			throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/baocaoluanchuyendiaban.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("baocaoluanchuyendiaban", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String fileName = reportFile.getPath();
		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		if (nhanvien.equals(""))
			return;
		long thang1 = Utilities.layThangLamViec(httpSession).getIdThang();
		long thang2 = Utilities.layThangTruoc(thang1);
		long thang3 = Utilities.layThangTruoc(thang2);
		long thang4 = Utilities.layThangTruoc(thang3);
		long thang5 = Utilities.layThangTruoc(thang4);
		long thang6 = Utilities.layThangTruoc(thang5);
		long thang7 = Utilities.layThangTruoc(thang6);
		long thang8 = Utilities.layThangTruoc(thang7);
		long thang9 = Utilities.layThangTruoc(thang8);
		long thang10 = Utilities.layThangTruoc(thang9);
		long thang11 = Utilities.layThangTruoc(thang10);
		long thang12 = Utilities.layThangTruoc(thang11);
		for (String s : nhanvien.split(",")) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("idnhanvien", Long.valueOf(s));
			parameters.put("tennhanvien", nguoiDungServ.layNguoiDung(Long.valueOf(s)).getHoTen());
			parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
			parameters.put("thang1", thang1 % 100 + "/" + thang1 / 100);
			parameters.put("thang2", thang2 % 100 + "/" + thang2 / 100);
			parameters.put("thang3", thang3 % 100 + "/" + thang3 / 100);
			parameters.put("thang4", thang4 % 100 + "/" + thang4 / 100);
			parameters.put("thang5", thang5 % 100 + "/" + thang5 / 100);
			parameters.put("thang6", thang6 % 100 + "/" + thang6 / 100);
			parameters.put("thang7", thang7 % 100 + "/" + thang7 / 100);
			parameters.put("thang8", thang8 % 100 + "/" + thang8 / 100);
			parameters.put("thang9", thang9 % 100 + "/" + thang9 / 100);
			parameters.put("thang10", thang10 % 100 + "/" + thang10 / 100);
			parameters.put("thang11", thang11 % 100 + "/" + thang11 / 100);
			parameters.put("thang12", thang12 % 100 + "/" + thang12 / 100);
			parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
			parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
			parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
			Connection conn = dataSource.getConnection();
			JasperPrint print = JasperFillManager.fillReport(fileName, parameters, conn);
			conn.close();
			jasperPrintList.add(print);
		}
		callReportServ.printListReport(jasperPrintList, "pdf", reportFile, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-xls", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLS(HttpServletResponse response, HttpSession httpSession, String nhanvien)
			throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/baocaoluanchuyendiaban.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("baocaoluanchuyendiaban", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String fileName = reportFile.getPath();
		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		if (nhanvien.equals(""))
			return;
		long thang1 = Utilities.layThangLamViec(httpSession).getIdThang();
		long thang2 = Utilities.layThangTruoc(thang1);
		long thang3 = Utilities.layThangTruoc(thang2);
		long thang4 = Utilities.layThangTruoc(thang3);
		long thang5 = Utilities.layThangTruoc(thang4);
		long thang6 = Utilities.layThangTruoc(thang5);
		long thang7 = Utilities.layThangTruoc(thang6);
		long thang8 = Utilities.layThangTruoc(thang7);
		long thang9 = Utilities.layThangTruoc(thang8);
		long thang10 = Utilities.layThangTruoc(thang9);
		long thang11 = Utilities.layThangTruoc(thang10);
		long thang12 = Utilities.layThangTruoc(thang11);
		for (String s : nhanvien.split(",")) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("idnhanvien", Long.valueOf(s));
			parameters.put("tennhanvien", nguoiDungServ.layNguoiDung(Long.valueOf(s)).getHoTen());
			parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
			parameters.put("thang1", thang1 % 100 + "/" + thang1 / 100);
			parameters.put("thang2", thang2 % 100 + "/" + thang2 / 100);
			parameters.put("thang3", thang3 % 100 + "/" + thang3 / 100);
			parameters.put("thang4", thang4 % 100 + "/" + thang4 / 100);
			parameters.put("thang5", thang5 % 100 + "/" + thang5 / 100);
			parameters.put("thang6", thang6 % 100 + "/" + thang6 / 100);
			parameters.put("thang7", thang7 % 100 + "/" + thang7 / 100);
			parameters.put("thang8", thang8 % 100 + "/" + thang8 / 100);
			parameters.put("thang9", thang9 % 100 + "/" + thang9 / 100);
			parameters.put("thang10", thang10 % 100 + "/" + thang10 / 100);
			parameters.put("thang11", thang11 % 100 + "/" + thang11 / 100);
			parameters.put("thang12", thang12 % 100 + "/" + thang12 / 100);
			parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
			parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
			parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
			Connection conn = dataSource.getConnection();
			JasperPrint print = JasperFillManager.fillReport(fileName, parameters, conn);
			conn.close();
			jasperPrintList.add(print);
		}
		callReportServ.printListReport(jasperPrintList, "xls", reportFile, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-xlsx", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLSX(HttpServletResponse response, HttpSession httpSession, String nhanvien)
			throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/baocaoluanchuyendiaban.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("baocaoluanchuyendiaban", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String fileName = reportFile.getPath();
		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		if (nhanvien.equals(""))
			return;
		long thang1 = Utilities.layThangLamViec(httpSession).getIdThang();
		long thang2 = Utilities.layThangTruoc(thang1);
		long thang3 = Utilities.layThangTruoc(thang2);
		long thang4 = Utilities.layThangTruoc(thang3);
		long thang5 = Utilities.layThangTruoc(thang4);
		long thang6 = Utilities.layThangTruoc(thang5);
		long thang7 = Utilities.layThangTruoc(thang6);
		long thang8 = Utilities.layThangTruoc(thang7);
		long thang9 = Utilities.layThangTruoc(thang8);
		long thang10 = Utilities.layThangTruoc(thang9);
		long thang11 = Utilities.layThangTruoc(thang10);
		long thang12 = Utilities.layThangTruoc(thang11);
		for (String s : nhanvien.split(",")) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("idnhanvien", Long.valueOf(s));
			parameters.put("tennhanvien", nguoiDungServ.layNguoiDung(Long.valueOf(s)).getHoTen());
			parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
			parameters.put("thang1", thang1 % 100 + "/" + thang1 / 100);
			parameters.put("thang2", thang2 % 100 + "/" + thang2 / 100);
			parameters.put("thang3", thang3 % 100 + "/" + thang3 / 100);
			parameters.put("thang4", thang4 % 100 + "/" + thang4 / 100);
			parameters.put("thang5", thang5 % 100 + "/" + thang5 / 100);
			parameters.put("thang6", thang6 % 100 + "/" + thang6 / 100);
			parameters.put("thang7", thang7 % 100 + "/" + thang7 / 100);
			parameters.put("thang8", thang8 % 100 + "/" + thang8 / 100);
			parameters.put("thang9", thang9 % 100 + "/" + thang9 / 100);
			parameters.put("thang10", thang10 % 100 + "/" + thang10 / 100);
			parameters.put("thang11", thang11 % 100 + "/" + thang11 / 100);
			parameters.put("thang12", thang12 % 100 + "/" + thang12 / 100);
			parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
			parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
			parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
			Connection conn = dataSource.getConnection();
			JasperPrint print = JasperFillManager.fillReport(fileName, parameters, conn);
			conn.close();
			jasperPrintList.add(print);
		}
		callReportServ.printListReport(jasperPrintList, "xlsx", reportFile, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-rtf", method = RequestMethod.GET)
	public @ResponseBody void xuatFileRTF(HttpServletResponse response, HttpSession httpSession, String nhanvien)
			throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/baocaoluanchuyendiaban.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("baocaoluanchuyendiaban", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String fileName = reportFile.getPath();
		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		if (nhanvien.equals(""))
			return;
		long thang1 = Utilities.layThangLamViec(httpSession).getIdThang();
		long thang2 = Utilities.layThangTruoc(thang1);
		long thang3 = Utilities.layThangTruoc(thang2);
		long thang4 = Utilities.layThangTruoc(thang3);
		long thang5 = Utilities.layThangTruoc(thang4);
		long thang6 = Utilities.layThangTruoc(thang5);
		long thang7 = Utilities.layThangTruoc(thang6);
		long thang8 = Utilities.layThangTruoc(thang7);
		long thang9 = Utilities.layThangTruoc(thang8);
		long thang10 = Utilities.layThangTruoc(thang9);
		long thang11 = Utilities.layThangTruoc(thang10);
		long thang12 = Utilities.layThangTruoc(thang11);
		for (String s : nhanvien.split(",")) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("idnhanvien", Long.valueOf(s));
			parameters.put("tennhanvien", nguoiDungServ.layNguoiDung(Long.valueOf(s)).getHoTen());
			parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
			parameters.put("thang1", thang1 % 100 + "/" + thang1 / 100);
			parameters.put("thang2", thang2 % 100 + "/" + thang2 / 100);
			parameters.put("thang3", thang3 % 100 + "/" + thang3 / 100);
			parameters.put("thang4", thang4 % 100 + "/" + thang4 / 100);
			parameters.put("thang5", thang5 % 100 + "/" + thang5 / 100);
			parameters.put("thang6", thang6 % 100 + "/" + thang6 / 100);
			parameters.put("thang7", thang7 % 100 + "/" + thang7 / 100);
			parameters.put("thang8", thang8 % 100 + "/" + thang8 / 100);
			parameters.put("thang9", thang9 % 100 + "/" + thang9 / 100);
			parameters.put("thang10", thang10 % 100 + "/" + thang10 / 100);
			parameters.put("thang11", thang11 % 100 + "/" + thang11 / 100);
			parameters.put("thang12", thang12 % 100 + "/" + thang12 / 100);
			parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
			parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
			parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
			Connection conn = dataSource.getConnection();
			JasperPrint print = JasperFillManager.fillReport(fileName, parameters, conn);
			conn.close();
			jasperPrintList.add(print);
		}
		callReportServ.printListReport(jasperPrintList, "rtf", reportFile, httpSession, response);
	}
}
