package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.MenuConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.MenuDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Menu;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.MenuRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.MenuService;

@Service
public class MenuServiceImp implements MenuService {

	@Autowired
	private MenuRepository repository;
	@Autowired
	private MenuConverter converter;

	@Override
	public List<MenuDto> layDsMenu2Cap() {
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		List<Menu> dsMenuCanLay = new ArrayList<Menu>();
		List<Menu> dsMenuCanKiemTra = repository.findByMenu_IdMenuIsNullOrderByOrderMenu();
		for (Menu menu : dsMenuCanKiemTra) {
			Set<Menu> dsMenuConCanLay = repository.findByVaiTros_NguoiDungs_IdNguoiDungAndMenu_IdMenuOrderByOrderMenu(
					idNguoiDungHienHanh, menu.getIdMenu());
			if (!dsMenuConCanLay.isEmpty()) {
				menu.setMenus(dsMenuConCanLay);
				dsMenuCanLay.add(menu);
			}
		}
		List<MenuDto> dsMenuDaChuyenDoi = dsMenuCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsMenuDaChuyenDoi;
	}

}
