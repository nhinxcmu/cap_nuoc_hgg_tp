package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoGhiDto;

public interface SoGhiService {

	Page<SoGhiDto> layDsSoGhi(long idDuong, String thongTinCanTim, Pageable pageable);
	
	List<SoGhiDto> layDsSoGhi(long idDuong);
	
	SoGhiDto laySoGhi(long id);
	
	SoGhiDto luuSoGhi(SoGhiDto dto) throws Exception;
	
	void xoaSoGhi(long id) throws Exception;
	
	List<SoGhiDto> layDsSoGhiTheoTrangThaiChiSo(List<Long> dsIdSoGhi, long idTrangThaiChiSo);
	
	// mobile
	List<SoGhiDto> layDsSoGhiTheoNguoiDung(long idDuong, long idNguoiDung);
	
}
