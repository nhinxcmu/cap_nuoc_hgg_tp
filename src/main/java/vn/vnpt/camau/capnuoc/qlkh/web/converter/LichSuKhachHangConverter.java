package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhachHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuKhachHang;

@Component
public class LichSuKhachHangConverter {

	public LichSuKhachHang convertToEntity(KhachHangDto dto) throws Exception {
		if (dto == null) {
			return new LichSuKhachHang();
		}
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String jsonString = ow.writeValueAsString(dto);

		ObjectMapper mapper = new ObjectMapper();
		LichSuKhachHang entity = mapper.readValue(jsonString, LichSuKhachHang.class);

		return entity;
	}

}
