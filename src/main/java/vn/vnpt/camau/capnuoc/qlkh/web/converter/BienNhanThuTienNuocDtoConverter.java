package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.BienNhanThuTienNuocDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThamSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.PhieuThanhToan;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThanhToan;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThongTinKhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;

@Component
public class BienNhanThuTienNuocDtoConverter {

	@Autowired
	private DozerBeanMapper mapper;
	@Autowired
	private ThamSoService thamSoService;
	private List<ThamSoDto> dsThamSo;

	public BienNhanThuTienNuocDto convertToDto(PhieuThanhToan entity) {
		if (entity == null) {
			return new BienNhanThuTienNuocDto();
		}
		BienNhanThuTienNuocDto dto = mapper.map(entity, BienNhanThuTienNuocDto.class);
		thietLapThongTinCongTy(dto);
		thietLapThongTinKhachHang(dto, entity);
		thietLapThongTinTienNuoc(dto, entity);
		thietLapNguoiCapNhat(dto, entity);
		return dto;
	}

	private void thietLapThongTinCongTy(BienNhanThuTienNuocDto dto) {
		this.dsThamSo = thamSoService.layDsThamSoCongTy();
		Date ngayHienHanh = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String[] arrayNgay = format.format(ngayHienHanh).split("/");
		String ngayThangNam = layGiaTriThamSo(4) + ", ngày " + arrayNgay[0] + " tháng " + arrayNgay[1] + " năm " + arrayNgay[2];
		dto.setTenCongTy(layGiaTriThamSo(0));
		dto.setDiaChiCongTy(layGiaTriThamSo(1));
		dto.setSoDienThoai(layGiaTriThamSo(3));
		dto.setNgayThangNam(ngayThangNam);
	}

	private String layGiaTriThamSo(int i) {
		return this.dsThamSo.get(i).getGiaTri();
	}

	private void thietLapThongTinKhachHang(BienNhanThuTienNuocDto dto, PhieuThanhToan phieuThanhToan) {
//		long idThang = phieuThanhToan.getThang().getIdThang();
//		long idPhieuThanhToan = phieuThanhToan.getIdPhieuThanhToan();
//		ThongTinKhachHang thongTinKhachHang = thongTinKhachHangRepository
//				.findByThangs_IdThangAndKhachHang_HoaDons_ThanhToans_PhieuThanhToan_IdPhieuThanhToanAndKhachHang_HoaDons_ThanhToans_PhieuThanhToan_Thang_IdThang(
//						idThang, idPhieuThanhToan, idThang);
		ThongTinKhachHang thongTinKhachHang = phieuThanhToan.getKhachHang().getThongTinKhachHangs().iterator().next();
		dto.setMaKhachHang(thongTinKhachHang.getMaKhachHang());
		dto.setTenKhachHang(thongTinKhachHang.getTenKhachHang());
		dto.setDiaChiSuDung(thongTinKhachHang.getDiaChiSuDung());
	}

	private void thietLapThongTinTienNuoc(BienNhanThuTienNuocDto dto, PhieuThanhToan phieuThanhToan) {
		List<String> dsThangTon = new ArrayList<String>();
		long tienNuocHienHanh = 0, tienNuocTon = 0;
		long idThang = phieuThanhToan.getThang().getIdThang();
		Set<ThanhToan> dsThanhToan = phieuThanhToan.getThanhToans();
		for (ThanhToan thanhToan : dsThanhToan) {
			HoaDon hoaDon = thanhToan.getHoaDon();
			if (hoaDon.getThang().getIdThang() == idThang) {
				tienNuocHienHanh = hoaDon.getTienThanhToan();
			} else {
				dsThangTon.add(hoaDon.getThang().getTenThang());
				tienNuocTon += hoaDon.getTienThanhToan();
			}
		}
		String thangTon = dsThangTon.stream().collect(Collectors.joining(", "));
		String thangHienHanh = phieuThanhToan.getThang().getTenThang();
		dto.setThangHienHanh(thangHienHanh);
		dto.setTienNuocHienHanh(tienNuocHienHanh);
		dto.setDsThangTon(thangTon);
		dto.setTienNuocTon(tienNuocTon);
	}

	private void thietLapNguoiCapNhat(BienNhanThuTienNuocDto dto, PhieuThanhToan entity) {
		if (entity.getNguoiDung() != null) {
			dto.setIdNguoiCapNhat(entity.getNguoiDung().getIdNguoiDung());
			dto.setHoTenCapNhat(entity.getNguoiDung().getHoTen());
		}
	}

}
