package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import lombok.Getter;
import lombok.Setter;

public class DoiTuongBaoCaoDto implements Comparable<DoiTuongBaoCaoDto> {

	@Getter
	@Setter
	private Long idDoiTuongBaoCao;
	@Getter
	@Setter
	private String tenDoiTuongBaoCao;
	
	@Override
	public int compareTo(DoiTuongBaoCaoDto object) {
		return this.tenDoiTuongBaoCao.compareTo(object.getTenDoiTuongBaoCao());
	}

}
