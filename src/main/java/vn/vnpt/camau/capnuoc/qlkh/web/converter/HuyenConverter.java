package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.HuyenDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Huyen;

@Component
public class HuyenConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public HuyenDto convertToDto(Huyen entity) {
		if (entity == null) {
			return new HuyenDto();
		}
		HuyenDto dto = mapper.map(entity, HuyenDto.class);
		return dto;
	}

	public Huyen convertToEntity(HuyenDto dto) {
		if (dto == null) {
			return new Huyen();
		}
		Huyen entity = mapper.map(dto, Huyen.class);
		return entity;
	}

}
