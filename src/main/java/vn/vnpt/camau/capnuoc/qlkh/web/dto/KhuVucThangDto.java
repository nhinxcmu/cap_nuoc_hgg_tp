package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class KhuVucThangDto {
	
	@Getter
	@Setter
	@Mapping("thang.idThang")
	private long idThang;
	@Getter
	@Setter
	@Mapping("thang.tenThang")
	private String tenThang;
	@Getter
	@Setter
	@Mapping("trangThaiThang.idTrangThaiThang")
	private Long idTrangThaiThang;
	@Getter
	@Setter
	@Mapping("trangThaiThang.tenTrangThaiThang")
	private String tenTrangThaiThang;
	@Getter
	@Setter
	@Mapping("nguoiDung.idNguoiDung")
	private Long idNguoiKetChuyen;
	@Getter
	@Setter
	@Mapping("nguoiDung.hoTen")
	private String tenNguoiKetChuyen;
	@Getter
	@Setter
	private String tuNgay;
	@Getter
	@Setter
	private String denNgay;
	@Getter
	@Setter
	private Date ngayGioKetChuyen;

}
