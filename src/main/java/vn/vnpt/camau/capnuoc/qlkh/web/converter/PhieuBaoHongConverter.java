package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.PhieuBaoHongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.PhieuBaoHong;

@Component
public class PhieuBaoHongConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public PhieuBaoHongDto convertToDto(PhieuBaoHong entity) {
		if (entity == null) {
			return new PhieuBaoHongDto();
		}
		PhieuBaoHongDto dto = mapper.map(entity, PhieuBaoHongDto.class);
		return dto;
	}

	public PhieuBaoHong convertToEntity(PhieuBaoHongDto dto) {
		if (dto == null) {
			return new PhieuBaoHong();
		}
		PhieuBaoHong entity = mapper.map(dto, PhieuBaoHong.class);
		return entity;
	}
}