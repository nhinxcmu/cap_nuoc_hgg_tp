package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class PhieuThanhToanDto {

	@Getter
	@Setter
	private Long idPhieuThanhToan;
	@Getter
	@Setter
	private String maKhachHang;
	@Getter
	@Setter
	private String tenKhachHang;
	@Getter
	@Setter
	private String diaChiSuDung;
	@Getter
	@Setter
	private String diaChiThanhToan;
	@Getter
	@Setter
	private String soDienThoai;
	@Getter
	@Setter
	@Mapping("nguoiDung.idNguoiDung")
	private Long idNguoiCapNhat;
	@Getter
	@Setter
	@Mapping("nguoiDung.tenNguoiDung")
	private String tenNguoiCapNhat;
	@Getter
	@Setter
	private String maPhieuThanhToan;
	@Getter
	@Setter
	private long tienThanhToan;
	@Getter
	@Setter
	private Date ngayGioCapNhat;
	@Getter
	@Setter
	private boolean huyThanhToan;
	@Getter
	@Setter
	private String maDongHo;
	
}
