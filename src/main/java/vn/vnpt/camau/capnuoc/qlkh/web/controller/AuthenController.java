package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.MenuDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThamSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.MenuService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.PhieuSuaSeriService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;

@Controller
@RequestMapping("authen")
public class AuthenController {
	@Autowired
	private MenuService menuServ;
	@Autowired
	private ThamSoService thamSoServ;
	@Autowired
	private PhieuSuaSeriService phieuSuaSeriService;
	
	@GetMapping("/main-menu")
	public @ResponseBody List<MenuDto> layDsMenu2Cap() {
		return menuServ.layDsMenu2Cap();
	}
	
	@GetMapping("/lay-tt-cty")
	public @ResponseBody List<ThamSoDto> layTTCty() {
		return thamSoServ.layDsThamSoCongTy();
	}

	@GetMapping("/nhac-viec-nhan-vien")
	public @ResponseBody String nhacViecNhanVien(){
		return phieuSuaSeriService.nhacViecNhanVien();
	}
	@GetMapping("/nhac-viec-phong-ke-hoach")
	public @ResponseBody String nhacViecPhongKeHoach(){
		return phieuSuaSeriService.nhacViecPhongKeHoach();
	}
}
