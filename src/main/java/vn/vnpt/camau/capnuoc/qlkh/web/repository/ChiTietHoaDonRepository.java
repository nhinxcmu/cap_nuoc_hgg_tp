package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.ChiTietHoaDon;

public interface ChiTietHoaDonRepository extends Repository<ChiTietHoaDon, Long> {

	List<ChiTietHoaDon> findByHoaDon_IdHoaDonOrderByIdChiTietHoaDon(Long idHoaDon);
	
}
