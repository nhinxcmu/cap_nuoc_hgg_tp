package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.DuongDto;

public interface DuongService {

	Page<DuongDto> layDsDuong(long idKhuVuc, String thongTinCanTim, Pageable pageable);
	
	List<DuongDto> layDsDuong(long idKhuVuc);
	
	DuongDto layDuong(long id);
	
	DuongDto luuDuong(DuongDto dto) throws Exception;
	
	void xoaDuong(long id) throws Exception;
	
	List<DuongDto> layDsDuongTheoHuyen(long idHuyen);
	
	List<DuongDto> layDsDuongTheoNguoiDung(long idNguoiDung);
	
}
