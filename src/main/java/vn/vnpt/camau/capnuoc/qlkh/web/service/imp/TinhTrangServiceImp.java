package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.TinhTrangConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.TinhTrangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.TinhTrangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.TinhTrangService;

@Service
public class TinhTrangServiceImp implements TinhTrangService{
    @Autowired
    private TinhTrangRepository repo;
    @Autowired
    private TinhTrangConverter converter;

    @Override
    public List<TinhTrangDto> layDs() {
        return repo.findAll().stream().map(converter::convertToDto).collect(Collectors.toList());
    }
    
}
