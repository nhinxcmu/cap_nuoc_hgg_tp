package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class HoaDonNganHangDto {

	@Getter
	@Setter
	private Long idHoaDon;

	@JsonIgnore
	@Getter
	@Setter
	private Long idThongTinKhachHang;

	@JsonIgnore
	@Getter
	@Setter
	private String maKhachHang;

	@JsonIgnore
	@Getter
	@Setter
	private String tenKhachHang;

	@JsonIgnore
	@Getter
	@Setter
	private String ten;

	@JsonIgnore
	@Getter
	@Setter
	private String diaChiSuDung;

	@JsonIgnore
	@Getter
	@Setter
	private String diaChiThanhToan; // diaChiThanhToan

	@JsonIgnore
	@Getter
	@Setter
	private String soDienThoai;

	@JsonIgnore
	@Getter
	@Setter
	private String maDongHo;

	@JsonIgnore
	@Getter
	@Setter
	private Long idDongHo;

	@JsonIgnore
	@Getter
	@Setter
	private String soSeri;

	@JsonIgnore
	@Getter
	@Setter
	@Mapping("trangThaiHoaDon.idTrangThaiHoaDon")
	private Long idTrangThaiHoaDon;

	@JsonIgnore
	@Getter
	@Setter
	@Mapping("trangThaiHoaDon.tenTrangThaiHoaDon")
	private String tenTrangThaiHoaDon;

	@JsonIgnore
	@Getter
	@Setter
	private int chiSoCu;

	@JsonIgnore
	@Getter
	@Setter
	private int chiSoMoi;

	@JsonIgnore
	@Getter
	@Setter
	private int tieuThu;

	@JsonIgnore
	@Getter
	@Setter
	private long tienNuoc;

	@JsonIgnore
	@Getter
	@Setter
	private long thue;

	@JsonIgnore
	@Getter
	@Setter
	private long phiBvmt;

	@Getter
	@Setter
	private long tongTien;
	
	@JsonIgnore
	@Getter
	@Setter
	private String bangChu;

	@Getter
	@Setter
	private long tienThanhToan;

	@JsonIgnore
	@Getter
	@Setter
	private long tienConLai;

	@JsonIgnore
	@Getter
	@Setter
	private String ghiChu;

	@Getter
	@Setter
	@Mapping("thang.tenThang")
	private String tenThang;

	@JsonIgnore
	@Getter
	@Setter
	@Mapping("thang.idThang")
	private Long idThang;

	@JsonIgnore
	@Getter
	@Setter
	private long phiBvr;

	@JsonIgnore
	@Getter
	@Setter
	private Date ngayThanhToan;

	@JsonIgnore
	private boolean daThanhToan;

	public boolean isDaThanhToan() {
		return tienThanhToan > 0;
	}

	public void setDaThanhToan(boolean daThanhToan) {
		this.daThanhToan = daThanhToan;
	}

	// private ThongTinKhachHangResult thongTinKhachHang;

	// public ThongTinKhachHangResult getThongTinKhachHang() {
	// 	return new ThongTinKhachHangResult(this);
	// }

	// public void setThongTinKhachHang(ThongTinKhachHangResult thongTinKhachHang) {
	// 	this.thongTinKhachHang = thongTinKhachHang;
	// }

	// private HoaDonResult hoaDon;

	// public HoaDonResult getHoaDon() {
	// 	return new HoaDonResult(this);
	// }

	// public void setHoaDon(HoaDonResult hoaDon) {
	// 	this.hoaDon = hoaDon;
	// }

}

// class ThongTinKhachHangResult {
// 	private String maKhachHang;
// 	private String tenKhachHang;
// 	private String diaChiSuDung;
// 	private String diaChiThanhToan;
// 	private String soDienThoai;
// 	private String maDongHo;
// 	private String soSeri;

// 	public ThongTinKhachHangResult(HoaDonNganHangDto hoaDonNganHangDto) {
// 		this.maKhachHang = hoaDonNganHangDto.getMaKhachHang();
// 		this.tenKhachHang = hoaDonNganHangDto.getTenKhachHang();
// 		this.diaChiSuDung = hoaDonNganHangDto.getDiaChiSuDung();
// 		this.soDienThoai = hoaDonNganHangDto.getSoDienThoai();
// 		this.maDongHo = hoaDonNganHangDto.getMaDongHo();
// 		this.soSeri = hoaDonNganHangDto.getSoSeri();
// 	}

// 	public String getMaDongHo() {
// 		return maDongHo;
// 	}

// 	public void setMaDongHo(String maDongHo) {
// 		this.maDongHo = maDongHo;
// 	}

// 	public String getSoSeri() {
// 		return soSeri;
// 	}

// 	public void setSoSeri(String soSeri) {
// 		this.soSeri = soSeri;
// 	}

// 	public String getMaKhachHang() {
// 		return maKhachHang;
// 	}

// 	public String getSoDienThoai() {
// 		return soDienThoai;
// 	}

// 	public void setSoDienThoai(String soDienThoai) {
// 		this.soDienThoai = soDienThoai;
// 	}

// 	public String getDiaChiThanhToan() {
// 		return diaChiThanhToan;
// 	}

// 	public void setDiaChiThanhToan(String diaChiThanhToan) {
// 		this.diaChiThanhToan = diaChiThanhToan;
// 	}

// 	public String getDiaChiSuDung() {
// 		return diaChiSuDung;
// 	}

// 	public void setDiaChiSuDung(String diaChiSuDung) {
// 		this.diaChiSuDung = diaChiSuDung;
// 	}

// 	public String getTenKhachHang() {
// 		return tenKhachHang;
// 	}

// 	public void setTenKhachHang(String tenKhachHang) {
// 		this.tenKhachHang = tenKhachHang;
// 	}

// 	public void setMaKhachHang(String maKhachHang) {
// 		this.maKhachHang = maKhachHang;
// 	}
// }

// class HoaDonResult {

// 	private Long idHoaDon;
// 	private int chiSoCu;
// 	private int chiSoMoi;
// 	private int tieuThu;
// 	private long tienNuoc;
// 	private long thue;
// 	private long phiBvmt;
// 	private long tongTien;
// 	private String bangChu;
// 	private long tienThanhToan;
// 	private long phiBvr;
// 	private String tenThang;

// 	public HoaDonResult(HoaDonNganHangDto hoaDon) {
// 		this.bangChu = hoaDon.getBangChu();
// 		this.chiSoCu = hoaDon.getChiSoCu();
// 		this.chiSoMoi = hoaDon.getChiSoMoi();
// 		this.idHoaDon = hoaDon.getIdHoaDon();
// 		this.phiBvmt = hoaDon.getPhiBvmt();
// 		this.phiBvr = hoaDon.getPhiBvr();
// 		this.thue = hoaDon.getThue();
// 		this.tienNuoc = hoaDon.getTienNuoc();
// 		this.tienThanhToan = hoaDon.getTienThanhToan();
// 		this.tieuThu = hoaDon.getTieuThu();
// 		this.tongTien = hoaDon.getTongTien();
// 		this.tenThang=hoaDon.getTenThang();
// 	}

// 	public String getTenThang() {
// 		return tenThang;
// 	}

// 	public void setTenThang(String tenThang) {
// 		this.tenThang = tenThang;
// 	}

// 	public Long getIdHoaDon() {
// 		return idHoaDon;
// 	}

// 	public void setIdHoaDon(Long idHoaDon) {
// 		this.idHoaDon = idHoaDon;
// 	}

// 	public int getChiSoCu() {
// 		return chiSoCu;
// 	}

// 	public void setChiSoCu(int chiSoCu) {
// 		this.chiSoCu = chiSoCu;
// 	}

// 	public int getChiSoMoi() {
// 		return chiSoMoi;
// 	}

// 	public void setChiSoMoi(int chiSoMoi) {
// 		this.chiSoMoi = chiSoMoi;
// 	}

// 	public int getTieuThu() {
// 		return tieuThu;
// 	}

// 	public void setTieuThu(int tieuThu) {
// 		this.tieuThu = tieuThu;
// 	}

// 	public long getTienNuoc() {
// 		return tienNuoc;
// 	}

// 	public void setTienNuoc(long tienNuoc) {
// 		this.tienNuoc = tienNuoc;
// 	}

// 	public long getThue() {
// 		return thue;
// 	}

// 	public void setThue(long thue) {
// 		this.thue = thue;
// 	}

// 	public long getPhiBvmt() {
// 		return phiBvmt;
// 	}

// 	public void setPhiBvmt(long phiBvmt) {
// 		this.phiBvmt = phiBvmt;
// 	}

// 	public long getTongTien() {
// 		return tongTien;
// 	}

// 	public void setTongTien(long tongTien) {
// 		this.tongTien = tongTien;
// 	}

// 	public String getBangChu() {
// 		return bangChu;
// 	}

// 	public void setBangChu(String bangChu) {
// 		this.bangChu = bangChu;
// 	}

// 	public long getTienThanhToan() {
// 		return tienThanhToan;
// 	}

// 	public void setTienThanhToan(long tienThanhToan) {
// 		this.tienThanhToan = tienThanhToan;
// 	}

// 	public long getPhiBvr() {
// 		return phiBvr;
// 	}

// 	public void setPhiBvr(long phiBvr) {
// 		this.phiBvr = phiBvr;
// 	}
// }
