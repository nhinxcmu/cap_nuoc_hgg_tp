package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import javax.validation.constraints.NotNull;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class KeHoachDto {

	@Getter
	@Setter
	@NotNull
	private Long idKeHoach;
	@Getter
	@Setter
	@Mapping("thang.idThang")
	private Long idThang;
	@Getter
	@Setter
	@Mapping("thang.tenThang")
	private String tenThang;
	@Getter
	@Setter
	@Mapping("duong.idDuong")
	private Long idDuong;
	@Getter
	@Setter
	@Mapping("duong.tenDuong")
	private String tenDuong;
	@Getter
	@Setter
	private int chiSoCu;
	@Getter
	@Setter
	private int chiSoMoi;
	@Getter
	@Setter
	private int sanXuat;
	@Getter
	@Setter
	private int tieuThu;
	@Getter
	@Setter
	private long doanhThu;

}
