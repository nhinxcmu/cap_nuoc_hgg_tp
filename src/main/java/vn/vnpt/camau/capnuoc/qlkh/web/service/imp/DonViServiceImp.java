package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.DonViConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DonViDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DonVi;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DonViRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DonViService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NguoiDungService;

@Service
public class DonViServiceImp implements DonViService {

	@Autowired
	private DonViConverter converter;
	@Autowired
	private DonViRepository repository;
	@Autowired
	private NguoiDungService nguoiDungService;

	@Override
	public DonViDto layXiNghiepLamViec(long idNguoiDung) {
		DonVi donViCanLay;
		if (nguoiDungService.laNguoiDungThuocXiNghiep(idNguoiDung)) {
			donViCanLay = repository.findByXiNghiepIsTrueAndNguoiDungs_IdNguoiDung(idNguoiDung);
		} else {
			donViCanLay = repository.findTopByXiNghiepIsTrueAndKhuVucs_IdKhuVucIsNotNullOrderByTenDonVi();
		}
		DonViDto donViDaChuyenDoi = converter.convertToDto(donViCanLay);
		return donViDaChuyenDoi;
	}

	@Override
	public List<DonViDto> layDsXiNghiepLamViec() {
		List<DonVi> dsDonViCanLay = new ArrayList<DonVi>(0);
		if (nguoiDungService.laNguoiDungThuocXiNghiep()) {
			Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
			DonVi donViCanLay = repository.findByXiNghiepIsTrueAndNguoiDungs_IdNguoiDung(idNguoiDungHienHanh);
			dsDonViCanLay.add(donViCanLay);
		} else {
			dsDonViCanLay = repository.findByXiNghiepIsTrueOrderByTenDonVi();
		}
		List<DonViDto> dsDonViDaChuyenDoi = dsDonViCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsDonViDaChuyenDoi;
	}

	@Override
	public Page<DonViDto> layDsDonVi(Pageable pageable) {
		Page<DonVi> dsDonViCanLay = repository.findByOrderByIdDonViDesc(pageable);
		Page<DonViDto> dsDonViDaChuyenDoi = dsDonViCanLay.map(converter::convertToDto);
		return dsDonViDaChuyenDoi;
	}

	@Override
	public List<DonViDto> layDsDonVi() {
		Pageable pageable = null;
		List<DonViDto> dsDonViCanLay = layDsDonVi(pageable).getContent().stream().sorted().collect(Collectors.toList());
		return dsDonViCanLay;
	}

	@Override
	public Page<DonViDto> layDsXiNghiep(Pageable pageable) {
		Page<DonVi> dsXiNghiepCanLay = repository.findByXiNghiepIsTrueOrderByIdDonViDesc(pageable);
		Page<DonViDto> dsXiNghiepDaChuyenDoi = dsXiNghiepCanLay.map(converter::convertToDto);
		return dsXiNghiepDaChuyenDoi;
	}

	@Override
	public List<DonViDto> layDsXiNghiep() {
		Pageable pageable = null;
		List<DonViDto> dsXiNghiepCanLay = layDsXiNghiep(pageable).getContent().stream().sorted()
				.collect(Collectors.toList());
		return dsXiNghiepCanLay;
	}

	@Override
	public DonViDto layDonVi(long id) {
		DonVi donViCanLay = repository.findByIdDonVi(id);
		DonViDto donViDaChuyenDoi = converter.convertToDto(donViCanLay);
		return donViDaChuyenDoi;
	}

	@Override
	public DonViDto luuDonVi(DonViDto dto) throws Exception {
		kiemTraDonVi(dto.getIdDonVi());
		DonVi donViCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(donViCanLuu);
			DonViDto donViDaChuyenDoi = converter.convertToDto(donViCanLuu);
			return donViDaChuyenDoi;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu đơn vị.");
		}
	}

	private void kiemTraDonVi(Long idDonVi) throws Exception {
		if (idDonVi != null) {
			DonVi donViCanKiemTra = repository.findOne(idDonVi);
			if (donViCanKiemTra == null) {
				throw new Exception("Không tìm thấy đơn vị.");
			}
		}
	}

	@Override
	public void xoaDonVi(long id) throws Exception {
		kiemTraDonVi(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa đơn vị.");
			}
			throw new Exception(e.getMessage());
		}
	}

}
