package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DonViDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DuongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DonViService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DuongService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucService;


@Controller
@RequestMapping("/phuong")
public class DuongController {
	private ModelAttr modelAttr = new ModelAttr("Danh mục phường", "duong", new String[]{"jdgrid/jdgrid-v3.js","jdpage/jdpage.js","bower_components/select2/dist/js/select2.min.js","js/duong.js"},
			new String[]{"bower_components/select2/dist/css/select2.min.css","jdgrid/jdgrid.css"});
	@Autowired
	private DonViService donViServ;
	@Autowired
	private KhuVucService khuVucServ;
	@Autowired
	private DuongService duongServ;
	
	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
	    return "layout";
	}
	
	@GetMapping("/init-form")
	public @ResponseBody List<DonViDto> initForm(){
		return donViServ.layDsXiNghiepLamViec();
	}
	
	@GetMapping("/lay-ds-khu-vuc")
	public @ResponseBody List<KhuVucDto> layDanhSachKhuVuc(long id){
		return khuVucServ.layDsKhuVuc(id);
	}
	
	@GetMapping("/lay-ds")
	public @ResponseBody Page<DuongDto> layDanhSach(long idKhuVuc,String thongTinCanTim, Pageable page){
		return duongServ.layDsDuong(idKhuVuc, thongTinCanTim, page);
	}
	
	@GetMapping("/lay-ct")
	public @ResponseBody DuongDto layChiTiet(long id){
		return duongServ.layDuong(id);
	}
	
	@PostMapping("/luu")
	public @ResponseBody String luu(DuongDto dto){
		try {
			duongServ.luuDuong(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/xoa")
	public @ResponseBody String xoa(long id){
		try {
			duongServ.xoaDuong(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	
}
