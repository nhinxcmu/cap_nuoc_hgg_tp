package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.LichSuInDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuIn;

@Component
public class LichSuInConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public LichSuInDto convertToDto(LichSuIn entity) {
		if (entity == null) {
			return new LichSuInDto();
		}
		LichSuInDto dto = mapper.map(entity, LichSuInDto.class);
		return dto;
	}

	public LichSuIn convertToEntity(LichSuInDto dto) {
		if (dto == null) {
			return new LichSuIn();
		}
		LichSuIn entity = mapper.map(dto, LichSuIn.class);
		return entity;
	}

}
