package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuDongBoChiSo;

public interface LichSuDongBoChiSoRepository extends CrudRepository<LichSuDongBoChiSo, Long> {

}
