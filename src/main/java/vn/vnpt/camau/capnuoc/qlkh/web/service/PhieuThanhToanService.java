package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.BienNhanThuTienNuocDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.PhieuThanhToanDto;

public interface PhieuThanhToanService {

	Page<PhieuThanhToanDto> layDsPhieuThanhToan(String thongTinCanTim, Long idHinhThucThanhToan, Pageable pageable) throws Exception;

	BienNhanThuTienNuocDto layBienNhanThuTienNuocDto(long idPhieuThanhToan);
	
	void huyPhieuThanhToan(long id) throws Exception;
	
	// mobile
	List<PhieuThanhToanDto> layDsPhieuThanhToanTheoNguoiDung(long idNguoiDung, String thongTinCanTim) throws Exception;
	
	void huyPhieuThanhToan(long id, long idNguoiDung) throws Exception;

}
