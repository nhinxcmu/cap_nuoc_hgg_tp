package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucThangService;

@Controller
@RequestMapping("/ket-chuyen-du-lieu")
public class KetChuyenDuLieuController {
	private ModelAttr modelAttr = new ModelAttr("Kết chuyển dữ liệu", "ketchuyendulieu",
			new String[] { "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js", "js/ketchuyendulieu.js" },
			new String[] { "jdgrid/jdgrid.css" });
	@Autowired
	private KhuVucThangService khuVucThangServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/lay-ds")
	public @ResponseBody Page<KhuVucThangDto> layDsKhuVucThangTheoKhuVucLamViec(Pageable page) {
		return khuVucThangServ.layDsKhuVucThangTheoKhuVucLamViec(page);
	}

	@PostMapping("/ket-chuyen")
	public @ResponseBody String ketChuyenKhuVucThang(long idThang) {
		try {
			khuVucThangServ.ketChuyenKhuVucThang(idThang);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/ket-chuyen-tat-ca")
	public @ResponseBody String ketChuyenTatCa() {
		try {
			khuVucThangServ.ketChuyenTatCa();
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/khoi-phuc")
	public @ResponseBody String khoiPhucKhuVucThangDangLamViec(long idThang) {
		try {
			khuVucThangServ.khoiPhucKhuVucThangDangLamViec(idThang);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
