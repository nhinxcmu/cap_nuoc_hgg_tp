package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuKhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.LichSuKhachHangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.LichSuKhachHangService;

@Service
public class LichSuKhachHangServiceImp implements LichSuKhachHangService {

	@Autowired
	private LichSuKhachHangRepository repository;

	@Override
	public Page<LichSuKhachHang> layDsLichSuKhachHang(Long idKhachHang, Pageable pageable) {
		return repository.findByIdKhachHangOrderByNgayGioCapNhatDesc(idKhachHang, pageable);
	}

	@Override
	public LichSuKhachHang layLichSuKhachHang(Long idLichSuKhachHang) {
		return repository.findByIdLichSuKhachHang(idLichSuKhachHang);
	}

}
