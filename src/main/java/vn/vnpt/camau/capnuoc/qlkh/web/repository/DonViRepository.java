package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DonVi;

public interface DonViRepository extends CrudRepository<DonVi, Long> {

	DonVi findTopByXiNghiepIsTrueAndKhuVucs_IdKhuVucIsNotNullOrderByTenDonVi();
	
	DonVi findByXiNghiepIsTrueAndNguoiDungs_IdNguoiDung(Long idNguoiDung);
	
	List<DonVi> findByXiNghiepIsTrueOrderByTenDonVi();
	
	Page<DonVi> findByXiNghiepIsTrueOrderByIdDonViDesc(Pageable pageable);

	@EntityGraph(attributePaths = { "huyen" })
	Page<DonVi> findByOrderByIdDonViDesc(Pageable pageable);
	
	@EntityGraph(attributePaths = { "huyen" })
	DonVi findByIdDonVi(Long idDonVi);

	DonVi findByNguoiDungs_IdNguoiDung(Long idNguoiDung);
	
}
