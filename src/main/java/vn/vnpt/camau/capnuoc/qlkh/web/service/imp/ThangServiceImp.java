package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.ThangConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThangService;

@Service
public class ThangServiceImp implements ThangService {

	@Autowired
	private ThangConverter converter;
	@Autowired
	private ThangRepository repository;

	@Override
	public ThangDto layThangLamViec(long idKhuVuc) {
		Thang thangCanLay = repository.findTopByKhuVucThangs_Id_IdKhuVucOrderByIdThangDesc(idKhuVuc);
		ThangDto thangDaChuyenDoi = converter.convertToDto(thangCanLay);
		return thangDaChuyenDoi;
	}

	@Override
	public List<ThangDto> layDsThangLamViec(long idKhuVuc) {
		List<Thang> dsThangCanLay = repository.findByKhuVucThangs_Id_IdKhuVucOrderByIdThangDesc(idKhuVuc);
		List<ThangDto> dsThangDaChuyenDoi = dsThangCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsThangDaChuyenDoi;
	}

	@Override
	public ThangDto layThang(long idThang) {
		Thang thangCanLay = repository.findOne(idThang);
		ThangDto thangDaChuyenDoi = converter.convertToDto(thangCanLay);
		return thangDaChuyenDoi;
	}

	@Override
	public Thang layThangPhanCongGhiThuMoiNhat(long idNguoiDung) throws Exception {
		// TODO: refactor
		Thang thangPhanCongGhiThuMoiNhat = repository
				.findTopByLichGhiThuThangs_NguoiDuocPhanCong_IdNguoiDungOrderByIdThangDesc(idNguoiDung);
		if (thangPhanCongGhiThuMoiNhat == null) {
			throw new Exception("Không thể lấy tháng thanh toán hóa đơn.");
		}
		return thangPhanCongGhiThuMoiNhat;
	}

	@Override
	public List<String> layDanhSachNam() {
		return repository.layDanhSachNam();
	}

	@Override
	public void themThangMoi() {
		long idThang = repository.findTopByOrderByIdThangDesc().getIdThang();
		String namMoi, thangMoi;
		long namHienTai = idThang / 100;
		long thangHienTai = idThang % 100;

		if (thangHienTai == 12) {
			thangMoi = "01";
			namMoi = "" + (namHienTai + 1);
		} else {
			if (thangHienTai + 1 < 10)
				thangMoi = "0" + (thangHienTai + 1);
			else
				thangMoi = "" + (thangHienTai + 1);

			namMoi = "" + namHienTai;
		}

		Long idThangMoi = Long.parseLong(namMoi + thangMoi);
		String tenThangMoi = thangMoi + "/" + namMoi;
		Thang thang = new Thang(idThangMoi, tenThangMoi);
		repository.save(thang);
	}

	@Override
	public List<ThangDto> layDanhSachThangTheoNam(String nam) {
		List<Thang> dsThangCanLay = repository.layDanhSachThangTheoNam(nam);
		List<ThangDto> dsThangDaChuyenDoi = dsThangCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsThangDaChuyenDoi;
	}

	@Override
	public List<ThangDto> layDsThang() {
		List<Thang> dsThangCanLay = repository.findByOrderByIdThangDesc();
		List<ThangDto> dsThangDaChuyenDoi = dsThangCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsThangDaChuyenDoi;
	}

}
