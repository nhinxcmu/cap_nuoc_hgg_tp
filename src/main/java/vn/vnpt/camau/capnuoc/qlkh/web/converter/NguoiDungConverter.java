package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.NguoiDungDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DonVi;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.VaiTro;

@Component
public class NguoiDungConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public NguoiDungDto convertToDto(NguoiDung entity) {
		if (entity == null) {
			return new NguoiDungDto();
		}
		NguoiDungDto dto = mapper.map(entity, NguoiDungDto.class);
		return dto;
	}
	
	public NguoiDung convertToEntity(NguoiDungDto dto) {
		if (dto == null) {
			return new NguoiDung();
		}
		NguoiDung entity = mapper.map(dto, NguoiDung.class);
		setDonVi(dto, entity);
		setVaiTro(dto, entity);
		return entity;
	}

	private void setDonVi(NguoiDungDto dto, NguoiDung entity) {
		if (dto.getIdDonVi() != null) {
			DonVi donVi = new DonVi();
			donVi.setIdDonVi(dto.getIdDonVi());
			entity.setDonVi(donVi);
		}
	}

	private void setVaiTro(NguoiDungDto dto, NguoiDung entity) {
		if (dto.getIdVaiTro() != null) {
			VaiTro vaiTro = new VaiTro();
			vaiTro.setIdVaiTro(dto.getIdVaiTro());
			entity.setVaiTro(vaiTro);
		}
	}

}
