package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichGhiThu;

public interface LichGhiThuRepository extends CrudRepository<LichGhiThu, Long> {

//	@EntityGraph(attributePaths = { "nguoiDuocPhanCong", "nguoiCapNhat" })
//	Page<LichGhiThu> findByThangs_IdThangAndNguoiDuocPhanCong_DonVi_IdDonViOrderByIdLichGhiThuDesc(Long idThang,
//			Long idDonVi, Pageable pageable);

	@EntityGraph(attributePaths = { "nguoiDuocPhanCong", "nguoiCapNhat" })
	Page<LichGhiThu> findByThangs_IdThangAndNguoiDuocPhanCong_DonVi_IdDonViAndNguoiDuocPhanCong_VaiTro_IdVaiTroOrderByIdLichGhiThuDesc(
			Long idThang, Long idDonVi, Long idVaiTro, Pageable pageable);

//	@EntityGraph(attributePaths = { "nguoiDuocPhanCong", "nguoiCapNhat" })
//	Page<LichGhiThu> findByThangs_IdThangAndNguoiDuocPhanCong_DonVi_IdDonViAndNguoiDuocPhanCong_HoTenContainsOrderByIdLichGhiThuDesc(
//			Long idThang, Long idDonVi, String hoTen, Pageable pageable);

	@EntityGraph(attributePaths = { "nguoiDuocPhanCong", "nguoiCapNhat" })
	Page<LichGhiThu> findByThangs_IdThangAndNguoiDuocPhanCong_DonVi_IdDonViAndNguoiDuocPhanCong_VaiTro_IdVaiTroAndNguoiDuocPhanCong_HoTenContainsOrderByIdLichGhiThuDesc(
			Long idThang, Long idDonVi, Long idVaiTro, String hoTen, Pageable pageable);

	@EntityGraph(attributePaths = { "nguoiDuocPhanCong", "soGhis" })
	LichGhiThu findByIdLichGhiThu(Long idLichGhiThu);

	Long countByNguoiDuocPhanCong_IdNguoiDungAndThang_IdThang(Long idNguoiDuocPhanCong, Long idThang);
	
	LichGhiThu findByNguoiDuocPhanCong_IdNguoiDungAndThang_IdThang(Long idNguoiDuocPhanCong, Long idThang);

	@EntityGraph(attributePaths = { "nguoiDuocPhanCong", "nguoiCapNhat" })
	Page<LichGhiThu> findByThangs_IdThangAndNguoiDuocPhanCong_DonVi_IdDonViAndNguoiDuocPhanCong_VaiTro_Menus_IdMenuOrderByIdLichGhiThuDesc(
			Long idThang, Long idDonVi, Long idMenu, Pageable pageable);

	@EntityGraph(attributePaths = { "nguoiDuocPhanCong", "nguoiCapNhat" })
	Page<LichGhiThu> findByThangs_IdThangAndNguoiDuocPhanCong_DonVi_IdDonViAndNguoiDuocPhanCong_VaiTro_Menus_IdMenuAndNguoiDuocPhanCong_HoTenContainsOrderByIdLichGhiThuDesc(
			Long idThang, Long idDonVi, Long idMenu, String hoTen, Pageable pageable);
}
