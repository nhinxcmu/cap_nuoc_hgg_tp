package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.TrangThaiChiSoDto;

public interface TrangThaiChiSoService {

	List<TrangThaiChiSoDto> layDsTrangThaiChiSo();
	
}
