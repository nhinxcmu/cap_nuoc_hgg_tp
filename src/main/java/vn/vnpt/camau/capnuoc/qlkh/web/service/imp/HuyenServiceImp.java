package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.HuyenConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HuyenDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Huyen;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HuyenRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HuyenService;

@Service
public class HuyenServiceImp implements HuyenService {

	@Autowired
	private HuyenConverter converter;
	@Autowired
	private HuyenRepository repository;

	@Override
	public Page<HuyenDto> layDsHuyen(Pageable pageable) {
		Page<Huyen> dsHuyenCanLay = repository.findByOrderByIdHuyenDesc(pageable);
		Page<HuyenDto> dsHuyenDaChuyenDoi = dsHuyenCanLay.map(converter::convertToDto);
		return dsHuyenDaChuyenDoi;
	}

	@Override
	public List<HuyenDto> layDsHuyen() {
		Pageable pageable = null;
		List<HuyenDto> dsHuyenCanLay = layDsHuyen(pageable).getContent().stream().sorted().collect(Collectors.toList());
		return dsHuyenCanLay;
	}

	@Override
	public HuyenDto layHuyen(long id) {
		Huyen huyenCanLay = repository.findOne(id);
		HuyenDto huyenDaChuyenDoi = converter.convertToDto(huyenCanLay);
		return huyenDaChuyenDoi;
	}

	@Override
	public HuyenDto luuHuyen(HuyenDto dto) throws Exception {
		kiemTraHuyen(dto.getIdHuyen());
		Huyen huyenCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(huyenCanLuu);
			HuyenDto huyenDaChuyenDoi = converter.convertToDto(huyenCanLuu);
			return huyenDaChuyenDoi;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu huyện.");
		}
	}

	private void kiemTraHuyen(Long idHuyen) throws Exception {
		if (idHuyen != null) {
			Huyen huyenCanKiemTra = repository.findOne(idHuyen);
			if (huyenCanKiemTra == null) {
				throw new Exception("Không tìm thấy huyện.");
			}
		}
	}

	@Override
	public void xoaHuyen(long id) throws Exception {
		kiemTraHuyen(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa huyện.");
			}
			throw new Exception(e.getMessage());
		}
	}

}
