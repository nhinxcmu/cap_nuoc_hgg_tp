package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;

public interface ThangService {

	ThangDto layThangLamViec(long idKhuVuc);

	List<ThangDto> layDsThangLamViec(long idKhuVuc);

	ThangDto layThang(long idThang);

	Thang layThangPhanCongGhiThuMoiNhat(long idNguoiDung) throws Exception;

	List<String> layDanhSachNam();

	void themThangMoi();

	List<ThangDto> layDanhSachThangTheoNam(String nam);

	List<ThangDto> layDsThang();
}
