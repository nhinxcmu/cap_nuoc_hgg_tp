package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.DonViDto;

public interface DonViService {

	DonViDto layXiNghiepLamViec(long idNguoiDung);
	
	List<DonViDto> layDsXiNghiepLamViec();
	
	Page<DonViDto> layDsDonVi(Pageable pageable);
	
	List<DonViDto> layDsDonVi();
	
	Page<DonViDto> layDsXiNghiep(Pageable pageable);
	
	List<DonViDto> layDsXiNghiep();
	
	DonViDto layDonVi(long id);
	
	DonViDto luuDonVi(DonViDto dto) throws Exception;
	
	void xoaDonVi(long id) throws Exception;
}
