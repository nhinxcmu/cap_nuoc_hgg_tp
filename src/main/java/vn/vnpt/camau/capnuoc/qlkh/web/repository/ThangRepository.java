package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;

public interface ThangRepository extends CrudRepository<Thang, Long> {

	Thang findTopByKhuVucThangs_Id_IdKhuVucOrderByIdThangDesc(Long idKhuVuc);

	List<Thang> findByKhuVucThangs_Id_IdKhuVucOrderByIdThangDesc(Long idKhuVuc);

	Long countByThongTinKhachHangThangs_IdThongTinKhachHang(Long idThongTinKhachHang);

	Thang findOne(Long id);

	Long countByLichGhiThuThangs_IdLichGhiThu(Long idLichGhiThu);

	Thang findTopByLichGhiThuThangs_NguoiDuocPhanCong_IdNguoiDungOrderByIdThangDesc(Long idNguoiDung);

	Thang findTopByIdThangLessThanOrderByIdThangDesc(Long idThang);

	Thang findTopByOrderByIdThangDesc();

	@Query("select distinct substring(t.idThang, 1, 4) from Thang t order by substring(t.idThang, 1, 4) desc")
	List<String> layDanhSachNam();

	@Query("select t from Thang t where substring(t.idThang, 1, 4) = ?1 order by t.idThang asc")
	List<Thang> layDanhSachThangTheoNam(String nam);

	List<Thang> findByOrderByIdThangDesc();
}
