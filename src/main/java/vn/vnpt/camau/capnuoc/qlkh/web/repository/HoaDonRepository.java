package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;

public interface HoaDonRepository extends CrudRepository<HoaDon, Long> {

	@EntityGraph(attributePaths = { "khachHang", "thang" })
	HoaDon findByIdHoaDon(Long idHoaDon);

	@EntityGraph(attributePaths = { "khachHang", "thang" })
	HoaDon findByIdHoaDonAndHuyIsFalse(Long idHoaDon);

	@Query("select coalesce(max(hd.idHoaDon), 0) from HoaDon hd where hd.khachHang = ?1 and hd.thang = ?2 and hd.idHoaDon < ?3 and hd.huy = 0")
	long getMaxIdHoaDon(KhachHang khachHang, Thang thang, long idHoaDon);

	@Query("select hd from HoaDon hd join hd.khachHang kh join kh.thongTinKhachHangs tt join tt.thangs th join tt.soGhi sg join hd.trangThaiChiSo ttcs join hd.trangThaiHoaDon tthd join sg.duong d "
			+ "where hd.thang.idThang = ?1 and (0L = ?2 or sg.idSoGhi = ?2) and th.idThang = ?1 and hd.huy = 0 and (0L = ?3 or ttcs.idTrangThaiChiSo = ?3) and (0L = ?4 or tthd.idTrangThaiHoaDon = ?4) and (0L = ?5 or d.khuVuc.idKhuVuc = ?5) and (0L = ?6 or d.idDuong =?6) and (0 = ?7 or (-1 = ?7 and hd.tienConLai > 0) or (1 = ?7 and hd.tienConLai = 0 and hd.tongTien > 0)) "
			+ "order by tt.thuTu")
	@EntityGraph(attributePaths = { "khachHang.thongTinKhachHangs.doiTuong", "trangThaiChiSo", "trangThaiHoaDon",
			"nguoiDung", "khachHang.thongTinKhachHangs.soGhi" })
	Page<HoaDon> layDsHoaDon(Long idThang, Long idSoGhi, Long idTrangThaiChiSo, Long idTrangThaiHoaDon, Long idKhuVuc,
			Long idDuong, int daThanhToan, Pageable pageable);

	@Query("select hd from HoaDon hd join hd.khachHang kh join kh.thongTinKhachHangs tt join tt.thangs th join tt.soGhi sg join hd.trangThaiChiSo ttcs join hd.trangThaiHoaDon tthd join sg.duong d "
			+ "where hd.thang.idThang = ?1 and (0L = ?2 or sg.idSoGhi = ?2) and th.idThang = ?1 and hd.huy = 0 and (0L = ?4 or ttcs.idTrangThaiChiSo = ?4) and (0L = ?5 or tthd.idTrangThaiHoaDon = ?5) and (0L = ?6 or d.khuVuc.idKhuVuc = ?6) and (0L = ?7 or d.idDuong =?7) and (0 = ?8 or (-1 = ?8 and hd.tienConLai > 0) or (1 = ?8 and hd.tienConLai = 0 and hd.tongTien > 0)) "
			+ "and (tt.maKhachHang like %?3% or tt.tenKhachHang like %?3% or tt.diaChiSuDung like %?3%) "
			+ "order by tt.thuTu")
	@EntityGraph(attributePaths = { "khachHang.thongTinKhachHangs.doiTuong", "trangThaiChiSo", "trangThaiHoaDon",
			"nguoiDung", "khachHang.thongTinKhachHangs.soGhi" })
	Page<HoaDon> layDsHoaDon(Long idThang, Long idSoGhi, String thongTinCanTim, Long idTrangThaiChiSo,
			Long idTrangThaiHoaDon, Long idKhuVuc, Long idDuong, int daThanhToan, Pageable pageable);

	@Query("select hd from HoaDon hd join hd.khachHang kh join kh.thongTinKhachHangs tt join tt.thangs th join tt.soGhi sg join hd.trangThaiChiSo ttcs join hd.trangThaiHoaDon tthd join sg.duong d "
			+ "where hd.thang.idThang = ?1 and (0L = ?2 or sg.idSoGhi = ?2) and th.idThang = ?1 and hd.huy = 0 and (0L = ?5 or ttcs.idTrangThaiChiSo = ?5) and (0L = ?6 or tthd.idTrangThaiHoaDon = ?6) and (0L = ?7 or d.khuVuc.idKhuVuc = ?7) and (0L = ?8 or d.idDuong =?8) and (0 = ?9 or (-1 = ?9 and hd.tienConLai > 0) or (1 = ?9 and hd.tienConLai = 0 and hd.tongTien > 0)) "
			+ "and (tt.maKhachHang like %?3% or tt.tenKhachHang like %?3% or tt.diaChiSuDung like %?3% or hd.chiSoCu = ?4 or hd.chiSoMoi = ?4 or hd.tieuThu = ?4) "
			+ "order by tt.thuTu")
	@EntityGraph(attributePaths = { "khachHang.thongTinKhachHangs.doiTuong", "trangThaiChiSo", "trangThaiHoaDon",
			"nguoiDung", "khachHang.thongTinKhachHangs.soGhi" })
	Page<HoaDon> layDsHoaDon(Long idThang, Long idSoGhi, String thongTinKhachHang, Integer thongTinChiSo,
			Long idTrangThaiChiSo, Long idTrangThaiHoaDon, Long idKhuVuc, Long idDuong, int daThanhToan,
			Pageable pageable);

	HoaDon findByKhachHang_IdKhachHangAndThang_IdThang(Long idKhachHang, Long idThang);

	@Query("select hd from HoaDon hd join hd.khachHang kh join kh.thongTinKhachHangs tt join tt.thangs th "
			+ "where hd.idHoaDon = ?1 and hd.thang.idThang = th.idThang")
	@EntityGraph(attributePaths = { "khachHang.thongTinKhachHangs.doiTuong", "trangThaiChiSo", "trangThaiHoaDon",
			"nguoiDung", "khachHang.thongTinKhachHangs.soGhi" })
	HoaDon layHoaDon(Long idHoaDon);

	Integer countByThang_IdThangAndKhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndHuyIsFalse(
			Long idThangHoaDon, Long idKhuVuc, Long idThangKhachHang);

	List<HoaDon> findByThang_IdThangAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndKhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndHuyIsFalseAndChiSos_IdChiSoIsNull(
			Long idThangHoaDon, Long idThangThongTinKhachHang, Long idKhuVuc);

	@EntityGraph(attributePaths = { "chiSos" })
	List<HoaDon> findByThang_IdThangAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndKhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndHuyIsFalse(
			Long idThangHoaDon, Long idThangThongTinKhachHang, Long idKhuVuc);

	@EntityGraph(attributePaths = { "thang", "nguoiDung" })
	List<HoaDon> findByKhachHang_ThongTinKhachHangs_IdThongTinKhachHangAndTrangThaiHoaDon_IdTrangThaiHoaDonAndTienThanhToanAndTienConLaiGreaterThanAndThang_IdThangLessThanEqual(
			Long idThongTinKhachHang, Long idTrangThaiHoaDon, Long tienThanhToan, Long tienConLai, Long idThang);

	@EntityGraph(attributePaths = { "thang" })
	List<HoaDon> findByKhachHang_ThongTinKhachHangs_IdThongTinKhachHangAndTrangThaiHoaDon_IdTrangThaiHoaDonAndTienThanhToanGreaterThanAndThanhToans_Thang_IdThangAndThanhToans_HuyThanhToanIsFalse(
			Long idThongTinKhachHang, Long idTrangThaiHoaDon, Long tienThanhToan, Long idThangThanhToan);

	@EntityGraph(attributePaths = { "trangThaiHoaDon", "khachHang" })
	List<HoaDon> findByIdHoaDonIn(List<Long> dsIdHoaDon);

	@EntityGraph(attributePaths = { "thang" })
	List<HoaDon> findByThanhToans_PhieuThanhToan_IdPhieuThanhToan(Long idPhieuThanhToan);

	@EntityGraph(attributePaths = { "trangThaiHoaDon", "khachHang.thongTinKhachHangs" })
	List<HoaDon> findByThang_IdThangAndKhachHang_ThongTinKhachHangs_MaKhachHangInAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndHuyIsFalse(
			Long idThangHoaDon, List<String> dsMaKhachHang, Long idThangKhachHang);

	List<HoaDon> findByThang_IdThangAndKhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndHuyIsFalseAndTrangThaiHoaDon_IdTrangThaiHoaDonAndTienNuocGreaterThan(
			Long idThangHoaDon, Long idKhuVuc, Long idThangKhachHang, Long idTrangThaiHoaDon, Long tienNuoc);

	List<HoaDon> findByThang_IdThangAndKhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndHuyIsFalseAndTrangThaiHoaDon_IdTrangThaiHoaDonAndTienThanhToan(
			Long idThangHoaDon, Long idKhuVuc, Long idThangKhachHang, Long idTrangThaiHoaDon, Long tienThanhToan);

	List<HoaDon> findByHoaDonDienTus_IdHoaDonDienTuIn(List<Long> dsIdHoaDonDienTu);

	@EntityGraph(attributePaths = { "thang" })
	Page<HoaDon> findByKhachHang_IdKhachHangOrderByThang_IdThangDesc(Long idKhachHang, Pageable pageable);

	@EntityGraph(attributePaths = { "khachHang.thongTinKhachHangs.doiTuong", "trangThaiChiSo", "trangThaiHoaDon",
			"nguoiDung", "khachHang.thongTinKhachHangs.soGhi","thang" })
			@Query("select hd from HoaDon hd join hd.khachHang kh join kh.thongTinKhachHangs tt join tt.thangs th "
			+ "where tt.maKhachHang = ?1 and hd.thang.idThang = th.idThang"
			+ " and hd.tienConLai>0 and hd.trangThaiHoaDon.idTrangThaiHoaDon = 6")
	List<HoaDon> layHoaDonTheoMaKhachHang(String maKhachHang);
}
