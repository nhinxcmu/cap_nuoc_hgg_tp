package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

public class VaiTroDto {

	@Getter
	@Setter
	@NotNull
	private Long idVaiTro;
	@Getter
	@Setter
	@NotNull
	private String tenVaiTro;
	@Getter
	@Setter
	List<CayDto> cayDtos = new ArrayList<CayDto>(0);
	@Getter
	@Setter
	private Set<MenuDto> menuDtos = new HashSet<MenuDto>(0);

}
