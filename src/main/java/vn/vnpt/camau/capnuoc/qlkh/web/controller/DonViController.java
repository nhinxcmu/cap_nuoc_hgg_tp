package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DonViDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HuyenDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DonViService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HuyenService;


@Controller
@RequestMapping("/don-vi")
public class DonViController {
	private ModelAttr modelAttr = new ModelAttr("Danh mục đơn vị", "donvi", new String[]{"jdgrid/jdgrid-v3.js","jdpage/jdpage.js","bower_components/select2/dist/js/select2.min.js","js/donvi.js"},
			new String[]{"bower_components/select2/dist/css/select2.min.css","jdgrid/jdgrid.css"});
	@Autowired
	private DonViService donViServ;
	@Autowired
	private HuyenService huyenServ;
	
	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
	    return "layout";
	}
	
	@GetMapping("/init-form")
	public @ResponseBody List<HuyenDto> layDanhSachHuyen(){
		return huyenServ.layDsHuyen();
	}
	
	@GetMapping("/lay-ds")
	public @ResponseBody Page<DonViDto> layDanhSach(Pageable page){
		return donViServ.layDsDonVi(page);
	}
	
	@GetMapping("/lay-ct")
	public @ResponseBody DonViDto layChiTiet(long id){
		return donViServ.layDonVi(id);
	}
	
	@PostMapping("/luu")
	public @ResponseBody String luu(DonViDto dto){
		try {
			donViServ.luuDonVi(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/xoa")
	public @ResponseBody String xoa(long id){
		try {
			donViServ.xoaDonVi(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	
}
