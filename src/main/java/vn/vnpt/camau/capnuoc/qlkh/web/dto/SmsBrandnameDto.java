package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import lombok.Getter;
import lombok.Setter;

public class SmsBrandnameDto {

	@Getter
	@Setter
	Long idHoaDon;
	@Getter
	@Setter
	String soDienThoai;
	@Getter
	@Setter
	String params;
	
}
