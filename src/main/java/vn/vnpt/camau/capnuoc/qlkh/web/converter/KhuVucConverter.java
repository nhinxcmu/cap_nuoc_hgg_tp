package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DonVi;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhuVuc;

@Component
public class KhuVucConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public KhuVucDto convertToDto(KhuVuc entity) {
		if (entity == null) {
			return new KhuVucDto();
		}
		KhuVucDto dto = mapper.map(entity, KhuVucDto.class);
		return dto;
	}
	
	public KhuVuc convertToEntity(KhuVucDto dto) {
		if (dto == null) {
			return new KhuVuc();
		}
		KhuVuc entity = mapper.map(dto, KhuVuc.class);
		setDonVi(dto, entity);
		return entity;
	}

	private void setDonVi(KhuVucDto dto, KhuVuc entity) {
		if (dto.getIdDonVi() != null) {
			DonVi donVi = new DonVi();
			donVi.setIdDonVi(dto.getIdDonVi());
			entity.setDonVi(donVi);
		}
	}

}
