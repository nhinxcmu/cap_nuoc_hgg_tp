package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHo;

public interface DongHoRepository extends CrudRepository<DongHo, Long> {

	@EntityGraph(attributePaths = { "trangThaiDongHo", "dongHoKhachHangThangs" })
	DongHo findByDongHoKhachHangThangs_Id_IdKhachHangAndDongHoKhachHangThangs_Id_IdThang(Long idKhachHang, Long idThang);

	DongHo findByIdDongHoAndDongHoKhachHangThangs_Id_IdKhachHangNot(Long idDongHo, Long idKhachHang);

	@EntityGraph(attributePaths = { "trangThaiDongHo", "dongHoKhachHangThangs" })
	DongHo findByMaDongHo(String maDongHo);
	
	Page<DongHo> findDistinctByDongHoKhachHangThangs_Id_IdKhachHangOrderByIdDongHoDesc(Long idKhachHang, Pageable pageable);
}
