package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

public class TinNhanDto {

	@Getter
	@Setter
	private Long idTinNhan;
	@Getter
	@Setter
	private String soDienThoai;
	@Getter
	@Setter
	private String noiDung;
	@Getter
	@Setter
	private Date thoiGianGui;
	@Getter
	@Setter
	private boolean loi;

}
