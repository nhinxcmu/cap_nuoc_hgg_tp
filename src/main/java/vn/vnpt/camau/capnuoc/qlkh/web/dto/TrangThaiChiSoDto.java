package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import lombok.Getter;
import lombok.Setter;

public class TrangThaiChiSoDto {

	@Getter
	@Setter
	private long idTrangThaiChiSo;
	@Getter
	@Setter
	private String tenTrangThaiChiSo;

}
