package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.TrangThaiHoaDonConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.TrangThaiHoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiHoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.TrangThaiHoaDonRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.TrangThaiHoaDonService;

@Service
public class TrangThaiHoaDonServiceImp implements TrangThaiHoaDonService {

	@Autowired
	private TrangThaiHoaDonRepository repository;
	@Autowired
	private TrangThaiHoaDonConverter converter;
	
	@Override
	public List<TrangThaiHoaDonDto> layDsTrangThaiHoaDon() {
		List<TrangThaiHoaDon> dsTrangThaiHoaDonCanLay = repository.findAll();
		List<TrangThaiHoaDonDto> dsTrangThaiHoaDonDaChuyenDoi = dsTrangThaiHoaDonCanLay.stream().map(converter::convertToDto).collect(Collectors.toList());
		return dsTrangThaiHoaDonDaChuyenDoi;
	}

}
