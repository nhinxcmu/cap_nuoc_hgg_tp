package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.TrangThaiDongHoConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.TrangThaiDongHoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiDongHo;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.TrangThaiDongHoRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.TrangThaiDongHoService;

@Service
public class TrangThaiDongHoServiceImp implements TrangThaiDongHoService {

	@Autowired
	private TrangThaiDongHoRepository repository;
	@Autowired
	private TrangThaiDongHoConverter converter;

	@Override
	public List<TrangThaiDongHoDto> layDsTrangThaiDongHo() {
		List<TrangThaiDongHo> dsTrangThaiDongHoCanLay = repository.findAll();
		List<TrangThaiDongHoDto> dsTrangThaiDongHoDaChuyenDoi = dsTrangThaiDongHoCanLay.stream()
				.map(converter::convertToDto).collect(Collectors.toList());
		return dsTrangThaiDongHoDaChuyenDoi;
	}

}
