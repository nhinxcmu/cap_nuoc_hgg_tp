package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.SoGhi;

public interface SoGhiRepository extends CrudRepository<SoGhi, Long> {

	@EntityGraph(attributePaths = { "duong" })
	Page<SoGhi> findByOrderByIdSoGhiDesc(Pageable pageable);

	@EntityGraph(attributePaths = { "duong" })
	Page<SoGhi> findByTenSoGhiContainsOrderByIdSoGhiDesc(String tenSoGhi, Pageable pageable);

	@EntityGraph(attributePaths = { "duong" })
	Page<SoGhi> findByDuong_IdDuongOrderByIdSoGhiDesc(Long idDuong, Pageable pageable);

	@EntityGraph(attributePaths = { "duong" })
	Page<SoGhi> findByDuong_IdDuongAndTenSoGhiContainsOrderByIdSoGhiDesc(Long idDuong, String tenSoGhi,
			Pageable pageable);

	@EntityGraph(attributePaths = { "duong.khuVuc.donVi" })
	SoGhi findByIdSoGhi(Long idSoGhi);

	List<SoGhi> findByDuong_IdDuongOrderByTenSoGhi(Long idDuong);

	@EntityGraph(attributePaths = { "duong.khuVuc.donVi" })
	List<SoGhi> findByLichGhiThus_Thangs_IdThangAndLichGhiThus_NguoiDuocPhanCong_IdNguoiDungOrderByTenSoGhi(
			Long idThang, Long idNguoiDung);

	@EntityGraph(attributePaths = { "duong.khuVuc.donVi" })
	List<SoGhi> findByLichGhiThus_Thangs_IdThangAndLichGhiThus_NguoiDuocPhanCong_IdNguoiDungAndDuong_IdDuongOrderByTenSoGhi(
			Long idThang, Long idNguoiDung, Long idDuong);

	@EntityGraph(attributePaths = { "duong.khuVuc.donVi" })
	List<SoGhi> findDistinctByIdSoGhiInAndThongTinKhachHangs_KhachHang_HoaDons_TrangThaiChiSo_IdTrangThaiChiSoAndThongTinKhachHangs_Thangs_IdThangAndThongTinKhachHangs_KhachHang_HoaDons_Thang_IdThang(
			List<Long> dsIdSoGhi, long idTrangThaiChiSo, long idThangKhachHang, long idThangHoaDon);

	SoGhi findByTenSoGhi(String tenSoGhi);

}
