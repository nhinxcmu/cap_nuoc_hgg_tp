package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.HoaDonDienTuLapDatConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDienTuLapDatDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDonDienTuLapDat;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HoaDonDienTuLapDatRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HoaDonDienTuLapDatService;

@Service
public class HoaDonDienTuLapDatServiceImp implements HoaDonDienTuLapDatService {

    @Autowired
    private HoaDonDienTuLapDatRepository repo;
    @Autowired
    private HoaDonDienTuLapDatConverter converter;
    @Override
    public List<HoaDonDienTuLapDatDto> layDsHddtTheoHopDong(Long idHoaDon) {
        List<HoaDonDienTuLapDat> hoaDons= repo.findByHoaDonLapDat_IdHoaDon(idHoaDon);
        return hoaDons.stream().map(converter::convertToDto).collect(Collectors.toList());
    }

}