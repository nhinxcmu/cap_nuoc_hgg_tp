package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.List;

public class LayDanhSachHoaDonNganHangDto {
    public ThongTinKhachHangNganHangDto getThongTinKhachHang() {
        return thongTinKhachHang;
    }
    public void setThongTinKhachHang(ThongTinKhachHangNganHangDto thongTinKhachHang) {
        this.thongTinKhachHang = thongTinKhachHang;
    }
    public List<HoaDonNganHangDto> getHoaDons() {
        return hoaDons;
    }
    public void setHoaDons(List<HoaDonNganHangDto> hoaDons) {
        this.hoaDons = hoaDons;
    }
    private ThongTinKhachHangNganHangDto thongTinKhachHang;
    private List<HoaDonNganHangDto> hoaDons;
}


