package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import lombok.Getter;
import lombok.Setter;

public class TrangThaiDongHoDto {

	@Getter
	@Setter
	private Long idTrangThaiDongHo;
	@Getter
	@Setter
	private String tenTrangThaiDongHo;

}
