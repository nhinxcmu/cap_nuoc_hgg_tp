package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.TinhTrangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TinhTrang;

@Component
public class TinhTrangConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public TinhTrangDto convertToDto(TinhTrang entity) {
		if (entity == null) {
			return new TinhTrangDto();
		}
		TinhTrangDto dto = mapper.map(entity, TinhTrangDto.class);
		return dto;
	}

}
