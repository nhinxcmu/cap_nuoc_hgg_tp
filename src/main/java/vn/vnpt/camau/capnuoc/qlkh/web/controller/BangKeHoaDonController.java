package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.jasperreports.engine.JRParameter;
import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.CallReportService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NguoiDungService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;

@Controller
@RequestMapping("/bang-ke-hoa-don")
public class BangKeHoaDonController {
	private ModelAttr modelAttr = new ModelAttr("Bảng kê hóa đơn", "bangkehoadon",
			new String[] { "js/bangkehoadon.js", "js/pdfobject.js" }, new String[] { "" });
	@Autowired
	DataSource dataSource;
	@Autowired
	private CallReportService callReportServ;
	@Autowired
	private NguoiDungService nguoiDungServ;
	@Autowired
	private ThamSoService thamSoServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@RequestMapping(value = "/xem-bao-cao", method = RequestMethod.GET)
	public @ResponseBody String xemBaoCao(HttpSession httpSession) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/bangkehoadon.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("bangkehoadon", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("tenkhuvuc", khuVucDto.getTenKhuVuc());
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		return callReportServ.viewReport(reportFile, parameters, dataSource, httpSession);
	}

	@RequestMapping(value = "/xuat-file-pdf", method = RequestMethod.GET)
	public @ResponseBody void xuatFilePDF(HttpServletResponse response, HttpSession httpSession) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/bangkehoadon.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("bangkehoadon", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("tenkhuvuc", khuVucDto.getTenKhuVuc());
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("pdf", reportFile, parameters, dataSource, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-xls", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLS(HttpServletResponse response, HttpSession httpSession) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/bangkehoadon.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("bangkehoadon", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("tenkhuvuc", khuVucDto.getTenKhuVuc());
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("xls", reportFile, parameters, dataSource, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-xlsx", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLSX(HttpServletResponse response, HttpSession httpSession) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/bangkehoadon.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("bangkehoadon", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("tenkhuvuc", khuVucDto.getTenKhuVuc());
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("xlsx", reportFile, parameters, dataSource, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-rtf", method = RequestMethod.GET)
	public @ResponseBody void xuatFileRTF(HttpServletResponse response, HttpSession httpSession) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/bangkehoadon.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("bangkehoadon", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("tenkhuvuc", khuVucDto.getTenKhuVuc());
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("rtf", reportFile, parameters, dataSource, httpSession, response);
	}
}
