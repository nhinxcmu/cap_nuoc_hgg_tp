package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuCupNuoc;

public interface LichSuCupNuocRepository extends CrudRepository<LichSuCupNuoc, Long> {
	
}
