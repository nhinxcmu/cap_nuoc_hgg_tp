package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Data;

@Data
public class PhieuBaoHongDto {
    private Long idPhieuBaoHong;

    public PhieuBaoHongDto() {
    }

    public PhieuBaoHongDto(Long idPhieuBaoHong) {
        this.idPhieuBaoHong = idPhieuBaoHong;
    }

    @Mapping("nguoiDung.idNguoiDung")
    private Long idNguoiDung;
    @Mapping("nguoiDung.hoTen")
    private String hoTenNguoiLap;
    @Mapping("thang.idThang")
    private Long idThang;
    @Mapping("thang.tenThang")
    private String tenThang;

    @Mapping("thongTinKhachHang.idThongTinKhachHang")
    private Long idThongTinKhachHang;
    @Mapping("thongTinKhachHang.khachHang.idKhachHang")
    private Long idKhachHang;
    @Mapping("thongTinKhachHang.maKhachHang")
    private String maKhachHang;
    @Mapping("thongTinKhachHang.tenKhachHang")
    private String tenKhachHang;

    @Mapping("tinhTrang.idTinhTrang")
    private Long idTinhTrang;
    @Mapping("tinhTrang.tenTinhTrang")
    private String tenTinhTrang;

    private String moTa;
    private String hinhAnh;
    private Integer trangThai;
    private Date ngayLap;
    private String base64Image;
    private String ghiChu;
}
