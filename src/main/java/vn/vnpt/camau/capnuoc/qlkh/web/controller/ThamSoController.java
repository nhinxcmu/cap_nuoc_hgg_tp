package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThamSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;


@Controller
@RequestMapping("/tham-so")
public class ThamSoController {
	private ModelAttr modelAttr = new ModelAttr("Danh mục tham số", "thamso", new String[]{"jdgrid/jdgrid-v3.js","jdpage/jdpage.js","js/thamso.js"},
			new String[]{"jdgrid/jdgrid.css"});
	@Autowired
	private ThamSoService thamSoServ;
	
	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
	    return "layout";
	}
	
	@GetMapping("/lay-ds")
	public @ResponseBody Page<ThamSoDto> layDanhSach(Pageable page){
		return thamSoServ.layDsThamSo("", page);
	}
	
	@GetMapping("/lay-ct")
	public @ResponseBody ThamSoDto layChiTiet(long id){
		return thamSoServ.layThamSo(id);
	}
	
	@PostMapping("/luu")
	public @ResponseBody String luu(ThamSoDto dto){
		try {
			thamSoServ.luuThamSo(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
