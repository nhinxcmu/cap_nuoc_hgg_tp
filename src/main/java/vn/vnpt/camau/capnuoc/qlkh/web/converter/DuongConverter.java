package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.DuongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Duong;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhuVuc;

@Component
public class DuongConverter {

	@Autowired
	private DozerBeanMapper mapper;
	
	public DuongDto convertToDto(Duong entity) {
		if (entity == null) {
			return new DuongDto();
		}
		DuongDto dto = mapper.map(entity, DuongDto.class);
		return dto;
	}
	
	public Duong convertToEntity(DuongDto dto) {
		if (dto == null) {
			return new Duong();
		}
		Duong entity = mapper.map(dto, Duong.class);
		setKhuVuc(dto, entity);
		return entity;
	}

	private void setKhuVuc(DuongDto dto, Duong entity) {
		if (dto.getIdKhuVuc() != null) {
			KhuVuc khuVuc = new KhuVuc();
			khuVuc.setIdKhuVuc(dto.getIdKhuVuc());
			entity.setKhuVuc(khuVuc);
		}
	}

}
