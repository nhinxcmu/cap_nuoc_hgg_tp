package vn.vnpt.camau.capnuoc.qlkh.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class Response {
	private int resCode;
	private String resMessage;
	
	public Response() {
	}

	public Response(int resCode, String resMessage) {
		this.resCode = resCode;
		this.resMessage = resMessage;
	}

	public int getResCode() {
		return resCode;
	}

	public void setResCode(int resCode) {
		this.resCode = resCode;
	}

	public String getResMessage() {
		return resMessage;
	}

	public void setResMessage(String resMessage) {
		this.resMessage = resMessage;
	}

	@Override
	public String toString() {
		ObjectMapper mapper=new ObjectMapper();
		JsonNode node=mapper.createObjectNode();
		((ObjectNode)node).put("resCode", resCode);
		((ObjectNode)node).put("resMessage", resMessage);
		try {
			return mapper.writeValueAsString(node);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "{\"resCode\":" + resCode + ",\"resMessage\":\"" + resMessage + "\"}";
		}
	}
	
}
