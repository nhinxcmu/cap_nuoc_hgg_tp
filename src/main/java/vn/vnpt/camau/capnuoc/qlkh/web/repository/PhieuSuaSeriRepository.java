package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.PhieuSuaSeri;

public interface PhieuSuaSeriRepository extends CrudRepository<PhieuSuaSeri, Long> {
    @EntityGraph(attributePaths = { "nguoiDung", "dongHo","thang","thongTinKhachHang.khachHang" })
    @Query("select p from PhieuSuaSeri p where (p.nguoiDung.idNguoiDung = ?1 or ?1=-1)"
            + " and (p.trangThai=?2 or ?2=-1)" + " and (p.ngayLap between ?3 and ?4)"
            +" order by p.idPhieuSuaSeri")
    Page<PhieuSuaSeri> layDs(Long idNguoiDung, int trangThai, Date tuNgay, Date denNgay, Pageable pageable);

    @EntityGraph(attributePaths = { "nguoiDung", "dongHo","thang","thongTinKhachHang.khachHang" })
    @Query("select p from PhieuSuaSeri p where (p.nguoiDung.idNguoiDung = ?1 or ?1=-1)"
            + " and (p.trangThai=?2 or ?2=-1)" + " and (p.ngayLap between ?3 and ?4)"
            +" order by p.idPhieuSuaSeri")
    List<PhieuSuaSeri> layDs(Long idNguoiDung, int trangThai, Date tuNgay, Date denNgay);

    @Query("select count(1) from PhieuSuaSeri p where (p.nguoiDung.idNguoiDung = ?1 or ?1=-1)  and (p.trangThai=?2 or ?2=-1) ")
    int demPhieuTheoTrangThai(Long idNguoiDung, int trangThai);

    @EntityGraph(attributePaths = { "nguoiDung", "dongHo","thang","thongTinKhachHang.khachHang" })
    PhieuSuaSeri findOne(Long idPhieuSuaSeri);
}
