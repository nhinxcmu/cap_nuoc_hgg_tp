package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.NganHangDto;

public interface NganHangService {

	Page<NganHangDto> layDsNganHang(Pageable pageable);
	
	List<NganHangDto> layDsNganHang();
	
	NganHangDto layNganHang(long id);
	
	NganHangDto luuNganHang(NganHangDto dto) throws Exception;
	
	void xoaNganHang(long id) throws Exception;

}
