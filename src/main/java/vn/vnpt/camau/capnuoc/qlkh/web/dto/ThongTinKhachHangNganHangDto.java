package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThongTinKhachHang;

public class ThongTinKhachHangNganHangDto {
    private String maKhachHang;
    private String tenKhachHang;
    private String diaChiSuDung;
    private String diaChiThanhToan;
    private String soDienThoai;
    private String maDongHo;
    private String soSeri;

    public ThongTinKhachHangNganHangDto(HoaDonNganHangDto hoaDonNganHangDto) {
        this.maKhachHang = hoaDonNganHangDto.getMaKhachHang();
        this.tenKhachHang = hoaDonNganHangDto.getTenKhachHang();
        this.diaChiSuDung = hoaDonNganHangDto.getDiaChiSuDung();
        this.soDienThoai = hoaDonNganHangDto.getSoDienThoai();
        this.maDongHo = hoaDonNganHangDto.getMaDongHo();
        this.soSeri = hoaDonNganHangDto.getSoSeri();
    }

    public ThongTinKhachHangNganHangDto(ThongTinKhachHang tt) {
        this.maKhachHang = tt.getMaKhachHang();
        this.tenKhachHang = tt.getTenKhachHang();
        this.diaChiSuDung = tt.getDiaChiSuDung();
        this.soDienThoai = tt.getSoDienThoai();
    }

    public ThongTinKhachHangNganHangDto(KhachHangDto kh) {
        this.maKhachHang = kh.getMaKhachHang();
        this.tenKhachHang = kh.getTenKhachHang();
        this.diaChiSuDung = kh.getDiaChiSuDung();
        this.soDienThoai = kh.getSoDienThoai();
        this.maDongHo = kh.getMaDongHo();
        this.soSeri = kh.getSoSeri();
    }

    public String getMaDongHo() {
        return maDongHo;
    }

    public void setMaDongHo(String maDongHo) {
        this.maDongHo = maDongHo;
    }

    public String getSoSeri() {
        return soSeri;
    }

    public void setSoSeri(String soSeri) {
        this.soSeri = soSeri;
    }

    public String getMaKhachHang() {
        return maKhachHang;
    }

    public String getSoDienThoai() {
        return soDienThoai;
    }

    public void setSoDienThoai(String soDienThoai) {
        this.soDienThoai = soDienThoai;
    }

    public String getDiaChiThanhToan() {
        return diaChiThanhToan;
    }

    public void setDiaChiThanhToan(String diaChiThanhToan) {
        this.diaChiThanhToan = diaChiThanhToan;
    }

    public String getDiaChiSuDung() {
        return diaChiSuDung;
    }

    public void setDiaChiSuDung(String diaChiSuDung) {
        this.diaChiSuDung = diaChiSuDung;
    }

    public String getTenKhachHang() {
        return tenKhachHang;
    }

    public void setTenKhachHang(String tenKhachHang) {
        this.tenKhachHang = tenKhachHang;
    }

    public void setMaKhachHang(String maKhachHang) {
        this.maKhachHang = maKhachHang;
    }
}
