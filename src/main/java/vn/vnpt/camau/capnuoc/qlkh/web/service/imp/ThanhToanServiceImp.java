package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.ThanhToanConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThanhToanDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThanhToan;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThanhToanRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThanhToanService;

@Service
public class ThanhToanServiceImp implements ThanhToanService {

	@Autowired
	private ThanhToanRepository repository;
	@Autowired
	private ThanhToanConverter converter;
	@Autowired
	private HttpSession httpSession;
	
	@Override
	public Page<ThanhToanDto> layDsThanhToan(long idThongTinKhachHang, Pageable pageable) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		Page<ThanhToan> dsThanhToanCanLay = repository.findByThang_IdThangAndHoaDon_KhachHang_ThongTinKhachHangs_IdThongTinKhachHangOrderByIdThanhToanDesc(idThangLamViec, idThongTinKhachHang, pageable);
		Page<ThanhToanDto> dsThanhToanDaChuyenDoi = dsThanhToanCanLay.map(converter::convertToDto);
		return dsThanhToanDaChuyenDoi;
	}

}
