package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

public class DoiTuongDto implements Comparable<DoiTuongDto> {

	@Getter
	@Setter
	@NotNull
	private Long idDoiTuong;
	@Getter
	@Setter
	@NotNull
	@Size(max = 10)
	private String maDoiTuong;
	@Getter
	@Setter
	@NotNull
	@Size(max = 250)
	private String tenDoiTuong;
	@Getter
	@Setter
	@NotNull
	private int mucSuDung;
	
	@Override
	public int compareTo(DoiTuongDto object) {
		return this.tenDoiTuong.compareTo(object.getTenDoiTuong());
	}

}
