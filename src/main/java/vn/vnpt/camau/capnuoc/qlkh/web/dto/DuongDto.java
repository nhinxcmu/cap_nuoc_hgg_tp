package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class DuongDto implements Comparable<DuongDto> {

	@Getter
	@Setter
	@NotNull
	private Long idDuong;
	@Getter
	@Setter
	@Mapping("khuVuc.idKhuVuc")
	private Long idKhuVuc;
	@Getter
	@Setter
	@Mapping("khuVuc.tenKhuVuc")
	private String tenKhuVuc;
	@Getter
	@Setter
	@Mapping("khuVuc.donVi.idDonVi")
	private Long idDonVi;
	@Getter
	@Setter
	@Mapping("khuVuc.donVi.tenDonVi")
	private String tenDonVi;
	@Getter
	@Setter
	@NotNull
	@Size(max = 250)
	private String tenDuong;
	
	@Override
	public int compareTo(DuongDto object) {
		return this.tenDuong.compareTo(object.getTenDuong());
	}

}
