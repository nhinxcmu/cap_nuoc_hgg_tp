package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.CayDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoGhiDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.TrangThaiChiSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.CallReportService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ChiTietGiaNuocService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.LichGhiThuService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NguoiDungService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.SoGhiService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.TrangThaiChiSoService;

@Controller
@RequestMapping("/bao-cao-trang-thai")
public class BaoCaoTrangThaiController {
	private ModelAttr modelAttr = new ModelAttr("Báo cáo sử dụng nước theo trạng thái", "baocaotrangthai",
			new String[] { "fancytree-2.23.0/jquery.fancytree-all-deps.min.js",
					"fancytree-2.23.0/jquery.fancytree.dnd.js", "bower_components/select2/dist/js/select2.min.js",
					"js/baocaotrangthai.js", "js/pdfobject.js" },
			new String[] { "fancytree-2.23.0/skin-win8/ui.fancytree.min.css",
					"bower_components/select2/dist/css/select2.min.css" });
	@Autowired
	DataSource dataSource;
	@Autowired
	private CallReportService callReportServ;
	@Autowired
	private LichGhiThuService lichGhiThuServ;
	@Autowired
	private SoGhiService soGhiServ;
	@Autowired
	private ChiTietGiaNuocService chiTietGiaNuocServ;
	@Autowired
	private NguoiDungService nguoiDungServ;
	@Autowired
	private ThamSoService thamSoServ;
	@Autowired
	private TrangThaiChiSoService trangThaiChiSoServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/lay-ds-trang-thai")
	public @ResponseBody List<TrangThaiChiSoDto> layDsTrangThaiChiSo() {
		return trangThaiChiSoServ.layDsTrangThaiChiSo();
	}

	@GetMapping("/du-lieu-cay")
	public @ResponseBody List<CayDto> layDsCaySoGhi() {
		return lichGhiThuServ.layDsCaySoGhi();
	}

	@RequestMapping(value = "/xem-bao-cao", method = RequestMethod.GET)
	public @ResponseBody String xemBaoCao(HttpSession httpSession, String idsoghi, long trangthai, String tentrangthai)
			throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;

		if (trangthai == 3 || trangthai == 10) {
			classPathResource = new ClassPathResource("/static/report/trangthai_chuaghi.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_chuaghi", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else if (trangthai == 9) {
			classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_binhthuong", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else if (trangthai == 7) {
			classPathResource = new ClassPathResource("/static/report/trangthai_cupnuoc.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_cupnuoc", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else {
			classPathResource = new ClassPathResource("");
			inputStream = classPathResource.getInputStream();
			reportFile = new File("");
		}

		String fileName = reportFile.getPath();
		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		List<Long> intList = new ArrayList<Long>();
		if (idsoghi.equals(""))
			return "";
		for (String s : idsoghi.split(","))
			intList.add(Long.valueOf(s));
		if (intList.size() == 0)
			return "";

		List<SoGhiDto> listSoGhiDto = soGhiServ.layDsSoGhiTheoTrangThaiChiSo(intList, trangthai);

		if (listSoGhiDto.size() == 0)
			return "";
		for (SoGhiDto soghi : listSoGhiDto) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			List<BigDecimal> dsMucGia = chiTietGiaNuocServ.layDsMucGia(soghi.getIdSoGhi());
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 5) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_5.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_5", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
			}
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 6) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_6.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_6", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
				parameters.put("tieude6", dsMucGia.get(5));
			}
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 7) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_7.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_7", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
				parameters.put("tieude6", dsMucGia.get(5));
				parameters.put("tieude7", dsMucGia.get(6));
			}
			/*
			 * if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() != 5 &&
			 * dsMucGia.size() != 6 && dsMucGia.size() != 5 && dsMucGia.size() != 7)
			 * continue;
			 */
			parameters.put("idtrangthai", trangthai);
			parameters.put("tentrangthai", tentrangthai);
			parameters.put("idsoghi", soghi.getIdSoGhi());
			parameters.put("tensoghi", soghi.getTenSoGhi());
			parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
			parameters.put("tenduong", soghi.getTenDuong());
			parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
			parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
			parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
			Connection conn = dataSource.getConnection();
			JasperPrint print = JasperFillManager.fillReport(fileName, parameters, conn);
			conn.close();
			jasperPrintList.add(print);
		}
		if (jasperPrintList.size() == 0)
			return "";
		return callReportServ.viewListReport(jasperPrintList, reportFile, httpSession);
	}

	@RequestMapping(value = "/xuat-file-pdf", method = RequestMethod.GET)
	public @ResponseBody void xuatFilePDF(HttpServletResponse response, HttpSession httpSession, String idsoghi,
			long trangthai, String tentrangthai) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;

		if (trangthai == 3 || trangthai == 10) {
			classPathResource = new ClassPathResource("/static/report/trangthai_chuaghi.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_chuaghi", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else if (trangthai == 9) {
			classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_binhthuong", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else if (trangthai == 7) {
			classPathResource = new ClassPathResource("/static/report/trangthai_cupnuoc.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_cupnuoc", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else {
			classPathResource = new ClassPathResource("");
			inputStream = classPathResource.getInputStream();
			reportFile = new File("");
		}

		String fileName = reportFile.getPath();
		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		List<Long> intList = new ArrayList<Long>();
		if (idsoghi.equals(""))
			return;
		for (String s : idsoghi.split(","))
			intList.add(Long.valueOf(s));
		if (intList.size() == 0)
			return;
		if (soGhiServ.layDsSoGhiTheoTrangThaiChiSo(intList, trangthai).size() == 0)
			return;
		for (SoGhiDto soghi : soGhiServ.layDsSoGhiTheoTrangThaiChiSo(intList, trangthai)) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			List<BigDecimal> dsMucGia = chiTietGiaNuocServ.layDsMucGia(soghi.getIdSoGhi());
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 5) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_5.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_5", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
			}
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 6) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_6.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_6", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
				parameters.put("tieude6", dsMucGia.get(5));
			}
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 7) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_7.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_7", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
				parameters.put("tieude6", dsMucGia.get(5));
				parameters.put("tieude7", dsMucGia.get(6));
			}
			/*
			 * if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() != 5 &&
			 * dsMucGia.size() != 6 && dsMucGia.size() != 5 && dsMucGia.size() != 7)
			 * continue;
			 */
			parameters.put("idtrangthai", trangthai);
			parameters.put("tentrangthai", tentrangthai);
			parameters.put("idsoghi", soghi.getIdSoGhi());
			parameters.put("tensoghi", soghi.getTenSoGhi());
			parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
			parameters.put("tenduong", soghi.getTenDuong());
			parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
			parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
			parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
			Connection conn = dataSource.getConnection();
			JasperPrint print = JasperFillManager.fillReport(fileName, parameters, conn);
			conn.close();
			jasperPrintList.add(print);
		}
		if (jasperPrintList.size() == 0)
			return;
		callReportServ.printListReport(jasperPrintList, "pdf", reportFile, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-xls", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLS(HttpServletResponse response, HttpSession httpSession, String idsoghi,
			long trangthai, String tentrangthai) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;

		if (trangthai == 3 || trangthai == 10) {
			classPathResource = new ClassPathResource("/static/report/trangthai_chuaghi.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_chuaghi", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else if (trangthai == 9) {
			classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_binhthuong", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else if (trangthai == 7) {
			classPathResource = new ClassPathResource("/static/report/trangthai_cupnuoc.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_cupnuoc", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else {
			classPathResource = new ClassPathResource("");
			inputStream = classPathResource.getInputStream();
			reportFile = new File("");
		}

		String fileName = reportFile.getPath();
		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		List<Long> intList = new ArrayList<Long>();
		if (idsoghi.equals(""))
			return;
		for (String s : idsoghi.split(","))
			intList.add(Long.valueOf(s));
		if (intList.size() == 0)
			return;
		if (soGhiServ.layDsSoGhiTheoTrangThaiChiSo(intList, trangthai).size() == 0)
			return;
		for (SoGhiDto soghi : soGhiServ.layDsSoGhiTheoTrangThaiChiSo(intList, trangthai)) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			List<BigDecimal> dsMucGia = chiTietGiaNuocServ.layDsMucGia(soghi.getIdSoGhi());
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 5) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_5.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_5", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
			}
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 6) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_6.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_6", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
				parameters.put("tieude6", dsMucGia.get(5));
			}
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 7) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_7.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_7", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
				parameters.put("tieude6", dsMucGia.get(5));
				parameters.put("tieude7", dsMucGia.get(6));
			}
			/*
			 * if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() != 5 &&
			 * dsMucGia.size() != 6 && dsMucGia.size() != 5 && dsMucGia.size() != 7)
			 * continue;
			 */
			parameters.put("idtrangthai", trangthai);
			parameters.put("tentrangthai", tentrangthai);
			parameters.put("idsoghi", soghi.getIdSoGhi());
			parameters.put("tensoghi", soghi.getTenSoGhi());
			parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
			parameters.put("tenduong", soghi.getTenDuong());
			parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
			parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
			parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
			Connection conn = dataSource.getConnection();
			JasperPrint print = JasperFillManager.fillReport(fileName, parameters, conn);
			conn.close();
			jasperPrintList.add(print);
		}
		if (jasperPrintList.size() == 0)
			return;
		callReportServ.printListReport(jasperPrintList, "xls", reportFile, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-xlsx", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLSX(HttpServletResponse response, HttpSession httpSession, String idsoghi,
			long trangthai, String tentrangthai) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;

		if (trangthai == 3 || trangthai == 10) {
			classPathResource = new ClassPathResource("/static/report/trangthai_chuaghi.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_chuaghi", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else if (trangthai == 9) {
			classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_binhthuong", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else if (trangthai == 7) {
			classPathResource = new ClassPathResource("/static/report/trangthai_cupnuoc.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_cupnuoc", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else {
			classPathResource = new ClassPathResource("");
			inputStream = classPathResource.getInputStream();
			reportFile = new File("");
		}

		String fileName = reportFile.getPath();
		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		List<Long> intList = new ArrayList<Long>();
		if (idsoghi.equals(""))
			return;
		for (String s : idsoghi.split(","))
			intList.add(Long.valueOf(s));
		if (intList.size() == 0)
			return;
		if (soGhiServ.layDsSoGhiTheoTrangThaiChiSo(intList, trangthai).size() == 0)
			return;
		for (SoGhiDto soghi : soGhiServ.layDsSoGhiTheoTrangThaiChiSo(intList, trangthai)) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			List<BigDecimal> dsMucGia = chiTietGiaNuocServ.layDsMucGia(soghi.getIdSoGhi());
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 5) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_5.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_5", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
			}
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 6) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_6.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_6", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
				parameters.put("tieude6", dsMucGia.get(5));
			}
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 7) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_7.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_7", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
				parameters.put("tieude6", dsMucGia.get(5));
				parameters.put("tieude7", dsMucGia.get(6));
			}
			/*
			 * if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() != 5 &&
			 * dsMucGia.size() != 6 && dsMucGia.size() != 5 && dsMucGia.size() != 7)
			 * continue;
			 */
			parameters.put("idtrangthai", trangthai);
			parameters.put("tentrangthai", tentrangthai);
			parameters.put("idsoghi", soghi.getIdSoGhi());
			parameters.put("tensoghi", soghi.getTenSoGhi());
			parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
			parameters.put("tenduong", soghi.getTenDuong());
			parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
			parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
			parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
			Connection conn = dataSource.getConnection();
			JasperPrint print = JasperFillManager.fillReport(fileName, parameters, conn);
			conn.close();
			jasperPrintList.add(print);
		}
		if (jasperPrintList.size() == 0)
			return;
		callReportServ.printListReport(jasperPrintList, "xlsx", reportFile, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-rtf", method = RequestMethod.GET)
	public @ResponseBody void xuatFileRTF(HttpServletResponse response, HttpSession httpSession, String idsoghi,
			long trangthai, String tentrangthai) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;

		if (trangthai == 3 || trangthai == 10) {
			classPathResource = new ClassPathResource("/static/report/trangthai_chuaghi.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_chuaghi", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else if (trangthai == 9) {
			classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_binhthuong", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else if (trangthai == 7) {
			classPathResource = new ClassPathResource("/static/report/trangthai_cupnuoc.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("trangthai_cupnuoc", ".jasper");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			} finally {
				IOUtils.closeQuietly(inputStream);
			}
		} else {
			classPathResource = new ClassPathResource("");
			inputStream = classPathResource.getInputStream();
			reportFile = new File("");
		}

		String fileName = reportFile.getPath();
		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		List<Long> intList = new ArrayList<Long>();
		if (idsoghi.equals(""))
			return;
		for (String s : idsoghi.split(","))
			intList.add(Long.valueOf(s));
		if (intList.size() == 0)
			return;
		if (soGhiServ.layDsSoGhiTheoTrangThaiChiSo(intList, trangthai).size() == 0)
			return;
		for (SoGhiDto soghi : soGhiServ.layDsSoGhiTheoTrangThaiChiSo(intList, trangthai)) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			List<BigDecimal> dsMucGia = chiTietGiaNuocServ.layDsMucGia(soghi.getIdSoGhi());
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 5) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_5.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_5", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
			}
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 6) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_6.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_6", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
				parameters.put("tieude6", dsMucGia.get(5));
			}
			if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() == 7) {
				classPathResource = new ClassPathResource("/static/report/trangthai_binhthuong_7.jasper");
				inputStream = classPathResource.getInputStream();
				reportFile = File.createTempFile("trangthai_binhthuong_7", ".jasper");
				try {
					FileUtils.copyInputStreamToFile(inputStream, reportFile);
				} finally {
					IOUtils.closeQuietly(inputStream);
				}
				fileName = reportFile.getPath();
				parameters.put("tieude1", dsMucGia.get(0));
				parameters.put("tieude2", dsMucGia.get(1));
				parameters.put("tieude3", dsMucGia.get(2));
				parameters.put("tieude4", dsMucGia.get(3));
				parameters.put("tieude5", dsMucGia.get(4));
				parameters.put("tieude6", dsMucGia.get(5));
				parameters.put("tieude7", dsMucGia.get(6));
			}
			/*
			 * if ((trangthai == 9 || trangthai == 11) && dsMucGia.size() != 5 &&
			 * dsMucGia.size() != 6 && dsMucGia.size() != 5 && dsMucGia.size() != 7)
			 * continue;
			 */
			parameters.put("idtrangthai", trangthai);
			parameters.put("tentrangthai", tentrangthai);
			parameters.put("idsoghi", soghi.getIdSoGhi());
			parameters.put("tensoghi", soghi.getTenSoGhi());
			parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
			parameters.put("tenduong", soghi.getTenDuong());
			parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
			parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
			parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
			Connection conn = dataSource.getConnection();
			JasperPrint print = JasperFillManager.fillReport(fileName, parameters, conn);
			conn.close();
			jasperPrintList.add(print);
		}
		if (jasperPrintList.size() == 0)
			return;
		callReportServ.printListReport(jasperPrintList, "rtf", reportFile, httpSession, response);
	}
}
