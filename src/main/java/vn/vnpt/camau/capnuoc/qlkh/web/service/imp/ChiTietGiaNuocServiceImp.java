package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ChiTietGiaNuocService;

@Service
public class ChiTietGiaNuocServiceImp implements ChiTietGiaNuocService {

	@Autowired
	private HttpSession httpSession;
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<BigDecimal> layDsMucGia(long idSoGhi) throws Exception {
		try {
			long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
			StoredProcedureQuery storedProcedure = entityManager
					.createStoredProcedureQuery("layMucGiaTheoSoGhi")
					.registerStoredProcedureParameter("idThang", Long.class, ParameterMode.IN)
					.registerStoredProcedureParameter("idSoGhi", Long.class, ParameterMode.IN)
					.setParameter("idThang", idThangLamViec)
					.setParameter("idSoGhi", idSoGhi);
			@SuppressWarnings("rawtypes")
			List list = storedProcedure.getResultList();
			List<BigDecimal> dsMucGia = new ArrayList<BigDecimal>();
			for (Object object : list) {
				dsMucGia.add(new BigDecimal(object.toString()));
			}
			return dsMucGia;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lấy danh sách mức giá.");
		}
	}

}
