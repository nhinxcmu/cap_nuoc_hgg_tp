package vn.vnpt.camau.capnuoc.qlkh.web.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import vn.vnpt.camau.capnuoc.qlkh.web.LoginRequest;
import vn.vnpt.camau.capnuoc.qlkh.web.LoginResponse;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.config.JwtTokenProvider;
import vn.vnpt.camau.capnuoc.qlkh.web.config.UserDetailsImp;
import vn.vnpt.camau.capnuoc.qlkh.web.config.UserDetailsServiceImp;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonNganHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhachHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.LayDanhSachHoaDonNganHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.LichSuApiDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LayDSHoaDonRequestBody;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThanhToanHoaDonRequestBody;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThongTinKhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HoaDonService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhachHangService;

@RestController
@RequestMapping("/api/vnptwater")
public class ApiController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private Md5PasswordEncoder passwordEncoder;


    @Autowired
    private HoaDonService hdService;

    @Autowired
    private KhachHangService khService;

    @RequestMapping("/login")
    public LoginResponse authenticateUser(@Valid @RequestBody LoginRequest loginRequest) throws Exception {

        // Xác thực từ username và password.
        // UserDetailsImp user = (UserDetailsImp)
        // userService.loadUserByUsername(loginRequest.getUsername());
        // if (user == null)
        // throw new Exception("Không tìm thấy người dùng");

        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
                        passwordEncoder.encodePassword(loginRequest.getPassword(), loginRequest.getUsername())));

        // Nếu không xảy ra exception tức là thông tin hợp lệ
        // Set thông tin authentication vào Security Context
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Trả về jwt cho người dùng.
        String jwt = tokenProvider.generateToken((UserDetailsImp) authentication.getPrincipal());
        return new LoginResponse(jwt);

    }

    // Api /api/random yêu cầu phải xác thực mới có thể request
    @GetMapping("/random")
    public String randomStuff() {
        return "JWT Hợp lệ mới có thể thấy được message này";
    }

    @PostMapping("/hoa-don-theo-ma-khach-hang")
    @ResponseBody
    LayDanhSachHoaDonNganHangDto layDsHoaDon(@RequestBody LayDSHoaDonRequestBody maKhachHang) {
        LayDanhSachHoaDonNganHangDto result = new LayDanhSachHoaDonNganHangDto();
        result.setHoaDons(hdService.layDsHoaDonTheoMaKhachHang(maKhachHang.getMaKhachHang()));
        result.setThongTinKhachHang(khService.layKhachHangTheoMaKhachHang(maKhachHang.getMaKhachHang()));
        return result;
    }

    @PostMapping("/thanh-toan-hoa-don")
    @ResponseBody
    Response thanhToanHoaDon(@RequestBody ThanhToanHoaDonRequestBody idHoaDons) {

        LichSuApiDto lichSu = new LichSuApiDto();
        lichSu.setIdNguoiDung(Utilities.layIdNguoiDung());
        lichSu.setThoiDiemPhatSinh(new Date());
        List<Long> dsIdHoaDon = new ArrayList<Long>();
        for (String id : idHoaDons.getIdHoaDons()) {
            try {
                Long _id = Long.parseLong(id);
                dsIdHoaDon.add(_id);
            } catch (Exception e) {
                return new Response(-1, "ERR:4_ID hóa đơn không đúng định dạng");
            }
        }
        Long result;
        try {
            result = hdService.thanhToanHoaDonTrucTuyenNganHang(dsIdHoaDon);
        } catch (Exception e) {
            e.printStackTrace();
            return new Response(-1, e.getMessage());
        }
        return new Response(result.intValue(), "OK");
    }

}
