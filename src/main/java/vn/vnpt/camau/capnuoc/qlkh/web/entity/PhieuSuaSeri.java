package vn.vnpt.camau.capnuoc.qlkh.web.entity;
// Generated Apr 19, 2021 10:54:01 AM by Hibernate Tools 5.2.12.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * PhieuSuaSeri generated by hbm2java
 */
@Entity
@Table(name = "PHIEU_SUA_SERI")
public class PhieuSuaSeri implements java.io.Serializable {

	private Long idPhieuSuaSeri;
	private DongHo dongHo;
	private NguoiDung nguoiDung;
	private String soSeriMoi;
	private String lyDo;
	private Integer trangThai;
	private Date ngayLap;
	private Date ngayCapNhat;
	private Thang thang;
	private ThongTinKhachHang thongTinKhachHang;
	private String lyDoTuChoi;
	private String soSeriCu;
	private NguoiDung nguoiCapNhat;

	public PhieuSuaSeri() {
	}

	public PhieuSuaSeri(DongHo dongHo, NguoiDung nguoiDung, String soSeriMoi, String lyDo, Integer trangThai,
			Date ngayLap) {
		this.dongHo = dongHo;
		this.nguoiDung = nguoiDung;
		this.soSeriMoi = soSeriMoi;
		this.lyDo = lyDo;
		this.trangThai = trangThai;
		this.ngayLap = ngayLap;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID_PHIEU_SUA_SERI", unique = true, nullable = false)
	public Long getIdPhieuSuaSeri() {
		return this.idPhieuSuaSeri;
	}

	public void setIdPhieuSuaSeri(Long idPhieuSuaSeri) {
		this.idPhieuSuaSeri = idPhieuSuaSeri;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_DONG_HO")
	public DongHo getDongHo() {
		return this.dongHo;
	}

	public void setDongHo(DongHo dongHo) {
		this.dongHo = dongHo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_NGUOI_DUNG")
	public NguoiDung getNguoiDung() {
		return this.nguoiDung;
	}

	public void setNguoiDung(NguoiDung nguoiDung) {
		this.nguoiDung = nguoiDung;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_NGUOI_CAP_NHAT")
	public NguoiDung getNguoiCapNhat() {
		return this.nguoiCapNhat;
	}

	public void setNguoiCapNhat(NguoiDung nguoiCapNhat) {
		this.nguoiCapNhat = nguoiCapNhat;
	}

	@Column(name = "SO_SERI_MOI")
	public String getSoSeriMoi() {
		return this.soSeriMoi;
	}

	public void setSoSeriMoi(String soSeriMoi) {
		this.soSeriMoi = soSeriMoi;
	}

	@Column(name = "LY_DO", length = 4000)
	public String getLyDo() {
		return this.lyDo;
	}

	public void setLyDo(String lyDo) {
		this.lyDo = lyDo;
	}

	@Column(name = "TRANG_THAI")
	public Integer getTrangThai() {
		return this.trangThai;
	}

	public void setTrangThai(Integer trangThai) {
		this.trangThai = trangThai;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "NGAY_LAP", length = 10)
	public Date getNgayLap() {
		return this.ngayLap;
	}

	public void setNgayLap(Date ngayLap) {
		this.ngayLap = ngayLap;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "NGAY_CAP_NHAT", length = 10)
	public Date getNgayCapNhat() {
		return this.ngayCapNhat;
	}

	public void setNgayCapNhat(Date ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_THANG")
	public Thang getThang() {
		return this.thang;
	}

	public void setThang(Thang thang) {
		this.thang = thang;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_THONG_TIN_KHACH_HANG")
	public ThongTinKhachHang getThongTinKhachHang() {
		return this.thongTinKhachHang;
	}

	public void setThongTinKhachHang(ThongTinKhachHang thongTinKhachHang) {
		this.thongTinKhachHang = thongTinKhachHang;
	}

	@Column(name = "LY_DO_TU_CHOI")
	public String getLyDoTuChoi() {
		return this.lyDoTuChoi;
	}

	public void setLyDoTuChoi(String lyDoTuChoi) {
		this.lyDoTuChoi = lyDoTuChoi;
	}

	@Column(name = "SO_SERI_CU")
	public String getSoSeriCu() {
		return this.soSeriCu;
	}

	public void setSoSeriCu(String soSeriCu) {
		this.soSeriCu = soSeriCu;
	}
}
