package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DonViDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KeHoachDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DonViService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KeHoachService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThangService;

@Controller
@RequestMapping("/ke-hoach")
public class KeHoachController {
	private ModelAttr modelAttr = new ModelAttr("Kế hoạch", "kehoach",
			new String[] { "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js", "bower_components/select2/dist/js/select2.min.js",
					"js/kehoach.js" },
			new String[] { "bower_components/select2/dist/css/select2.min.css", "jdgrid/jdgrid.css" });
	@Autowired
	private DonViService donViServ;
	@Autowired
	private KhuVucService khuVucServ;
	@Autowired
	private KeHoachService keHoachServ;
	@Autowired
	private ThangService thangServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/init-form")
	public @ResponseBody List<DonViDto> initForm() {
		return donViServ.layDsXiNghiepLamViec();
	}

	@GetMapping("/lay-ds-khu-vuc")
	public @ResponseBody List<KhuVucDto> layDanhSachKhuVuc(long id) {
		return khuVucServ.layDsKhuVuc(id);
	}

	@GetMapping("/lay-ds")
	public @ResponseBody List<KeHoachDto> layDanhSach(long idKhuVuc, String nam, long idThang) {
		return keHoachServ.layDanhSach(idKhuVuc, nam, idThang);
	}

	@GetMapping("/lay-ct")
	public @ResponseBody KeHoachDto layChiTiet(long id) {
		return keHoachServ.layKeHoach(id);
	}

	@PostMapping("/luu")
	public @ResponseBody String luu(KeHoachDto dto) {
		try {
			keHoachServ.luuKeHoach(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			// e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@GetMapping("/lay-ds-nam")
	public @ResponseBody List<String> layDanhSachNam() {
		return thangServ.layDanhSachNam();
	}

	@GetMapping("/lay-ds-thang-theo-nam")
	public @ResponseBody List<ThangDto> layDanhSachThangTheoNam(String nam) {
		return thangServ.layDanhSachThangTheoNam(nam);
	}

	@PostMapping("/them-thang-moi")
	public @ResponseBody String themThangMoi() {
		try {
			thangServ.themThangMoi();
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			// e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

}
