package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.FileBean;
import vn.vnpt.camau.capnuoc.qlkh.web.service.CallReportService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ImportService;

@Controller
@RequestMapping("/import")
public class ImportController {
	private ModelAttr modelAttr = new ModelAttr("Import chỉ số nước", "import",
			new String[] { "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js", "js/import.js" },
			new String[] { "jdgrid/jdgrid.css" });
	@Autowired
	private ImportService importServ;
	@Autowired
	DataSource dataSource;
	@Autowired
	private CallReportService callReportServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}
	
	@RequestMapping(value = "/xuat-file-xlsx", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLSX(HttpServletResponse response, HttpSession httpSession)
			throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/mauimport.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("mauimport", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());

		callReportServ.printReport("xlsx", reportFile, parameters, dataSource, httpSession, response);
	}

	@PostMapping("/xu-ly")
	public @ResponseBody String upload(FileBean uploadItem, BindingResult result) {
		return importServ.importChiSo(uploadItem);
	}
}
