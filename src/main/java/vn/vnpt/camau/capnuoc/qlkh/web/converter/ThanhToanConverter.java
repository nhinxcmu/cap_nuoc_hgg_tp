package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThanhToanDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThanhToan;

@Component
public class ThanhToanConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThanhToanDto convertToDto(ThanhToan entity) {
		if (entity == null) {
			return new ThanhToanDto();
		}
		ThanhToanDto dto = mapper.map(entity, ThanhToanDto.class);
		return dto;
	}

}
