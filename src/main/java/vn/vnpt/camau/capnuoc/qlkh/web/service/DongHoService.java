package vn.vnpt.camau.capnuoc.qlkh.web.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.DongHoDto;

public interface DongHoService {

	DongHoDto layDongHoTheoKhachHang(long idKhachHang);

	DongHoDto layDongHo(String maDongHo, long idKhachHang) throws Exception;
	
	DongHoDto luuDongHo(DongHoDto dto) throws Exception;
	
	void xoaDongHo(long idDongHo, long idKhachHang) throws Exception;

	Page<DongHoDto> layDsDongHoTheoKhachHang(long idKhachHang, Pageable pageable);

	void xoaDongHoKhachHangThangTheoThang(long idKhachHang, long idThang);
	
}
