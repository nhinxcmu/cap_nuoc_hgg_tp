package vn.vnpt.camau.capnuoc.qlkh.web.entity;
// Generated Apr 19, 2018 10:52:34 AM by Hibernate Tools 5.2.8.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * DonVi generated by hbm2java
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "DON_VI")
public class DonVi implements java.io.Serializable {

	private Long idDonVi;
	private Huyen huyen;
	private String tenDonVi;
	private boolean xiNghiep;
	private Set<NguoiDung> nguoiDungs = new HashSet<NguoiDung>(0);
	private Set<KhuVuc> khuVucs = new HashSet<KhuVuc>(0);

	public DonVi() {
	}

	public DonVi(Huyen huyen, String tenDonVi, boolean xiNghiep) {
		this.huyen = huyen;
		this.tenDonVi = tenDonVi;
		this.xiNghiep = xiNghiep;
	}

	public DonVi(Huyen huyen, String tenDonVi, boolean xiNghiep, Set<NguoiDung> nguoiDungs, Set<KhuVuc> khuVucs) {
		this.huyen = huyen;
		this.tenDonVi = tenDonVi;
		this.xiNghiep = xiNghiep;
		this.nguoiDungs = nguoiDungs;
		this.khuVucs = khuVucs;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID_DON_VI", unique = true, nullable = false)
	public Long getIdDonVi() {
		return this.idDonVi;
	}

	public void setIdDonVi(Long idDonVi) {
		this.idDonVi = idDonVi;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_HUYEN", nullable = false)
	public Huyen getHuyen() {
		return this.huyen;
	}

	public void setHuyen(Huyen huyen) {
		this.huyen = huyen;
	}

	@Column(name = "TEN_DON_VI", nullable = false, length = 250)
	public String getTenDonVi() {
		return this.tenDonVi;
	}

	public void setTenDonVi(String tenDonVi) {
		this.tenDonVi = tenDonVi;
	}

	@Column(name = "XI_NGHIEP", nullable = false)
	public boolean isXiNghiep() {
		return this.xiNghiep;
	}

	public void setXiNghiep(boolean xiNghiep) {
		this.xiNghiep = xiNghiep;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "donVi")
	public Set<NguoiDung> getNguoiDungs() {
		return this.nguoiDungs;
	}

	public void setNguoiDungs(Set<NguoiDung> nguoiDungs) {
		this.nguoiDungs = nguoiDungs;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "donVi")
	public Set<KhuVuc> getKhuVucs() {
		return this.khuVucs;
	}

	public void setKhuVucs(Set<KhuVuc> khuVucs) {
		this.khuVucs = khuVucs;
	}

}
