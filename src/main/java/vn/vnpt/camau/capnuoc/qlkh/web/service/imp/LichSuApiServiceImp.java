package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.LichSuApiConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.LichSuApiDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuApi;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.LichSuApiRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.LichSuKhachHangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.LichSuApiService;

public class LichSuApiServiceImp implements LichSuApiService {
    @Autowired
    private LichSuApiRepository repo;
    @Autowired
    private LichSuApiConverter converter;

    @Override
    public LichSuApiDto luu(LichSuApiDto lichSuApi) {
        LichSuApi model = converter.convertToEntity(lichSuApi);
        model=repo.save(model);
        return converter.convertToDto(model);
    }
    
}
