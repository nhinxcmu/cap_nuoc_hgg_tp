package vn.vnpt.camau.capnuoc.qlkh.web.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucThangDto;

public interface KhuVucThangService {

	Page<KhuVucThangDto> layDsKhuVucThangTheoKhuVucLamViec(Pageable pageable);

	void ketChuyenKhuVucThang(long idThang) throws Exception;

	void ketChuyenTatCa() throws Exception;

	void khoiPhucKhuVucThangDangLamViec(long idThang) throws Exception;

	KhuVucThangDto layKhuVucThangLamViec();
}
