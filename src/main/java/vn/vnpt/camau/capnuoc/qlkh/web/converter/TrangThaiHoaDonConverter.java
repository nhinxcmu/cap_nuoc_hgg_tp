package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.TrangThaiHoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiHoaDon;

@Component
public class TrangThaiHoaDonConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public TrangThaiHoaDonDto convertToDto(TrangThaiHoaDon entity) {
		if (entity == null) {
			return new TrangThaiHoaDonDto();
		}
		TrangThaiHoaDonDto dto = mapper.map(entity, TrangThaiHoaDonDto.class);
		return dto;
	}

}
