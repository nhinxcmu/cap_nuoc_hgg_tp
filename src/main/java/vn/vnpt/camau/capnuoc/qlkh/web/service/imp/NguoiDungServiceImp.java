package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.NguoiDungConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.NguoiDungDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThamSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DonVi;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.NguoiDungRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NguoiDungService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.VaiTroService;

@Service
public class NguoiDungServiceImp implements NguoiDungService {

	@Autowired
	private NguoiDungConverter converter;
	@Autowired
	private NguoiDungRepository repository;
	@Autowired
	private ThamSoService thamSoService;
	@Autowired
	private Md5PasswordEncoder passwordEncoder;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private VaiTroService vaiTroService;

	@Override
	public boolean laNguoiDungThuocXiNghiep() {
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		NguoiDung nguoiDungCanKiemTra = repository.findByIdNguoiDung(idNguoiDungHienHanh);
		DonVi donVi = nguoiDungCanKiemTra.getDonVi();
		return donVi.isXiNghiep();
	}

	@Override
	public boolean laNguoiDungThuocXiNghiep(long idNguoiDung) {
		NguoiDung nguoiDungCanKiemTra = repository.findByIdNguoiDung(idNguoiDung);
		DonVi donVi = nguoiDungCanKiemTra.getDonVi();
		return donVi.isXiNghiep();
	}

	@Override
	public List<NguoiDungDto> layDsNguoiDungThuocXiNghiep() {
		List<NguoiDung> dsNguoiDungCanLay = repository.findByDonVi_XiNghiepIsTrueOrderByHoTen();
		List<NguoiDungDto> dsNguoiDungDaChuyenDoi = dsNguoiDungCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsNguoiDungDaChuyenDoi;
	}

	@Override
	public Page<NguoiDungDto> layDsNguoiDung(long idDonVi, String thongTinCanTim, Pageable pageable) {
		Page<NguoiDung> dsNguoiDungCanLay;
		if (idDonVi == 0) {
			if (thongTinCanTim.equals("")) {
				dsNguoiDungCanLay = repository.findByOrderByIdNguoiDungDesc(pageable);
			} else {
				dsNguoiDungCanLay = repository.findByTenNguoiDungContainsOrHoTenContainsOrderByIdNguoiDungDesc(
						thongTinCanTim, thongTinCanTim, pageable);
			}
		} else {
			if (thongTinCanTim.equals("")) {
				dsNguoiDungCanLay = repository.findByDonVi_IdDonViOrderByIdNguoiDungDesc(idDonVi, pageable);
			} else {
				dsNguoiDungCanLay = repository
						.findByDonVi_IdDonViAndTenNguoiDungContainsOrHoTenContainsOrderByIdNguoiDungDesc(idDonVi,
								thongTinCanTim, thongTinCanTim, pageable);
			}
		}
		Page<NguoiDungDto> dsNguoiDungDaChuyenDoi = dsNguoiDungCanLay.map(converter::convertToDto);
		return dsNguoiDungDaChuyenDoi;
	}

	@Override
	public List<NguoiDungDto> layDsNguoiDung(long idDonVi) {
		String thongTinCanTim = "";
		Pageable pageable = null;
		List<NguoiDungDto> dsNguoiDungCanLay = layDsNguoiDung(idDonVi, thongTinCanTim, pageable).getContent().stream()
				.sorted().collect(Collectors.toList());
		return dsNguoiDungCanLay;
	}

	@Override
	public NguoiDungDto layNguoiDung(long id) {
		NguoiDung nguoiDungCanLay = repository.findByIdNguoiDung(id);
		NguoiDungDto nguoiDungDaChuyenDoi = converter.convertToDto(nguoiDungCanLay);
		return nguoiDungDaChuyenDoi;
	}

	@Override
	public NguoiDungDto luuNguoiDung(NguoiDungDto dto) throws Exception {
		NguoiDung nguoiDungCanLuu;
		if (dto.getIdNguoiDung() == null) {
			nguoiDungCanLuu = themNguoiDung(dto);
		} else {
			nguoiDungCanLuu = capNhatNguoiDung(dto);
		}
		NguoiDungDto nguoiDungDaChuyenDoi = converter.convertToDto(nguoiDungCanLuu);
		return nguoiDungDaChuyenDoi;
	}

	private NguoiDung themNguoiDung(NguoiDungDto dto) throws Exception {
		if (laTenNguoiDungDaTonTai(dto.getIdNguoiDung(), dto.getTenNguoiDung())) {
			throw new Exception("Tên người dùng đã tồn tại.");
		}
		NguoiDung nguoiDungCanLuu = converter.convertToEntity(dto);
		String matKhauMacDinh = layMatKhauMacDinh(dto.getTenNguoiDung());
		nguoiDungCanLuu.setMatKhau(matKhauMacDinh);
		try {
			repository.save(nguoiDungCanLuu);
			return nguoiDungCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu người dùng.");
		}
	}

	private boolean laTenNguoiDungDaTonTai(Long idNguoiDung, String tenNguoiDung) {
		NguoiDung nguoiDungCanKiemTra = repository.findByTenNguoiDung(tenNguoiDung);
		if (nguoiDungCanKiemTra == null) {
			return false;
		}
		return true;
	}

	private String layMatKhauMacDinh(String salt) {
		ThamSoDto thamSoCanLay = thamSoService.layThamSo(Utilities.ID_THAM_SO_MAT_KHAU_MAC_DINH);
		String matKhauMacDinh = thamSoCanLay.getGiaTri();
		String matKhauMaHoa = maHoaMatKhau(matKhauMacDinh, salt);
		return matKhauMaHoa;
	}

	private String maHoaMatKhau(String matKhau, String salt) {
		return passwordEncoder.encodePassword(matKhau, salt);
	}

	private NguoiDung capNhatNguoiDung(NguoiDungDto dto) throws Exception {
		NguoiDung nguoiDungCanKiemTra = kiemTraNguoiDung(dto.getIdNguoiDung());
		NguoiDung nguoiDungCanLuu = converter.convertToEntity(dto);
		nguoiDungCanLuu.setMatKhau(nguoiDungCanKiemTra.getMatKhau());
		try {
			repository.save(nguoiDungCanLuu);
			return nguoiDungCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu người dùng.");
		}
	}

	private NguoiDung kiemTraNguoiDung(Long idNguoiDung) throws Exception {
		NguoiDung nguoiDungCanKiemTra = repository.findOne(idNguoiDung);
		if (nguoiDungCanKiemTra == null) {
			throw new Exception("Không tìm thấy người dùng.");
		}
		return nguoiDungCanKiemTra;
	}

	@Override
	public void xoaNguoiDung(long id) throws Exception {
		kiemTraNguoiDung(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa người dùng.");
			}
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void datLaiMatKhauCuaNguoiDung(long id) throws Exception {
		NguoiDung nguoiDungCanCapNhat = kiemTraNguoiDung(id);
		String matKhauMacDinh = layMatKhauMacDinh(nguoiDungCanCapNhat.getTenNguoiDung());
		nguoiDungCanCapNhat.setMatKhau(matKhauMacDinh);
		try {
			repository.save(nguoiDungCanCapNhat);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể cập nhật người dùng.");
		}
	}

	@Override
	public NguoiDungDto khoaNguoiDung(long id) throws Exception {
		NguoiDung nguoiDungCanCapNhat = kiemTraNguoiDung(id);
		nguoiDungCanCapNhat.setKhoa(true);
		try {
			repository.save(nguoiDungCanCapNhat);
			NguoiDungDto nguoiDungDaChuyenDoi = converter.convertToDto(nguoiDungCanCapNhat);
			return nguoiDungDaChuyenDoi;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể cập nhật người dùng.");
		}
	}

	@Override
	public NguoiDungDto moKhoaNguoiDung(long id) throws Exception {
		NguoiDung nguoiDungCanCapNhat = kiemTraNguoiDung(id);
		nguoiDungCanCapNhat.setKhoa(false);
		try {
			repository.save(nguoiDungCanCapNhat);
			NguoiDungDto nguoiDungDaChuyenDoi = converter.convertToDto(nguoiDungCanCapNhat);
			return nguoiDungDaChuyenDoi;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể cập nhật người dùng.");
		}
	}

	@Override
	public NguoiDungDto capNhatThongTinNguoiDung(NguoiDungDto dto) throws Exception {
		NguoiDung nguoiDungCanCapNhat = kiemTraNguoiDung(dto.getIdNguoiDung());
		nguoiDungCanCapNhat.setHoTen(dto.getHoTen());
		try {
			repository.save(nguoiDungCanCapNhat);
			NguoiDungDto nguoiDungDaChuyenDoi = converter.convertToDto(nguoiDungCanCapNhat);
			return nguoiDungDaChuyenDoi;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể cập nhật người dùng.");
		}
	}

	@Override
	public void doiMatKhauCuaNguoiDung(String matKhauCu, String matKhauMoi) throws Exception {
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		NguoiDung nguoiDungCanCapNhat = kiemTraNguoiDung(idNguoiDungHienHanh);
		String matKhauMaHoa = maHoaMatKhau(matKhauMoi, nguoiDungCanCapNhat.getTenNguoiDung());
		nguoiDungCanCapNhat.setMatKhau(matKhauMaHoa);
		try {
			repository.save(nguoiDungCanCapNhat);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể cập nhật người dùng.");
		}
	}

	@Override
	public List<NguoiDungDto> layDsNhanVienGhiThu(long idVaiTro) {
		Long idDonViLamViec = Utilities.layIdDonViLamViec(httpSession);
		List<NguoiDung> dsNguoiDungCanLay = repository.findByDonVi_IdDonViAndVaiTro_IdVaiTroOrderByHoTen(idDonViLamViec,
				idVaiTro);
		List<NguoiDungDto> dsNguoiDungDaChuyenDoi = dsNguoiDungCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsNguoiDungDaChuyenDoi;
	}

	@Override
	public List<NguoiDungDto> layDsNhanVienGhiThu() {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		long idDonViLamViec = Utilities.layIdDonViLamViec(httpSession);
		List<Long> dsIdVaiTro = vaiTroService.layDsIdVaiTroGhiThu();
		List<NguoiDung> dsNguoiDungCanLay = repository
				.findByDonVi_IdDonViAndVaiTro_IdVaiTroInAndLichGhiThusForIdNguoiDuocPhanCong_Thangs_IdThangOrderByHoTen(
						idDonViLamViec, dsIdVaiTro, idThangLamViec);
		List<NguoiDungDto> dsNguoiDungDaChuyenDoi = dsNguoiDungCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsNguoiDungDaChuyenDoi;
	}

	@Override
	public List<NguoiDungDto> layDsNhanVienThanhToanGiaoDich() {
		Long idDonViLamViec = Utilities.layIdDonViLamViec(httpSession);
		List<NguoiDung> dsNguoiDungCanLay = repository.findByDonVi_IdDonViAndVaiTro_Menus_IdMenuOrderByHoTen(idDonViLamViec,
				Utilities.ID_MENU_THANH_TOAN_GIAO_DICH);
		List<NguoiDungDto> dsNguoiDungDaChuyenDoi = dsNguoiDungCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsNguoiDungDaChuyenDoi;
	}

}
