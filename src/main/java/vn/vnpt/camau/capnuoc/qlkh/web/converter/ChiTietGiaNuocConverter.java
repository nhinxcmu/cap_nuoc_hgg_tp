package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.ChiTietGiaNuocDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ChiTietGiaNuoc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.GiaNuoc;

@Component
public class ChiTietGiaNuocConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ChiTietGiaNuocDto convertToDto(ChiTietGiaNuoc entity) {
		if (entity == null) {
			return new ChiTietGiaNuocDto();
		}
		ChiTietGiaNuocDto dto = mapper.map(entity, ChiTietGiaNuocDto.class);
		return dto;
	}

	public ChiTietGiaNuoc convertToEntity(ChiTietGiaNuocDto dto) {
		if (dto == null) {
			return new ChiTietGiaNuoc();
		}
		ChiTietGiaNuoc entity = mapper.map(dto, ChiTietGiaNuoc.class);
		setGiaNuoc(dto, entity);
		return entity;
	}

	private void setGiaNuoc(ChiTietGiaNuocDto dto, ChiTietGiaNuoc entity) {
		if (dto.getIdGiaNuoc() != null) {
			GiaNuoc giaNuoc = new GiaNuoc();
			giaNuoc.setIdGiaNuoc(dto.getIdGiaNuoc());
			entity.setGiaNuoc(giaNuoc);
		}
	}
}
