package vn.vnpt.camau.capnuoc.qlkh.web.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.service.PhienLamViecService;

@Component
public class AuthenticationSuccessEventImp implements ApplicationListener<AuthenticationSuccessEvent> {

	private final Logger logger = LoggerFactory.getLogger(AuthenticationSuccessEventImp.class);
	@Autowired
	private PhienLamViecService phienLamViecService;
	@Override
	public void onApplicationEvent(AuthenticationSuccessEvent successEvent) {
		Object principal = successEvent.getAuthentication().getPrincipal(); 
		if (principal instanceof UserDetailsImp) {
			UserDetailsImp userDetails = (UserDetailsImp) principal;
			phienLamViecService.thietLapPhienLamViec(userDetails.getId());
			logger.info(String.format("User [%s] logged.", userDetails.getUsername()));
		} else {
			logger.info(principal.toString());
		}
	}

}
