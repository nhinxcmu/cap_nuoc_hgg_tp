package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperPrint;

public interface CallReportService {
	String viewReport(File reportFile, Map<String, Object> parameters, DataSource dataSource, HttpSession httpSession)
			throws Exception;

	String viewListReport(List<JasperPrint> jasperPrintList, File reportFile, HttpSession httpSession) throws Exception;

	void printReport(String type, File reportFile, Map<String, Object> parameters, DataSource dataSource,
			HttpSession httpSession, HttpServletResponse response) throws Exception;

	void printListReport(List<JasperPrint> jasperPrintList, String type, File reportFile, HttpSession httpSession,
			HttpServletResponse response) throws Exception;
}
