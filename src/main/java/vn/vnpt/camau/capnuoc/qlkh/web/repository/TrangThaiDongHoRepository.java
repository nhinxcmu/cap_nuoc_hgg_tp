package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiDongHo;

public interface TrangThaiDongHoRepository extends Repository<TrangThaiDongHo, Long> {

	List<TrangThaiDongHo> findAll();
	
}
