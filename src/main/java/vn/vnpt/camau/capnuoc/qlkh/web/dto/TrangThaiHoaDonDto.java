package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import lombok.Data;

@Data
public class TrangThaiHoaDonDto {

	private Long idTrangThaiHoaDon;
	private String tenTrangThaiHoaDon;
}
