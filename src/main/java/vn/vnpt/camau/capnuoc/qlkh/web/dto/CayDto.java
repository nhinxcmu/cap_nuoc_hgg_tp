package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class CayDto {

	@Getter
	@Setter
	private long key;
	@Getter
	@Setter
	private String title;
	@Getter
	@Setter
	private Boolean isFolder;
	@Getter
	@Setter
	private Boolean selected;
	@Getter
	@Setter
	private List<CayDto> children = new ArrayList<CayDto>(0);
	
}
