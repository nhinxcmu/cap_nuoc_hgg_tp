package vn.vnpt.camau.capnuoc.qlkh.web.config;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.dao.ReflectionSaltSource;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.Menu;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.MenuRepository;

@Configuration
@EnableWebSecurity
@Order(2)
public class BasicWebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private Md5PasswordEncoder passwordEncoder;
	@Autowired
	private MenuRepository menuRepository;

	
    
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		List<Menu> dsMenu = menuRepository.findByMenu_IdMenuIsNotNullOrderByOrderMenu();
		for (Menu menu : dsMenu) {
			http.authorizeRequests().antMatchers("/" + menu.getUrl() + "/**").hasAuthority(menu.getUrl());
		}
		
		http
			.csrf().disable()
			.antMatcher("/**")
			.authorizeRequests()
				.anyRequest().authenticated()			
				.and()
			.formLogin()
				.loginPage("/login")
				.permitAll()
				.and()
			.logout()
				.permitAll()
				.and()
			.exceptionHandling().accessDeniedPage("/forbidden")
			.and()
			.httpBasic();
		http.headers()
		.frameOptions().sameOrigin()
		.httpStrictTransportSecurity().disable();

	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		ReflectionSaltSource saltSource = new ReflectionSaltSource();
		saltSource.setUserPropertyToUse("username");
		authProvider.setSaltSource(saltSource);
		authProvider.setUserDetailsService(userDetailsService);
		authProvider.setPasswordEncoder(passwordEncoder);
		auth.authenticationProvider(authProvider);
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web
			.ignoring()
			.antMatchers("/bower_components/**", "/dist/**", "/css/**", "/img/**", "/plugins/**","/files/**");
	}
}
