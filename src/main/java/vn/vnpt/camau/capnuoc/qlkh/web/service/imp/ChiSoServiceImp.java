package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.ChiSoConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ChiSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ChiSo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuDongBoChiSo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiChiSo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiHoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ChiSoRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HoaDonRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ChiSoService;

@Service
public class ChiSoServiceImp implements ChiSoService {

	@Autowired
	private ChiSoRepository repository;
	@Autowired
	private ChiSoConverter converter;
	@Autowired
	private ThangRepository thangRepository;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private HoaDonRepository hoaDonRepository;

	@Override
	public List<ChiSoDto> layDsChiSoTheoNguoiDung(long idNguoiDung) {
		Thang thangPhanCongGhiThuMoiNhat = thangRepository
				.findTopByLichGhiThuThangs_NguoiDuocPhanCong_IdNguoiDungOrderByIdThangDesc(idNguoiDung);
		if (thangPhanCongGhiThuMoiNhat == null) {
			return new ArrayList<ChiSoDto>();
		}
		long idThangPhanCongGhiThuMoiNhat = thangPhanCongGhiThuMoiNhat.getIdThang();
		List<ChiSo> dsChiSoCanLay = repository.layDsChiSoTheoNguoiDung(idThangPhanCongGhiThuMoiNhat, idNguoiDung);
		List<ChiSoDto> dsChiSoDaChuyenDoi = dsChiSoCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsChiSoDaChuyenDoi;
	}

	@Override
	@Transactional
	public void chuyenDuLieuChiSo(boolean chuyenTatCa) throws Exception {
		List<HoaDon> dsHoaDonCanLay;
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		if (chuyenTatCa) {
			dsHoaDonCanLay = hoaDonRepository
					.findByThang_IdThangAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndKhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndHuyIsFalse(
							idThangLamViec, idThangLamViec, idKhuVucLamViec);
		} else {
			dsHoaDonCanLay = hoaDonRepository
					.findByThang_IdThangAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndKhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndHuyIsFalseAndChiSos_IdChiSoIsNull(
							idThangLamViec, idThangLamViec, idKhuVucLamViec);
		}
		List<ChiSo> dsChiSo = dsHoaDonCanLay.stream().map(converter::convertHoaDonToChiSo).collect(Collectors.toList());
		Thang thangTruoc = thangRepository.findTopByIdThangLessThanOrderByIdThangDesc(idThangLamViec);
		if (thangTruoc != null) {
			themChiSoThangTruoc(dsChiSo, thangTruoc.getIdThang(), idKhuVucLamViec);
		}
		try {
			repository.save(dsChiSo);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể chuyển dữ liệu chỉ số.");
		}
	}

	private void themChiSoThangTruoc(List<ChiSo> dsChiSo, long idThang, long idKhuVuc) {
		List<HoaDon> dsHoaDonCanLay = hoaDonRepository
				.findByThang_IdThangAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndKhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndHuyIsFalse(
						idThang, idThang, idKhuVuc);
		if (!dsHoaDonCanLay.isEmpty()) {
			for (ChiSo chiSo : dsChiSo) {
				HoaDon hoaDon = dsHoaDonCanLay.stream().filter(
						h -> h.getKhachHang().getIdKhachHang() == chiSo.getHoaDon().getKhachHang().getIdKhachHang())
						.findFirst().orElse(new HoaDon());
				chiSo.setChiSoCuThangTruoc(hoaDon.getChiSoCu());
				chiSo.setTieuThuThangTruoc(hoaDon.getTieuThu());
			}
		}
	}

	@Override
	@Transactional
	public void dongBoChiSo(List<ChiSoDto> dsChiSo, boolean boQua) throws Exception {
		List<Long> dsIdChiSo = dsChiSo.stream().map(converter::convertToIdChiSo).collect(Collectors.toList());
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		List<ChiSo> dsChiSoCanKiemTra = repository.findByThang_IdThangAndIdChiSoIn(idThangLamViec, dsIdChiSo);
		if (laSoLuongChiSoKhongPhuHop(dsChiSo, dsChiSoCanKiemTra)) {
			throw new Exception("Số lượng chỉ số không phù hợp.");
		}
		capNhatChiSo(dsChiSo, dsChiSoCanKiemTra, boQua);

	}

	private boolean laSoLuongChiSoKhongPhuHop(List<ChiSoDto> dsChiSo, List<ChiSo> dsChiSoCanKiemTra) {
		return dsChiSo.size() != dsChiSoCanKiemTra.size();
	}

	private void capNhatChiSo(List<ChiSoDto> dsChiSo, List<ChiSo> dsChiSoCanCapNhat, boolean boQua) throws Exception {
		List<LichSuDongBoChiSo> dsLichSuDongBoChiSo = new ArrayList<LichSuDongBoChiSo>();
		for (ChiSo chiSo : dsChiSoCanCapNhat) {
			ChiSoDto chiSoDto = dsChiSo.stream().filter(c -> c.getIdChiSo().equals(chiSo.getIdChiSo())).findFirst()
					.orElse(null);
			if (chiSoDto == null) {
				throw new Exception("Không tìm thấy chỉ số.");
			}

			if (chiSo.isDuyetChiSo() && boQua) {
				continue;
				// throw new Exception("Có chỉ số đã kiểm duyệt.");
			}
			if (chiSo.getNgayGioDongBo() != null && boQua) {
				continue;
				// throw new Exception("Có chỉ số đã đồng bộ.");
			}

			thietLapChiSoCanCapNhat(chiSoDto, chiSo);
			LichSuDongBoChiSo lichSuDongBoChiSo = thietLapLichSuDongBoChiSoCanThem(chiSoDto, chiSo);
			dsLichSuDongBoChiSo.add(lichSuDongBoChiSo);
		}
		try {
			repository.save(dsChiSoCanCapNhat);
		} catch (Exception e) {
			e.printStackTrace();
			luuLichSuDongBoChiSo(dsLichSuDongBoChiSo);
			throw new Exception("Không thể đồng bộ chỉ số.");
		}
	}

	private void thietLapChiSoCanCapNhat(ChiSoDto chiSoDto, ChiSo chiSo) {
		setNguoiCapNhat(chiSo, chiSoDto);
		setNguoiDongBo(chiSo);
		setTrangThaiChiSo(chiSo, chiSoDto);
		chiSo.setChiSoMoi(chiSoDto.getChiSoMoi());
		chiSo.setTieuThu(chiSoDto.getTieuThu());
		chiSo.setNgayGioCapNhat(chiSoDto.getNgayGioCapNhat());
		chiSo.setGhiChu(chiSoDto.getGhiChu());
		chiSo.setNgayGioDongBo(new Date());
		chiSo.setDuyetChiSo(false);
	}

	private void setNguoiCapNhat(ChiSo entity, ChiSoDto dto) {
		if (dto.getIdNguoiCapNhat() != null) {
			NguoiDung nguoiCapNhat = new NguoiDung();
			nguoiCapNhat.setIdNguoiDung(dto.getIdNguoiCapNhat());
			entity.setNguoiDungByIdNguoiCapNhat(nguoiCapNhat);
		}
	}

	private void setNguoiDongBo(ChiSo chiSo) {
		// TODO Auto-generated method stub
		NguoiDung nguoiDongBo = new NguoiDung();
		long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		nguoiDongBo.setIdNguoiDung(idNguoiDungHienHanh);
		chiSo.setNguoiDungByIdNguoiDongBo(nguoiDongBo);
	}

	private void setTrangThaiChiSo(ChiSo entity, ChiSoDto dto) {
		TrangThaiChiSo trangThaiChiSo = new TrangThaiChiSo();
		trangThaiChiSo.setIdTrangThaiChiSo(dto.getIdTrangThaiChiSo());
		entity.setTrangThaiChiSo(trangThaiChiSo);
	}

	private LichSuDongBoChiSo thietLapLichSuDongBoChiSoCanThem(ChiSoDto chiSoDto, ChiSo chiSo) {
		LichSuDongBoChiSo lichSuDongBoChiSo = new LichSuDongBoChiSo();
		lichSuDongBoChiSo.setChiSoCu(chiSo.getChiSoCu());
		lichSuDongBoChiSo.setChiSoMoi(chiSo.getChiSoMoi());
		lichSuDongBoChiSo.setGhiChu(chiSo.getGhiChu());
		lichSuDongBoChiSo.setIdChiSo(chiSo.getIdChiSo());
		lichSuDongBoChiSo.setIdHoaDon(chiSo.getHoaDon().getIdHoaDon());
		lichSuDongBoChiSo.setIdNguoiCapNhat(chiSo.getNguoiDungByIdNguoiCapNhat().getIdNguoiDung());
		lichSuDongBoChiSo.setIdThang(chiSo.getThang().getIdThang());
		lichSuDongBoChiSo.setIdTrangThaiChiSo(chiSo.getTrangThaiChiSo().getIdTrangThaiChiSo());
		lichSuDongBoChiSo.setNgayGioCapNhat(chiSo.getNgayGioCapNhat());
		lichSuDongBoChiSo.setTieuThu(chiSo.getTieuThu());
		return lichSuDongBoChiSo;
	}

	private void luuLichSuDongBoChiSo(List<LichSuDongBoChiSo> dsLichSuDongBoChiSo) {
		// try {
		// lichSuDongBoChiSoRepository.save(dsLichSuDongBoChiSo);
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
	}

	@Override
	public Page<ChiSoDto> layDsChiSoTheoNguoiDung(long idNguoiDung, long idSoGhi, Pageable pageable) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		Page<ChiSo> dsChiSoCanLay;
		if (idSoGhi == 0) {
			dsChiSoCanLay = repository.layDsChiSoTheoNguoiDung(idThangLamViec, idNguoiDung, pageable);
		} else {
			dsChiSoCanLay = repository.layDsChiSoTheoNguoiDungVaSoGhi(idThangLamViec, idNguoiDung, idSoGhi, pageable);
		}
		Page<ChiSoDto> dsChiSoDaChuyenDoi = dsChiSoCanLay.map(converter::convertToDto);
		return dsChiSoDaChuyenDoi;
	}

	@Override
	public List<ChiSoDto> layDsChiSoCoTheKiemDuyetTheoNguoiDung(long idNguoiDung, long idSoGhi) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		List<Long> dsIdTrangThaiChiSo = layDsIdTrangThaiChiSoCoTheKiemDuyet();
		List<ChiSo> dsChiSoCanLay;
		if (idSoGhi == 0) {
			dsChiSoCanLay = repository.layDsChiSoTheoNguoiDung(idThangLamViec, idNguoiDung, dsIdTrangThaiChiSo);
		} else {
			dsChiSoCanLay = repository.layDsChiSoTheoNguoiDung(idThangLamViec, idNguoiDung, idSoGhi,
					dsIdTrangThaiChiSo);
		}
		List<ChiSoDto> dsChiSoDaChuyenDoi = dsChiSoCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsChiSoDaChuyenDoi;
	}

	private List<Long> layDsIdTrangThaiChiSoCoTheKiemDuyet() {
		List<Long> dsIdTrangThaiChiSoCoTheKiemDuyet = new ArrayList<Long>();
		dsIdTrangThaiChiSoCoTheKiemDuyet.add(Utilities.ID_TRANG_THAI_BINH_THUONG);
		dsIdTrangThaiChiSoCoTheKiemDuyet.add(Utilities.ID_TRANG_THAI_KHONG_XAI);
		return dsIdTrangThaiChiSoCoTheKiemDuyet;
	}

	@Override
	@Transactional
	public void kiemDuyetChiSo(long idNguoiDung, long idSoGhi) throws Exception {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		List<Long> dsIdTrangThaiChiSo = layDsIdTrangThaiChiSoCoTheKiemDuyet();
		List<ChiSo> dsChiSoCanLay;
		if (idSoGhi == 0) {
			dsChiSoCanLay = repository.layDsChiSoTheoNguoiDungDeKiemDuyet(idThangLamViec, idNguoiDung,
					dsIdTrangThaiChiSo);
		} else {
			dsChiSoCanLay = repository.layDsChiSoTheoNguoiDungDeKiemDuyet(idThangLamViec, idNguoiDung, idSoGhi,
					dsIdTrangThaiChiSo);
		}
		try {
			capNhatChiSo(dsChiSoCanLay);
			capNhatHoaDon(dsChiSoCanLay);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không kiểm duyệt chỉ số.");
		}
	}

	private void capNhatChiSo(List<ChiSo> dsChiSoCanLay) {
		for (ChiSo chiSo : dsChiSoCanLay) {
			Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
			NguoiDung nguoiDuyet = new NguoiDung();
			nguoiDuyet.setIdNguoiDung(idNguoiDungHienHanh);
			chiSo.setNguoiDungByIdNguoiDuyet(nguoiDuyet);
			chiSo.setDuyetChiSo(true);
			chiSo.setNgayGioDuyet(new Date());
		}
		repository.save(dsChiSoCanLay);
	}

	private void capNhatHoaDon(List<ChiSo> dsChiSoCanLay) {
		List<HoaDon> dsHoaDonCanLuu = new ArrayList<HoaDon>();
		for (ChiSo chiSo : dsChiSoCanLay) {
			HoaDon hoaDonCanLuu = chiSo.getHoaDon();
			hoaDonCanLuu.setNguoiDung(chiSo.getNguoiDungByIdNguoiDuyet());
			hoaDonCanLuu.setTrangThaiChiSo(chiSo.getTrangThaiChiSo());
			setTrangThaiHoaDon(hoaDonCanLuu);
			hoaDonCanLuu.setChiSoMoi(chiSo.getChiSoMoi());
			hoaDonCanLuu.setTieuThu(chiSo.getTieuThu());
			hoaDonCanLuu.setGhiChu(chiSo.getGhiChu());
			dsHoaDonCanLuu.add(hoaDonCanLuu);
		}
		hoaDonRepository.save(dsHoaDonCanLuu);
	}

	private void setTrangThaiHoaDon(HoaDon hoaDon) {
		long idTrangThaiChiSo = hoaDon.getTrangThaiChiSo().getIdTrangThaiChiSo();
		if (idTrangThaiChiSo == Utilities.ID_TRANG_THAI_KHONG_XAI) {
			TrangThaiHoaDon trangThaiHoaDon = new TrangThaiHoaDon();
			trangThaiHoaDon.setIdTrangThaiHoaDon(Utilities.ID_TRANG_THAI_KHONG_PHAT_HANH_HOA_DON);
			hoaDon.setTrangThaiHoaDon(trangThaiHoaDon);
		}
	}

	@Override
	public void kiemDuyetChiSo(List<Long> dsIdChiSo) throws Exception {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		List<Long> dsIdTrangThaiChiSo = layDsIdTrangThaiChiSoCoTheKiemDuyet();
		List<ChiSo> dsChiSoCanLay = repository.layDsChiSoTheoDsIdChiSoDeKiemDuyet(idThangLamViec, dsIdChiSo,
				dsIdTrangThaiChiSo);
		try {
			capNhatChiSo(dsChiSoCanLay);
			capNhatHoaDon(dsChiSoCanLay);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không kiểm duyệt chỉ số.");
		}
	}

}
