package vn.vnpt.camau.capnuoc.qlkh.web.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThanhToanDto;

public interface ThanhToanService {

	Page<ThanhToanDto> layDsThanhToan(long idThongTinKhachHang, Pageable pageable);
	
}
