package vn.vnpt.camau.capnuoc.qlkh.web;

public class ModelAttr {
	private String title;
	private String content;
	private String[] js;
	private String[] css;
	private boolean inJsGrid=true;
	
	public ModelAttr(){
	}
	
	public ModelAttr(String title, String content, String[] js, String[] css, boolean inJsGrid){
		this.title=title;
		this.content=content;
		this.js=js;
		this.css=css;
		this.inJsGrid=inJsGrid;
	}
	
	public ModelAttr(String title, String content, String[] js){
		this.title=title;
		this.content=content;
		this.js=js;
		this.css=new String[]{};
	}
	
	public ModelAttr(String title, String content, String[] js, String[]css){
		this.title=title;
		this.content=content;
		this.js=js;
		this.css=css;
	}
	
	public ModelAttr(String title, String content){
		this.title=title;
		this.content=content;
		this.js=new String[]{};
		this.css=new String[]{};
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String[] getJs() {
		return js;
	}
	public void setJs(String[] js) {
		this.js = js;
	}
	public String[] getCss() {
		return css;
	}
	public void setCss(String[] css) {
		this.css = css;
	}

	public boolean isInJsGrid() {
		return inJsGrid;
	}

	public void setInJsGrid(boolean inJsGrid) {
		this.inJsGrid = inJsGrid;
	}

	public String getHelp() {
		return "help/"+content+".pdf";
	}
}
