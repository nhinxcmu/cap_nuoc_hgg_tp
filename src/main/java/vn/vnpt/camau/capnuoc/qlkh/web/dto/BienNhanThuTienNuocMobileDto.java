package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class BienNhanThuTienNuocMobileDto {

	@Getter
	@Setter
	private String tenCongTy;
	@Getter
	@Setter
	private String tenDonVi;
	@Getter
	@Setter
	private String diaChiCongTy;
	@Getter
	@Setter
	private String soDienThoaiCongTy;
	@Getter
	@Setter
	@Mapping("thang.idThang")
	private Long idThang;
	@Getter
	@Setter
	@Mapping("thang.tenThang")
	private String tenThang;
	@Getter
	@Setter
	private String tuNgay;
	@Getter
	@Setter
	private String denNgay;
	@Getter
	@Setter
	private String maKhachHang;
	@Getter
	@Setter
	private String tenKhachHang;
	@Getter
	@Setter
	private String diaChiSuDung;
	@Getter
	@Setter
	private String soDienThoai;
	@Getter
	@Setter
	private Long idHoaDon;
	@Getter
	@Setter
	private String kyHieu;
	@Getter
	@Setter
	private String soHoaDon;
	@Getter
	@Setter
	private String maDongHo;
	@Getter
	@Setter
	private int chiSoCu;
	@Getter
	@Setter
	private int chiSoMoi;
	@Getter
	@Setter
	private int tieuThu;
	@Getter
	@Setter
	private long tienNuoc;
	@Getter
	@Setter
	private long thue;
	@Getter
	@Setter
	private long phiBvmt;
	@Getter
	@Setter
	private long phiBvr;
	@Getter
	@Setter
	private long tongTien;
	@Getter
	@Setter
	@Mapping("tienThanhToan")
	private long tienNop;
	@Getter
	@Setter
	private String ngayIn;
	@Getter
	@Setter
	private String nhanVienThuNgan;
	@Getter
	@Setter
	private int soLanIn;
	@Getter
	@Setter
	private List<ChiTietHoaDonDto> chiTietHoaDonDtos = new ArrayList<>();

}
