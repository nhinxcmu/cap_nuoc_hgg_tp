package vn.vnpt.camau.capnuoc.qlkh.web.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuKhachHang;

public interface LichSuKhachHangService {

	Page<LichSuKhachHang> layDsLichSuKhachHang(Long idKhachHang, Pageable pageable);

	LichSuKhachHang layLichSuKhachHang(Long idLichSuKhachHang);

}
