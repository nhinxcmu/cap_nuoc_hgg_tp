package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.Repository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuongCupNuoc;

public interface DoiTuongCupNuocRepository extends Repository<DoiTuongCupNuoc, Long> {

	List<DoiTuongCupNuoc> findAll();

	@EntityGraph(attributePaths = { "lyDoCupNuocs" })

	List<DoiTuongCupNuoc> findByLyDoCupNuocs_IdLyDoCupNuoc(long idLyDoCupNuoc);

}
