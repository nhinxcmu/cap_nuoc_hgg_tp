package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;

public interface KhuVucService {

	KhuVucDto layKhuVucLamViec(long idDonVi);
	
	List<KhuVucDto> layDsKhuVucLamViec(long idDonVi);

	Page<KhuVucDto> layDsKhuVuc(long idDonVi, Pageable pageable);
	
	List<KhuVucDto> layDsKhuVuc(long idDonVi);
	
	KhuVucDto layKhuVuc(long id);
	
	KhuVucDto luuKhuVuc(KhuVucDto dto) throws Exception;
	
	void xoaKhuVuc(long id) throws Exception;

	boolean laKhuVucSuDungHDDT();

	boolean laKhuVucSuDungHDDT(Long idKhuVuc);
}
