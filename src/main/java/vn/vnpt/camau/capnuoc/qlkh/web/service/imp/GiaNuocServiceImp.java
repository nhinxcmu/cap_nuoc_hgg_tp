package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.ChiTietGiaNuocConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.DuongConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.GiaNuocConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ChiTietGiaNuocDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DuongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.GiaNuocDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ChiTietGiaNuoc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Duong;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.GiaNuoc;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ChiTietGiaNuocRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DuongRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.GiaNuocRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.GiaNuocService;

@Service
public class GiaNuocServiceImp implements GiaNuocService {

	@Autowired
	private GiaNuocConverter converter;
	@Autowired
	private GiaNuocRepository repository;
	@Autowired
	private DuongRepository duongRepository;
	@Autowired
	private DuongConverter duongConverter;
	@Autowired
	private ChiTietGiaNuocRepository chiTietGiaNuocRepository;
	@Autowired
	private ChiTietGiaNuocConverter chiTietGiaNuocConverter;

	@Override
	public Page<GiaNuocDto> layDsGiaNuoc(long idHuyen, String thongTinCanTim, Pageable pageable) {
		Page<GiaNuoc> dsGiaNuocCanLay;
		if (idHuyen == 0) {
			if (thongTinCanTim.equals("")) {
				dsGiaNuocCanLay = repository.findByOrderByNgayApDungDescIdGiaNuocDesc(pageable);
			} else {
				dsGiaNuocCanLay = repository.findByTenGiaNuocContainsOrderByNgayApDungDescIdGiaNuocDesc(thongTinCanTim,
						pageable);
			}
		} else {
			if (thongTinCanTim.equals("")) {
				dsGiaNuocCanLay = repository.findByHuyen_IdHuyenOrderByNgayApDungDescIdGiaNuocDesc(idHuyen, pageable);
			} else {
				dsGiaNuocCanLay = repository.findByHuyen_IdHuyenAndTenGiaNuocContainsOrderByNgayApDungDescIdGiaNuocDesc(
						idHuyen, thongTinCanTim, pageable);
			}
		}
		Page<GiaNuocDto> dsGiaNuocDaChuyenDoi = dsGiaNuocCanLay.map(converter::convertToDto);
		return dsGiaNuocDaChuyenDoi;
	}

	@Override
	public GiaNuocDto layGiaNuoc(long id) {
		GiaNuoc giaNuocCanLay = repository.findByIdGiaNuoc(id);
		GiaNuocDto giaNuocDaChuyenDoi = converter.convertToDto(giaNuocCanLay);
		List<Duong> dsDuongTheoGiaNuoc = duongRepository.findByGiaNuocs_IdGiaNuocOrderByTenDuong(id);
		List<DuongDto> dsDuongDaChuyenDoi = dsDuongTheoGiaNuoc.stream().map(duongConverter::convertToDto)
				.collect(Collectors.toList());
		List<ChiTietGiaNuoc> dsChiTietGiaNuocTheoGiaNuoc = chiTietGiaNuocRepository
				.findByGiaNuoc_IdGiaNuocOrderByThuTu(id);
		List<ChiTietGiaNuocDto> dsChiTietGiaNuocDaChuyenDoi = dsChiTietGiaNuocTheoGiaNuoc.stream()
				.map(chiTietGiaNuocConverter::convertToDto).collect(Collectors.toList());
		giaNuocDaChuyenDoi.setDuongDtos(dsDuongDaChuyenDoi);
		giaNuocDaChuyenDoi.setChiTietGiaNuocDtos(dsChiTietGiaNuocDaChuyenDoi);
		return giaNuocDaChuyenDoi;
	}

	@Override
	public GiaNuocDto luuGiaNuoc(GiaNuocDto dto) throws Exception {
		GiaNuoc giaNuocCanLuu;
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		dto.setIdNguoiCapNhat(idNguoiDungHienHanh);
		dto.setNgayGioCapNhat(new Date());
		if (dto.getIdGiaNuoc() == null) {
			giaNuocCanLuu = themGiaNuoc(dto);
		} else {
			giaNuocCanLuu = capNhatGiaNuoc(dto);
		}
		GiaNuocDto giaNuocDaChuyenDoi = converter.convertToDto(giaNuocCanLuu);
		return giaNuocDaChuyenDoi;
	}

	private GiaNuoc themGiaNuoc(GiaNuocDto dto) throws Exception {
		dto.getChiTietGiaNuocDtos().forEach(c -> c.setIdChiTietGiaNuoc(null));
		GiaNuoc giaNuocCanLuu = converter.convertToEntity(dto);
		giaNuocCanLuu.getChiTietGiaNuocs().forEach(c -> c.setDoiTuongBaoCao(null));
		try {
			repository.save(giaNuocCanLuu);
			return giaNuocCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu giá nước.");
		}
	}

	private GiaNuoc capNhatGiaNuoc(GiaNuocDto dto) throws Exception {
		GiaNuoc giaNuocCanKiemTra = kiemTraGiaNuoc(dto.getIdGiaNuoc());
		kiemTraChiTietGiaNuoc(dto.getChiTietGiaNuocDtos(), giaNuocCanKiemTra.getChiTietGiaNuocs());
		GiaNuoc giaNuocCanLuu = converter.convertToEntity(dto);
		giaNuocCanLuu.getChiTietGiaNuocs().forEach(c -> c.setDoiTuongBaoCao(null));
		try {
			repository.save(giaNuocCanLuu);
			return giaNuocCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu giá nước.");
		}
	}

	private GiaNuoc kiemTraGiaNuoc(Long idGiaNuoc) throws Exception {
		GiaNuoc giaNuocCanKiemTra = repository.findOne(idGiaNuoc);
		if (giaNuocCanKiemTra == null) {
			throw new Exception("Không tìm thấy giá nước.");
		}
		return giaNuocCanKiemTra;
	}

	private void kiemTraChiTietGiaNuoc(List<ChiTietGiaNuocDto> chiTietGiaNuocDtos, Set<ChiTietGiaNuoc> chiTietGiaNuocs)
			throws Exception {
		for (ChiTietGiaNuocDto chiTietGiaNuocDto : chiTietGiaNuocDtos) {
			if (chiTietGiaNuocDto.getIdChiTietGiaNuoc() > 0) {
				long soLuong = chiTietGiaNuocs.stream()
						.filter(c -> c.getIdChiTietGiaNuoc().equals(chiTietGiaNuocDto.getIdChiTietGiaNuoc())).count();
				if (soLuong == 0) {
					throw new Exception("Không tìm thấy chi tiết giá nước.");
				}
			} else {
				chiTietGiaNuocDto.setIdChiTietGiaNuoc(null);
			}
		}
	}

	@Override
	public void xoaGiaNuoc(long id) throws Exception {
		kiemTraGiaNuoc(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa giá nước.");
			}
			throw new Exception(e.getMessage());
		}
	}

}
