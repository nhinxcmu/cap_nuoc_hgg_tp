package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Data;

@Data
public class PhieuSuaSeriDto {

    public PhieuSuaSeriDto(Long idPhieuSuaSeri){
        this.idPhieuSuaSeri=idPhieuSuaSeri;
    }
    public PhieuSuaSeriDto(){
    }

    private Long idPhieuSuaSeri;    

	@Mapping("dongHo.idDongHo")
    private Long idDongHo;
	@Mapping("dongHo.soSeri")
    private String soSeri;

	@Mapping("nguoiDung.idNguoiDung")
    private Long idNguoiDung;
	@Mapping("nguoiDung.hoTen")
    private String hoTenNguoiLap;

	@Mapping("nguoiCapNhat.idNguoiDung")
    private Long idNguoiCapNhat;
	@Mapping("nguoiCapNhat.hoTen")
    private String hoTenNguoiCapNhat;

	@Mapping("thang.idThang")
    private Long idThang;
	@Mapping("thang.tenThang")
    private String tenThang;

    @Mapping("thongTinKhachHang.idThongTinKhachHang")
    private Long idThongTinKhachHang;
    @Mapping("thongTinKhachHang.khachHang.idKhachHang")
    private Long idKhachHang;
    @Mapping("thongTinKhachHang.maKhachHang")
    private String maKhachHang;
    @Mapping("thongTinKhachHang.tenKhachHang")
    private String tenKhachHang;

	private String soSeriMoi;
	private String lyDo;
	private Integer trangThai;
    private Date ngayLap;
    private Date ngayCapNhat;
    private String lyDoTuChoi;
    private String soSeriCu;
    
}
