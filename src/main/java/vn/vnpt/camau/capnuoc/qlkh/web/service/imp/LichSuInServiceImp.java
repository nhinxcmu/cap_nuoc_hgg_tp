package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.LichSuInConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.LichSuInDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuIn;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.LichSuInRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.LichSuInService;

@Service
public class LichSuInServiceImp implements LichSuInService {

	@Autowired
	private LichSuInConverter converter;
	@Autowired
	private LichSuInRepository repository;

	@Override
	public List<LichSuInDto> layDsLichSuIn(long idHoaDon) {
		List<LichSuIn> dsLichSuInCanLay = repository.findByHoaDon_IdHoaDonOrderByNgayGioInDesc(idHoaDon);
		List<LichSuInDto> dsLichSuInDaChuyenDoi = dsLichSuInCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsLichSuInDaChuyenDoi;
	}

}
