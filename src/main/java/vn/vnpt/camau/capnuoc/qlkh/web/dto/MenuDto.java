package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class MenuDto {

	@Getter
	@Setter
	private Long idMenu;
	@Getter
	@Setter
	private String url;
	@Getter
	@Setter
	private String title;
	@Getter
	@Setter
	private String icon;
	@Getter
	@Setter
	private int orderMenu;
	@Getter
	@Setter
	private List<MenuDto> menuDtos = new ArrayList<MenuDto>(0);
	
}
