package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHoKhachHangThang;

public interface DongHoKhachHangThangRepository extends CrudRepository<DongHoKhachHangThang, Long> {

	DongHoKhachHangThang findByDongHo_IdDongHoAndId_IdThang(Long idDongHo, Long idThang);
	
	void deleteByDongHo_IdDongHoAndId_IdKhachHang(Long idDongHo, Long idKhachHang);
	DongHoKhachHangThang findByKhachHang_IdKhachHangAndId_IdThang(Long idKhachHang, Long idThang);
}
