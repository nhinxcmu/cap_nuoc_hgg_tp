package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.PhieuSuaSeriDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.PhieuSuaSeri;

@Component
public class PhieuSuaSeriConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public PhieuSuaSeriDto convertToDto(PhieuSuaSeri entity) {
		if (entity == null) {
			return new PhieuSuaSeriDto();
		}
		PhieuSuaSeriDto dto = mapper.map(entity, PhieuSuaSeriDto.class);
		return dto;
	}

	public PhieuSuaSeri convertToEntity(PhieuSuaSeriDto dto) {
		if (dto == null) {
			return new PhieuSuaSeri();
		}
		PhieuSuaSeri entity = mapper.map(dto, PhieuSuaSeri.class);
		return entity;
	}
}