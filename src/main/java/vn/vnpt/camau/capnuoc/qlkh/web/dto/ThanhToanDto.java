package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class ThanhToanDto {

	@Getter
	@Setter
	private Long idThanhToan;
	@Getter
	@Setter
	@Mapping("hinhThucThanhToan.idHinhThucThanhToan")
	private Long idHinhThucThanhToan;
	@Getter
	@Setter
	@Mapping("hinhThucThanhToan.tenHinhThucThanhToan")
	private String tenHinhThucThanhToan;
	@Getter
	@Setter
	@Mapping("nguoiDung.idNguoiDung")
	private Long idNguoiCapNhat;
	@Getter
	@Setter
	@Mapping("nguoiDung.tenNguoiDung")
	private String tenNguoiCapNhat;
	@Getter
	@Setter
	private long tienThanhToan;
	@Getter
	@Setter
	private Date ngayGioCapNhat;
	@Getter
	@Setter
	private boolean huyThanhToan;
	@Getter
	@Setter
	@Mapping("hoaDon.thang.idThang")
	private long idThangHoaDon;
	@Getter
	@Setter
	@Mapping("hoaDon.thang.tenThang")
	private String tenThangHoaDon;
	
}
