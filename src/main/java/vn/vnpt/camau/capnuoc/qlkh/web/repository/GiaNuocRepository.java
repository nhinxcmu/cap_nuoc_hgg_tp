package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.GiaNuoc;

public interface GiaNuocRepository extends CrudRepository<GiaNuoc, Long> {

	@EntityGraph(attributePaths = { "doiTuong", "huyen", "nguoiDung" })
	Page<GiaNuoc> findByOrderByNgayApDungDescIdGiaNuocDesc(Pageable pageable);

	@EntityGraph(attributePaths = { "doiTuong", "huyen", "nguoiDung" })
	Page<GiaNuoc> findByTenGiaNuocContainsOrderByNgayApDungDescIdGiaNuocDesc(String tenGiaNuoc, Pageable pageable);
	
	@EntityGraph(attributePaths = { "doiTuong", "huyen", "nguoiDung" })
	Page<GiaNuoc> findByHuyen_IdHuyenOrderByNgayApDungDescIdGiaNuocDesc(Long idHuyen, Pageable pageable);
	
	@EntityGraph(attributePaths = { "doiTuong", "huyen", "nguoiDung" })
	Page<GiaNuoc> findByHuyen_IdHuyenAndTenGiaNuocContainsOrderByNgayApDungDescIdGiaNuocDesc(Long idHuyen, String tenGiaNuoc, Pageable pageable);
	
	@EntityGraph(attributePaths = { "doiTuong", "huyen", "nguoiDung" })
	GiaNuoc findByIdGiaNuoc(Long idGiaNuoc);
	
}
