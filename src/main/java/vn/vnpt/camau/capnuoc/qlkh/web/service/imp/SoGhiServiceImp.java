package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.SoGhiConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoGhiDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.SoGhi;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.SoGhiRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.SoGhiService;

@Service
public class SoGhiServiceImp implements SoGhiService {

	@Autowired
	private SoGhiConverter converter;
	@Autowired
	private SoGhiRepository repository;
	@Autowired
	private ThangRepository thangRepository;
	@Autowired
	private HttpSession httpSession;

	@Override
	public Page<SoGhiDto> layDsSoGhi(long idDuong, String thongTinCanTim, Pageable pageable) {
		Page<SoGhi> dsSoGhiCanLay;
		if (idDuong == 0) {
			if (thongTinCanTim.equals("")) {
				dsSoGhiCanLay = repository.findByOrderByIdSoGhiDesc(pageable);
			} else {
				dsSoGhiCanLay = repository.findByTenSoGhiContainsOrderByIdSoGhiDesc(thongTinCanTim, pageable);
			}
		} else {
			if (thongTinCanTim.equals("")) {
				dsSoGhiCanLay = repository.findByDuong_IdDuongOrderByIdSoGhiDesc(idDuong, pageable);
			} else {
				dsSoGhiCanLay = repository.findByDuong_IdDuongAndTenSoGhiContainsOrderByIdSoGhiDesc(idDuong,
						thongTinCanTim, pageable);
			}
		}
		Page<SoGhiDto> dsSoGhiDaChuyenDoi = dsSoGhiCanLay.map(converter::convertToDto);
		return dsSoGhiDaChuyenDoi;
	}

	@Override
	public List<SoGhiDto> layDsSoGhi(long idDuong) {
		String thongTinCanTim = "";
		Pageable pageable = null;
		List<SoGhiDto> dsSoGhiCanLay = layDsSoGhi(idDuong, thongTinCanTim, pageable).getContent().stream().sorted()
				.collect(Collectors.toList());
		return dsSoGhiCanLay;
	}

	@Override
	public SoGhiDto laySoGhi(long id) {
		SoGhi soGhiCanLay = repository.findByIdSoGhi(id);
		SoGhiDto soGhiDaChuyenDoi = converter.convertToDto(soGhiCanLay);
		return soGhiDaChuyenDoi;
	}

	@Override
	public SoGhiDto luuSoGhi(SoGhiDto dto) throws Exception {
		kiemTraSoGhi(dto.getIdSoGhi());
		SoGhi soGhiCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(soGhiCanLuu);
			SoGhiDto soGhiDaChuyenDoi = converter.convertToDto(soGhiCanLuu);
			return soGhiDaChuyenDoi;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu tổ.");
		}
	}

	private void kiemTraSoGhi(Long idSoGhi) throws Exception {
		if (idSoGhi != null) {
			SoGhi soGhiCanKiemTra = repository.findOne(idSoGhi);
			if (soGhiCanKiemTra == null) {
				throw new Exception("Không tìm thấy tổ.");
			}
		}
	}

	@Override
	public void xoaSoGhi(long id) throws Exception {
		kiemTraSoGhi(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa tổ.");
			}
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public List<SoGhiDto> layDsSoGhiTheoNguoiDung(long idDuong, long idNguoiDung) {
		Thang thangPhanCongGhiThuMoiNhat = thangRepository
				.findTopByLichGhiThuThangs_NguoiDuocPhanCong_IdNguoiDungOrderByIdThangDesc(idNguoiDung);
		if (thangPhanCongGhiThuMoiNhat == null) {
			return new ArrayList<SoGhiDto>();
		}
		long idThangPhanCongGhiThuMoiNhat = thangPhanCongGhiThuMoiNhat.getIdThang();
		List<SoGhi> dsSoGhiCanLay;
		if (idDuong == 0) {
			dsSoGhiCanLay = repository
					.findByLichGhiThus_Thangs_IdThangAndLichGhiThus_NguoiDuocPhanCong_IdNguoiDungOrderByTenSoGhi(
							idThangPhanCongGhiThuMoiNhat, idNguoiDung);
		} else {
			dsSoGhiCanLay = repository
					.findByLichGhiThus_Thangs_IdThangAndLichGhiThus_NguoiDuocPhanCong_IdNguoiDungAndDuong_IdDuongOrderByTenSoGhi(
							idThangPhanCongGhiThuMoiNhat, idNguoiDung, idDuong);
		}
		List<SoGhiDto> dsSoGhiDaChuyenDoi = dsSoGhiCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsSoGhiDaChuyenDoi;
	}

	@Override
	public List<SoGhiDto> layDsSoGhiTheoTrangThaiChiSo(List<Long> dsIdSoGhi, long idTrangThaiChiSo) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		List<SoGhi> dsSoGhiCanLay = repository
				.findDistinctByIdSoGhiInAndThongTinKhachHangs_KhachHang_HoaDons_TrangThaiChiSo_IdTrangThaiChiSoAndThongTinKhachHangs_Thangs_IdThangAndThongTinKhachHangs_KhachHang_HoaDons_Thang_IdThang(
						dsIdSoGhi, idTrangThaiChiSo, idThangLamViec, idThangLamViec);
		List<SoGhiDto> dsSoGhiDaChuyenDoi = dsSoGhiCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsSoGhiDaChuyenDoi;
	}

}
