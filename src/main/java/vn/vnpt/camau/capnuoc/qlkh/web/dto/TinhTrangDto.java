package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import lombok.Data;

@Data
public class TinhTrangDto {
    private Long idTinhTrang;
	private String tenTinhTrang;
}
