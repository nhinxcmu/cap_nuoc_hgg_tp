package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.ChiTietHoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ChiTietHoaDon;

@Component
public class ChiTietHoaDonConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ChiTietHoaDonDto convertToDto(ChiTietHoaDon entity) {
		if (entity == null) {
			return new ChiTietHoaDonDto();
		}
		ChiTietHoaDonDto dto = mapper.map(entity, ChiTietHoaDonDto.class);
		return dto;
	}


}
