package vn.vnpt.camau.capnuoc.qlkh.web.entity;

public class LayDSHoaDonRequestBody{
    private String maKhachHang;

    public String getMaKhachHang() {
        return maKhachHang;
    }

    public void setMaKhachHang(String maKhachHang) {
        this.maKhachHang = maKhachHang;
    }
}
