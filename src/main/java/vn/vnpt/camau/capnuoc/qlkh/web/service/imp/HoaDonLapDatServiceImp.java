package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import vn.vnpt.camau.capnuoc.qlkh.web.ReadNumber;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.HoaDonLapDatConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonLapDatDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDonDienTuLapDat;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDonLapDat;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KetQuaHddt;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KieuKetQuaHddt;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiHoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HoaDonDienTuLapDatRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HoaDonLapDatRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.KetQuaHddtRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HoaDonLapDatService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucService;
import vn.vnpt.camau.capnuoc.qlkh.web.webservice.BusinessServiceClient;
import vn.vnpt.camau.capnuoc.qlkh.web.webservice.PortalServiceClient;
import vn.vnpt.camau.capnuoc.qlkh.web.webservice.PublishServiceClient;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.CancelInvResponse;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.DownloadInvPDFFkeyNoPayResponse;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.DownloadInvPDFFkeyResponse;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.ImportAndPublishInvResponse;

@Service
public class HoaDonLapDatServiceImp implements HoaDonLapDatService {

    @Autowired
    private HoaDonLapDatRepository repo;

    @Autowired
    private HoaDonLapDatConverter converter;

    @Autowired
    private HttpSession httpSession;
    
	@Autowired
	private BusinessServiceClient businessServiceClient;
	@Autowired
	private PublishServiceClient publishServiceClient;
	@Autowired
    private PortalServiceClient portalServiceClient;
    
	@Autowired
	private KetQuaHddtRepository ketQuaHddtRepository;
	@Autowired
	private HoaDonDienTuLapDatRepository hoaDonDienTuLapDatRepository;

	@PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private KhuVucService khuVucService;
    
    @Override
    public Page<HoaDonLapDatDto> layDs(String searchString, Long idDuong, Long idSoGhi,Long idTrangThaiHoaDon, Pageable page) {
        return repo
                .layDs(searchString, Utilities.layThangLamViec(httpSession).getIdThang(),
                        Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc(), idDuong, idSoGhi,idTrangThaiHoaDon, page)
                .map(converter::convertToDto);
    }

    @Override
    public HoaDonLapDatDto layHoaDonLapDat(long idHoaDon) {
        return converter.convertToDto(repo.findOne(idHoaDon));
    }

    @Override
    public HoaDonLapDatDto luu(HoaDonLapDatDto hoaDon) throws Exception {
        HoaDonLapDat entityKiemTra = repo.findOne(hoaDon.getIdHoaDon());
        if (entityKiemTra.getTrangThaiHoaDon().getIdTrangThaiHoaDon() != 5l) {
            throw new Exception("Hóa đơn đã phát hành, không thể điều chỉnh giá dịch vụ");
        }
        HoaDonLapDat _hoaDon = converter.convertToEntity(hoaDon);
        _hoaDon.setNgayGioCapNhat(new Date());
        _hoaDon.setNguoiDung(Utilities.layNguoiDung());
        _hoaDon.setBangChu(ReadNumber.numberToString(BigDecimal.valueOf(_hoaDon.getTongTien())));
        _hoaDon = repo.save(_hoaDon);
        return converter.convertToDto(_hoaDon);
    }

    @Override
    public Integer xoa(HoaDonLapDatDto hoaDon) {
        try {
            repo.delete(converter.convertToEntity(hoaDon));
            return 1;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void phatHanhHoaDon(Long id) throws Exception {
        // kiểm tra hóa đơn
        kiemTraHoaDonCoThePhatHanh(id);
        List<Long> dsIdHoaDon = taoDsIdHoaDon(id);
        phatHanhHoaDonDienTu(dsIdHoaDon, Utilities.layNguoiDung());
    }

    @Override
    public void ngungPhatHanhHoaDon(Long id) throws Exception {
        // kiểm tra hóa đơn
        kiemTraHoaDonCoTheNgungPhatHanh(id);
        List<Long> dsIdHoaDon = taoDsIdHoaDon(id);
        ngungPhatHanhHoaDonDienTu(dsIdHoaDon, Utilities.layNguoiDung());
    }

    private List<Long> taoDsIdHoaDon(Long id) {
        List<Long> dsIdHoaDon = new ArrayList<Long>();
        dsIdHoaDon.add(id);
        return dsIdHoaDon;
    }

    private HoaDonLapDat kiemTraHoaDon(Long id) throws Exception {
        HoaDonLapDat hoaDonCanKiemTra = repo.findOne(id);
        if (hoaDonCanKiemTra == null) {
            throw new Exception("Không tìm thấy hóa đơn.");
        }
        if(hoaDonCanKiemTra.getTrangThaiHoaDon().getIdTrangThaiHoaDon().compareTo(Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON)==0){
            throw new Exception("Hóa đơn đã được phát hành, không thể phát hành hóa đơn");
        }
        return hoaDonCanKiemTra;
    }

    private void kiemTraHoaDonCoThePhatHanh(Long id) throws Exception {
        HoaDonLapDat hoaDonDaKiemTra = kiemTraHoaDon(id);
        if (hoaDonDaKiemTra.getTongTien() <= 0) {
            throw new Exception("Không thể phát hành hóa đơn vì không có tiền.");
        }
    }

    private void phatHanhHoaDonDienTu(List<Long> dsIdHoaDon, NguoiDung nguoiCapNhat) throws Exception {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		List<Long> dsIdHoaDonPhatHanh = new ArrayList<>();
		Iterator<Long> ds = dsIdHoaDon.iterator();
		while (ds.hasNext()) {
			Long idHoaDon = (Long) ds.next();
			dsIdHoaDonPhatHanh.add(idHoaDon);
			if (dsIdHoaDonPhatHanh.size() == 100) {
				phatHanhHoaDonDienTu(dsIdHoaDonPhatHanh, nguoiCapNhat, idThangLamViec);
				dsIdHoaDonPhatHanh = new ArrayList<>();
			}
		}
		if (!dsIdHoaDonPhatHanh.isEmpty()) {
			phatHanhHoaDonDienTu(dsIdHoaDonPhatHanh, nguoiCapNhat, idThangLamViec);
		}
	}

    private void phatHanhHoaDonDienTu(List<Long> dsIdHoaDon, NguoiDung nguoiCapNhat, long idThangLamViec)
			throws Exception {
		String[] dsKetQua = themHoaDonDienTu(dsIdHoaDon, nguoiCapNhat, idThangLamViec);
		String xmlInvData = dsKetQua[0];
		String dsIdHoaDonDienTu = dsKetQua[1];
		String ketQua = goiWebServicePhatHanhHDDT(xmlInvData, dsIdHoaDonDienTu);
		if (ketQua.substring(0, 2).equals("OK")) {
			capNhatKyHieuVaSoHoaDon(ketQua, nguoiCapNhat);
			phatHanhHoaDon(dsIdHoaDon, nguoiCapNhat);
		} else {
			throw new Exception("Phát hành hóa đơn điện tử không thành công.");
		}
	}

    private String goiWebServicePhatHanhHDDT(String xmlInvData, String dsIdHoaDonDienTu) throws Exception {
        try {
            ImportAndPublishInvResponse response = publishServiceClient.importAndPublishInvLapDat(xmlInvData, 0);
            String ketQua = response.getImportAndPublishInvResult();
            themKetQuaHDDT(dsIdHoaDonDienTu, ketQua, Utilities.ID_KIEU_PHAT_HANH_HOA_DON);
            return ketQua;
        } catch (Exception e) {
            String ketQua = "Không thể gọi web service phát hành hóa đơn điện tử.";
            themKetQuaHDDT(dsIdHoaDonDienTu, ketQua, Utilities.ID_KIEU_PHAT_HANH_HOA_DON);
            e.printStackTrace();
            throw new Exception("Không thể gọi web service phát hành hóa đơn điện tử.");
        }
    }

    private void themKetQuaHDDT(String dsIdHoaDonDienTu, String ketQua, long idKieuKetQuaHddt) throws Exception {
        try {
            KetQuaHddt ketQuaHddt = new KetQuaHddt();
            KieuKetQuaHddt kieuKetQuaHddt = new KieuKetQuaHddt();
            kieuKetQuaHddt.setIdKieuKetQuaHddt(idKieuKetQuaHddt);
            ketQuaHddt.setKieuKetQuaHddt(kieuKetQuaHddt);
            ketQuaHddt.setTrangThai(ketQua);
            ketQuaHddt.setChuoiKetQua(dsIdHoaDonDienTu);
            ketQuaHddt.setNgayGioCapNhat(new Date());
            ketQuaHddtRepository.save(ketQuaHddt);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Không thể thêm kết quả hóa đơn điện tử.");
        }
    }

    private String[] themHoaDonDienTu(List<Long> dsIdHoaDon, NguoiDung nguoiCapNhat, long idThangLamViec)throws Exception {
        try {
            String stringDsIdHoaDon = dsIdHoaDon.stream().map(Object::toString).collect(Collectors.joining(","));
            Long idNguoiCapNhat = nguoiCapNhat.getIdNguoiDung();
            StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("taoXMLHDDTLD")
                    .registerStoredProcedureParameter("p_list_id_hd", String.class, ParameterMode.IN)
                    .registerStoredProcedureParameter("idNguoiDung", Long.class, ParameterMode.IN)
                    .registerStoredProcedureParameter("idThang", Long.class, ParameterMode.IN)
                    .setParameter("p_list_id_hd", stringDsIdHoaDon)
                    .setParameter("idNguoiDung", idNguoiCapNhat)
					.setParameter("idThang", idThangLamViec);
            Object[] rs = (Object[]) storedProcedure.getSingleResult();
            String[] dsKetQua = new String[rs.length];
            for (int i = 0; i < rs.length; i++)
                dsKetQua[i] = String.valueOf(rs[i]);
            return dsKetQua;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Không thể thêm hóa đơn điện tử.");
        }
    }

    private void capNhatKyHieuVaSoHoaDon(String ketQua, NguoiDung nguoiCapNhat) throws Exception {
        List<HoaDonDienTuLapDat> dsHoaDonDienTu = taoDsHoaDonDienTuTuKetQua(ketQua, nguoiCapNhat);
        try {
            hoaDonDienTuLapDatRepository.save(dsHoaDonDienTu);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Không thể cập nhật ký hiệu và số hóa đơn điện tử.");
        }
    }

    private List<HoaDonDienTuLapDat> taoDsHoaDonDienTuTuKetQua(String ketQua, NguoiDung nguoiCapNhat) throws Exception {
        try {
            List<HoaDonDienTuLapDat> dsHoaDonDienTu = new ArrayList<HoaDonDienTuLapDat>();

            String[] lan1 = ketQua.split(":");
            String[] lan2 = lan1[1].split("-");
            String paSe[] = lan2[0].split(";");
            String strKey = lan2[1];
            String pattern = paSe[0];
            // String serial = paSe[1];

            for (String s : strKey.split(",")) {
                String[] strS = s.split("_");
                Long idHoaDonDienTu = Long.parseLong(strS[0].replace("LD", ""));
                String soHoaDon = strS[1];
                String kyHieu = pattern;

                HoaDonDienTuLapDat hoaDonDienTuLapDat = hoaDonDienTuLapDatRepository.findOne(idHoaDonDienTu);
                hoaDonDienTuLapDat.setNguoiDung(nguoiCapNhat);
                hoaDonDienTuLapDat.setKyHieu(kyHieu);
                hoaDonDienTuLapDat.setSoHoaDon(soHoaDon);
                hoaDonDienTuLapDat.setNgayGioCapNhat(new Date());
                hoaDonDienTuLapDat.setHuy(false);
                hoaDonDienTuLapDat.setThanhToan(false);
                hoaDonDienTuLapDat.setPhatHanhThanhCong(true);
                dsHoaDonDienTu.add(hoaDonDienTuLapDat);
            }

            return dsHoaDonDienTu;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Không thể phân tích kết quả gọi web service phát hành hóa đơn điện tử.");
        }
    }

    private void phatHanhHoaDon(List<Long> dsIdHoaDon, NguoiDung nguoiCapNhat) throws Exception {
        TrangThaiHoaDon trangThaiHoaDonDaPhatHanh = taoTrangThaiHoaDon(Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON);
        List<HoaDonLapDat> dsHoaDonCanPhatHanh = repo.findByIdHoaDonIn(dsIdHoaDon);
        for (HoaDonLapDat hoaDonCanPhatHanh : dsHoaDonCanPhatHanh) {
            hoaDonCanPhatHanh.setTrangThaiHoaDon(trangThaiHoaDonDaPhatHanh);
        }
        try {
            repo.save(dsHoaDonCanPhatHanh);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Không thể phát hành hóa đơn.");
        }
    }

    private TrangThaiHoaDon taoTrangThaiHoaDon(Long idTrangThaiHoaDon) {
        TrangThaiHoaDon trangThaiHoaDonDaPhatHanh = new TrangThaiHoaDon();
        trangThaiHoaDonDaPhatHanh.setIdTrangThaiHoaDon(idTrangThaiHoaDon);
        return trangThaiHoaDonDaPhatHanh;
    }

    private void ngungPhatHanhHoaDonDienTu(List<Long> dsIdHoaDon, NguoiDung nguoiCapNhat) throws Exception {
        List<HoaDonDienTuLapDat> dsHoaDonDienTu = layDsHoaDonDienTuChuaThanhToan(dsIdHoaDon);
        for (HoaDonDienTuLapDat HoaDonDienTuLapDat : dsHoaDonDienTu) {
            String ketQua = goiWebServiceHuyPhatHanhHoaDon(Long.toString(HoaDonDienTuLapDat.getIdHoaDonDienTu()));
            if (ketQua.substring(0, 2).equals("OK")) {
                capNhatNgungPhatHanhHoaDonDienTu(HoaDonDienTuLapDat, nguoiCapNhat);
                ngungPhatHanhHoaDon(HoaDonDienTuLapDat.getHoaDonLapDat().getIdHoaDon(), nguoiCapNhat);
            } else {
                throw new Exception("Ngưng phát hành hóa đơn điện tử không thành công.");
            }
        }
    }

    private void ngungPhatHanhHoaDonDienTu(Long idHoaDonDienTu, NguoiDung nguoiCapNhat) throws Exception {
        HoaDonDienTuLapDat HoaDonDienTuLapDat = hoaDonDienTuLapDatRepository.findByIdHoaDonDienTu(idHoaDonDienTu);
        String ketQua = goiWebServiceHuyPhatHanhHoaDon("LD"+Long.toString(HoaDonDienTuLapDat.getIdHoaDonDienTu()));
        if (ketQua.substring(0, 2).equals("OK")) {
            capNhatNgungPhatHanhHoaDonDienTu(HoaDonDienTuLapDat, nguoiCapNhat);
            ngungPhatHanhHoaDon(HoaDonDienTuLapDat.getHoaDonLapDat().getIdHoaDon(), nguoiCapNhat);
        } else {
            throw new Exception("Ngưng phát hành hóa đơn điện tử không thành công.");
        }
    }

    private List<HoaDonDienTuLapDat> layDsHoaDonDienTuChuaThanhToan(List<Long> dsIdHoaDon) throws Exception {
        List<HoaDonDienTuLapDat> dsHoaDonDienTu = hoaDonDienTuLapDatRepository
                .findByHoaDonLapDat_TrangThaiHoaDon_IdTrangThaiHoaDonAndHoaDonLapDat_IdHoaDonIn(6L, dsIdHoaDon);
        if (dsHoaDonDienTu.size() != dsIdHoaDon.size()) {
            throw new Exception("Số lượng hóa đơn điện tử không bằng số lượng hóa đơn.");
        }
        return dsHoaDonDienTu;
    }

    private void capNhatNgungPhatHanhHoaDonDienTu(HoaDonDienTuLapDat HoaDonDienTuLapDat, NguoiDung nguoiCapNhat) throws Exception {
        HoaDonDienTuLapDat.setNguoiDung(nguoiCapNhat);
        HoaDonDienTuLapDat.setNgayGioCapNhat(new Date());
        HoaDonDienTuLapDat.setHuy(true);

        try {
            hoaDonDienTuLapDatRepository.save(HoaDonDienTuLapDat);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Không thể cập nhật ngưng phát hành hóa đơn điện tử.");
        }
    }

    private String goiWebServiceHuyPhatHanhHoaDon(String idHoaDonDienTu) throws Exception {
        try {
            CancelInvResponse response = businessServiceClient.cancelInv(idHoaDonDienTu);
            String ketQua = response.getCancelInvResult();
            themKetQuaHDDT(idHoaDonDienTu, ketQua, Utilities.ID_KIEU_HUY_PHAT_HANH_HOA_DON);
            return ketQua;
        } catch (Exception e) {
            String ketQua = "Không thể gọi web service hủy phát hành hóa đơn điện tử.";
            themKetQuaHDDT(idHoaDonDienTu, ketQua, Utilities.ID_KIEU_HUY_PHAT_HANH_HOA_DON);
            e.printStackTrace();
            throw new Exception("Không thể gọi web service hủy phát hành hóa đơn điện tử.");
        }
    }

    private void ngungPhatHanhHoaDon(Long idHoaDon, NguoiDung nguoiCapNhat) throws Exception {
        TrangThaiHoaDon trangThaiHoaDonChuaPhatHanh = taoTrangThaiHoaDon(
                Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON);
        HoaDonLapDat hoaDonCanNgungPhatHanh = repo.findOne(idHoaDon);

        hoaDonCanNgungPhatHanh.setTrangThaiHoaDon(trangThaiHoaDonChuaPhatHanh);

        try {
            repo.save(hoaDonCanNgungPhatHanh);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Không thể ngưng phát hành hóa đơn.");
        }
    }

    private void kiemTraHoaDonCoTheNgungPhatHanh(Long id) throws Exception {
        kiemTraHoaDon(id);
    }

   

    @Override
    public void huyHoaDon(Long idHoaDonDienTu, NguoiDung nguoiCapNhat) throws Exception {
        ngungPhatHanhHoaDonDienTu(idHoaDonDienTu, nguoiCapNhat);

    }

    @Override
    public void taiHoaDon(Long idHoaDonDienTu, HttpServletResponse response) throws FileNotFoundException, IOException {
        HoaDonDienTuLapDat hoaDon = hoaDonDienTuLapDatRepository.findByIdHoaDonDienTu(idHoaDonDienTu);
		String result = "";
		if (hoaDon.isThanhToan()) {
			DownloadInvPDFFkeyResponse downloadInvPDFFkeyResponse = portalServiceClient
					.downloadInvPDFFkey("LD"+idHoaDonDienTu.toString());
			result = downloadInvPDFFkeyResponse.getDownloadInvPDFFkeyResult();
		} else {
			DownloadInvPDFFkeyNoPayResponse downloadInvPDFFkeyNoPayResponse = portalServiceClient
					.downloadInvPDFNoPayFkey("LD"+idHoaDonDienTu.toString());
			result = downloadInvPDFFkeyNoPayResponse.getDownloadInvPDFFkeyNoPayResult();
		}
		if (!result.contains("ERR:1")) {
			byte[] byteArray = Base64.getDecoder().decode(result);
			response.setContentType("application/pdf");
			// response.setContentLength(new Long(byteArray.).intValue());
			response.setHeader("Content-Disposition", "attachment; filename=" + idHoaDonDienTu.toString()+".pdf");
			FileCopyUtils.copy(new ByteArrayInputStream(byteArray), response.getOutputStream());

		}

    }

    @Override
    public void phatHanhHoaDonHangLoat() throws Exception {
        List<HoaDonLapDat> dsHoaDonCoThePhatHanh = layDsHoaDonCoThePhatHanh();
		List<Long> dsIdHoaDonCoThePhatHanh = dsHoaDonCoThePhatHanh.stream().map(converter::convertToIdHoaDon)
				.collect(Collectors.toList());
		NguoiDung nguoiCapNhat = taoNguoiCapNhatHienHanh();
		// kiểm tra công ty có sử dụng hóa đơn điện tử
		if (khuVucService.laKhuVucSuDungHDDT()) {
			phatHanhHoaDonDienTu(dsIdHoaDonCoThePhatHanh, nguoiCapNhat);
		} else {
			phatHanhHoaDon(dsIdHoaDonCoThePhatHanh, nguoiCapNhat);
		}
    }
    private List<HoaDonLapDat> layDsHoaDonCoThePhatHanh() {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		long tienDichVu = 0;
		List<HoaDonLapDat> dsHoaDonCoThePhatHanh = repo
				.findByThang_IdThangAndThongTinKhachHang_SoGhi_Duong_KhuVuc_IdKhuVucAndHuyIsFalseAndTrangThaiHoaDon_IdTrangThaiHoaDonAndTienDichVuGreaterThan(
						idThangLamViec, idKhuVucLamViec, Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON,
						tienDichVu);
		return dsHoaDonCoThePhatHanh;
    }
    private NguoiDung taoNguoiCapNhatHienHanh() {
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		NguoiDung nguoiCapNhat = new NguoiDung();
		nguoiCapNhat.setIdNguoiDung(idNguoiDungHienHanh);
		return nguoiCapNhat;
    }
    @Override
	@Transactional
	public void ngungPhatHanhHoaDonHangLoat() throws Exception {
		List<HoaDonLapDat> dsHoaDonCoTheNgungPhatHanh = layDsHoaDonCoTheNgungPhatHanh();
		List<Long> dsIdHoaDonCoTheNgungPhatHanh = dsHoaDonCoTheNgungPhatHanh.stream().map(converter::convertToIdHoaDon)
				.collect(Collectors.toList());
		NguoiDung nguoiCapNhat = taoNguoiCapNhatHienHanh();
		// kiểm tra công ty có sử dụng hóa đơn điện tử
		if (khuVucService.laKhuVucSuDungHDDT()) {
			ngungPhatHanhHoaDonDienTu(dsIdHoaDonCoTheNgungPhatHanh, nguoiCapNhat);
		} else {
			for (long idHoaDon : dsIdHoaDonCoTheNgungPhatHanh) {
				ngungPhatHanhHoaDon(idHoaDon, nguoiCapNhat);
			}
		}
    }
    private List<HoaDonLapDat> layDsHoaDonCoTheNgungPhatHanh() {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		List<HoaDonLapDat> dsHoaDonDaPhatHanh = repo
				.findByThang_IdThangAndThongTinKhachHang_SoGhi_Duong_KhuVuc_IdKhuVucAndHuyIsFalseAndTrangThaiHoaDon_IdTrangThaiHoaDon(
						idThangLamViec, idKhuVucLamViec, Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON);
		return dsHoaDonDaPhatHanh;
	}


}