package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.NganHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NganHang;

@Component
public class NganHangConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public NganHangDto convertToDto(NganHang entity) {
		if (entity == null) {
			return new NganHangDto();
		}
		NganHangDto dto = mapper.map(entity, NganHangDto.class);
		return dto;
	}

	public NganHang convertToEntity(NganHangDto dto) {
		if (dto == null) {
			return new NganHang();
		}
		NganHang entity = mapper.map(dto, NganHang.class);
		return entity;
	}

}
