package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.PhieuSuaSeriDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.FileBean;

public interface PhieuSuaSeriService {

    Page<PhieuSuaSeriDto> layDs(Long idNguoiDung, int trangThai, Date tuNgay, Date denNgay, Pageable pageable);

    List<PhieuSuaSeriDto> layDs(Long idNguoiDung, int trangThai, Date tuNgay, Date denNgay);

    PhieuSuaSeriDto luu(PhieuSuaSeriDto phieuSuaSeri) throws Exception;

    PhieuSuaSeriDto luu(String maKhachHang, String soSeriMoi, String lyDoSua) throws Exception;

    PhieuSuaSeriDto luu(Long idPhieuSuaSeri, String soSeriMoi, String lyDo) throws Exception;

    PhieuSuaSeriDto guiDuyet(PhieuSuaSeriDto phieuSuaSeri) throws Exception;

    PhieuSuaSeriDto tuChoiDuyet(PhieuSuaSeriDto phieuSuaSeri) throws Exception;

    PhieuSuaSeriDto duyet(PhieuSuaSeriDto phieuSuaSeri) throws Exception;

    int xoa(PhieuSuaSeriDto phieuSuaSeri) throws Exception;

    String importChiSo(FileBean fileBean);

    String nhacViecNhanVien();

    String nhacViecPhongKeHoach();

    PhieuSuaSeriDto layPhieuSuaSeri(Long idPhieuSuaSeri);
}