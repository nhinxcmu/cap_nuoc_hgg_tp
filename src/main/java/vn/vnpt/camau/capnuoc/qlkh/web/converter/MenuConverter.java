package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.MenuDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Menu;

@Component
public class MenuConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public MenuDto convertToDto(Menu entity) {
		if (entity == null) {
			return new MenuDto();
		}
		MenuDto dto = mapper.map(entity, MenuDto.class);
		Set<Menu> dsMenuCon = entity.getMenus();
		if (dsMenuCon.size() > 0) {
			List<MenuDto> dsMenuConDaChuyenDoi = entity.getMenus().stream().map(this::convertToMenuConDto).collect(Collectors.toList());
			dto.setMenuDtos(dsMenuConDaChuyenDoi);
		}
		return dto;
	}

	private MenuDto convertToMenuConDto(Menu entity) {
		if (entity == null) {
			return new MenuDto();
		}
		MenuDto dto = mapper.map(entity, MenuDto.class);
		return dto;
	}
	
	public Menu convertToEntity(MenuDto dto) {
		if (dto == null) {
			return new Menu();
		}
		Menu entity = mapper.map(dto, Menu.class);
		return entity;
	}

}
