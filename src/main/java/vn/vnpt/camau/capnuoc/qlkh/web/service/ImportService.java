package vn.vnpt.camau.capnuoc.qlkh.web.service;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.FileBean;

public interface ImportService {

	String importChiSo(FileBean fileBean);

}
