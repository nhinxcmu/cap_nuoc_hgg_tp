package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.DongHoConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.KhachHangConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.PhieuBaoHongConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.PhieuBaoHongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.FileBean;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.PhieuBaoHong;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThongTinKhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TinhTrang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DongHoRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.PhieuBaoHongRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThongTinKhachHangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DongHoService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.PhieuBaoHongService;

@Service
public class PhieuBaoHongServiceImp implements PhieuBaoHongService {
    @Value("${upload.path}")
    private String uploadPath;
    @Autowired
    private PhieuBaoHongConverter converter;
    @Autowired
    private PhieuBaoHongRepository repo;
    @Autowired
    private HttpSession httpSession;
    @Autowired
    private ThongTinKhachHangRepository ttkhRepo;
    @Autowired
    private KhachHangConverter khConverter;
    @Autowired
    private DongHoRepository dongHoRepo;
    @Autowired
    private DongHoService dongHoService;
    @Autowired
    private DongHoConverter dhConverter;

    @Override
    public Page<PhieuBaoHongDto> layDs(Long idNguoiDung, int trangThai, Date tuNgay, Date denNgay, Pageable pageable) {
        // TODO Auto-generated method stub
        return repo.layDs(idNguoiDung, trangThai, tuNgay, denNgay, pageable).map(converter::convertToDto);
    }

    @Override
    public List<PhieuBaoHongDto> layDs(Long idNguoiDung, int trangThai, Date tuNgay, Date denNgay) {
        if (idNguoiDung == 1) {
            if (trangThai != 1) {
                idNguoiDung = -1l;
            }
        }
        return repo.layDs(idNguoiDung, trangThai, tuNgay, denNgay).stream().map(converter::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public PhieuBaoHongDto luu(PhieuBaoHongDto phieuBaoHong) throws Exception {
        PhieuBaoHong phieuMoi = converter.convertToEntity(phieuBaoHong);
        ThangDto thang = Utilities.layThangLamViec(httpSession);
        phieuMoi.setThang(new Thang(thang.getIdThang(), thang.getTenThang()));
        phieuMoi.setNguoiDung(Utilities.layNguoiDung());
        phieuMoi.setTrangThai(1);
        phieuMoi.setNgayLap(new Date());
        if (phieuBaoHong.getBase64Image() != null)
            phieuMoi.setHinhAnh(luuAnh(phieuBaoHong.getBase64Image()));
        phieuMoi = repo.save(phieuMoi);
        return converter.convertToDto(phieuMoi);
    }

    @Override
    public PhieuBaoHongDto guiDuyet(PhieuBaoHongDto phieuBaoHong) throws Exception {
        PhieuBaoHong phieuMoi = repo.findOne(phieuBaoHong.getIdPhieuBaoHong());
        phieuMoi.setNguoiDung(Utilities.layNguoiDung());
        phieuMoi.setTrangThai(2);
        phieuMoi = repo.save(phieuMoi);
        return converter.convertToDto(phieuMoi);
    }

    @Override
    public PhieuBaoHongDto duyet(PhieuBaoHongDto phieuBaoHong) throws Exception {
        PhieuBaoHong phieuMoi = repo.findOne(phieuBaoHong.getIdPhieuBaoHong());
        phieuMoi.setNguoiDung(Utilities.layNguoiDung());
        phieuMoi.setTrangThai(3);
        phieuMoi = repo.save(phieuMoi);
        return converter.convertToDto(phieuMoi);
    }

    @Override
    public int xoa(PhieuBaoHongDto phieuBaoHong) throws Exception {
        PhieuBaoHong phieuMoi = repo.findOne(phieuBaoHong.getIdPhieuBaoHong());
        repo.delete(phieuMoi);
        return 0;
    }

    @Override
    public String importChiSo(FileBean fileBean) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String nhacViecPhongKeHoach() {
        int soPhieuChoDuyet = repo.demPhieuTheoTrangThai(-1l, 2);
        return "{\"choDuyet\":" + soPhieuChoDuyet + "}";
    }

    @Override
    public PhieuBaoHongDto layPhieuBaoHong(Long idPhieuBaoHong) {
        return converter.convertToDto(repo.findOne(idPhieuBaoHong));
    }

    String luuAnh(String base64) throws IOException {
        byte[] decodedImg = Base64.getDecoder().decode(base64.replace("\n", "").replace("\r", ""));
        String fileName = new Date().getTime() + ".jpg";
        Path destinationFile = Paths.get(uploadPath, fileName);
        Files.write(destinationFile, decodedImg);
        return fileName;
    }

    @Override
    public PhieuBaoHongDto luu(String maKhachHang, Long idTinhTrang, String moTa, String hinhAnh) throws Exception {
        ThongTinKhachHang ttkh = ttkhRepo.findByMaKhachHangAndThangs_IdThang(maKhachHang,
                Utilities.layThangLamViec(httpSession).getIdThang());
        if (ttkh == null) {
            throw new Exception("Không tìm thấy thông tin khách hàng");
        }

        PhieuBaoHong phieuBaoHong = new PhieuBaoHong();
        phieuBaoHong.setMoTa(moTa);
        phieuBaoHong.setNgayLap(new Date());
        phieuBaoHong.setNguoiDung(Utilities.layNguoiDung());
        Thang thang = new Thang();
        thang.setIdThang(Utilities.layThangLamViec(httpSession).getIdThang());
        phieuBaoHong.setThang(thang);
        phieuBaoHong.setThongTinKhachHang(ttkh);
        phieuBaoHong.setTrangThai(1);
        TinhTrang tinhTrang = new TinhTrang();
        tinhTrang.setIdTinhTrang(idTinhTrang);
        phieuBaoHong.setTinhTrang(tinhTrang);
        if (hinhAnh != null)
            phieuBaoHong.setHinhAnh(luuAnh(hinhAnh));
        return converter.convertToDto(repo.save(phieuBaoHong));
    }

    @Override
    public PhieuBaoHongDto luu(Long idPhieuBaoHong, Long idTinhTrang, String moTa, String hinhAnh) throws Exception {
        PhieuBaoHong phieu = repo.findOne(idPhieuBaoHong);
        phieu.setMoTa(moTa);
        TinhTrang tinhTrang = new TinhTrang();
        tinhTrang.setIdTinhTrang(idTinhTrang);
        phieu.setTinhTrang(tinhTrang);
        if (hinhAnh != null)
            phieu.setHinhAnh(luuAnh(hinhAnh));
        return converter.convertToDto(repo.save(phieu));
    }

}
