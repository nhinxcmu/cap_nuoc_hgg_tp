package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.TinNhanDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TinNhan;

@Component
public class TinNhanConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public TinNhanDto convertToDto(TinNhan entity) {
		if (entity == null) {
			return new TinNhanDto();
		}
		TinNhanDto dto = mapper.map(entity, TinNhanDto.class);
		return dto;
	}

}
