package vn.vnpt.camau.capnuoc.qlkh.web.entity;
// Generated Jun 30, 2021 3:59:06 PM by Hibernate Tools 5.2.12.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * PhieuBaoHong generated by hbm2java
 */
@Entity
@Table(name = "PHIEU_BAO_HONG")
public class PhieuBaoHong implements java.io.Serializable {

	private Long idPhieuBaoHong;
	private NguoiDung nguoiDung;
	private Thang thang;
	private ThongTinKhachHang thongTinKhachHang;
	private TinhTrang tinhTrang;
	private String moTa;
	private String hinhAnh;
	private Integer trangThai;
	private Date ngayLap;
	private String ghiChu;

	public PhieuBaoHong() {
	}

	public PhieuBaoHong(NguoiDung nguoiDung, Thang thang, ThongTinKhachHang thongTinKhachHang, TinhTrang tinhTrang,
			String moTa, String hinhAnh, Integer trangThai, Date ngayLap) {
		this.nguoiDung = nguoiDung;
		this.thang = thang;
		this.thongTinKhachHang = thongTinKhachHang;
		this.tinhTrang = tinhTrang;
		this.moTa = moTa;
		this.hinhAnh = hinhAnh;
		this.trangThai = trangThai;
		this.ngayLap = ngayLap;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID_PHIEU_BAO_HONG", unique = true, nullable = false)
	public Long getIdPhieuBaoHong() {
		return this.idPhieuBaoHong;
	}

	public void setIdPhieuBaoHong(Long idPhieuBaoHong) {
		this.idPhieuBaoHong = idPhieuBaoHong;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_NGUOI_DUNG")
	public NguoiDung getNguoiDung() {
		return this.nguoiDung;
	}

	public void setNguoiDung(NguoiDung nguoiDung) {
		this.nguoiDung = nguoiDung;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_THANG")
	public Thang getThang() {
		return this.thang;
	}

	public void setThang(Thang thang) {
		this.thang = thang;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_THONG_TIN_KHACH_HANG")
	public ThongTinKhachHang getThongTinKhachHang() {
		return this.thongTinKhachHang;
	}

	public void setThongTinKhachHang(ThongTinKhachHang thongTinKhachHang) {
		this.thongTinKhachHang = thongTinKhachHang;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TINH_TRANG")
	public TinhTrang getTinhTrang() {
		return this.tinhTrang;
	}

	public void setTinhTrang(TinhTrang tinhTrang) {
		this.tinhTrang = tinhTrang;
	}

	@Column(name = "MO_TA", length = 4000)
	public String getMoTa() {
		return this.moTa;
	}

	public void setMoTa(String moTa) {
		this.moTa = moTa;
	}

	@Column(name = "HINH_ANH", length = 4000)
	public String getHinhAnh() {
		return this.hinhAnh;
	}

	public void setHinhAnh(String hinhAnh) {
		this.hinhAnh = hinhAnh;
	}

	@Column(name = "TRANG_THAI")
	public Integer getTrangThai() {
		return this.trangThai;
	}

	public void setTrangThai(Integer trangThai) {
		this.trangThai = trangThai;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "NGAY_LAP", length = 19)
	public Date getNgayLap() {
		return this.ngayLap;
	}

	public void setNgayLap(Date ngayTao) {
		this.ngayLap = ngayTao;
	}
	
	@Column(name = "GHI_CHU", length = 4000)
	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}
}
