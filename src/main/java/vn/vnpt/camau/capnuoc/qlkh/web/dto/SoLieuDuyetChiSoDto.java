package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import lombok.Getter;
import lombok.Setter;

public class SoLieuDuyetChiSoDto {

	@Getter
	@Setter
	private int idNhanVienGhiThu;
	@Getter
	@Setter
	private String tenNhanVienGhiThu;
	@Getter
	@Setter
	private int idSoGhi;
	@Getter
	@Setter
	private String tenSoGhi;
	@Getter
	@Setter
	private int tongKhachHang;
	@Getter
	@Setter
	private int tongCoTheDuyet;
	@Getter
	@Setter
	private int tongDaDuyet;
	@Getter
	@Setter
	private int tongChuaGhi;
	@Getter
	@Setter
	private int tongBinhThuong;
	@Getter
	@Setter
	private int tongKhongXai;
	@Getter
	@Setter
	private int tongKhoanTieuThu;
	@Getter
	@Setter
	private int tongCupNuoc;
	@Getter
	@Setter
	private int idDuong;
	@Getter
	@Setter
	private String tenDuong;

}
