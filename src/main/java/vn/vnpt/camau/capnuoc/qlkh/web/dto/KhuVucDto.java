package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class KhuVucDto implements Comparable<KhuVucDto> {

	@Getter
	@Setter
	@NotNull
	private Long idKhuVuc;
	@Getter
	@Setter
	@Mapping("donVi.idDonVi")
	private Long idDonVi;
	@Getter
	@Setter
	@Mapping("donVi.tenDonVi")
	private String tenDonVi;
	@Getter
	@Setter
	@NotNull
	private String tenKhuVuc;
	@Getter
	@Setter
	@NotNull
	private String ngayBatDau;
	@Getter
	@Setter
	@NotNull
	private String ngayKetThuc;
	@Getter
	@Setter
	private String ngayIn;
	@Getter
	@Setter
	private boolean suDungHddt;
	@Getter
	@Setter
	private String soDienThoai;
	@Getter
	@Setter
	private Date hanThanhToan;
	@Getter
	@Setter
	private String tenThuNgan;
	
	@Override
	public int compareTo(KhuVucDto object) {
		return this.tenKhuVuc.compareTo(object.getTenKhuVuc());
	}
}
