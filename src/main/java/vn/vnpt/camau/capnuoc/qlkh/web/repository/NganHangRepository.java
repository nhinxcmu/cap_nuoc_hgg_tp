package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.NganHang;

public interface NganHangRepository extends CrudRepository<NganHang, Long> {

	Page<NganHang> findByOrderByIdNganHangDesc(Pageable pageable);

	NganHang findByTenNganHang(String tenNganHang);
}
