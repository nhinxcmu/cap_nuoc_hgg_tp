package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import lombok.Getter;
import lombok.Setter;

public class SoLieuPhatHanhHoaDonDto {

	@Getter
	@Setter
	private int tongKhachHang;
	@Getter
	@Setter
	private int tongDaThanhToan;
	@Getter
	@Setter
	private int tongDaPhatHanh;
	@Getter
	@Setter
	private int tongCoThePhatHanh;
	@Getter
	@Setter
	private int tongKhongPhatHanh;
	@Getter
	@Setter
	private int tongChuaPhatHanh;

}
