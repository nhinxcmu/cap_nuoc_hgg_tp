package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.NguoiDungDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DonViService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NguoiDungService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.VaiTroService;


@Controller
@RequestMapping("/nguoi-dung")
public class NguoiDungController {
	private ModelAttr modelAttr = new ModelAttr("Danh mục người dùng", "nguoidung", new String[]{"jdgrid/jdgrid-v3.js","jdpage/jdpage.js","bower_components/select2/dist/js/select2.min.js","js/nguoidung.js"},
			new String[]{"bower_components/select2/dist/css/select2.min.css","jdgrid/jdgrid.css"});
	@Autowired
	private DonViService donViServ;
	@Autowired
	private VaiTroService vaiTroServ;
	@Autowired
	private NguoiDungService nguoiDungServ;
	
	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
	    return "layout";
	}
	
	@GetMapping("/init-form")
	public @ResponseBody String initForm(){
		ObjectMapper mapper =new ObjectMapper();
		try {
			return "{\"nhoms\":"+mapper.writeValueAsString(vaiTroServ.layDsVaiTro())+",\"donVis\":"+mapper.writeValueAsString(donViServ.layDsDonVi())+"}";
		} catch (JsonProcessingException e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@GetMapping("/lay-ds")
	public @ResponseBody Page<NguoiDungDto> layDanhSach(long idDonVi, String thongTinCanTim,Pageable page){
		return nguoiDungServ.layDsNguoiDung(idDonVi, thongTinCanTim, page);
	}
	
	@GetMapping("/lay-ct")
	public @ResponseBody NguoiDungDto layChiTiet(long id){
		return nguoiDungServ.layNguoiDung(id);
	}
	
	@PostMapping("/luu")
	public @ResponseBody String luu(NguoiDungDto dto){
		try {
			nguoiDungServ.luuNguoiDung(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/xoa")
	public @ResponseBody String xoa(long id){
		try {
			nguoiDungServ.xoaNguoiDung(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/khoa")
	public @ResponseBody String khoa(long id){
		try {
			nguoiDungServ.khoaNguoiDung(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/mo-khoa")
	public @ResponseBody String moKhoa(long id){
		try {
			nguoiDungServ.moKhoaNguoiDung(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/reset-pwd")
	public @ResponseBody String resetPwd(long id){
		try {
			nguoiDungServ.datLaiMatKhauCuaNguoiDung(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
