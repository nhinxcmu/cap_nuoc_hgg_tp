package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThamSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThamSo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;

@Component
public class ThamSoConverter {

	@Autowired
	private DozerBeanMapper mapper;
	
	public ThamSoDto convertToDto(ThamSo entity) {
		if (entity == null) {
			return new ThamSoDto();
		}
		ThamSoDto dto = mapper.map(entity, ThamSoDto.class);
		return dto;
	}
	
	public ThamSo convertToEntity(ThamSoDto dto) {
		if (dto == null) {
			return new ThamSo();
		}
		ThamSo entity = mapper.map(dto, ThamSo.class);
		setNguoiDung(dto, entity);
		return entity;
	}

	private void setNguoiDung(ThamSoDto dto, ThamSo entity) {
		if (dto.getIdNguoiCapNhat() != null) {
			NguoiDung nguoiDung = new NguoiDung();
			nguoiDung.setIdNguoiDung(dto.getIdNguoiCapNhat());
			entity.setNguoiDung(nguoiDung);
		}
	}

}
