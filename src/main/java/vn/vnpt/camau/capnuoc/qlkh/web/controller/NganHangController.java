package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.NganHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NganHangService;


@Controller
@RequestMapping("/ngan-hang")
public class NganHangController {
	private ModelAttr modelAttr = new ModelAttr("Danh mục ngân hàng", "nganhang", new String[]{"jdgrid/jdgrid-v3.js","jdpage/jdpage.js","js/nganhang.js"},
			new String[]{"jdgrid/jdgrid.css"});
	@Autowired
	private NganHangService nganHangServ;
	
	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
	    return "layout";
	}
	
	@GetMapping("/lay-ds")
	public @ResponseBody Page<NganHangDto> layDanhSach(Pageable page){
		return nganHangServ.layDsNganHang(page);
	}
	
	@GetMapping("/lay-ct")
	public @ResponseBody NganHangDto layChiTiet(long id){
		return nganHangServ.layNganHang(id);
	}
	
	@PostMapping("/luu")
	public @ResponseBody String luu(NganHangDto dto){
		try {
			nganHangServ.luuNganHang(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/xoa")
	public @ResponseBody String xoa(long id){
		try {
			nganHangServ.xoaNganHang(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
