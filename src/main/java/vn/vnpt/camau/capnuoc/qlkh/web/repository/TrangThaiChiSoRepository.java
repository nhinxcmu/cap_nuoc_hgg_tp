package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiChiSo;

public interface TrangThaiChiSoRepository extends Repository<TrangThaiChiSo, Long> {

	List<TrangThaiChiSo> findAll();
	
}
