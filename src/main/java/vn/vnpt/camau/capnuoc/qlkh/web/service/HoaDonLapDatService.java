package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonLapDatDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;

public interface HoaDonLapDatService {

	Page<HoaDonLapDatDto> layDs(String searchString, Long idDuong, Long idSoGhi, Long idTrangThaiHoaDon, Pageable page);

	HoaDonLapDatDto layHoaDonLapDat(long idHoaDon);

	HoaDonLapDatDto luu(HoaDonLapDatDto hoaDon) throws Exception;

	Integer xoa(HoaDonLapDatDto hoaDon);

	void phatHanhHoaDon(Long id) throws Exception;

	void ngungPhatHanhHoaDon(Long id) throws Exception;

	void huyHoaDon(Long maHoaDonDienTu, NguoiDung nguoiCapNhat) throws Exception;

	void taiHoaDon(Long idHoaDon, HttpServletResponse response) throws FileNotFoundException, IOException;

	public void phatHanhHoaDonHangLoat() throws Exception;

	public void ngungPhatHanhHoaDonHangLoat() throws Exception;

}