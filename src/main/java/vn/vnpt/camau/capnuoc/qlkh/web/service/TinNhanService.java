package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.TinNhanDto;

public interface TinNhanService {

	List<TinNhanDto> layDsTinNhan(long idHoaDon);
	
	int themTinNhan(List<Long> dsIdHoaDon) throws Exception;
}
