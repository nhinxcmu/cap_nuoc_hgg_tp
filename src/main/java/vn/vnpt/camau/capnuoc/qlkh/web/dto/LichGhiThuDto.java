package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class LichGhiThuDto {

	@Getter
	@Setter
	private Long idLichGhiThu;
	@Getter
	@Setter
	@Mapping("nguoiDuocPhanCong.idNguoiDung")
	private Long idNhanVienGhiThu;
	@Getter
	@Setter
	@Mapping("nguoiDuocPhanCong.hoTen")
	private String tenNhanVienGhiThu;
	@Getter
	@Setter
	@Mapping("nguoiCapNhat.idNguoiDung")
	private Long idNguoiPhanCong;
	@Getter
	@Setter
	@Mapping("nguoiCapNhat.hoTen")
	private String tenNguoiPhanCong;
	@Getter
	@Setter
	private Date ngayGioCapNhat;
	@Getter
	@Setter
	List<CayDto> cayDtos = new ArrayList<CayDto>(0);
	@Getter
	@Setter
	private Set<SoGhiDto> soGhiDtos = new HashSet<SoGhiDto>(0);
}
