package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDonDienTu;

@Component
public class HoaDonDienTuConverter {

	public Long convertToIdHoaDon(HoaDonDienTu entity) {
		return entity.getIdHoaDonDienTu();
	}

}
