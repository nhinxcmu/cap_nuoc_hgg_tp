package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DonViDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DonViService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucService;

@Controller
@RequestMapping("/khu-vuc")
public class KhuVucController {
	private ModelAttr modelAttr = new ModelAttr("Danh mục khu vực", "khuvuc",
			new String[] { "plugins/input-mask/jquery.inputmask.js",
					"plugins/input-mask/jquery.inputmask.date.extensions.js",
					"bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js", "jdgrid/jdgrid-v3.js",
					"jdpage/jdpage.js", "bower_components/select2/dist/js/select2.min.js", "js/khuvuc.js" },
			new String[] { "bower_components/bootstrap-datepicker/css/bootstrap-datepicker.min.css",
					"bower_components/select2/dist/css/select2.min.css", "jdgrid/jdgrid.css" });
	@Autowired
	private DonViService donViServ;
	@Autowired
	private KhuVucService khuVucServ;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
	}
	
	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/init-form")
	public @ResponseBody List<DonViDto> initForm() {
		return donViServ.layDsXiNghiepLamViec();
	}

	@GetMapping("/lay-ds")
	public @ResponseBody Page<KhuVucDto> layDanhSach(long idDonVi, Pageable page) {
		return khuVucServ.layDsKhuVuc(idDonVi, page);
	}

	@GetMapping("/lay-ct")
	public @ResponseBody KhuVucDto layChiTiet(long id) {
		return khuVucServ.layKhuVuc(id);
	}

	@PostMapping("/luu")
	public @ResponseBody String luu(KhuVucDto dto) {
		try {
			khuVucServ.luuKhuVuc(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			// e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/xoa")
	public @ResponseBody String xoa(long id) {
		try {
			khuVucServ.xoaKhuVuc(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			// e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

}
