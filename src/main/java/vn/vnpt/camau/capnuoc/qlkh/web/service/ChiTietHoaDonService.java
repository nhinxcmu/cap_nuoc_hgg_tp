package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.ChiTietHoaDonDto;

public interface ChiTietHoaDonService {

	List<ChiTietHoaDonDto> layDsChiTietHoaDon(long idHoaDon); 
	
}
