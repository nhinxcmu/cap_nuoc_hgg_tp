package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class BienNhanThuTienNuocDto {

	@Getter
	@Setter
	private String tenCongTy;
	@Getter
	@Setter
	private String diaChiCongTy;
	@Getter
	@Setter
	private String soDienThoai;
	@Getter
	@Setter
	private String maPhieuThanhToan;
	@Getter
	@Setter
	private String maKhachHang;
	@Getter
	@Setter
	private String tenKhachHang;
	@Getter
	@Setter
	private String diaChiSuDung;
	@Getter
	@Setter
	private String thangHienHanh;
	@Getter
	@Setter
	private long tienNuocHienHanh;
	@Getter
	@Setter
	private String dsThangTon;
	@Getter
	@Setter
	private long tienNuocTon;
	@Getter
	@Setter
	private long tienThanhToan;
	@Getter
	@Setter
	private String bangChu;
	@Getter
	@Setter
	private String ngayThangNam;
	@Getter
	@Setter
	@Mapping("nguoiDung.idNguoiDung")
	private Long idNguoiCapNhat;
	@Getter
	@Setter
	@Mapping("nguoiDung.hoTen")
	private String hoTenCapNhat;

	
}
