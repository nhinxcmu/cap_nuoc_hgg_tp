package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.pattern.Util;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.BienNhanThuTienNuocMobileDtoConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.HoaDonConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.HoaDonDienTuConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.ThangConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.BienNhanThuTienNuocMobileDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDeNhanTinDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonNganHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhachHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuongCupNuoc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HinhThucThanhToan;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDonDienTu;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KetQuaHddt;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KieuKetQuaHddt;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuCupNuoc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuIn;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LyDoCupNuoc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.PhieuThanhToan;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThanhToan;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThongTinKhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiChiSo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiHoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HoaDonDienTuRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HoaDonRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.KetQuaHddtRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.KhachHangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.LichSuCupNuocRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.LichSuInRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.LyDoCupNuocRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.PhieuThanhToanRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThanhToanRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThongTinKhachHangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HoaDonService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhachHangService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThangService;
import vn.vnpt.camau.capnuoc.qlkh.web.webservice.BusinessServiceClient;
import vn.vnpt.camau.capnuoc.qlkh.web.webservice.PublishServiceClient;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.CancelInvResponse;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.ConfirmPaymentFkeyResponse;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.ImportAndPublishInvResponse;

@Service
public class HoaDonServiceImp implements HoaDonService {

	@Autowired
	private HoaDonRepository repository;
	@Autowired
	private HoaDonConverter converter;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private LyDoCupNuocRepository lyDoCupNuocRepository;
	@Autowired
	private ThangService thangService;
	@Autowired
	private PhieuThanhToanRepository phieuThanhToanRepository;
	@Autowired
	private ThanhToanRepository thanhToanRepository;
	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	private ThangConverter thangConverter;
	@Autowired
	private HoaDonDienTuRepository hoaDonDienTuRepository;
	@Autowired
	private BusinessServiceClient businessServiceClient;
	@Autowired
	private PublishServiceClient publishServiceClient;
	@Autowired
	private KetQuaHddtRepository ketQuaHddtRepository;
	@Autowired
	private HoaDonDienTuConverter hoaDonDienTuConverter;
	@Autowired
	private BienNhanThuTienNuocMobileDtoConverter bienNhanThuTienNuocMobileDtoConverter;
	@Autowired
	private KhuVucService khuVucService;
	@Autowired
	private KhachHangRepository khachHangRepository;
	@Autowired
	private LichSuInRepository lichSuInRepository;
	@Autowired
	private ThongTinKhachHangRepository thongTinKhachHangRepository;
	@Autowired
	private LichSuCupNuocRepository lsCupNuocRepository;

	@Override
	public Page<HoaDonDto> layDsHoaDon(long idKhuVuc, long idDuong, long idSoGhi, String thongTinCanTim,
			long idTrangThaiChiSo, long idTrangThaiHoaDon, int daThanhToan, Pageable pageable) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		Page<HoaDon> dsHoaDonCanLay = null;

		if (thongTinCanTim.equals("")) {
			dsHoaDonCanLay = repository.layDsHoaDon(idThangLamViec, idSoGhi, idTrangThaiChiSo, idTrangThaiHoaDon,
					idKhuVuc, idDuong, daThanhToan, pageable);
		} else {
			if (Utilities.kiemTraSoNguyen(thongTinCanTim)) {
				dsHoaDonCanLay = repository.layDsHoaDon(idThangLamViec, idSoGhi, thongTinCanTim,
						Integer.parseInt(thongTinCanTim), idTrangThaiChiSo, idTrangThaiHoaDon, idKhuVuc, idDuong,
						daThanhToan, pageable);
			} else {
				dsHoaDonCanLay = repository.layDsHoaDon(idThangLamViec, idSoGhi, thongTinCanTim, idTrangThaiChiSo,
						idTrangThaiHoaDon, idKhuVuc, idDuong, daThanhToan, pageable);
			}
		}

		Page<HoaDonDto> dsHoaDonDaChuyenDoi = dsHoaDonCanLay.map(converter::convertToDtoIncludeThongTinKhachHang);
		return dsHoaDonDaChuyenDoi;
	}

	@Override
	public HoaDonDto layHoaDon(long id) {
		HoaDon hoaDonCanLay = repository.layHoaDon(id);
		HoaDonDto hoaDonDaChuyenDoi = converter.convertToDtoIncludeLyDoCupNuoc(hoaDonCanLay);
		return hoaDonDaChuyenDoi;
	}

	@Override
	public HoaDonDto luuChiSoMoi(long id, int chiSoMoi) throws Exception {
		// kiểm tra hóa đơn
		HoaDon hoaDonDaKiemTra = kiemTraHoaDon(id);
		kiemTraHoaDonCupNuoc(hoaDonDaKiemTra);
		int chiSoCu = hoaDonDaKiemTra.getChiSoCu();
		if (chiSoMoi != 0 && chiSoMoi < chiSoCu) {
			throw new Exception("Chỉ số mới không phù hợp.");
		}
		int tieuThu = 0;
		long idTrangThaiChiSo, idTrangThaiHoaDon;
		if (chiSoMoi == 0) {
			idTrangThaiChiSo = Utilities.ID_TRANG_THAI_CHUA_GHI_CHI_SO;
			idTrangThaiHoaDon = Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON;
		} else if (chiSoMoi == chiSoCu) {
			idTrangThaiChiSo = Utilities.ID_TRANG_THAI_KHONG_XAI;
			idTrangThaiHoaDon = Utilities.ID_TRANG_THAI_KHONG_PHAT_HANH_HOA_DON;
		} else {
			tieuThu = chiSoMoi - chiSoCu;
			idTrangThaiChiSo = Utilities.ID_TRANG_THAI_BINH_THUONG;
			idTrangThaiHoaDon = Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON;
		}
		hoaDonDaKiemTra.setChiSoMoi(chiSoMoi);
		hoaDonDaKiemTra.setTieuThu(tieuThu);
		try {
			HoaDonDto hoaDonDaLuu = luuHoaDon(hoaDonDaKiemTra, idTrangThaiChiSo, idTrangThaiHoaDon);
			return hoaDonDaLuu;
		} catch (JpaSystemException e) {
			throw new Exception(Utilities.getMessage_SQLException(e, "Không thể lưu chỉ số mới."));
		} catch (Exception e) {
			throw new Exception("Không thể lưu chỉ số mới.");
		}
	}

	private HoaDon kiemTraHoaDon(long id) throws Exception {
		HoaDon hoaDonCanKiemTra = repository.findOne(id);
		if (hoaDonCanKiemTra == null) {
			throw new Exception("Không tìm thấy hóa đơn.");
		}
		return hoaDonCanKiemTra;
	}

	private HoaDonDto luuHoaDon(HoaDon hoaDonCanLuu, long idTrangThaiChiSo, long idTrangThaiHoaDon) {
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		NguoiDung nguoiCapNhat = taoNguoiCapNhat(idNguoiDungHienHanh);
		hoaDonCanLuu.setNguoiDung(nguoiCapNhat);
		if (idTrangThaiChiSo != 0) {
			TrangThaiChiSo trangThaiChiSo = new TrangThaiChiSo();
			trangThaiChiSo.setIdTrangThaiChiSo(idTrangThaiChiSo);
			hoaDonCanLuu.setTrangThaiChiSo(trangThaiChiSo);
		}
		TrangThaiHoaDon trangThaiHoaDon = new TrangThaiHoaDon();
		trangThaiHoaDon.setIdTrangThaiHoaDon(idTrangThaiHoaDon);
		hoaDonCanLuu.setTrangThaiHoaDon(trangThaiHoaDon);
		hoaDonCanLuu.setNgayGioCapNhat(new Date());
		try {
			repository.save(hoaDonCanLuu);
			HoaDonDto hoaDonDaChuyenDoi = converter.convertToDtoIncludeThongTinKhachHang(hoaDonCanLuu);
			return hoaDonDaChuyenDoi;
		} catch (JpaSystemException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public HoaDonDto khoanTieuThu(long id, int tieuThu) throws Exception {
		// kiểm tra hóa đơn
		HoaDon hoaDonDaKiemTra = kiemTraHoaDon(id);
		hoaDonDaKiemTra.setChiSoMoi(0);
		hoaDonDaKiemTra.setTieuThu(tieuThu);
		try {
			HoaDonDto hoaDonDaLuu = luuHoaDon(hoaDonDaKiemTra, Utilities.ID_TRANG_THAI_KHOAN_TIEU_THU,
					Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON);
			return hoaDonDaLuu;
		} catch (JpaSystemException e) {
			throw new Exception(Utilities.getMessage_SQLException(e, "Không thể khoán tiêu thụ."));
		} catch (Exception e) {
			throw new Exception("Không thể khoán tiêu thụ.");
		}
	}

	@Override
	public HoaDonDto luuKhongXai(long id) throws Exception {
		// kiểm tra hóa đơn
		HoaDon hoaDonDaKiemTra = kiemTraHoaDon(id);
		int chiSoCu = hoaDonDaKiemTra.getChiSoCu();
		hoaDonDaKiemTra.setChiSoMoi(chiSoCu);
		hoaDonDaKiemTra.setTieuThu(0);
		try {
			HoaDonDto hoaDonDaLuu = luuHoaDon(hoaDonDaKiemTra, Utilities.ID_TRANG_THAI_KHONG_XAI,
					Utilities.ID_TRANG_THAI_KHONG_PHAT_HANH_HOA_DON);
			return hoaDonDaLuu;
		} catch (JpaSystemException e) {
			throw new Exception(Utilities.getMessage_SQLException(e, "Không thể cập nhật không sử dụng."));
		} catch (Exception e) {
			throw new Exception("Không thể cập nhật không sử dụng.");
		}
	}

	@Override
	public HoaDonDto luuDaIn(long id) throws Exception {
		// kiểm tra hóa đơn
		HoaDon hoaDonDaKiemTra = kiemTraHoaDon(id);

		if (hoaDonDaKiemTra.isDaIn())
			hoaDonDaKiemTra.setDaIn(false);
		else
			hoaDonDaKiemTra.setDaIn(true);

		repository.save(hoaDonDaKiemTra);
		HoaDonDto hoaDonDaChuyenDoi = converter.convertToDtoIncludeThongTinKhachHang(hoaDonDaKiemTra);
		return hoaDonDaChuyenDoi;
	}

	@Override
	public HoaDonDto luuChiSoCu(long id, int chiSoCu) throws Exception {
		// kiểm tra hóa đơn
		HoaDon hoaDonDaKiemTra = kiemTraHoaDon(id);
		int chiSoMoi = hoaDonDaKiemTra.getChiSoMoi();
		int tieuThu = hoaDonDaKiemTra.getTieuThu();
		long idTrangThaiChiSo, idTrangThaiHoaDon;
		if (chiSoCu < 0 || (chiSoMoi != 0 && chiSoMoi < chiSoCu)) {
			throw new Exception("Chỉ số cũ không phù hợp.");
		}
		if (chiSoMoi == 0) {
			if (tieuThu > 0) {
				idTrangThaiChiSo = Utilities.ID_TRANG_THAI_KHOAN_TIEU_THU;
				idTrangThaiHoaDon = Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON;
			} else {
				idTrangThaiChiSo = Utilities.ID_TRANG_THAI_CHUA_GHI_CHI_SO;
				idTrangThaiHoaDon = Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON;
				tieuThu = 0;
			}
		} else if (chiSoMoi == chiSoCu) {
			idTrangThaiChiSo = Utilities.ID_TRANG_THAI_KHONG_XAI;
			idTrangThaiHoaDon = Utilities.ID_TRANG_THAI_KHONG_PHAT_HANH_HOA_DON;
			tieuThu = 0;
		} else {
			idTrangThaiChiSo = Utilities.ID_TRANG_THAI_BINH_THUONG;
			idTrangThaiHoaDon = Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON;
			tieuThu = chiSoMoi - chiSoCu;
		}
		hoaDonDaKiemTra.setChiSoCu(chiSoCu);
		hoaDonDaKiemTra.setTieuThu(tieuThu);
		try {
			HoaDonDto hoaDonDaLuu = luuHoaDon(hoaDonDaKiemTra, idTrangThaiChiSo, idTrangThaiHoaDon);
			return hoaDonDaLuu;
		} catch (JpaSystemException e) {
			throw new Exception(Utilities.getMessage_SQLException(e, "Không thể lưu chỉ số cũ."));
		} catch (Exception e) {
			throw new Exception("Không thể lưu chỉ số cũ.");
		}
	}

	@Override
	public HoaDonDto luuChuaGhiChiSo(long id) throws Exception {
		// kiểm tra hóa đơn
		HoaDon hoaDonDaKiemTra = kiemTraHoaDon(id);
		kiemTraHoaDonCupNuoc(hoaDonDaKiemTra);
		hoaDonDaKiemTra.setChiSoMoi(0);
		hoaDonDaKiemTra.setTieuThu(0);
		try {
			HoaDonDto hoaDonDaLuu = luuHoaDon(hoaDonDaKiemTra, Utilities.ID_TRANG_THAI_CHUA_GHI_CHI_SO,
					Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON);
			return hoaDonDaLuu;
		} catch (JpaSystemException e) {
			throw new Exception(Utilities.getMessage_SQLException(e, "Không thể cập nhật chưa ghi chỉ số."));
		} catch (Exception e) {
			throw new Exception("Không thể cập nhật chưa ghi chỉ số.");
		}
	}

	private void kiemTraHoaDonCupNuoc(HoaDon hoaDonDaKiemTra) throws Exception {
		long idTrangThaiChiSoCanKiemTra = hoaDonDaKiemTra.getTrangThaiChiSo().getIdTrangThaiChiSo();
		if (idTrangThaiChiSoCanKiemTra == Utilities.ID_TRANG_THAI_CUP_NUOC) {
			throw new Exception("Không thể cập nhật vì Trạng thái chỉ số là Cúp nước.");
		}
	}

	@Override
	@Transactional
	public HoaDonDto luuCupNuoc(long id, String lyDoCupNuoc, long idDoiTuong, String logCupNuoc) throws Exception {
		// kiểm tra hóa đơn
		HoaDon hoaDonDaKiemTra = kiemTraHoaDon(id);
		hoaDonDaKiemTra.setChiSoMoi(0);
		hoaDonDaKiemTra.setTieuThu(0);
		ghiLogCupNuoc(hoaDonDaKiemTra, logCupNuoc);
		try {
			themLyDoCupNuoc(hoaDonDaKiemTra, lyDoCupNuoc, idDoiTuong);
			HoaDonDto hoaDonDaLuu = luuHoaDon(hoaDonDaKiemTra, Utilities.ID_TRANG_THAI_CUP_NUOC,
					Utilities.ID_TRANG_THAI_KHONG_PHAT_HANH_HOA_DON);
			return hoaDonDaLuu;
		} catch (JpaSystemException e) {
			throw new Exception(Utilities.getMessage_SQLException(e, "Không thể cập nhật cúp nước."));
		} catch (Exception e) {
			throw new Exception("Không thể cập nhật cúp nước.");
		}
	}

	private void themLyDoCupNuoc(HoaDon hoaDon, String lyDo, long idDoiTuong) {
		DoiTuongCupNuoc dt = new DoiTuongCupNuoc();
		dt.setIdDoiTuongCupNuoc(idDoiTuong);
		LyDoCupNuoc lyDoCupNuoc = new LyDoCupNuoc();
		lyDoCupNuoc.setHoaDon(hoaDon);
		lyDoCupNuoc.setLyDo(lyDo);
		lyDoCupNuoc.setDoiTuongCupNuoc(dt);

		try {
			lyDoCupNuocRepository.save(lyDoCupNuoc);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void ghiLogCupNuoc(HoaDon hoaDon, String lsCupNuoc) {
		LichSuCupNuoc logCupNuoc = new LichSuCupNuoc();
		logCupNuoc.setHoaDon(hoaDon);
		logCupNuoc.setIdNguoiCapNhat(Utilities.layIdNguoiDung());
		logCupNuoc.setNgayGioCapNhat(new Date());
		logCupNuoc.setCupNuoc(true);
		logCupNuoc.setIdThang(hoaDon.getThang().getIdThang());
		try {
			lsCupNuocRepository.save(logCupNuoc);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	@Transactional
	public HoaDonDto luuMoNuoc(long id, String logMoNuoc) throws Exception {
		// kiểm tra hóa đơn
		HoaDon hoaDonDaKiemTra = kiemTraHoaDon(id);
		ghiLogMoNuoc(hoaDonDaKiemTra, logMoNuoc);
		try {
			xoaLyDoCupNuoc(id);
			HoaDonDto hoaDonDaLuu = luuHoaDon(hoaDonDaKiemTra, Utilities.ID_TRANG_THAI_CHUA_GHI_CHI_SO,
					Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON);
			return hoaDonDaLuu;
		} catch (JpaSystemException e) {
			throw new Exception(Utilities.getMessage_SQLException(e, "Không thể cập nhật mở nước."));
		} catch (Exception e) {
			throw new Exception("Không thể cập nhật mở nước.");
		}
	}

	private void xoaLyDoCupNuoc(long idHoaDon) {
		try {
			lyDoCupNuocRepository.deleteByHoaDon_IdHoaDon(idHoaDon);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void ghiLogMoNuoc(HoaDon hoaDon, String lsCupNuoc) {
		LichSuCupNuoc logMoNuoc = new LichSuCupNuoc();
		logMoNuoc.setHoaDon(hoaDon);
		logMoNuoc.setIdNguoiCapNhat(Utilities.layIdNguoiDung());
		logMoNuoc.setNgayGioCapNhat(new Date());
		logMoNuoc.setCupNuoc(false);
		logMoNuoc.setIdThang(hoaDon.getThang().getIdThang());
		try {
			lsCupNuocRepository.save(logMoNuoc);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * + Kiểm tra tham số đang phát hành HDDT - Nếu chưa hoặc ngày giờ hiện tại -
	 * ngày giờ cập nhật > 30 phút thì cho phát hành - Ngược lại thì ko cho phát
	 * hành + Nếu phát hành 1 HD thì kiểm tra HD có thể phát hành ko + Ngược lại lấy
	 * ds IdHoaDon có thể phát hành + Gọi procedure tạo HDDT gồm: XML và ds
	 * IdHoaDonDienTu + Gọi web service phát hành HDDT + Cập nhật ký hiệu và số HDDT
	 * + Cập nhật ds hóa đơn đã phát hành
	 */

	@Override
	@Transactional
	public void phatHanhHoaDon(long id) throws Exception {
		// kiểm tra hóa đơn
		kiemTraHoaDonCoThePhatHanh(id);
		List<Long> dsIdHoaDon = taoDsIdHoaDon(id);
		NguoiDung nguoiCapNhat = taoNguoiCapNhatHienHanh();
		// kiểm tra công ty có sử dụng hóa đơn điện tử
		if (khuVucService.laKhuVucSuDungHDDT()) {
			phatHanhHoaDonDienTu(dsIdHoaDon, nguoiCapNhat);
		} else {
			phatHanhHoaDon(dsIdHoaDon, nguoiCapNhat);
		}
	}

	private void kiemTraHoaDonCoThePhatHanh(long id) throws Exception {
		HoaDon hoaDonDaKiemTra = kiemTraHoaDon(id);
		if (hoaDonDaKiemTra.getTienNuoc() <= 0 || hoaDonDaKiemTra.getTrangThaiHoaDon().getIdTrangThaiHoaDon() == 6L) {
			throw new Exception("Không thể phát hành hóa đơn.");
		}
	}

	private List<Long> taoDsIdHoaDon(long id) {
		List<Long> dsIdHoaDon = new ArrayList<Long>();
		dsIdHoaDon.add(id);
		return dsIdHoaDon;
	}

	private void phatHanhHoaDonDienTu(List<Long> dsIdHoaDon, NguoiDung nguoiCapNhat) throws Exception {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		List<Long> dsIdHoaDonPhatHanh = new ArrayList<>();
		Iterator<Long> ds = dsIdHoaDon.iterator();
		while (ds.hasNext()) {
			Long idHoaDon = (Long) ds.next();
			dsIdHoaDonPhatHanh.add(idHoaDon);
			if (dsIdHoaDonPhatHanh.size() == 100) {
				phatHanhHoaDonDienTu(dsIdHoaDonPhatHanh, nguoiCapNhat, idThangLamViec);
				dsIdHoaDonPhatHanh = new ArrayList<>();
			}
		}
		if (!dsIdHoaDonPhatHanh.isEmpty()) {
			phatHanhHoaDonDienTu(dsIdHoaDonPhatHanh, nguoiCapNhat, idThangLamViec);
		}
	}

	private void phatHanhHoaDonDienTu(List<Long> dsIdHoaDon, NguoiDung nguoiCapNhat, long idThangLamViec)
			throws Exception {
		String[] dsKetQua = themHoaDonDienTu(dsIdHoaDon, nguoiCapNhat, idThangLamViec);
		String xmlInvData = dsKetQua[0];
		String dsIdHoaDonDienTu = dsKetQua[1];
		String ketQua = goiWebServicePhatHanhHDDT(xmlInvData, dsIdHoaDonDienTu);
		if (ketQua.substring(0, 2).equals("OK")) {
			capNhatKyHieuVaSoHoaDon(ketQua, nguoiCapNhat);
			phatHanhHoaDon(dsIdHoaDon, nguoiCapNhat);
		} else {
			throw new Exception("Phát hành hóa đơn điện tử không thành công.");
		}
	}

	private String[] themHoaDonDienTu(List<Long> dsIdHoaDon, NguoiDung nguoiCapNhat, long idThangLamViec)
			throws Exception {
		try {
			String stringDsIdHoaDon = dsIdHoaDon.stream().map(Object::toString).collect(Collectors.joining(","));
			long idNguoiCapNhat = nguoiCapNhat.getIdNguoiDung();
			StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("taoXMLHDDT")
					.registerStoredProcedureParameter("p_list_id_hd", String.class, ParameterMode.IN)
					.registerStoredProcedureParameter("idNguoiDung", Long.class, ParameterMode.IN)
					.registerStoredProcedureParameter("idThang", Long.class, ParameterMode.IN)
					.setParameter("p_list_id_hd", stringDsIdHoaDon).setParameter("idNguoiDung", idNguoiCapNhat)
					.setParameter("idThang", idThangLamViec);
			Object[] rs = (Object[]) storedProcedure.getSingleResult();
			String[] dsKetQua = new String[rs.length];
			for (int i = 0; i < rs.length; i++)
				dsKetQua[i] = String.valueOf(rs[i]);
			return dsKetQua;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể thêm hóa đơn điện tử.");
		}
	}

	private String goiWebServicePhatHanhHDDT(String xmlInvData, String dsIdHoaDonDienTu) throws Exception {
		try {
			ImportAndPublishInvResponse response = publishServiceClient.importAndPublishInv(xmlInvData, 0);
			String ketQua = response.getImportAndPublishInvResult();
			themKetQuaHDDT(dsIdHoaDonDienTu, ketQua, Utilities.ID_KIEU_PHAT_HANH_HOA_DON);
			return ketQua;
		} catch (Exception e) {
			String ketQua = "Không thể gọi web service phát hành hóa đơn điện tử.";
			themKetQuaHDDT(dsIdHoaDonDienTu, ketQua, Utilities.ID_KIEU_PHAT_HANH_HOA_DON);
			e.printStackTrace();
			throw new Exception("Không thể gọi web service phát hành hóa đơn điện tử.");
		}
	}

	private void capNhatKyHieuVaSoHoaDon(String ketQua, NguoiDung nguoiCapNhat) throws Exception {
		List<HoaDonDienTu> dsHoaDonDienTu = taoDsHoaDonDienTuTuKetQua(ketQua, nguoiCapNhat);
		try {
			hoaDonDienTuRepository.save(dsHoaDonDienTu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể cập nhật ký hiệu và số hóa đơn điện tử.");
		}
	}

	private List<HoaDonDienTu> taoDsHoaDonDienTuTuKetQua(String ketQua, NguoiDung nguoiCapNhat) throws Exception {
		try {
			List<HoaDonDienTu> dsHoaDonDienTu = new ArrayList<HoaDonDienTu>();

			String[] lan1 = ketQua.split(":");
			String[] lan2 = lan1[1].split("-");
			String paSe[] = lan2[0].split(";");
			String strKey = lan2[1];
			String pattern = paSe[0];
			// String serial = paSe[1];

			for (String s : strKey.split(",")) {
				String[] strS = s.split("_");
				Long idHoaDonDienTu = Long.parseLong(strS[0]);
				String soHoaDon = strS[1];
				String kyHieu = pattern;

				HoaDonDienTu hoaDonDienTu = new HoaDonDienTu();
				hoaDonDienTu.setIdHoaDonDienTu(idHoaDonDienTu);
				hoaDonDienTu.setNguoiDung(nguoiCapNhat);
				hoaDonDienTu.setKyHieu(kyHieu);
				hoaDonDienTu.setSoHoaDon(soHoaDon);
				hoaDonDienTu.setNgayGioCapNhat(new Date());
				hoaDonDienTu.setHuy(false);
				hoaDonDienTu.setThanhToan(false);
				hoaDonDienTu.setPhatHanhThanhCong(true);
				dsHoaDonDienTu.add(hoaDonDienTu);
			}

			return dsHoaDonDienTu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể phân tích kết quả gọi web service phát hành hóa đơn điện tử.");
		}
	}

	private void themKetQuaHDDT(String dsIdHoaDonDienTu, String ketQua, long idKieuKetQuaHddt) throws Exception {
		try {
			KetQuaHddt ketQuaHddt = new KetQuaHddt();
			KieuKetQuaHddt kieuKetQuaHddt = new KieuKetQuaHddt();
			kieuKetQuaHddt.setIdKieuKetQuaHddt(idKieuKetQuaHddt);
			ketQuaHddt.setKieuKetQuaHddt(kieuKetQuaHddt);
			ketQuaHddt.setTrangThai(ketQua);
			ketQuaHddt.setChuoiKetQua(dsIdHoaDonDienTu);
			ketQuaHddt.setNgayGioCapNhat(new Date());
			ketQuaHddtRepository.save(ketQuaHddt);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể thêm kết quả hóa đơn điện tử.");
		}
	}

	private void phatHanhHoaDon(List<Long> dsIdHoaDon, NguoiDung nguoiCapNhat) throws Exception {
		TrangThaiHoaDon trangThaiHoaDonDaPhatHanh = taoTrangThaiHoaDon(Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON);
		List<HoaDon> dsHoaDonCanPhatHanh = repository.findByIdHoaDonIn(dsIdHoaDon);
		for (HoaDon hoaDonCanPhatHanh : dsHoaDonCanPhatHanh) {
			hoaDonCanPhatHanh.setNguoiDung(nguoiCapNhat);
			hoaDonCanPhatHanh.setTrangThaiHoaDon(trangThaiHoaDonDaPhatHanh);
			hoaDonCanPhatHanh.setNgayGioCapNhat(new Date());
		}
		try {
			repository.save(dsHoaDonCanPhatHanh);
		} catch (JpaSystemException e) {
			throw new Exception(Utilities.getMessage_SQLException(e, "Không thể phát hành hóa đơn."));
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể phát hành hóa đơn.");
		}
	}

	private TrangThaiHoaDon taoTrangThaiHoaDon(long idTrangThaiHoaDon) {
		TrangThaiHoaDon trangThaiHoaDonDaPhatHanh = new TrangThaiHoaDon();
		trangThaiHoaDonDaPhatHanh.setIdTrangThaiHoaDon(idTrangThaiHoaDon);
		return trangThaiHoaDonDaPhatHanh;
	}

	@Override
	public void ngungPhatHanhHoaDon(long id) throws Exception {
		// kiểm tra hóa đơn
		kiemTraHoaDonCoTheNgungPhatHanh(id);
		List<Long> dsIdHoaDon = taoDsIdHoaDon(id);
		NguoiDung nguoiCapNhat = taoNguoiCapNhatHienHanh();
		// kiểm tra công ty có sử dụng hóa đơn điện tử
		if (khuVucService.laKhuVucSuDungHDDT()) {
			ngungPhatHanhHoaDonDienTu(dsIdHoaDon, nguoiCapNhat);
		} else {
			ngungPhatHanhHoaDon(id, nguoiCapNhat);
		}
	}

	private void kiemTraHoaDonCoTheNgungPhatHanh(long id) throws Exception {
		HoaDon hoaDonDaKiemTra = kiemTraHoaDon(id);
		if (laHoaDonDaThanhToanTienNuoc(hoaDonDaKiemTra.getTienThanhToan())) {
			throw new Exception("Không thể ngưng phát hành hóa đơn vì đã thanh toán tiền nước.");
		}
	}

	private boolean laHoaDonDaThanhToanTienNuoc(long tienThanhToan) {
		return tienThanhToan > 0;
	}

	private void ngungPhatHanhHoaDonDienTu(List<Long> dsIdHoaDon, NguoiDung nguoiCapNhat) throws Exception {
		List<HoaDonDienTu> dsHoaDonDienTu = layDsHoaDonDienTuChuaThanhToan(dsIdHoaDon);
		for (HoaDonDienTu hoaDonDienTu : dsHoaDonDienTu) {
			String ketQua = goiWebServiceHuyPhatHanhHoaDon(Long.toString(hoaDonDienTu.getIdHoaDonDienTu()));
			if (ketQua.substring(0, 2).equals("OK")) {
				capNhatNgungPhatHanhHoaDonDienTu(hoaDonDienTu, nguoiCapNhat);
				ngungPhatHanhHoaDon(hoaDonDienTu.getHoaDon().getIdHoaDon(), nguoiCapNhat);
			} else {
				throw new Exception("Ngưng phát hành hóa đơn điện tử không thành công.");
			}
		}
	}

	private String goiWebServiceHuyPhatHanhHoaDon(String idHoaDonDienTu) throws Exception {
		try {
			CancelInvResponse response = businessServiceClient.cancelInv(idHoaDonDienTu);
			String ketQua = response.getCancelInvResult();
			themKetQuaHDDT(idHoaDonDienTu, ketQua, Utilities.ID_KIEU_HUY_PHAT_HANH_HOA_DON);
			return ketQua;
		} catch (Exception e) {
			String ketQua = "Không thể gọi web service hủy phát hành hóa đơn điện tử.";
			themKetQuaHDDT(idHoaDonDienTu, ketQua, Utilities.ID_KIEU_HUY_PHAT_HANH_HOA_DON);
			e.printStackTrace();
			throw new Exception("Không thể gọi web service hủy phát hành hóa đơn điện tử.");
		}
	}

	private void capNhatNgungPhatHanhHoaDonDienTu(HoaDonDienTu hoaDonDienTu, NguoiDung nguoiCapNhat) throws Exception {
		hoaDonDienTu.setNguoiDung(nguoiCapNhat);
		// hoaDonDienTu.setNgayGioCapNhat(new Date());
		hoaDonDienTu.setHuy(true);

		try {
			hoaDonDienTuRepository.save(hoaDonDienTu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể cập nhật ngưng phát hành hóa đơn điện tử.");
		}
	}

	private void ngungPhatHanhHoaDon(long idHoaDon, NguoiDung nguoiCapNhat) throws Exception {
		TrangThaiHoaDon trangThaiHoaDonChuaPhatHanh = taoTrangThaiHoaDon(
				Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON);
		HoaDon hoaDonCanNgungPhatHanh = repository.layHoaDon(idHoaDon);

		hoaDonCanNgungPhatHanh.setNguoiDung(nguoiCapNhat);
		hoaDonCanNgungPhatHanh.setTrangThaiHoaDon(trangThaiHoaDonChuaPhatHanh);
		hoaDonCanNgungPhatHanh.setNgayGioCapNhat(new Date());

		try {
			repository.save(hoaDonCanNgungPhatHanh);
		} catch (JpaSystemException e) {
			throw new Exception(Utilities.getMessage_SQLException(e, "Không thể ngưng phát hành hóa đơn."));
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể ngưng phát hành hóa đơn.");
		}
	}

	@Override
	public List<HoaDonDto> layDsHoaDon(long idThongTinKhachHang, long idNguoiDung) throws Exception {
		Thang thangPhanCongGhiThuMoiNhat = thangService.layThangPhanCongGhiThuMoiNhat(idNguoiDung);
		List<HoaDonDto> dsHoaDonCanLay = layDsHoaDon2(idThongTinKhachHang, thangPhanCongGhiThuMoiNhat.getIdThang());
		return dsHoaDonCanLay;
	}

	@Override
	public List<HoaDonDto> layDsHoaDonChuaThanhToan(long idThongTinKhachHang, long idNguoiDung) throws Exception {
		Thang thangPhanCongGhiThuMoiNhat = thangService.layThangPhanCongGhiThuMoiNhat(idNguoiDung);
		List<HoaDonDto> dsHoaDonCanLay = layDsHoaDon3(idThongTinKhachHang, thangPhanCongGhiThuMoiNhat.getIdThang());
		return dsHoaDonCanLay;
	}

	private List<HoaDonDto> layDsHoaDon2(long idThongTinKhachHang, long idThang) {
		long tienThanhToan = 0, tienConLai = 0;
		// long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		List<HoaDon> dsHoaDonCanLay = new ArrayList<HoaDon>();
		List<HoaDon> dsHoaDonChuaThanhToan = repository
				.findByKhachHang_ThongTinKhachHangs_IdThongTinKhachHangAndTrangThaiHoaDon_IdTrangThaiHoaDonAndTienThanhToanAndTienConLaiGreaterThanAndThang_IdThangLessThanEqual(
						idThongTinKhachHang, Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON, tienThanhToan, tienConLai,
						idThang);
		List<HoaDon> dsHoaDonDaThanhToan = repository
				.findByKhachHang_ThongTinKhachHangs_IdThongTinKhachHangAndTrangThaiHoaDon_IdTrangThaiHoaDonAndTienThanhToanGreaterThanAndThanhToans_Thang_IdThangAndThanhToans_HuyThanhToanIsFalse(
						idThongTinKhachHang, Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON, tienThanhToan, idThang);
		dsHoaDonCanLay.addAll(dsHoaDonChuaThanhToan);
		dsHoaDonCanLay.addAll(dsHoaDonDaThanhToan);
		dsHoaDonCanLay.sort(new Comparator<HoaDon>() {
			@Override
			public int compare(HoaDon h1, HoaDon h2) {
				Long idThang1 = h1.getThang().getIdThang();
				Long idThang2 = h2.getThang().getIdThang();
				return idThang2.compareTo(idThang1);
			}
		});
		List<HoaDonDto> dsHoaDonDaChuyenDoi = dsHoaDonCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsHoaDonDaChuyenDoi;
	}

	private List<HoaDonDto> layDsHoaDon3(long idThongTinKhachHang, long idThang) {
		long tienThanhToan = 0, tienConLai = 0;
		List<HoaDon> dsHoaDonCanLay = new ArrayList<HoaDon>();
		List<HoaDon> dsHoaDonChuaThanhToan = repository
				.findByKhachHang_ThongTinKhachHangs_IdThongTinKhachHangAndTrangThaiHoaDon_IdTrangThaiHoaDonAndTienThanhToanAndTienConLaiGreaterThanAndThang_IdThangLessThanEqual(
						idThongTinKhachHang, Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON, tienThanhToan, tienConLai,
						idThang);
		dsHoaDonCanLay.addAll(dsHoaDonChuaThanhToan);
		dsHoaDonCanLay.sort(new Comparator<HoaDon>() {
			@Override
			public int compare(HoaDon h1, HoaDon h2) {
				Long idThang1 = h1.getThang().getIdThang();
				Long idThang2 = h2.getThang().getIdThang();
				return idThang2.compareTo(idThang1);
			}
		});
		List<HoaDonDto> dsHoaDonDaChuyenDoi = dsHoaDonCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsHoaDonDaChuyenDoi;
	}

	private NguoiDung taoNguoiCapNhat(long idNguoiDung) {
		NguoiDung nguoiCapNhat = new NguoiDung();
		nguoiCapNhat.setIdNguoiDung(idNguoiDung);
		return nguoiCapNhat;
	}

	private void kiemTraDsHoaDon(List<Long> dsIdHoaDon, List<HoaDon> dsHoaDonCanKiemTra) throws Exception {
		if (dsIdHoaDon.size() != dsHoaDonCanKiemTra.size()) {
			throw new Exception("ERR:2_Không tìm thấy hóa đơn cần thanh toán.");
		}
	}

	@Override
	@Transactional
	public long thanhToanHoaDonTaiNha(List<Long> dsIdHoaDon, long idNguoiDung) throws Exception {
		Thang thangPhanCongGhiThuMoiNhat = thangService.layThangPhanCongGhiThuMoiNhat(idNguoiDung);
		long idPhieuThanhToan = thanhToanHoaDon(dsIdHoaDon, Utilities.ID_HINH_THUC_THANH_TOAN_TAI_NHA, idNguoiDung,
				thangPhanCongGhiThuMoiNhat);
		return idPhieuThanhToan;
	}

	private long thanhToanHoaDon(List<Long> dsIdHoaDon, long idHinhThucThanhToan, long idNguoiDung, Thang thang)
			throws Exception {
		long tongTienThanhToan = 0;
		NguoiDung nguoiCapNhat = taoNguoiCapNhat(idNguoiDung);
		HinhThucThanhToan hinhThucThanhToan = taoHinhThucThanhToan(idHinhThucThanhToan);
		List<ThanhToan> dsThanhToanCanThem = new ArrayList<ThanhToan>();
		List<HoaDon> dsHoaDonCanKiemTra = repository.findByIdHoaDonIn(dsIdHoaDon);
		KhachHang khachHang = dsHoaDonCanKiemTra.get(0).getKhachHang();
		// kiểm tra hóa đơn đã thanh toán chưa
		kiemTraDsHoaDon(dsIdHoaDon, dsHoaDonCanKiemTra);
		for (HoaDon hoaDon : dsHoaDonCanKiemTra) {
			if (laHoaDonDaThanhToanTienNuoc(hoaDon.getTienThanhToan())) {
				throw new Exception("Không thể thanh toán hóa đơn vì hóa đơn đã thanh toán.");
			}
			if (laHoaDonChuaPhatHanh(hoaDon.getTrangThaiHoaDon())) {
				throw new Exception("Không thể thanh toán hóa đơn vì hóa đơn chưa phát hành.");
			}
			// tạo ds thanh toán cần thêm
			tongTienThanhToan += hoaDon.getTienConLai();
			ThanhToan thanhToanCanThem = taoThanhToanCanThem(hoaDon, nguoiCapNhat, thang, hinhThucThanhToan);
			dsThanhToanCanThem.add(thanhToanCanThem);
		}
		// cập nhật thanh toán hóa đơn điện tử
		thanhToanHoaDonDienTu(dsHoaDonCanKiemTra, nguoiCapNhat);
		// cập nhật hóa đơn đã thanh toán
		capNhatDsHoaDonCanThanhToan(dsHoaDonCanKiemTra, nguoiCapNhat);
		// thêm phiếu thanh toán
		PhieuThanhToan phieuThanhToanDaThem = themPhieuThanhToan(tongTienThanhToan, nguoiCapNhat, thang, khachHang);
		// thêm thanh toán
		themDsThanhToan(dsThanhToanCanThem, phieuThanhToanDaThem);
		// trả id phiếu thanh toán
		entityManager.refresh(phieuThanhToanDaThem);
		return phieuThanhToanDaThem.getIdPhieuThanhToan();
	}

	private long thanhToanHoaDon(List<Long> dsIdHoaDon, long idHinhThucThanhToan, long idNguoiDung, Thang thang,
			long idKhuVuc) throws Exception {
		long tongTienThanhToan = 0;
		NguoiDung nguoiCapNhat = taoNguoiCapNhat(idNguoiDung);
		HinhThucThanhToan hinhThucThanhToan = taoHinhThucThanhToan(idHinhThucThanhToan);
		List<ThanhToan> dsThanhToanCanThem = new ArrayList<ThanhToan>();
		List<HoaDon> dsHoaDonCanKiemTra = repository.findByIdHoaDonIn(dsIdHoaDon);
		KhachHang khachHang = dsHoaDonCanKiemTra.get(0).getKhachHang();
		// kiểm tra hóa đơn đã thanh toán chưa
		kiemTraDsHoaDon(dsIdHoaDon, dsHoaDonCanKiemTra);
		for (HoaDon hoaDon : dsHoaDonCanKiemTra) {
			if (laHoaDonDaThanhToanTienNuoc(hoaDon.getTienThanhToan())) {
				throw new Exception("ERR:2_Không thể thanh toán hóa đơn vì hóa đơn đã thanh toán.");
			}
			if (laHoaDonChuaPhatHanh(hoaDon.getTrangThaiHoaDon())) {
				throw new Exception("ERR:3_Không thể thanh toán hóa đơn vì hóa đơn chưa phát hành.");
			}
			// tạo ds thanh toán cần thêm
			tongTienThanhToan += hoaDon.getTienConLai();
			ThanhToan thanhToanCanThem = taoThanhToanCanThem(hoaDon, nguoiCapNhat, thang, hinhThucThanhToan);
			dsThanhToanCanThem.add(thanhToanCanThem);
		}
		// cập nhật thanh toán hóa đơn điện tử
		thanhToanHoaDonDienTu(dsHoaDonCanKiemTra, nguoiCapNhat,idKhuVuc);
		// cập nhật hóa đơn đã thanh toán
		capNhatDsHoaDonCanThanhToan(dsHoaDonCanKiemTra, nguoiCapNhat);
		// thêm phiếu thanh toán
		PhieuThanhToan phieuThanhToanDaThem = themPhieuThanhToan(tongTienThanhToan, nguoiCapNhat, thang, khachHang);
		// thêm thanh toán
		themDsThanhToan(dsThanhToanCanThem, phieuThanhToanDaThem);
		// trả id phiếu thanh toán
		entityManager.refresh(phieuThanhToanDaThem);
		return phieuThanhToanDaThem.getIdPhieuThanhToan();
	}

	private HinhThucThanhToan taoHinhThucThanhToan(long idHinhThucThanhToan) {
		HinhThucThanhToan hinhThucThanhToanTaiNha = new HinhThucThanhToan();
		hinhThucThanhToanTaiNha.setIdHinhThucThanhToan(idHinhThucThanhToan);
		return hinhThucThanhToanTaiNha;
	}

	private boolean laHoaDonChuaPhatHanh(TrangThaiHoaDon trangThaiHoaDon) {
		return trangThaiHoaDon.getIdTrangThaiHoaDon() != Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON;
	}

	private ThanhToan taoThanhToanCanThem(HoaDon hoaDon, NguoiDung nguoiCapNhat, Thang thang,
			HinhThucThanhToan hinhThucThanhToan) {
		ThanhToan thanhToanCanThem = new ThanhToan();
		thanhToanCanThem.setNguoiDung(nguoiCapNhat);
		thanhToanCanThem.setHoaDon(hoaDon);
		thanhToanCanThem.setHinhThucThanhToan(hinhThucThanhToan);
		thanhToanCanThem.setThang(thang);
		thanhToanCanThem.setTienThanhToan(hoaDon.getTienConLai());
		thanhToanCanThem.setNgayGioCapNhat(new Date());
		return thanhToanCanThem;
	}

	private void capNhatDsHoaDonCanThanhToan(List<HoaDon> dsHoaDon, NguoiDung nguoiDung) throws Exception {
		for (HoaDon hoaDon : dsHoaDon) {
			hoaDon.setNguoiDung(nguoiDung);
			hoaDon.setTienThanhToan(hoaDon.getTienConLai());
			hoaDon.setTienConLai(0);
			hoaDon.setNgayGioCapNhat(new Date());
		}
		try {
			repository.save(dsHoaDon);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể cập nhật hóa đơn đã thanh toán.");
		}
	}

	private void thanhToanHoaDonDienTu(List<HoaDon> dsHoaDon, NguoiDung nguoiDung) throws Exception {
		if (khuVucService.laKhuVucSuDungHDDT()) {
			List<Long> dsIdHoaDon = dsHoaDon.stream().map(converter::convertToIdHoaDon).collect(Collectors.toList());
			List<HoaDonDienTu> dsHoaDonDienTu = layDsHoaDonDienTuChuaThanhToan(dsIdHoaDon);
			String ketQua = goiWebServiceThanhToan(dsHoaDonDienTu);
			if (!ketQua.substring(0, 2).startsWith("OK") && !ketQua.substring(0, 6).startsWith("ERR:13")) {
				throw new Exception("Không thể thanh toán hóa đơn điện tử.");
			}
			thanhToanHoaDonDienTu2(dsHoaDonDienTu, nguoiDung);
		}
	}

	private void thanhToanHoaDonDienTu(List<HoaDon> dsHoaDon, NguoiDung nguoiDung, Long idKhuVuc) throws Exception {
		if (khuVucService.laKhuVucSuDungHDDT(idKhuVuc)) {
			List<Long> dsIdHoaDon = dsHoaDon.stream().map(converter::convertToIdHoaDon).collect(Collectors.toList());
			List<HoaDonDienTu> dsHoaDonDienTu = layDsHoaDonDienTuChuaThanhToan(dsIdHoaDon);
			String ketQua = goiWebServiceThanhToan(dsHoaDonDienTu);
			if (!ketQua.substring(0, 2).startsWith("OK") && !ketQua.substring(0, 6).startsWith("ERR:13")) {
				throw new Exception("Không thể thanh toán hóa đơn điện tử.");
			}
			thanhToanHoaDonDienTu2(dsHoaDonDienTu, nguoiDung);
		}
	}

	private List<HoaDonDienTu> layDsHoaDonDienTuChuaThanhToan(List<Long> dsIdHoaDon) throws Exception {
		List<HoaDonDienTu> dsHoaDonDienTu = hoaDonDienTuRepository
				.findByHuyIsFalseAndPhatHanhThanhCongIsTrueAndThanhToanIsFalseAndHoaDon_IdHoaDonIn(dsIdHoaDon);
		if (dsHoaDonDienTu.size() != dsIdHoaDon.size()) {
			throw new Exception("Số lượng hóa đơn điện tử không bằng số lượng hóa đơn.");
		}
		return dsHoaDonDienTu;
	}

	private String goiWebServiceThanhToan(List<HoaDonDienTu> dsHoaDonDienTu) throws Exception {
		String dsFKey = dsHoaDonDienTu.stream().map(hoaDonDienTuConverter::convertToIdHoaDon).map(Object::toString)
				.collect(Collectors.joining("_"));
		String dsIdHoaDonDienTu = dsFKey.replace("_", ",");
		try {
			ConfirmPaymentFkeyResponse response = businessServiceClient.confirmPaymentFkey(dsFKey);
			String ketQua = response.getConfirmPaymentFkeyResult();
			themKetQuaHDDT(dsIdHoaDonDienTu, ketQua, Utilities.ID_KIEU_THANH_TOAN_HOA_DON);
			return ketQua;
		} catch (Exception e) {
			String ketQua = "Không thể gọi web service thanh toán hóa đơn điện tử.";
			themKetQuaHDDT(dsIdHoaDonDienTu, ketQua, Utilities.ID_KIEU_THANH_TOAN_HOA_DON);
			e.printStackTrace();
			throw new Exception("Không thể gọi web service thanh toán hóa đơn điện tử.");
		}
	}

	private void thanhToanHoaDonDienTu2(List<HoaDonDienTu> dsHoaDonDienTu, NguoiDung nguoiDung) throws Exception {
		for (HoaDonDienTu hoaDonDienTu : dsHoaDonDienTu) {
			hoaDonDienTu.setNguoiDung(nguoiDung);
			hoaDonDienTu.setThanhToan(true);
			// hoaDonDienTu.setNgayGioCapNhat(new Date());
		}
		try {
			hoaDonDienTuRepository.save(dsHoaDonDienTu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể cập nhật thanh toán hóa đơn điện tử.");
		}
	}

	private PhieuThanhToan themPhieuThanhToan(long tienThanhToan, NguoiDung nguoiCapNhat, Thang thang,
			KhachHang khachHang) throws Exception {
		PhieuThanhToan phieuThanhToanCanThem = new PhieuThanhToan();
		phieuThanhToanCanThem.setKhachHang(khachHang);
		phieuThanhToanCanThem.setNguoiDung(nguoiCapNhat);
		phieuThanhToanCanThem.setThang(thang);
		phieuThanhToanCanThem.setTienThanhToan(tienThanhToan);
		phieuThanhToanCanThem.setNgayGioCapNhat(new Date());
		try {
			phieuThanhToanRepository.save(phieuThanhToanCanThem);
			return phieuThanhToanCanThem;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể thêm phiếu thanh toán.");
		}
	}

	private void themDsThanhToan(List<ThanhToan> dsThanhToanCanThem, PhieuThanhToan phieuThanhToan) throws Exception {
		for (ThanhToan thanhToanCanThem : dsThanhToanCanThem) {
			thanhToanCanThem.setPhieuThanhToan(phieuThanhToan);
		}
		try {
			thanhToanRepository.save(dsThanhToanCanThem);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể thêm thanh toán.");
		}
	}

	@Override
	@Transactional(rollbackOn = { Exception.class })
	public void capNhatChiSoNuocBangFileExcel(List<HoaDonDto> dsHoaDon) throws Exception {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		NguoiDung nguoiCapNhat = taoNguoiCapNhat(idNguoiDungHienHanh);
		List<String> dsMaKhachHangDeLayDsHoaDon = taoDsMaKhachHang(dsHoaDon);
		// lấy ds hóa đơn cần cập nhật
		List<HoaDon> dsHoaDonCanCapNhat = repository
				.findByThang_IdThangAndKhachHang_ThongTinKhachHangs_MaKhachHangInAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndHuyIsFalse(
						idThangLamViec, dsMaKhachHangDeLayDsHoaDon, idThangLamViec);
		// kiểm tra số lượng hóa đơn so với số lượng đầu vào
		kiemTraSoLuongHoaDon(dsHoaDonCanCapNhat.size(), dsHoaDon.size());
		// thiết lập chỉ số nước cần lưu
		thietLapChiSoCanLuu(dsHoaDonCanCapNhat, dsHoaDon, nguoiCapNhat);
		// lưu ds hóa đơn cần cập nhật
		luuDsHoaDon(dsHoaDonCanCapNhat);
	}

	private List<String> taoDsMaKhachHang(List<HoaDonDto> dsHoaDon) {
		List<String> dsMaKhachHang = new ArrayList<String>();
		for (HoaDonDto hoaDonDto : dsHoaDon) {
			dsMaKhachHang.add(hoaDonDto.getMaKhachHang());
		}
		return dsMaKhachHang;
	}

	private void kiemTraSoLuongHoaDon(int soLuongHoaDonCanKiemTra, int soLuongHoaDonCanCapNhat) throws Exception {
		if (soLuongHoaDonCanKiemTra != soLuongHoaDonCanCapNhat) {
			throw new Exception("Số lượng hóa đơn cần cập nhật chỉ số không phù hợp.");
		}
	}

	private void thietLapChiSoCanLuu(List<HoaDon> dsHoaDonCanCapNhat, List<HoaDonDto> dsHoaDon, NguoiDung nguoiCapNhat)
			throws Exception {
		for (HoaDonDto hoaDonDto : dsHoaDon) {
			// kiểm tra chỉ số cũ và chỉ số mới
			String maKhachHang = hoaDonDto.getMaKhachHang();
			int chiSoCuCanLuu = hoaDonDto.getChiSoCu();
			int chiSoMoiCanLuu = hoaDonDto.getChiSoMoi();
			if (chiSoCuCanLuu < 0 || (chiSoMoiCanLuu != 0 && chiSoMoiCanLuu < chiSoCuCanLuu)) {
				throw new Exception(String.format("[Mã khách hàng: %s] Chỉ số cũ không phù hợp.", maKhachHang));
			}
			HoaDon hoaDonCanCapNhat = dsHoaDonCanCapNhat.stream().filter(h -> h.getKhachHang().getThongTinKhachHangs()
					.iterator().next().getMaKhachHang().equals(maKhachHang)).findFirst().get();
			// kiểm tra chỉ số cũ import vào
			/*
			 * if (chiSoCuCanLuu != hoaDonCanCapNhat.getChiSoCu()) { throw new Exception(
			 * String.format("[Mã khách hàng: %s] Chỉ số cũ không bằng chỉ số tháng trước.",
			 * maKhachHang)); }
			 */

			// kiểm tra trạng thái hóa đơn khác Đã phát hành
			if (laHoaDonDaPhatHanh(hoaDonCanCapNhat.getTrangThaiHoaDon())) {
				throw new Exception(String.format("[Mã khách hàng: %s] Hóa đơn đã phát hành.", maKhachHang));
			}
			// thiết lập chỉ số nước cần lưu
			hoaDonCanCapNhat.setNguoiDung(nguoiCapNhat);
			// không cập nhật chỉ số cũ
			hoaDonCanCapNhat.setChiSoCu(chiSoCuCanLuu);
			hoaDonCanCapNhat.setChiSoMoi(chiSoMoiCanLuu);
			hoaDonCanCapNhat.setNgayGioCapNhat(new Date());
			thietLapTrangThaiChiSoVaTrangThaiHoaDon(hoaDonCanCapNhat);
		}
	}

	private boolean laHoaDonDaPhatHanh(TrangThaiHoaDon trangThaiHoaDon) {
		return trangThaiHoaDon.getIdTrangThaiHoaDon() == Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON;
	}

	private void thietLapTrangThaiChiSoVaTrangThaiHoaDon(HoaDon hoaDon) {
		int chiSoCu = hoaDon.getChiSoCu();
		int chiSoMoi = hoaDon.getChiSoMoi();
		int tieuThu;
		long idTrangThaiChiSo, idTrangThaiHoaDon;
		if (chiSoCu == chiSoMoi) {
			tieuThu = 0;
			idTrangThaiChiSo = Utilities.ID_TRANG_THAI_KHONG_XAI;
			idTrangThaiHoaDon = Utilities.ID_TRANG_THAI_KHONG_PHAT_HANH_HOA_DON;
		} else if (chiSoCu > chiSoMoi) {
			tieuThu = 0;
			idTrangThaiChiSo = Utilities.ID_TRANG_THAI_CHUA_GHI_CHI_SO;
			idTrangThaiHoaDon = Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON;
		} else {
			tieuThu = chiSoMoi - chiSoCu;
			idTrangThaiChiSo = Utilities.ID_TRANG_THAI_BINH_THUONG;
			idTrangThaiHoaDon = Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON;
		}
		TrangThaiChiSo trangThaiChiSo = new TrangThaiChiSo();
		trangThaiChiSo.setIdTrangThaiChiSo(idTrangThaiChiSo);
		hoaDon.setTrangThaiChiSo(trangThaiChiSo);
		TrangThaiHoaDon trangThaiHoaDon = new TrangThaiHoaDon();
		trangThaiHoaDon.setIdTrangThaiHoaDon(idTrangThaiHoaDon);
		hoaDon.setTrangThaiHoaDon(trangThaiHoaDon);
		hoaDon.setTieuThu(tieuThu);
	}

	private void luuDsHoaDon(List<HoaDon> dsHoaDonCanCapNhat) throws Exception {
		try {
			repository.save(dsHoaDonCanCapNhat);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu danh sách hóa đơn.");
		}
	}

	@Override
	@Transactional
	public void phatHanhHoaDonHangLoat() throws Exception {
		List<HoaDon> dsHoaDonCoThePhatHanh = layDsHoaDonCoThePhatHanh();
		List<Long> dsIdHoaDonCoThePhatHanh = dsHoaDonCoThePhatHanh.stream().map(converter::convertToIdHoaDon)
				.collect(Collectors.toList());
		NguoiDung nguoiCapNhat = taoNguoiCapNhatHienHanh();
		// kiểm tra công ty có sử dụng hóa đơn điện tử
		if (khuVucService.laKhuVucSuDungHDDT()) {
			phatHanhHoaDonDienTu(dsIdHoaDonCoThePhatHanh, nguoiCapNhat);
		} else {
			phatHanhHoaDon(dsIdHoaDonCoThePhatHanh, nguoiCapNhat);
		}
	}

	private List<HoaDon> layDsHoaDonCoThePhatHanh() {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		long tienNuoc = 0;
		List<HoaDon> dsHoaDonCoThePhatHanh = repository
				.findByThang_IdThangAndKhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndHuyIsFalseAndTrangThaiHoaDon_IdTrangThaiHoaDonAndTienNuocGreaterThan(
						idThangLamViec, idKhuVucLamViec, idThangLamViec, Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON,
						tienNuoc);
		return dsHoaDonCoThePhatHanh;
	}

	private NguoiDung taoNguoiCapNhatHienHanh() {
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		NguoiDung nguoiCapNhat = new NguoiDung();
		nguoiCapNhat.setIdNguoiDung(idNguoiDungHienHanh);
		return nguoiCapNhat;
	}

	@Override
	@Transactional
	public void ngungPhatHanhHoaDonHangLoat() throws Exception {
		List<HoaDon> dsHoaDonCoTheNgungPhatHanh = layDsHoaDonCoTheNgungPhatHanh();
		List<Long> dsIdHoaDonCoTheNgungPhatHanh = dsHoaDonCoTheNgungPhatHanh.stream().map(converter::convertToIdHoaDon)
				.collect(Collectors.toList());
		NguoiDung nguoiCapNhat = taoNguoiCapNhatHienHanh();
		// kiểm tra công ty có sử dụng hóa đơn điện tử
		if (khuVucService.laKhuVucSuDungHDDT()) {
			ngungPhatHanhHoaDonDienTu(dsIdHoaDonCoTheNgungPhatHanh, nguoiCapNhat);
		} else {
			for (long idHoaDon : dsIdHoaDonCoTheNgungPhatHanh) {
				ngungPhatHanhHoaDon(idHoaDon, nguoiCapNhat);
			}
		}
	}

	private List<HoaDon> layDsHoaDonCoTheNgungPhatHanh() {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		long thanhToan = 0;
		List<HoaDon> dsHoaDonDaPhatHanh = repository
				.findByThang_IdThangAndKhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndHuyIsFalseAndTrangThaiHoaDon_IdTrangThaiHoaDonAndTienThanhToan(
						idThangLamViec, idKhuVucLamViec, idThangLamViec, Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON,
						thanhToan);
		return dsHoaDonDaPhatHanh;
	}

	@Override
	public List<HoaDonDto> layDsHoaDon(long idThongTinKhachHang) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		List<HoaDonDto> dsHoaDonCanLay = layDsHoaDon2(idThongTinKhachHang, idThangLamViec);
		return dsHoaDonCanLay;
	}

	@Override
	@Transactional
	public long thanhToanHoaDonTaiGiaoDich(List<Long> dsIdHoaDon) throws Exception {
		ThangDto thangLamViec = Utilities.layThangLamViec(httpSession);
		Thang thangDaChuyenDoi = thangConverter.convertToEntity(thangLamViec);
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		long idPhieuThanhToan = thanhToanHoaDon(dsIdHoaDon, Utilities.ID_HINH_THUC_THANH_TOAN_TAI_GIAO_DICH,
				idNguoiDungHienHanh, thangDaChuyenDoi);
		return idPhieuThanhToan;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HoaDonDeNhanTinDto> layDsHoaDonDeNhanTin(String thongTinCanTim, int nhanTin) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		StoredProcedureQuery storedProcedure = entityManager
				.createStoredProcedureQuery("layDsHoaDonDeNhanTin", HoaDonDeNhanTinDto.class)
				.registerStoredProcedureParameter("idThang", Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter("idKhuVuc", Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter("nhanTin", Integer.class, ParameterMode.IN)
				.registerStoredProcedureParameter("thongTinCanTim", String.class, ParameterMode.IN)
				.setParameter("idThang", idThangLamViec).setParameter("idKhuVuc", idKhuVucLamViec)
				.setParameter("nhanTin", nhanTin).setParameter("thongTinCanTim", thongTinCanTim);
		@SuppressWarnings("rawtypes")
		List list = storedProcedure.getResultList();
		return list;
	}

	@Override
	public String layDsIdHoaDonTheoPhieuThanhToan(long idPhieuThanhToan) {
		List<HoaDon> dsHoaDonCanLay = repository.findByThanhToans_PhieuThanhToan_IdPhieuThanhToan(idPhieuThanhToan);
		String dsIdPhieuThanhToan = dsHoaDonCanLay.stream().map(converter::convertToIdHoaDon).map(Object::toString)
				.collect(Collectors.joining(","));
		return dsIdPhieuThanhToan;
	}

	@Override
	public List<BienNhanThuTienNuocMobileDto> layDSBienNhanThuTienNuocMobileDto(long idPhieuThanhToan, boolean xem) {
		List<HoaDon> dsHoaDonCanLay = repository.findByThanhToans_PhieuThanhToan_IdPhieuThanhToan(idPhieuThanhToan);

		if (!xem) {
			int i = 0;
			for (HoaDon hoaDon : dsHoaDonCanLay) {
				if (hoaDon.isDaIn())
					i++;
				else {
					hoaDon.setDaIn(true);
					NguoiDung nguoiDung = new NguoiDung();
					nguoiDung.setIdNguoiDung(Utilities.layIdNguoiDung());
					LichSuIn lichSuIn = new LichSuIn(hoaDon, nguoiDung, new Date());
					lichSuInRepository.save(lichSuIn);
					repository.save(hoaDon);
				}
			}

			if (i > 0)
				return null;
		}

		List<BienNhanThuTienNuocMobileDto> dsBienNhanThuTienNuocMobileDto = dsHoaDonCanLay.stream()
				.map(bienNhanThuTienNuocMobileDtoConverter::convertToDto).collect(Collectors.toList());
		return dsBienNhanThuTienNuocMobileDto;
	}

	@Override
	public void thanhToanHoaDonChuyenKhoan() throws Exception {
		// lấy ds khách hàng (bao gồm ds hóa đơn) cần thanh toán, lọc theo: tháng thông
		// tin khách hàng, ngân hàng not null, đã phát hành hóa đơn, số tiền còn lại > 0
		// lập phiếu thanh toán cho từng khách hàng để hủy thanh toán từng khách hàng
		HinhThucThanhToan hinhThucThanhToan = taoHinhThucThanhToan(Utilities.ID_HINH_THUC_THANH_TOAN_CHUYEN_KHOAN);
		NguoiDung nguoiCapNhat = taoNguoiCapNhat(Utilities.layIdNguoiDung());
		Thang thang = thangConverter.convertToEntity(Utilities.layThangLamViec(httpSession));
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();

		List<KhachHang> dsKhachHangCanThanhToan = khachHangRepository
				.findByThongTinKhachHangs_Thangs_IdThangAndThongTinKhachHangs_NganHang_IdNganHangIsNotNullAndHoaDons_TrangThaiHoaDon_IdTrangThaiHoaDonAndHoaDons_TienConLaiGreaterThanAndThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndHoaDons_Thang_IdThangLessThanEqual(
						thang.getIdThang(), Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON, 0L, idKhuVucLamViec,
						thang.getIdThang());
		for (KhachHang khachHang : dsKhachHangCanThanhToan) {
			thanhToanHoaDonTheoHinhThuc(khachHang, hinhThucThanhToan, nguoiCapNhat, thang);
		}
	}

	private void thanhToanHoaDonTheoHinhThuc(KhachHang khachHang, HinhThucThanhToan hinhThucThanhToan,
			NguoiDung nguoiCapNhat, Thang thang) throws Exception {
		long tongTienThanhToan = 0;
		List<ThanhToan> dsThanhToanCanThem = new ArrayList<ThanhToan>();
		List<HoaDon> dsHoaDonCanKiemTra = khachHang.getHoaDons().stream().collect(Collectors.toList());

		for (HoaDon hoaDon : dsHoaDonCanKiemTra) {
			if (!laHoaDonDaThanhToanTienNuoc(hoaDon.getTienThanhToan())) {
				if (laHoaDonChuaPhatHanh(hoaDon.getTrangThaiHoaDon())) {
					throw new Exception("Không thể thanh toán hóa đơn vì hóa đơn chưa phát hành.");
				}
				// tạo ds thanh toán cần thêm
				tongTienThanhToan += hoaDon.getTienConLai();
				ThanhToan thanhToanCanThem = taoThanhToanCanThem(hoaDon, nguoiCapNhat, thang, hinhThucThanhToan);
				dsThanhToanCanThem.add(thanhToanCanThem);
			}
		}
		if (tongTienThanhToan > 0) {
			// cập nhật thanh toán hóa đơn điện tử
			thanhToanHoaDonDienTu(dsHoaDonCanKiemTra, nguoiCapNhat);
			// cập nhật hóa đơn đã thanh toán
			capNhatDsHoaDonCanThanhToan(dsHoaDonCanKiemTra, nguoiCapNhat);
			// thêm phiếu thanh toán
			PhieuThanhToan phieuThanhToanDaThem = themPhieuThanhToan(tongTienThanhToan, nguoiCapNhat, thang, khachHang);
			// thêm thanh toán
			themDsThanhToan(dsThanhToanCanThem, phieuThanhToanDaThem);
		}
	}

	@Override
	public List<Long> layDsIdHoaDon(long idDuong, long idSoGhi, String thongTinCanTim, long idTrangThaiChiSo,
			long idTrangThaiHoaDon, int daThanhToan) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		Page<HoaDon> dsHoaDonCanLay = null;
		Pageable pageable = null;

		if (thongTinCanTim.equals("")) {
			dsHoaDonCanLay = repository.layDsHoaDon(idThangLamViec, idSoGhi, idTrangThaiChiSo, idTrangThaiHoaDon,
					idKhuVucLamViec, idDuong, daThanhToan, pageable);
		} else {
			if (Utilities.kiemTraSoNguyen(thongTinCanTim)) {
				dsHoaDonCanLay = repository.layDsHoaDon(idThangLamViec, idSoGhi, thongTinCanTim,
						Integer.parseInt(thongTinCanTim), idTrangThaiChiSo, idTrangThaiHoaDon, idKhuVucLamViec, idDuong,
						daThanhToan, pageable);
			} else {
				dsHoaDonCanLay = repository.layDsHoaDon(idThangLamViec, idSoGhi, thongTinCanTim, idTrangThaiChiSo,
						idTrangThaiHoaDon, idKhuVucLamViec, idDuong, daThanhToan, pageable);
			}
		}

		return dsHoaDonCanLay.map(converter::convertToIdHoaDon).getContent();
	}

	@Override
	public void ngungPhatHanhHoaDonHangLoatTheoFKey() throws Exception {
		try {
			StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("tempHoaDonTrung");

			@SuppressWarnings("unchecked")
			List<BigInteger> dsIdHoaDonDienTu = storedProcedure.getResultList();

			Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
			NguoiDung nguoiCapNhat = taoNguoiCapNhat(idNguoiDungHienHanh);
			for (BigInteger idHoaDonDienTu : dsIdHoaDonDienTu) {
				String ketQua = goiWebServiceHuyPhatHanhHoaDon(idHoaDonDienTu.toString());
				if (ketQua.substring(0, 2).equals("OK")) {
					HoaDonDienTu hoaDonDienTu = hoaDonDienTuRepository.findOne(idHoaDonDienTu.longValue());
					capNhatNgungPhatHanhHoaDonDienTu(hoaDonDienTu, nguoiCapNhat);
				} else {
					throw new Exception("Ngưng phát hành hóa đơn điện tử không thành công.");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void thanhToanHoaDonGiaoDich(boolean tatCa) throws Exception {
		// TODO Auto-generated method stub
		// lấy ds khách hàng (bao gồm ds hóa đơn) cần thanh toán, lọc theo: tháng thông
		// tin khách hàng, ngân hàng not null, đã phát hành hóa đơn, số tiền còn lại > 0
		// lập phiếu thanh toán cho từng khách hàng để hủy thanh toán từng khách hàng
		HinhThucThanhToan hinhThucThanhToan = taoHinhThucThanhToan(Utilities.ID_HINH_THUC_THANH_TOAN_TAI_GIAO_DICH);
		NguoiDung nguoiCapNhat = taoNguoiCapNhat(Utilities.layIdNguoiDung());
		Thang thang = thangConverter.convertToEntity(Utilities.layThangLamViec(httpSession));
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();

		List<KhachHang> dsKhachHangCanThanhToan;
		if (tatCa)
			dsKhachHangCanThanhToan = khachHangRepository
					.findByThongTinKhachHangs_Thangs_IdThangAndThongTinKhachHangs_NganHang_IdNganHangIsNullAndHoaDons_TrangThaiHoaDon_IdTrangThaiHoaDonAndHoaDons_TienConLaiGreaterThanAndHoaDons_Thang_IdThangLessThanEqual(
							thang.getIdThang(), Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON, 0L, thang.getIdThang());
		else
			dsKhachHangCanThanhToan = khachHangRepository
					.findByThongTinKhachHangs_Thangs_IdThangAndThongTinKhachHangs_NganHang_IdNganHangIsNullAndHoaDons_TrangThaiHoaDon_IdTrangThaiHoaDonAndHoaDons_TienConLaiGreaterThanAndThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndHoaDons_Thang_IdThangLessThanEqual(
							thang.getIdThang(), Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON, 0L, idKhuVucLamViec,
							thang.getIdThang());
		for (KhachHang khachHang : dsKhachHangCanThanhToan) {
			thanhToanHoaDonTheoHinhThuc(khachHang, hinhThucThanhToan, nguoiCapNhat, thang);
		}
	}

	@Override
	@Transactional
	public HoaDonDto themHoaDon(long id) throws Exception {
		// TODO Auto-generated method stub
		// kiểm tra có phải hóa đơn cuối không
		// thêm hóa đơn mới
		// cập nhật hóa đơn cũ
		HoaDon hoaDonCanKiemTra = repository.findByIdHoaDonAndHuyIsFalse(id);
		if (hoaDonCanKiemTra == null) {
			throw new Exception("Không tìm thấy hóa đơn.");
		}
		if (!hoaDonCanKiemTra.getHoaDonCuoi()) {
			throw new Exception("Không phải hóa đơn cuối.");
		}

		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		NguoiDung nguoiCapNhat = taoNguoiCapNhat(idNguoiDungHienHanh);
		TrangThaiChiSo trangThaiChiSo = new TrangThaiChiSo();
		trangThaiChiSo.setIdTrangThaiChiSo(3l);
		TrangThaiHoaDon trangThaiHoaDon = new TrangThaiHoaDon();
		trangThaiHoaDon.setIdTrangThaiHoaDon(5l);

		HoaDon hoaDonCanThem = new HoaDon();
		hoaDonCanThem.setKhachHang(hoaDonCanKiemTra.getKhachHang());
		hoaDonCanThem.setNguoiDung(nguoiCapNhat);
		hoaDonCanThem.setThang(hoaDonCanKiemTra.getThang());
		hoaDonCanThem.setTrangThaiChiSo(trangThaiChiSo);
		hoaDonCanThem.setTrangThaiHoaDon(trangThaiHoaDon);
		hoaDonCanThem.setChiSoCu(hoaDonCanKiemTra.getChiSoMoi());
		hoaDonCanThem.setBangChu("");
		hoaDonCanThem.setGhiChu("");
		hoaDonCanThem.setNgayGioCapNhat(new Date());
		hoaDonCanThem.setHuy(false);
		hoaDonCanThem.setHoaDonDau(false);
		hoaDonCanThem.setHoaDonCuoi(true);
		hoaDonCanThem = repository.save(hoaDonCanThem);
		hoaDonCanKiemTra.setHoaDonCuoi(false);
		repository.save(hoaDonCanKiemTra);
		return converter.convertToDto(hoaDonCanThem);
	}

	@Override
	@Transactional
	public void xoaHoaDon(long id) throws Exception {
		// TODO Auto-generated method stub
		// kiểm tra hóa đơn phát hành
		// kiểm tra có phải hóa đơn cuối không
		// kiểm tra có phải hóa đơn đầu không
		// hủy hóa đơn
		// cập nhật hóa đơn trước
		HoaDon hoaDonCanKiemTra = repository.findByIdHoaDonAndHuyIsFalse(id);
		if (hoaDonCanKiemTra == null) {
			throw new Exception("Không tìm thấy hóa đơn.");
		}
		if (hoaDonCanKiemTra.getTrangThaiHoaDon().getIdTrangThaiHoaDon() == 6) {
			throw new Exception("Hóa đơn đã phát hành.");
		}
		if (!hoaDonCanKiemTra.getHoaDonCuoi()) {
			throw new Exception("Không phải hóa đơn cuối.");
		}
		if (hoaDonCanKiemTra.getHoaDonDau()) {
			throw new Exception("Không thể hủy hóa đơn đầu.");
		}
		hoaDonCanKiemTra.setHuy(true);
		repository.save(hoaDonCanKiemTra);

		long idHoaDonTruoc = repository.getMaxIdHoaDon(hoaDonCanKiemTra.getKhachHang(), hoaDonCanKiemTra.getThang(),
				id);
		HoaDon hoaDonTruoc = repository.findByIdHoaDonAndHuyIsFalse(idHoaDonTruoc);
		hoaDonTruoc.setHoaDonCuoi(true);
		repository.save(hoaDonTruoc);
	}

	@Transactional
	@Override
	public HoaDonDto luuChiSoMoi(String maKhachHang, int chiSoMoi) throws Exception {
		long idThang = thangService.layThangPhanCongGhiThuMoiNhat(Utilities.layIdNguoiDung()).getIdThang();
		ThongTinKhachHang ttkh = thongTinKhachHangRepository.findByMaKhachHangAndThangs_IdThang(maKhachHang, idThang);

		long idKhachHang = ttkh.getKhachHang().getIdKhachHang();
		HoaDon hoaDon = repository.findByKhachHang_IdKhachHangAndThang_IdThang(idKhachHang, idThang);
		long id = hoaDon.getIdHoaDon();

		// kiểm tra hóa đơn
		HoaDon hoaDonDaKiemTra = kiemTraHoaDon(id);
		if (hoaDonDaKiemTra.getTrangThaiHoaDon().getIdTrangThaiHoaDon() == 6L) {
			throw new Exception("Không thể phát hành hóa đơn.");
		}

		kiemTraHoaDonCupNuoc(hoaDonDaKiemTra);
		int chiSoCu = hoaDonDaKiemTra.getChiSoCu();
		if (chiSoMoi != 0 && chiSoMoi < chiSoCu) {
			throw new Exception("Chỉ số mới không phù hợp.");
		}
		int tieuThu = 0;
		long idTrangThaiChiSo, idTrangThaiHoaDon;
		if (chiSoMoi == 0) {
			idTrangThaiChiSo = Utilities.ID_TRANG_THAI_CHUA_GHI_CHI_SO;
			idTrangThaiHoaDon = Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON;
		} else if (chiSoMoi == chiSoCu) {
			idTrangThaiChiSo = Utilities.ID_TRANG_THAI_KHONG_XAI;
			idTrangThaiHoaDon = Utilities.ID_TRANG_THAI_KHONG_PHAT_HANH_HOA_DON;
		} else {
			tieuThu = chiSoMoi - chiSoCu;
			idTrangThaiChiSo = Utilities.ID_TRANG_THAI_BINH_THUONG;
			idTrangThaiHoaDon = Utilities.ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON;
		}
		hoaDonDaKiemTra.setChiSoMoi(chiSoMoi);
		hoaDonDaKiemTra.setTieuThu(tieuThu);
		try {
			HoaDonDto hoaDonDaLuu = luuHoaDon(hoaDonDaKiemTra, idTrangThaiChiSo, idTrangThaiHoaDon);
			// phatHanhHoaDon(id);
			return hoaDonDaLuu;
		} catch (JpaSystemException e) {
			throw new Exception(Utilities.getMessage_SQLException(e, "Không thể lưu chỉ số mới."));
		} catch (Exception e) {
			throw new Exception("Không thể lưu chỉ số mới.");
		}
	}

	@Override
	public List<HoaDonNganHangDto> layDsHoaDonTheoMaKhachHang(String maKhachHang) {
		List<HoaDon> hds = repository.layHoaDonTheoMaKhachHang(maKhachHang);
		return hds.stream().map(converter::convertToNganHangDtoIncludeThongTinKhachHang).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public long thanhToanHoaDonTrucTuyenNganHang(List<Long> dsIdHoaDon) throws Exception {
		NguoiDung nguoiDung = Utilities.layNguoiDung();
		for (Long idHoaDon : dsIdHoaDon) {
			HoaDon hoaDon = repository.findByIdHoaDon(idHoaDon);
			if(hoaDon==null){
				throw new Exception("ERR:1_Không tìm thấy hóa đơn cần thanh toán");
			}
			ThongTinKhachHang ttkh = thongTinKhachHangRepository.layThongTinKhachHangTheoHoaDon(idHoaDon,
					hoaDon.getThang().getIdThang());
			ThangDto thang = thangService.layThangLamViec(ttkh.getSoGhi().getDuong().getKhuVuc().getIdKhuVuc());
			Thang t = thangConverter.convertToEntity(thang);
			nguoiDung.getIdNguoiDung();
			List<Long> tempIdHoaDon = new ArrayList<Long>();
			tempIdHoaDon.add(idHoaDon);
			thanhToanHoaDon(tempIdHoaDon, 4l, nguoiDung.getIdNguoiDung(), t,
					ttkh.getSoGhi().getDuong().getKhuVuc().getIdKhuVuc());
		}
		return 0;
	}

	@Override
	public long thanhToanHoaDonTrucTuyenViDienTu(List<Long> dsIdHoaDon) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}
}
