package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhachHang;

public interface KhachHangRepository extends CrudRepository<KhachHang, Long> {

	@EntityGraph(attributePaths = { "hoaDons" })
	List<KhachHang> findByThongTinKhachHangs_Thangs_IdThangAndThongTinKhachHangs_NganHang_IdNganHangIsNullAndHoaDons_TrangThaiHoaDon_IdTrangThaiHoaDonAndHoaDons_TienConLaiGreaterThanAndThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndHoaDons_Thang_IdThangLessThanEqual(
			Long idThangThongTinKhachHang, Long idTrangThaiHoaDon, Long tienConLai, Long idKhuVuc, Long idThangHoaDon);

	@EntityGraph(attributePaths = { "hoaDons" })
	List<KhachHang> findByThongTinKhachHangs_Thangs_IdThangAndThongTinKhachHangs_NganHang_IdNganHangIsNullAndHoaDons_TrangThaiHoaDon_IdTrangThaiHoaDonAndHoaDons_TienConLaiGreaterThanAndHoaDons_Thang_IdThangLessThanEqual(
			Long idThangThongTinKhachHang, Long idTrangThaiHoaDon, Long tienConLai, Long idThangHoaDon);

	@EntityGraph(attributePaths = { "hoaDons" })
	List<KhachHang> findByThongTinKhachHangs_Thangs_IdThangAndThongTinKhachHangs_NganHang_IdNganHangIsNotNullAndHoaDons_TrangThaiHoaDon_IdTrangThaiHoaDonAndHoaDons_TienConLaiGreaterThanAndThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndHoaDons_Thang_IdThangLessThanEqual(
			Long idThangThongTinKhachHang, Long idTrangThaiHoaDon, Long tienConLai, Long idKhuVuc, Long idThangHoaDon);
}
