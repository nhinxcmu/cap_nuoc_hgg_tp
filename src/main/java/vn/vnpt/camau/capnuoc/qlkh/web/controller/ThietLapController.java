package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DonViDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DonViService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.PhienLamViecService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThangService;

@Controller
@RequestMapping("/")
public class ThietLapController {
	private ModelAttr modelAttr = new ModelAttr("Thiết lập tham số làm việc", "thietlap",
			new String[] { "bower_components/select2/dist/js/select2.min.js", "js/thietlap.js" },
			new String[] { "bower_components/select2/dist/css/select2.min.css" });
	@Autowired
	private PhienLamViecService phienLamViecServ;
	@Autowired
	private ThangService thangServ;
	@Autowired
	private DonViService donViServ;
	@Autowired
	private KhuVucService khuVucServ;
	@Autowired
	private HttpSession httpSession;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/lay-don-vi-hien-tai")
	public @ResponseBody long layDonViHienTai() {
		return Utilities.layIdDonViLamViec(httpSession);
	}

	@GetMapping("/lay-khu-vuc-hien-tai")
	public @ResponseBody long layKhuVucHienTai() {
		return Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
	}

	@GetMapping("/lay-thang-hien-tai")
	public @ResponseBody long layDuLieuHienTai() {
		return Utilities.layThangLamViec(httpSession).getIdThang();
	}

	@GetMapping("/lay-ds-thang")
	public @ResponseBody List<ThangDto> layDsThangLamViec(long id) {
		return thangServ.layDsThangLamViec(id);
	}

	@GetMapping("/lay-ds-don-vi")
	public @ResponseBody List<DonViDto> layDsDonVi() {
		return donViServ.layDsXiNghiepLamViec();
	}

	@GetMapping("/lay-ds-khu-vuc")
	public @ResponseBody List<KhuVucDto> layDsKhuVuc(long id) {
		return khuVucServ.layDsKhuVucLamViec(id);
	}

	@PostMapping("/luu")
	public @ResponseBody String thietLapPhienLamViec(long idDonViLamViec, long idKhuVucLamViec, long idThangLamViec) {
		try {
			phienLamViecServ.thietLapPhienLamViec(idDonViLamViec, idKhuVucLamViec, idThangLamViec);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
