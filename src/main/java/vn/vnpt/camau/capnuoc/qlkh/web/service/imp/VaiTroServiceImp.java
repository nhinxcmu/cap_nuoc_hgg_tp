package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.MenuConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.VaiTroConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.CayDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.MenuDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.VaiTroDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Menu;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.VaiTro;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.MenuRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.VaiTroRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.VaiTroService;

@Service
public class VaiTroServiceImp implements VaiTroService {

	@Autowired
	private VaiTroConverter converter;
	@Autowired
	private VaiTroRepository repository;
	@Autowired
	private MenuRepository menuRepository;
	@Autowired
	private MenuConverter menuConverter;
	private Set<Menu> menus;

	@Override
	public List<VaiTroDto> layDsVaiTro() {
		List<VaiTro> dsVaiTroCanLay = repository.findByOrderByTenVaiTro();
		List<VaiTroDto> dsVaiTroDaChuyenDoi = dsVaiTroCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsVaiTroDaChuyenDoi;
	}

	@Override
	public List<VaiTroDto> layDsVaiTroGhiThu() {
		List<Long> dsIdVaiTro = layDsIdVaiTroGhiThu();
		List<VaiTro> dsVaiTroCanLay = repository.findByIdVaiTroInOrderByTenVaiTro(dsIdVaiTro);
		List<VaiTroDto> dsVaiTroDaChuyenDoi = dsVaiTroCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsVaiTroDaChuyenDoi;
	}

	@Override
	public List<Long> layDsIdVaiTroGhiThu() {
		List<Long> dsIdVaiTro = new ArrayList<Long>();
		dsIdVaiTro.add(Utilities.ID_VAI_TRO_GHI_CHI_SO);
		dsIdVaiTro.add(Utilities.ID_VAI_TRO_THU_TIEN);
		dsIdVaiTro.add(Utilities.ID_VAI_TRO_GHI_THU);
		dsIdVaiTro.add(Utilities.ID_VAI_TRO_CAP_NHAT_TT);
		return dsIdVaiTro;
	}

	@Override
	public Page<VaiTroDto> layDsVaiTro(Pageable pageable) {
		List<Long> dsIdVaiTro = layDsVaiTroKhongHienThi();
		Page<VaiTro> dsVaiTroCanLay = repository.findByIdVaiTroNotInOrderByIdVaiTroDesc(dsIdVaiTro , pageable);
		Page<VaiTroDto> dsVaiTroDaChuyenDoi = dsVaiTroCanLay.map(converter::convertToDto);
		return dsVaiTroDaChuyenDoi;
	}

	private List<Long> layDsVaiTroKhongHienThi() {
		List<Long> dsIdVaiTroKhongHienThi = layDsIdVaiTroGhiThu();
		dsIdVaiTroKhongHienThi.add(Utilities.ID_VAI_TRO_QUAN_TRI);
		return dsIdVaiTroKhongHienThi;
	}

	@Override
	public VaiTroDto layVaiTro(long id) {
		VaiTro vaiTroCanLay = repository.findByIdVaiTro(id);
		this.menus = vaiTroCanLay.getMenus();
		List<CayDto> dsCay = layDsCay();
		VaiTroDto vaiTroDaChuyenDoi = converter.convertToDto(vaiTroCanLay);
		vaiTroDaChuyenDoi.setCayDtos(dsCay);
		return vaiTroDaChuyenDoi;
	}

	private List<CayDto> layDsCay() {
		List<CayDto> dsCayMenuCha = new ArrayList<CayDto>();
		List<Menu> dsMenuCha = menuRepository.findByMenu_IdMenuIsNullOrderByOrderMenu();
		for (Menu menuCha : dsMenuCha) {
			CayDto cayMenuCha = taoCayMenuCha(menuCha);
			dsCayMenuCha.add(cayMenuCha);
		}
		return dsCayMenuCha;
	}

	private CayDto taoCayMenuCha(Menu menuCha) {
		List<CayDto> dsCayMenu = taoDsCayMenu(menuCha.getIdMenu());
		if (dsCayMenu.size() == 0) {
			return new CayDto();
		}
		CayDto cayMenuCha = new CayDto(-1, menuCha.getTitle(), true, null, dsCayMenu);
		return cayMenuCha;
	}

	private List<CayDto> taoDsCayMenu(Long idMenu) {
		List<CayDto> dsCayMenu = new ArrayList<CayDto>();
		List<Menu> dsMenu = menuRepository.findByMenu_IdMenuOrderByOrderMenu(idMenu);
		for (Menu menu : dsMenu) {
			CayDto cayMenu = taoCayMenu(menu);
			dsCayMenu.add(cayMenu);
		}
		return dsCayMenu;
	}

	private CayDto taoCayMenu(Menu menu) {
		boolean select = false;
		if (this.menus != null) {
			select = this.menus.stream().filter(m -> m.getIdMenu() == menu.getIdMenu()).count() > 0;
		}
		CayDto cayMenu = new CayDto(menu.getIdMenu(), menu.getTitle(), null, select, null);
		return cayMenu;
	}

	@Override
	public List<CayDto> layDsCayMenu() {
		this.menus = null;
		List<CayDto> dsCay = layDsCay();
		return dsCay;
	}

	@Override
	public VaiTroDto luuVaiTro(VaiTroDto dto) throws Exception {
		VaiTro vaiTroCanLuu;
		if (dto.getIdVaiTro() == null) {
			vaiTroCanLuu = themVaiTro(dto);
		} else {
			vaiTroCanLuu = capNhatVaiTro(dto);
		}
		VaiTroDto vaiTroDaChuyenDoi = converter.convertToDto(vaiTroCanLuu);
		return vaiTroDaChuyenDoi;
	}

	private VaiTro themVaiTro(VaiTroDto dto) throws Exception {
		VaiTro vaiTroCanLuu = converter.convertToEntity(dto);		
		VaiTro vaiTroDaLuu = luuVaiTro2(vaiTroCanLuu, dto.getMenuDtos());
		return vaiTroDaLuu;
	}

	private VaiTro luuVaiTro2(VaiTro vaiTroCanLuu, Set<MenuDto> dsMenuDto) throws Exception {
		Set<Menu> dsMenu = dsMenuDto.stream().map(menuConverter::convertToEntity).collect(Collectors.toSet());
		vaiTroCanLuu.setMenus(dsMenu);
		try {
			repository.save(vaiTroCanLuu);
			return vaiTroCanLuu;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu vai trò.");
		}
	}

	private VaiTro capNhatVaiTro(VaiTroDto dto) throws Exception {
		VaiTro vaiTroDaKiemTra = kiemTraVaiTro(dto.getIdVaiTro());
		vaiTroDaKiemTra.setTenVaiTro(dto.getTenVaiTro());
		VaiTro vaiTroDaLuu = luuVaiTro2(vaiTroDaKiemTra, dto.getMenuDtos());
		return vaiTroDaLuu;
	}

	private VaiTro kiemTraVaiTro(long id) throws Exception {
		VaiTro vaiTroCanKiemTra = repository.findOne(id);
		if (vaiTroCanKiemTra == null) {
			throw new Exception("Không tìm thấy vai trò.");
		}
		return vaiTroCanKiemTra;
	}

	@Override
	public void xoaVaiTro(long id) throws Exception {
		kiemTraVaiTro(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa vai trò.");
			}
			throw new Exception(e.getMessage());
		}
	}

}
