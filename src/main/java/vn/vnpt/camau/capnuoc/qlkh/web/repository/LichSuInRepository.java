package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuIn;

public interface LichSuInRepository extends CrudRepository<LichSuIn, Long> {

	@EntityGraph(attributePaths = { "nguoiDung" })
	List<LichSuIn> findByHoaDon_IdHoaDonOrderByNgayGioInDesc(Long idHoaDon);

}
