package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DonViDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DuongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoGhiDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DonViService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DuongService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.SoGhiService;


@Controller
@RequestMapping("/to")
public class SoGhiController {
	private ModelAttr modelAttr = new ModelAttr("Danh mục tổ", "soghi", new String[]{"jdgrid/jdgrid-v3.js","jdpage/jdpage.js","bower_components/select2/dist/js/select2.min.js","js/soghi.js"},
			new String[]{"bower_components/select2/dist/css/select2.min.css","jdgrid/jdgrid.css"});
	@Autowired
	private DonViService donViServ;
	@Autowired
	private KhuVucService khuVucServ;
	@Autowired
	private DuongService duongServ;
	@Autowired
	private SoGhiService soGhiServ;
	
	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
	    return "layout";
	}
	
	@GetMapping("/init-form")
	public @ResponseBody List<DonViDto> initForm(){
		return donViServ.layDsXiNghiepLamViec();
	}
	
	@GetMapping("/lay-ds-khu-vuc")
	public @ResponseBody List<KhuVucDto> layDanhSachKhuVuc(long id){
		return khuVucServ.layDsKhuVuc(id);
	}
	
	@GetMapping("/lay-ds-duong")
	public @ResponseBody List<DuongDto> layDanhSachDuong(long id){
		return duongServ.layDsDuong(id);
	}
	
	@GetMapping("/lay-ds")
	public @ResponseBody Page<SoGhiDto> layDanhSach(long idDuong,String thongTinCanTim, Pageable page){
		return soGhiServ.layDsSoGhi(idDuong, thongTinCanTim, page);
	}
	
	@GetMapping("/lay-ct")
	public @ResponseBody SoGhiDto layChiTiet(long id){
		return soGhiServ.laySoGhi(id);
	}
	
	@PostMapping("/luu")
	public @ResponseBody String luu(SoGhiDto dto){
		try {
			soGhiServ.luuSoGhi(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/xoa")
	public @ResponseBody String xoa(long id){
		try {
			soGhiServ.xoaSoGhi(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	
}
