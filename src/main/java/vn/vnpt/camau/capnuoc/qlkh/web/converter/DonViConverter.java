package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.DonViDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DonVi;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Huyen;

@Component
public class DonViConverter {

	@Autowired
	private DozerBeanMapper mapper;
	
	public DonViDto convertToDto(DonVi entity) {
		if (entity == null) {
			return new DonViDto();
		}
		DonViDto dto = mapper.map(entity, DonViDto.class);
		return dto;
	}
	
	public DonVi convertToEntity(DonViDto dto) {
		if (dto == null) {
			return new DonVi();
		}
		DonVi entity = mapper.map(dto, DonVi.class);
		setHuyen(dto, entity);
		return entity;
	}

	private void setHuyen(DonViDto dto, DonVi entity) {
		if (dto.getIdHuyen() != null) {
			Huyen huyen = new Huyen();
			huyen.setIdHuyen(dto.getIdHuyen());
			entity.setHuyen(huyen);
		}
	}

}
