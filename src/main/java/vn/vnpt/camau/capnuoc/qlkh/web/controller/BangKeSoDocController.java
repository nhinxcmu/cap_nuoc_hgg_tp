package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.jasperreports.engine.JRParameter;
import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.CallReportService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NguoiDungService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;

@Controller
@RequestMapping("/bang-ke-so-doc")
public class BangKeSoDocController {
	private ModelAttr modelAttr = new ModelAttr("Bảng kê sổ đọc", "bangkesodoc",
			new String[] { "js/bangkesodoc.js", "js/pdfobject.js" }, new String[] { "" });
	@Autowired
	DataSource dataSource;
	@Autowired
	private CallReportService callReportServ;
	@Autowired
	private NguoiDungService nguoiDungServ;
	@Autowired
	private ThamSoService thamSoServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@RequestMapping(value = "/xem-bao-cao", method = RequestMethod.GET)
	public @ResponseBody String xemBaoCao(HttpSession httpSession, String quy) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/bangkesodoc.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("bangkesodoc", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}
		
		String idquy, tieude1, tieude2, tieude3, tieude4, tieude5, tieude6, tieude7, tieude8;
		long idthang;
		long nam = Utilities.layThangLamViec(httpSession).getIdThang() / 100;
		if (quy.equals("I")) {
			idthang = (nam - 1) * 100 + 12;
			tieude1 = "CSC T12";
			tieude2 = "SL T12";
			tieude3 = "CSC T1";
			tieude4 = "SL T1";
			tieude5 = "CSC T2";
			tieude6 = "SL T2";
			tieude7 = "CSC T3";
			tieude8 = "SL T3";
			idquy = "QUÝ I, NĂM " + nam;
		} else if (quy.equals("II")) {
			idthang = nam * 1000 + 3;
			tieude1 = "CSC T3";
			tieude2 = "SL T3";
			tieude3 = "CSC T4";
			tieude4 = "SL T4";
			tieude5 = "CSC T5";
			tieude6 = "SL T5";
			tieude7 = "CSC T6";
			tieude8 = "SL T6";
			idquy = "QUÝ II, NĂM " + nam;
		} else if (quy.equals("III")) {
			idthang = nam * 1000 + 6;
			tieude1 = "CSC T6";
			tieude2 = "SL T6";
			tieude3 = "CSC T7";
			tieude4 = "SL T7";
			tieude5 = "CSC T8";
			tieude6 = "SL T8";
			tieude7 = "CSC T9";
			tieude8 = "SL T9";
			idquy = "QUÝ III, NĂM " + nam;
		} else {
			idthang = nam * 1000 + 9;
			tieude1 = "CSC T9";
			tieude2 = "SL T9";
			tieude3 = "CSC T10";
			tieude4 = "SL T10";
			tieude5 = "CSC T11";
			tieude6 = "SL T11";
			tieude7 = "CSC T12";
			tieude8 = "SL T12";
			idquy = "QUÝ IV, NĂM " + nam;
		}

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("tieude1", tieude1);
		parameters.put("tieude2", tieude2);
		parameters.put("tieude3", tieude3);
		parameters.put("tieude4", tieude4);
		parameters.put("tieude5", tieude5);
		parameters.put("tieude6", tieude6);
		parameters.put("tieude7", tieude7);
		parameters.put("tieude8", tieude8);
		parameters.put("idquy", idquy);
		idthang = Utilities.layThangLamViec(httpSession).getIdThang();
		parameters.put("idthang", idthang);
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("tenkhuvuc", khuVucDto.getTenKhuVuc());
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		return callReportServ.viewReport(reportFile, parameters, dataSource, httpSession);
	}

	@RequestMapping(value = "/xuat-file-pdf", method = RequestMethod.GET)
	public @ResponseBody void xuatFilePDF(HttpServletResponse response, HttpSession httpSession, String quy) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/bangkesodoc.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("bangkesodoc", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String idquy, tieude1, tieude2, tieude3, tieude4, tieude5, tieude6, tieude7, tieude8;
		long idthang;
		long nam = Utilities.layThangLamViec(httpSession).getIdThang() / 100;
		if (quy.equals("I")) {
			idthang = (nam - 1) * 100 + 12;
			tieude1 = "CSC T12";
			tieude2 = "SL T12";
			tieude3 = "CSC T1";
			tieude4 = "SL T1";
			tieude5 = "CSC T2";
			tieude6 = "SL T2";
			tieude7 = "CSC T3";
			tieude8 = "SL T3";
			idquy = "QUÝ I, NĂM " + nam;
		} else if (quy.equals("II")) {
			idthang = nam * 1000 + 3;
			tieude1 = "CSC T3";
			tieude2 = "SL T3";
			tieude3 = "CSC T4";
			tieude4 = "SL T4";
			tieude5 = "CSC T5";
			tieude6 = "SL T5";
			tieude7 = "CSC T6";
			tieude8 = "SL T6";
			idquy = "QUÝ II, NĂM " + nam;
		} else if (quy.equals("III")) {
			idthang = nam * 1000 + 6;
			tieude1 = "CSC T6";
			tieude2 = "SL T6";
			tieude3 = "CSC T7";
			tieude4 = "SL T7";
			tieude5 = "CSC T8";
			tieude6 = "SL T8";
			tieude7 = "CSC T9";
			tieude8 = "SL T9";
			idquy = "QUÝ III, NĂM " + nam;
		} else {
			idthang = nam * 1000 + 9;
			tieude1 = "CSC T9";
			tieude2 = "SL T9";
			tieude3 = "CSC T10";
			tieude4 = "SL T10";
			tieude5 = "CSC T11";
			tieude6 = "SL T11";
			tieude7 = "CSC T12";
			tieude8 = "SL T12";
			idquy = "QUÝ IV, NĂM " + nam;
		}

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("tieude1", tieude1);
		parameters.put("tieude2", tieude2);
		parameters.put("tieude3", tieude3);
		parameters.put("tieude4", tieude4);
		parameters.put("tieude5", tieude5);
		parameters.put("tieude6", tieude6);
		parameters.put("tieude7", tieude7);
		parameters.put("tieude8", tieude8);
		parameters.put("idquy", idquy);
		idthang = Utilities.layThangLamViec(httpSession).getIdThang();
		parameters.put("idthang", idthang);
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("tenkhuvuc", khuVucDto.getTenKhuVuc());
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("pdf", reportFile, parameters, dataSource, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-xls", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLS(HttpServletResponse response, HttpSession httpSession, String quy) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/bangkesodoc.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("bangkesodoc", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String idquy, tieude1, tieude2, tieude3, tieude4, tieude5, tieude6, tieude7, tieude8;
		long idthang;
		long nam = Utilities.layThangLamViec(httpSession).getIdThang() / 100;
		if (quy.equals("I")) {
			idthang = (nam - 1) * 100 + 12;
			tieude1 = "CSC T12";
			tieude2 = "SL T12";
			tieude3 = "CSC T1";
			tieude4 = "SL T1";
			tieude5 = "CSC T2";
			tieude6 = "SL T2";
			tieude7 = "CSC T3";
			tieude8 = "SL T3";
			idquy = "QUÝ I, NĂM " + nam;
		} else if (quy.equals("II")) {
			idthang = nam * 1000 + 3;
			tieude1 = "CSC T3";
			tieude2 = "SL T3";
			tieude3 = "CSC T4";
			tieude4 = "SL T4";
			tieude5 = "CSC T5";
			tieude6 = "SL T5";
			tieude7 = "CSC T6";
			tieude8 = "SL T6";
			idquy = "QUÝ II, NĂM " + nam;
		} else if (quy.equals("III")) {
			idthang = nam * 1000 + 6;
			tieude1 = "CSC T6";
			tieude2 = "SL T6";
			tieude3 = "CSC T7";
			tieude4 = "SL T7";
			tieude5 = "CSC T8";
			tieude6 = "SL T8";
			tieude7 = "CSC T9";
			tieude8 = "SL T9";
			idquy = "QUÝ III, NĂM " + nam;
		} else {
			idthang = nam * 1000 + 9;
			tieude1 = "CSC T9";
			tieude2 = "SL T9";
			tieude3 = "CSC T10";
			tieude4 = "SL T10";
			tieude5 = "CSC T11";
			tieude6 = "SL T11";
			tieude7 = "CSC T12";
			tieude8 = "SL T12";
			idquy = "QUÝ IV, NĂM " + nam;
		}

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("tieude1", tieude1);
		parameters.put("tieude2", tieude2);
		parameters.put("tieude3", tieude3);
		parameters.put("tieude4", tieude4);
		parameters.put("tieude5", tieude5);
		parameters.put("tieude6", tieude6);
		parameters.put("tieude7", tieude7);
		parameters.put("tieude8", tieude8);
		parameters.put("idquy", idquy);
		idthang = Utilities.layThangLamViec(httpSession).getIdThang();
		parameters.put("idthang", idthang);
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("tenkhuvuc", khuVucDto.getTenKhuVuc());
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("xls", reportFile, parameters, dataSource, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-xlsx", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLSX(HttpServletResponse response, HttpSession httpSession, String quy) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/bangkesodoc.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("bangkesodoc", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String idquy, tieude1, tieude2, tieude3, tieude4, tieude5, tieude6, tieude7, tieude8;
		long idthang;
		long nam = Utilities.layThangLamViec(httpSession).getIdThang() / 100;
		if (quy.equals("I")) {
			idthang = (nam - 1) * 100 + 12;
			tieude1 = "CSC T12";
			tieude2 = "SL T12";
			tieude3 = "CSC T1";
			tieude4 = "SL T1";
			tieude5 = "CSC T2";
			tieude6 = "SL T2";
			tieude7 = "CSC T3";
			tieude8 = "SL T3";
			idquy = "QUÝ I, NĂM " + nam;
		} else if (quy.equals("II")) {
			idthang = nam * 1000 + 3;
			tieude1 = "CSC T3";
			tieude2 = "SL T3";
			tieude3 = "CSC T4";
			tieude4 = "SL T4";
			tieude5 = "CSC T5";
			tieude6 = "SL T5";
			tieude7 = "CSC T6";
			tieude8 = "SL T6";
			idquy = "QUÝ II, NĂM " + nam;
		} else if (quy.equals("III")) {
			idthang = nam * 1000 + 6;
			tieude1 = "CSC T6";
			tieude2 = "SL T6";
			tieude3 = "CSC T7";
			tieude4 = "SL T7";
			tieude5 = "CSC T8";
			tieude6 = "SL T8";
			tieude7 = "CSC T9";
			tieude8 = "SL T9";
			idquy = "QUÝ III, NĂM " + nam;
		} else {
			idthang = nam * 1000 + 9;
			tieude1 = "CSC T9";
			tieude2 = "SL T9";
			tieude3 = "CSC T10";
			tieude4 = "SL T10";
			tieude5 = "CSC T11";
			tieude6 = "SL T11";
			tieude7 = "CSC T12";
			tieude8 = "SL T12";
			idquy = "QUÝ IV, NĂM " + nam;
		}

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("tieude1", tieude1);
		parameters.put("tieude2", tieude2);
		parameters.put("tieude3", tieude3);
		parameters.put("tieude4", tieude4);
		parameters.put("tieude5", tieude5);
		parameters.put("tieude6", tieude6);
		parameters.put("tieude7", tieude7);
		parameters.put("tieude8", tieude8);
		parameters.put("idquy", idquy);
		idthang = Utilities.layThangLamViec(httpSession).getIdThang();
		parameters.put("idthang", idthang);
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("tenkhuvuc", khuVucDto.getTenKhuVuc());
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("xlsx", reportFile, parameters, dataSource, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-rtf", method = RequestMethod.GET)
	public @ResponseBody void xuatFileRTF(HttpServletResponse response, HttpSession httpSession, String quy) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/bangkesodoc.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("bangkesodoc", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String idquy, tieude1, tieude2, tieude3, tieude4, tieude5, tieude6, tieude7, tieude8;
		long idthang;
		long nam = Utilities.layThangLamViec(httpSession).getIdThang() / 100;
		if (quy.equals("I")) {
			idthang = (nam - 1) * 100 + 12;
			tieude1 = "CSC T12";
			tieude2 = "SL T12";
			tieude3 = "CSC T1";
			tieude4 = "SL T1";
			tieude5 = "CSC T2";
			tieude6 = "SL T2";
			tieude7 = "CSC T3";
			tieude8 = "SL T3";
			idquy = "QUÝ I, NĂM " + nam;
		} else if (quy.equals("II")) {
			idthang = nam * 1000 + 3;
			tieude1 = "CSC T3";
			tieude2 = "SL T3";
			tieude3 = "CSC T4";
			tieude4 = "SL T4";
			tieude5 = "CSC T5";
			tieude6 = "SL T5";
			tieude7 = "CSC T6";
			tieude8 = "SL T6";
			idquy = "QUÝ II, NĂM " + nam;
		} else if (quy.equals("III")) {
			idthang = nam * 1000 + 6;
			tieude1 = "CSC T6";
			tieude2 = "SL T6";
			tieude3 = "CSC T7";
			tieude4 = "SL T7";
			tieude5 = "CSC T8";
			tieude6 = "SL T8";
			tieude7 = "CSC T9";
			tieude8 = "SL T9";
			idquy = "QUÝ III, NĂM " + nam;
		} else {
			idthang = nam * 1000 + 9;
			tieude1 = "CSC T9";
			tieude2 = "SL T9";
			tieude3 = "CSC T10";
			tieude4 = "SL T10";
			tieude5 = "CSC T11";
			tieude6 = "SL T11";
			tieude7 = "CSC T12";
			tieude8 = "SL T12";
			idquy = "QUÝ IV, NĂM " + nam;
		}

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("tieude1", tieude1);
		parameters.put("tieude2", tieude2);
		parameters.put("tieude3", tieude3);
		parameters.put("tieude4", tieude4);
		parameters.put("tieude5", tieude5);
		parameters.put("tieude6", tieude6);
		parameters.put("tieude7", tieude7);
		parameters.put("tieude8", tieude8);
		parameters.put("idquy", idquy);
		idthang = Utilities.layThangLamViec(httpSession).getIdThang();
		parameters.put("idthang", idthang);
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("tenkhuvuc", khuVucDto.getTenKhuVuc());
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("rtf", reportFile, parameters, dataSource, httpSession, response);
	}
}
