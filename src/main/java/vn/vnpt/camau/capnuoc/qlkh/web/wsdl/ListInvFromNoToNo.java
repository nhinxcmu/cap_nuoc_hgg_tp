//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.05.29 at 05:51:20 PM ICT 
//


package vn.vnpt.camau.capnuoc.qlkh.web.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="invFromNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="invToNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="invPattern" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="invSerial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="userName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="userPass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "invFromNo",
    "invToNo",
    "invPattern",
    "invSerial",
    "userName",
    "userPass"
})
@XmlRootElement(name = "listInvFromNoToNo")
public class ListInvFromNoToNo {

    protected String invFromNo;
    protected String invToNo;
    protected String invPattern;
    protected String invSerial;
    protected String userName;
    protected String userPass;

    /**
     * Gets the value of the invFromNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvFromNo() {
        return invFromNo;
    }

    /**
     * Sets the value of the invFromNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvFromNo(String value) {
        this.invFromNo = value;
    }

    /**
     * Gets the value of the invToNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvToNo() {
        return invToNo;
    }

    /**
     * Sets the value of the invToNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvToNo(String value) {
        this.invToNo = value;
    }

    /**
     * Gets the value of the invPattern property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvPattern() {
        return invPattern;
    }

    /**
     * Sets the value of the invPattern property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvPattern(String value) {
        this.invPattern = value;
    }

    /**
     * Gets the value of the invSerial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvSerial() {
        return invSerial;
    }

    /**
     * Sets the value of the invSerial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvSerial(String value) {
        this.invSerial = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the userPass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserPass() {
        return userPass;
    }

    /**
     * Sets the value of the userPass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserPass(String value) {
        this.userPass = value;
    }

}
