package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DuongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDienTuLapDatDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonLapDatDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoGhiDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.TrangThaiHoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.SearchForm;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DuongService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HoaDonDienTuLapDatService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HoaDonLapDatService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.SoGhiService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.TrangThaiHoaDonService;

@Controller
@RequestMapping("/hoa-don-lap-dat")
public class HoaDonLapDatController {
	private ModelAttr modelAttr = new ModelAttr("Danh sách hóa đơn lắp đặt", "hoadonlapdat",
			new String[] { "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js", "bower_components/select2/dist/js/select2.min.js",
					"js/controllers/HoaDonLapDatController.js", "js/pdfobject.js" },
			new String[] { "bower_components/select2/dist/css/select2.min.css", "jdgrid/jdgrid.css" });

	@Autowired
	private HoaDonLapDatService hoaDonLapDatService;
	@Autowired
	private DuongService duongServ;
	@Autowired
	private SoGhiService soGhiServ;
	@Autowired
	private HoaDonDienTuLapDatService hoaDonDienTuLapDatService;
	@Autowired
	private TrangThaiHoaDonService trangThaiHoaDonService;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@RequestMapping("/lay-ds")
	public @ResponseBody Page<HoaDonLapDatDto> layDs(@RequestBody SearchForm searchForm) {
		return hoaDonLapDatService.layDs(searchForm.searchString, searchForm.idDuong, searchForm.idSoGhi,
				searchForm.idTrangThaiHoaDon, new PageRequest(searchForm.page, searchForm.pageSize));
	}

	@RequestMapping(value = "/luu")
	@ResponseBody
	public Response luu(@RequestBody HoaDonLapDatDto hopDong) {
		try {
			hoaDonLapDatService.luu(hopDong);
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

	@RequestMapping("/lay-ds-phuong")
	public @ResponseBody List<DuongDto> layDsDuong(HttpSession httpSession) {
		return duongServ.layDsDuong(Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc());
	}

	@RequestMapping("/lay-ds-to")
	public @ResponseBody List<SoGhiDto> layDsSoGhi(long idDuong) {
		return soGhiServ.layDsSoGhi(idDuong);
	}

	@RequestMapping("/phat-hanh-hddt")
	@ResponseBody
	public Response phatHanh(@RequestBody HoaDonLapDatDto hoaDon) {
		try {
			hoaDonLapDatService.phatHanhHoaDon(hoaDon.getIdHoaDon());
			return new Response(1, "Thành công");
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(0, e.getMessage());
		}
	}

	@RequestMapping("/lay-ds-hddt")
	@ResponseBody
	public List<HoaDonDienTuLapDatDto> layDsHddt(@RequestBody HoaDonLapDatDto hoaDon) {
		try {
			List<HoaDonDienTuLapDatDto> hoaDons = hoaDonDienTuLapDatService.layDsHddtTheoHopDong(hoaDon.getIdHoaDon());
			return hoaDons;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping("/ngung-phat-hanh-hddt")
	@ResponseBody
	public Response ngungPhatHanh(@RequestBody HoaDonDienTuLapDatDto hoaDon) {
		try {
			hoaDonLapDatService.huyHoaDon(hoaDon.getIdHoaDonDienTu(), Utilities.layNguoiDung());
			return new Response(1, "Thành công");
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(0, e.getMessage());
		}
	}

	@RequestMapping(value = "/in-hoa-don")
	void taiHoaDon(Long idHoaDonDienTu, HttpServletResponse response) throws FileNotFoundException, IOException {
		hoaDonLapDatService.taiHoaDon(idHoaDonDienTu, response);
	}

	@GetMapping("/lay-ds-tt-hoa-don")
	public @ResponseBody List<TrangThaiHoaDonDto> layDsTrangThaiHoaDon() {
		return trangThaiHoaDonService.layDsTrangThaiHoaDon();
	}
}