package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.LichGhiThuDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichGhiThu;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;

@Component
public class LichGhiThuConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public LichGhiThuDto convertToDto(LichGhiThu entity) {
		if (entity == null) {
			return new LichGhiThuDto();
		}
		LichGhiThuDto dto = mapper.map(entity, LichGhiThuDto.class);
		return dto;
	}
	
	public LichGhiThu convertToEntity(LichGhiThuDto dto) {
		if (dto == null) {
			return new LichGhiThu();
		}
		LichGhiThu entity = mapper.map(dto, LichGhiThu.class);
		setNguoiPhanCong(dto, entity);
		setNguoiDuocPhanCong(dto, entity);
		return entity;
	}

	private void setNguoiPhanCong(LichGhiThuDto dto, LichGhiThu entity) {
		if (dto.getIdNguoiPhanCong() != null) {
			NguoiDung nguoiPhanCong = new NguoiDung();
			nguoiPhanCong.setIdNguoiDung(dto.getIdNguoiPhanCong());
			entity.setNguoiCapNhat(nguoiPhanCong);
		}
	}

	private void setNguoiDuocPhanCong(LichGhiThuDto dto, LichGhiThu entity) {
		if (dto.getIdNhanVienGhiThu() != null) {
			NguoiDung nguoiDuocPhanCong = new NguoiDung();
			nguoiDuocPhanCong.setIdNguoiDung(dto.getIdNhanVienGhiThu());
			entity.setNguoiDuocPhanCong(nguoiDuocPhanCong);
		}
	}

}
