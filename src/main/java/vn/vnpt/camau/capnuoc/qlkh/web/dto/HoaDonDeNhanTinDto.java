package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class HoaDonDeNhanTinDto {

	@Id
	@Column(name = "ID_HOA_DON", unique = true, nullable = false)
	private Long idHoaDon;
	@Column(name = "THU_TU", nullable = false)
	private int thuTu;
	@Column(name = "MA_KHACH_HANG", nullable = false, length = 10)
	private String maKhachHang;
	@Column(name = "TEN_KHACH_HANG", nullable = false, length = 250)
	private String tenKhachHang;
	@Column(name = "SO_DIEN_THOAI", nullable = false, length = 20)
	private String soDienThoai;
	@Column(name = "DIA_CHI_SU_DUNG", nullable = false, length = 250)
	private String diaChiSuDung;
	@Column(name = "TIEU_THU", nullable = false)
	private int tieuThu;
	@Column(name = "TONG_TIEN", nullable = false, precision = 10, scale = 0)
	private long tongTien;
	@Column(name = "SO_LUONG_TIN_NHAN", nullable = false)
	private int soLuongTinNhan;
	
}
