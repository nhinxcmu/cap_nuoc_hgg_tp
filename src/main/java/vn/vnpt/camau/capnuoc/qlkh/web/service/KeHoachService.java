package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.KeHoachDto;

public interface KeHoachService {

	KeHoachDto layKeHoach(long idKeHoach);

	KeHoachDto luuKeHoach(KeHoachDto dto) throws Exception;

	List<KeHoachDto> layDanhSach(long idKhuVuc, String nam, long idThang);
}
