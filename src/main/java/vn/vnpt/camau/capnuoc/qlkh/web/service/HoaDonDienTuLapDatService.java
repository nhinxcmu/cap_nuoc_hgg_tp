package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDienTuLapDatDto;

public interface HoaDonDienTuLapDatService {
	List<HoaDonDienTuLapDatDto> layDsHddtTheoHopDong(Long idHoaDon);
}