package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import lombok.Data;

@Data
public class SoLieuThanhToanChuyenKhoanDto {

	private int tongHoaDon, tongDaThanhToan, tongChuaThanhToan;
}
