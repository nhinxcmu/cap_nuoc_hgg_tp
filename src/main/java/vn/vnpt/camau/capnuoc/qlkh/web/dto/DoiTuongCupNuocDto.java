package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import lombok.Getter;
import lombok.Setter;

//@Data
public class DoiTuongCupNuocDto implements Comparable<DoiTuongCupNuocDto> {

	@Getter
	@Setter
	private long idDoiTuongCupNuoc;
	@Getter
	@Setter
	private String tenDoiTuongCupNuoc;
	
	@Override
	public int compareTo(DoiTuongCupNuocDto object) {
		return this.tenDoiTuongCupNuoc.compareTo(object.getTenDoiTuongCupNuoc());
	}
}
