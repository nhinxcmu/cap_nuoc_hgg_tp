package vn.vnpt.camau.capnuoc.qlkh.web.webservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.CancelInv;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.CancelInvResponse;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.ConfirmPaymentFkey;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.ConfirmPaymentFkeyResponse;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.ReplaceInv;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.ReplaceInvResponse;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.UnConfirmPaymentFkey;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.UnConfirmPaymentFkeyResponse;

public class BusinessServiceClient extends WebServiceGatewaySupport {

	@Autowired
	private ThamSoService thamSoService;
	
	public ReplaceInvResponse replaceInv(String xmlInvData, String fkey, int convert) {
		ReplaceInv request = new ReplaceInv();
		
		request.setAccount(thamSoService.layAccount());
		request.setACpass(thamSoService.layACpass());
		request.setXmlInvData(xmlInvData);
		request.setUsername(thamSoService.layUsername());
		request.setPass(thamSoService.layPassword());
		request.setFkey(fkey);
		request.setConvert(convert);
		
		String soapAction = Utilities.NAMESPACE + "replaceInv";
		ReplaceInvResponse response = (ReplaceInvResponse) getWebServiceTemplate()
				.marshalSendAndReceive(thamSoService.layUriBusinessService(), request, new SoapActionCallback(soapAction));
		
		return response;
	}
	
	public CancelInvResponse cancelInv(String fkey) {
		CancelInv request = new CancelInv();
		
		request.setAccount(thamSoService.layAccount());
		request.setACpass(thamSoService.layACpass());
		request.setUserName(thamSoService.layUsername());
		request.setUserPass(thamSoService.layPassword());
		request.setFkey(fkey);
		
		String soapAction = Utilities.NAMESPACE + "cancelInv";
		CancelInvResponse response = (CancelInvResponse) getWebServiceTemplate()
				.marshalSendAndReceive(thamSoService.layUriBusinessService(), request, new SoapActionCallback(soapAction));
		
		return response;
	}
	
	public ConfirmPaymentFkeyResponse confirmPaymentFkey(String lstFkey) {
		ConfirmPaymentFkey request = new ConfirmPaymentFkey();
		
		request.setUserName(thamSoService.layUsername());
		request.setUserPass(thamSoService.layPassword());
		request.setLstFkey(lstFkey);
		
		String soapAction = Utilities.NAMESPACE + "confirmPaymentFkey";
		ConfirmPaymentFkeyResponse response = (ConfirmPaymentFkeyResponse) getWebServiceTemplate()
				.marshalSendAndReceive(thamSoService.layUriBusinessService(), request, new SoapActionCallback(soapAction));
		
		return response;
	}
	
	public UnConfirmPaymentFkeyResponse unConfirmPaymentFkey(String lstFkey) {
		UnConfirmPaymentFkey request = new UnConfirmPaymentFkey();
		
		request.setUserName(thamSoService.layUsername());
		request.setUserPass(thamSoService.layPassword());
		request.setLstFkey(lstFkey);
		
		String soapAction = Utilities.NAMESPACE + "UnConfirmPaymentFkey";
		UnConfirmPaymentFkeyResponse response = (UnConfirmPaymentFkeyResponse) getWebServiceTemplate()
				.marshalSendAndReceive(thamSoService.layUriBusinessService(), request, new SoapActionCallback(soapAction));
		
		return response;
	}

}
