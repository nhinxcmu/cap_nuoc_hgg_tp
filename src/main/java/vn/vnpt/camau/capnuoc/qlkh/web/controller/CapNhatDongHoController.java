package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.jasperreports.engine.JRParameter;
import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.FileBean;
import vn.vnpt.camau.capnuoc.qlkh.web.service.CallReportService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhachHangService;

@Controller
@RequestMapping("/cap-nhat-dong-ho")
public class CapNhatDongHoController {
	private ModelAttr modelAttr = new ModelAttr("Cập nhật đồng hồ", "capnhatdongho",
			new String[] { "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js", "js/capnhatdongho.js" },
			new String[] { "jdgrid/jdgrid.css" });
	@Autowired
	private KhachHangService khachHangServ;
	@Autowired
	private CallReportService callReportServ;
	@Autowired
	DataSource dataSource;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@PostMapping("/xu-ly-import")
	public @ResponseBody String upload(FileBean uploadItem, BindingResult result) {
		return khachHangServ.capNhatDongHo(uploadItem);
	}
	@RequestMapping(value = "/xuat-file-xlsx", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLSX(HttpServletResponse response, HttpSession httpSession) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		/*
		long idThang = Utilities.layThangLamViec(httpSession).getIdThang();
		if (idThang == 202011l || idThang == 202012l) {
			classPathResource = new ClassPathResource("/static/report/danhsachkhachhanghoantra.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("danhsachkhachhanghoantra", ".jasper");
		} else {
			classPathResource = new ClassPathResource("/static/report/danhsachkhachhang.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("danhsachkhachhang", ".jasper");
		}
		*/
		classPathResource = new ClassPathResource("/static/report/importSoSeri.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("importSoSeri", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idKhuVuc", Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc());
		// parameters.put("idDuong", idDuong);
		// parameters.put("idSoGhi", idSoGhi);
		parameters.put("idThang", Utilities.layThangLamViec(httpSession).getIdThang());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("xlsx", reportFile, parameters, dataSource, httpSession, response);
	}
}
