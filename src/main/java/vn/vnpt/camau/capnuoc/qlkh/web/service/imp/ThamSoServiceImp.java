package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.ThamSoConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThamSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThamSo;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThamSoRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;

@Service
public class ThamSoServiceImp implements ThamSoService {

	@Autowired
	private ThamSoConverter converter;
	@Autowired
	private ThamSoRepository repository;
	
	@Override
	public Page<ThamSoDto> layDsThamSo(String thongTinCanTim, Pageable pageable) {
		Page<ThamSo> dsThamSoCanLay;
		if (thongTinCanTim.equals("")) {
			dsThamSoCanLay = repository.findByHienThiIsTrue(pageable);
		} else {
			dsThamSoCanLay = repository.findByHienThiIsTrueAndTenThamSoContains(thongTinCanTim, pageable);
		}
		Page<ThamSoDto> dsThamSoDaChuyenDoi = dsThamSoCanLay.map(converter::convertToDto);
		return dsThamSoDaChuyenDoi;
	}

	@Override
	public ThamSoDto layThamSo(long id) {
		ThamSo thamSoCanLay = repository.findOne(id);
		ThamSoDto thamSoDaChuyenDoi = converter.convertToDto(thamSoCanLay);
		return thamSoDaChuyenDoi;
	}

	@Override
	public ThamSoDto luuThamSo(ThamSoDto dto) throws Exception {
		kiemTraThamSo(dto.getIdThamSo());
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		dto.setIdNguoiCapNhat(idNguoiDungHienHanh);
		dto.setNgayGioCapNhat(new Date());
		ThamSo thamSoCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(thamSoCanLuu);
			ThamSoDto thamSoDaChuyenDoi = converter.convertToDto(thamSoCanLuu);
			return thamSoDaChuyenDoi;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu tham số.");
		}
	}

	private void kiemTraThamSo(Long idThamSo) throws Exception {
		if (idThamSo == null) {
			throw new Exception("Không tìm thấy tham số.");
		}
		ThamSo thamSoCanKiemTra = repository.findOne(idThamSo);
		if (thamSoCanKiemTra == null) {
			throw new Exception("Không tìm thấy tham số.");
		}
	}

	@Override
	public List<ThamSoDto> layDsThamSoCongTy() {
		List<Long> dsIdThamSoCongTy = layDsIdThamSoCongTy();
		List<ThamSo> dsThamSoCanLay = repository.findByIdThamSoInAndHienThiIsTrueOrderByIdThamSo(dsIdThamSoCongTy);
		List<ThamSoDto> dsThamSoDaChuyenDoi = dsThamSoCanLay.stream().map(converter::convertToDto).collect(Collectors.toList());
		return dsThamSoDaChuyenDoi;
	}

	private List<Long> layDsIdThamSoCongTy() {
		List<Long> dsIdThamSo = new ArrayList<Long>();
		dsIdThamSo.add(Utilities.ID_THAM_SO_TEN_CONG_TY);
		dsIdThamSo.add(Utilities.ID_THAM_SO_DIA_CHI_CONG_TY);
		dsIdThamSo.add(Utilities.ID_THAM_SO_MA_SO_THUE);
		dsIdThamSo.add(Utilities.ID_THAM_SO_SO_DIEN_THOAI);
		dsIdThamSo.add(Utilities.ID_THAM_SO_DON_VI_SU_DUNG);
		return dsIdThamSo;
	}

	@Override
	public String layUriPublishService() {
		ThamSo thamSo = repository.findOne(Utilities.ID_THAM_SO_URI_PUBLISH_SERVICE);
		return thamSo.getGiaTri();
	}

	@Override
	public String layUriBusinessService() {
		ThamSo thamSo = repository.findOne(Utilities.ID_THAM_SO_URI_BUSINESS_SERVICE);
		return thamSo.getGiaTri();
	}

	@Override
	public String layUriPortalService() {
		ThamSo thamSo = repository.findOne(Utilities.ID_THAM_SO_URI_PORTAL_SERVICE);
		return thamSo.getGiaTri();
	}

	@Override
	public String layUsername() {
		ThamSo thamSo = repository.findOne(Utilities.ID_THAM_SO_USERNAME);
		return thamSo.getGiaTri();
	}

	@Override
	public String layPassword() {
		ThamSo thamSo = repository.findOne(Utilities.ID_THAM_SO_PASSWORD);
		return thamSo.getGiaTri();
	}

	@Override
	public String layAccount() {
		ThamSo thamSo = repository.findOne(Utilities.ID_THAM_SO_ACCOUNT);
		return thamSo.getGiaTri();
	}

	@Override
	public String layACpass() {
		ThamSo thamSo = repository.findOne(Utilities.ID_THAM_SO_ACPASS);
		return thamSo.getGiaTri();
	}

	@Override
	public String layPattern() {
		ThamSo thamSo = repository.findOne(Utilities.ID_THAM_SO_PATTERN);
		return thamSo.getGiaTri();
	}

	@Override
	public String laySerial() {
		ThamSo thamSo = repository.findOne(Utilities.ID_THAM_SO_SERIAL);
		return thamSo.getGiaTri();
	}

	@Override
	public boolean laCongTySuDungHDDT() {
		ThamSo thamSo = repository.findOne(Utilities.ID_THAM_SO_SU_DUNG_HDDT);
		if (thamSo == null) {
			return false;
		}
		return Boolean.parseBoolean(thamSo.getGiaTri());
	}

	@Override
	public String layPatternLapDat() {
		ThamSo thamSo = repository.findOne(Utilities.ID_THAM_SO_PATTERN_LD);
		return thamSo.getGiaTri();
	}

	@Override
	public String laySerialLapDat() {
		ThamSo thamSo = repository.findOne(Utilities.ID_THAM_SO_SERIAL_LD);
		return thamSo.getGiaTri();
	}

}
