package vn.vnpt.camau.capnuoc.qlkh.web.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.GiaNuocDto;

public interface GiaNuocService {

	Page<GiaNuocDto> layDsGiaNuoc(long idHuyen, String thongTinCanTim, Pageable pageable);
	
	GiaNuocDto layGiaNuoc(long id);
	
	GiaNuocDto luuGiaNuoc(GiaNuocDto dto) throws Exception;
	
	void xoaGiaNuoc(long id) throws Exception;

}
