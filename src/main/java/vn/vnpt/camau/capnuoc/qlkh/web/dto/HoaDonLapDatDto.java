package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Data;

@Data
public class HoaDonLapDatDto {
	private Long idHoaDon;

	@Mapping("thongTinKhachHang.idThongTinKhachHang")
	private Long idThongTinKhachHang;

	@Mapping("thongTinKhachHang.tenKhachHang")
	private String tenKhachHang;
	@Mapping("thongTinKhachHang.maKhachHang")
	private String maKhachHang;
	@Mapping("thongTinKhachHang.diaChiSuDung")
	private String diaChiSuDung;
	@Mapping("thongTinKhachHang.maSoThue")
	private String maSoThue;
	@Mapping("thongTinKhachHang.soCmnd")
	private String soCmnd;
	@Mapping("thongTinKhachHang.soTaiKhoan")
	private String soTaiKhoan;
	@Mapping("thongTinKhachHang.soDienThoai")
	private String soDienThoai;

	// private NguoiDung nguoiDung;

	@Mapping("trangThaiHoaDon.idTrangThaiHoaDon")
	private Long idTrangThaiHoaDon;
	@Mapping("trangThaiHoaDon.tenTrangThaiHoaDon")
	private String tenTrangThaiHoaDon;

	@Mapping("thang.idThang")
	private Long idThang;
	@Mapping("thang.tenThang")
	private String tenThang;

	private Long tienDichVu;
	private Long tienThue;
	private Long tongTien;
	private String bangChu;
	private Date ngayGioCapNhat;
	private String tenHangHoa;
}