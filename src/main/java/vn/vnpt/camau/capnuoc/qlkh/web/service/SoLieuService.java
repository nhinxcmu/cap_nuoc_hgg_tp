package vn.vnpt.camau.capnuoc.qlkh.web.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoLieuChuyenDuLieuDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoLieuDuyetChiSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoLieuPhatHanhHoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoLieuPhatHanhHoaDonLapDatDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoLieuThanhToanChuyenKhoanDto;

public interface SoLieuService {

	SoLieuChuyenDuLieuDto thongKeChuyenDuLieu();

	Page<SoLieuDuyetChiSoDto> thongKeDuyetChiSo(long idNhanVienGhiThu, Pageable pageable) throws Exception;

	SoLieuPhatHanhHoaDonDto thongKePhatHanhHoaDon() throws Exception;

	SoLieuPhatHanhHoaDonLapDatDto thongKePhatHanhHoaDonLapDat() throws Exception;

	SoLieuThanhToanChuyenKhoanDto thongKeThanhToanChuyenKhoan() throws Exception;

	SoLieuThanhToanChuyenKhoanDto thongKeThanhToanGiaoDich() throws Exception;
}
