package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDonLapDat;

public interface HoaDonLapDatRepository extends CrudRepository<HoaDonLapDat, Long> {

        @EntityGraph(attributePaths = { "thongTinKhachHang", "trangThaiHoaDon", "thang" })
        @Query("select hd from HoaDonLapDat hd "
                        + " where (hd.thongTinKhachHang.tenKhachHang like CONCAT('%',?1,'%') or hd.thongTinKhachHang.maKhachHang like CONCAT('%',?1,'%'))"
                        + " and hd.thang.idThang=?2"
                        + " and (hd.thongTinKhachHang.soGhi.duong.khuVuc.idKhuVuc=?3 or ?3=-1) "
                        + " and (hd.thongTinKhachHang.soGhi.duong.idDuong=?4 or ?4=-1) "
                        + " and (hd.thongTinKhachHang.soGhi.idSoGhi=?5 or ?5=-1) "
                        +" and (hd.trangThaiHoaDon.idTrangThaiHoaDon = ?6 or ?6 =-1)" + " order by hd.idHoaDon desc ")
        Page<HoaDonLapDat> layDs(String searchString, Long idThang, Long idKhuVuc, Long idDuong, Long idSoGhi, Long idTrangThaiHoaDon,
                        Pageable page);

        List<HoaDonLapDat> findByIdHoaDonIn(List<Long> dsIdHoaDon);

        List<HoaDonLapDat> findByThang_IdThangAndThongTinKhachHang_SoGhi_Duong_KhuVuc_IdKhuVucAndHuyIsFalseAndTrangThaiHoaDon_IdTrangThaiHoaDonAndTienDichVuGreaterThan(long idThangHoaDon, long idKhuVuc, long idTrangThaiHoaDon, long tienDichVu);

        List<HoaDonLapDat> findByThang_IdThangAndThongTinKhachHang_SoGhi_Duong_KhuVuc_IdKhuVucAndHuyIsFalseAndTrangThaiHoaDon_IdTrangThaiHoaDon(long idThangHoaDon, long idKhuVuc, long idTrangThaiHoaDon);
        // findByThang_IdThangAndKhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndHuyIsFalseAndTrangThaiHoaDon_IdTrangThaiHoaDonAndTienDichVuGreaterThan(long, long, long, long, long) is undefined for the type HoaDonLapDatRepositoryJava(67108964)

        HoaDonLapDat findByThang_IdThangAndThongTinKhachHang_IdThongTinKhachHang(long idThang, long idThongTinKhachHang);

}