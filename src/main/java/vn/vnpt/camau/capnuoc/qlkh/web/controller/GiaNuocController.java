package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.beans.PropertyEditorSupport;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ChiTietGiaNuocDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DuongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.GiaNuocDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DoiTuongService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DuongService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.GiaNuocService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HuyenService;


@Controller
@RequestMapping("/gia-nuoc")
public class GiaNuocController {
	private ModelAttr modelAttr = new ModelAttr("Danh mục giá nước", "gianuoc", new String[]{"plugins/input-mask/jquery.inputmask.js","plugins/input-mask/jquery.inputmask.date.extensions.js","bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js","jdgrid/jdgrid-v3.js","jdpage/jdpage.js","bower_components/select2/dist/js/select2.min.js","bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect.js","js/gianuoc.js"},
			new String[]{"bower_components/bootstrap-datepicker/css/bootstrap-datepicker.min.css","bower_components/select2/dist/css/select2.min.css","bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css","jdgrid/jdgrid.css"});
	@Autowired
	private GiaNuocService giaNuocServ;
	@Autowired
	private HuyenService huyenServ;
	@Autowired
	private DoiTuongService doiTuongServ;
	@Autowired
	private DuongService duongServ;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
	
	@InitBinder("giaNuocDto")
	public void _initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(List.class, "duongDtos", new CustomGiaNuocEditor());
	}
	
	private class CustomGiaNuocEditor extends PropertyEditorSupport {

		private List<DuongDto> list;

		@Override
		public void setValue(Object value) {
			list=new ArrayList<>();
			for(String s:(String[])value){
				DuongDto d=new DuongDto();
				d.setIdDuong(Long.valueOf(s));
				list.add(d);
			}
		}

		@Override
		public Object getValue() {
			return list;
		}
	}
	
	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
	    return "layout";
	}
	
	@GetMapping("/init-form")
	public @ResponseBody String initForm(){
		ObjectMapper mapper =new ObjectMapper();
		try {
			return "{\"huyens\":"+mapper.writeValueAsString(huyenServ.layDsHuyen())+",\"doiTuongs\":"+mapper.writeValueAsString(doiTuongServ.layDsDoiTuong())+"}";
		} catch (JsonProcessingException e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@GetMapping("/lay-ds")
	public @ResponseBody Page<GiaNuocDto> layDanhSach(long idHuyen, String thongTinCanTim,Pageable page){
		return giaNuocServ.layDsGiaNuoc(idHuyen, thongTinCanTim, page);
	}
	
	@GetMapping("/lay-ct")
	public @ResponseBody GiaNuocDto layChiTiet(long id){
		return giaNuocServ.layGiaNuoc(id);
	}
	
	@PostMapping("/luu")
	public @ResponseBody String luu(GiaNuocDto dto,String ct){
		ObjectMapper mapper = new ObjectMapper();
		List<ChiTietGiaNuocDto> chiTietGiaNuocDtos = new ArrayList<>();
		try {
			chiTietGiaNuocDtos=mapper.readValue(ct, new TypeReference<List<ChiTietGiaNuocDto>>(){});
			dto.setChiTietGiaNuocDtos(chiTietGiaNuocDtos);
			giaNuocServ.luuGiaNuoc(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/xoa")
	public @ResponseBody String xoa(long id){
		try {
			giaNuocServ.xoaGiaNuoc(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/lay-ds-phuong")
	public @ResponseBody List<DuongDto> khoa(long id){
		return duongServ.layDsDuongTheoHuyen(id);
	}
}
