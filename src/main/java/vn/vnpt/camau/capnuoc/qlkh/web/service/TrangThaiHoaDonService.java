package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.TrangThaiHoaDonDto;

public interface TrangThaiHoaDonService {

	List<TrangThaiHoaDonDto> layDsTrangThaiHoaDon();
}
