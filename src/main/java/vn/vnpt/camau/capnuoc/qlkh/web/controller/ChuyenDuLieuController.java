package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoLieuChuyenDuLieuDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ChiSoService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.SoLieuService;

@Controller
@RequestMapping("/chuyen-du-lieu")
public class ChuyenDuLieuController {
	private ModelAttr modelAttr = new ModelAttr("Chuyển dữ liệu chỉ số nước", "chuyendulieu",
			new String[] { "js/chuyendulieu.js" }, new String[] {});
	@Autowired
	private SoLieuService soLieuServ;
	@Autowired
	private ChiSoService chiSoServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/lay-so-luong")
	public @ResponseBody SoLieuChuyenDuLieuDto layThongKeChuyenDuLieu() {
		return soLieuServ.thongKeChuyenDuLieu();
	}

	@PostMapping("/luu")
	public @ResponseBody String chuyenDuLieuChiSo(boolean chuyenTatCa) {
		try {
			chiSoServ.chuyenDuLieuChiSo(chuyenTatCa);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
