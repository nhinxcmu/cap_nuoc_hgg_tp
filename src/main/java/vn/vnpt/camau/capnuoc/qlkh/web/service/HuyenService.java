package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.HuyenDto;

public interface HuyenService {

	Page<HuyenDto> layDsHuyen(Pageable pageable);
	
	List<HuyenDto> layDsHuyen();
	
	HuyenDto layHuyen(long id);
	
	HuyenDto luuHuyen(HuyenDto dto) throws Exception;
	
	void xoaHuyen(long id) throws Exception;

}
