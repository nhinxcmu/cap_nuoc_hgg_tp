package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import lombok.Getter;
import lombok.Setter;

public class SoLieuChuyenDuLieuDto {

	@Getter
	@Setter
	private int tongKhachHang;
	@Getter
	@Setter
	private int tongDaChuyen;
	@Getter
	@Setter
	private int tongChuaChuyen;
	
}
