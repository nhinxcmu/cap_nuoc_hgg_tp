package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.VaiTro;

public interface VaiTroRepository extends CrudRepository<VaiTro, Long> {

	List<VaiTro> findByOrderByTenVaiTro();
	
	List<VaiTro> findByIdVaiTroInOrderByTenVaiTro(List<Long> dsIdVaiTro);
	
	Page<VaiTro> findByIdVaiTroNotInOrderByIdVaiTroDesc(List<Long> dsIdVaiTro, Pageable pageable);
	
	@EntityGraph(attributePaths = { "menus" })
	VaiTro findByIdVaiTro(Long idVaiTro);
	
}
