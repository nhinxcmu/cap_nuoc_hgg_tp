package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.PhieuBaoHong;

public interface PhieuBaoHongRepository extends CrudRepository<PhieuBaoHong, Long> {
    @EntityGraph(attributePaths = { "nguoiDung", "thang", "thongTinKhachHang.khachHang","tinhTrang" })
    @Query("select p from PhieuBaoHong p where (p.nguoiDung.idNguoiDung = ?1 or ?1=-1)"
            + " and (p.trangThai=?2 or ?2=-1)" + " and (p.ngayLap between ?3 and ?4)" + " order by p.idPhieuBaoHong")
    Page<PhieuBaoHong> layDs(Long idNguoiDung, int trangThai, Date tuNgay, Date denNgay, Pageable pageable);

    @EntityGraph(attributePaths = { "nguoiDung", "thang", "thongTinKhachHang.khachHang","tinhTrang" })
    @Query("select p from PhieuBaoHong p where (p.nguoiDung.idNguoiDung = ?1 or ?1=-1)"
            + " and (p.trangThai=?2 or ?2=-1)" + " and (p.ngayLap between ?3 and ?4)" + " order by p.idPhieuBaoHong")
    List<PhieuBaoHong> layDs(Long idNguoiDung, int trangThai, Date tuNgay, Date denNgay);

    @Query("select count(1) from PhieuBaoHong p where (p.nguoiDung.idNguoiDung = ?1 or ?1=-1)  and (p.trangThai=?2 or ?2=-1) ")
    int demPhieuTheoTrangThai(Long idNguoiDung, int trangThai);

    @EntityGraph(attributePaths = { "nguoiDung", "thang", "thongTinKhachHang.khachHang","tinhTrang" })
    PhieuBaoHong findOne(Long idPhieuBaoHong);
}
