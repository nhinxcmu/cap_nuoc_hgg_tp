package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.ChiTietHoaDonConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ChiTietHoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ChiTietHoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ChiTietHoaDonRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ChiTietHoaDonService;

@Service
public class ChiTietHoaDonServiceImp implements ChiTietHoaDonService {

	@Autowired
	private ChiTietHoaDonRepository repository;
	@Autowired
	private ChiTietHoaDonConverter converter;

	@Override
	public List<ChiTietHoaDonDto> layDsChiTietHoaDon(long idHoaDon) {
		List<ChiTietHoaDon> dsChiTietHoaDonCanLay = repository.findByHoaDon_IdHoaDonOrderByIdChiTietHoaDon(idHoaDon);
		List<ChiTietHoaDonDto> dsChiTietHoaDonDaChuyenDoi = dsChiTietHoaDonCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsChiTietHoaDonDaChuyenDoi;
	}

}
