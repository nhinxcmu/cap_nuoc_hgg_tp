package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DoiTuongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DoiTuongService;


@Controller
@RequestMapping("/doi-tuong")
public class DoiTuongController {
	private ModelAttr modelAttr = new ModelAttr("Danh mục đối tượng", "doituong", new String[]{"jdgrid/jdgrid-v3.js","jdpage/jdpage.js","js/doituong.js"},
			new String[]{"jdgrid/jdgrid.css"});
	@Autowired
	private DoiTuongService doiTuongServ;
	
	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
	    return "layout";
	}
	
	@GetMapping("/lay-ds")
	public @ResponseBody Page<DoiTuongDto> layDanhSach(Pageable page){
		return doiTuongServ.layDsDoiTuong(page);
	}
	
	@GetMapping("/lay-ct")
	public @ResponseBody DoiTuongDto layChiTiet(long id){
		return doiTuongServ.layDoiTuong(id);
	}
	
	@PostMapping("/luu")
	public @ResponseBody String luu(DoiTuongDto dto){
		try {
			doiTuongServ.luuDoiTuong(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/xoa")
	public @ResponseBody String xoa(long id){
		try {
			doiTuongServ.xoaDoiTuong(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
