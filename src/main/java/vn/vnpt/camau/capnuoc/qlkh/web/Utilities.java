package vn.vnpt.camau.capnuoc.qlkh.web;

import java.sql.SQLException;

import javax.servlet.http.HttpSession;

import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.security.core.context.SecurityContextHolder;

import vn.vnpt.camau.capnuoc.qlkh.web.config.UserDetailsImp;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;

public class Utilities {

	public final static long ID_THAM_SO_TEN_CONG_TY = 1L;
	public final static long ID_THAM_SO_DIA_CHI_CONG_TY = 2L;
	public final static long ID_THAM_SO_MA_SO_THUE = 3L;
	public final static long ID_THAM_SO_SO_DIEN_THOAI = 4L;
	public final static long ID_THAM_SO_MAT_KHAU_MAC_DINH = 7L;
	public final static long ID_THAM_SO_DON_VI_SU_DUNG = 19L;
	public final static long ID_THAM_SO_SU_DUNG_HDDT = 20L;
	public final static long ID_THAM_SO_MA_KHACH_HANG_DUY_NHAT = 22L;
	
	public final static long ID_THAM_SO_URI_PUBLISH_SERVICE = 10L;
	public final static long ID_THAM_SO_URI_BUSINESS_SERVICE = 11L;
	public final static long ID_THAM_SO_URI_PORTAL_SERVICE = 12L;
	public final static long ID_THAM_SO_USERNAME = 13L;
	public final static long ID_THAM_SO_PASSWORD = 14L;
	public final static long ID_THAM_SO_ACCOUNT = 15L;
	public final static long ID_THAM_SO_ACPASS = 16L;
	public final static long ID_THAM_SO_PATTERN = 17L;
	public final static long ID_THAM_SO_SERIAL = 18L;
	
	public final static long ID_THAM_SO_LABELID = 23L;
	public final static long ID_THAM_SO_TEMPLATEID = 24L;
	public final static long ID_THAM_SO_AGENTID = 25L;
	public final static long ID_THAM_SO_APIUSER = 26L;
	public final static long ID_THAM_SO_APIPASS = 27L;
	public final static long ID_THAM_SO_USERNAME_SMS = 28L;
	public final static long ID_THAM_SO_CONTRACTID = 29L;
	
	public final static long ID_TRANG_THAI_CHUA_GHI_CHI_SO = 3L;
	public final static long ID_TRANG_THAI_CUP_NUOC = 7L;
	public final static long ID_TRANG_THAI_BINH_THUONG = 9L;
	public final static long ID_TRANG_THAI_KHONG_XAI = 10L;
	public final static long ID_TRANG_THAI_KHOAN_TIEU_THU = 11L;
	
	public final static long ID_TRANG_THAI_CHUA_PHAT_HANH_HOA_DON = 5L;
	public final static long ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON = 6L;
	public final static long ID_TRANG_THAI_KHONG_PHAT_HANH_HOA_DON = 13L;
	
	public final static long ID_TRANG_THAI_DANG_LAM_VIEC = 1L;
	public final static long ID_TRANG_THAI_DA_KET_CHUYEN = 2L;
	
	public final static long ID_VAI_TRO_QUAN_TRI = 1L;
	public final static long ID_VAI_TRO_GHI_CHI_SO = 2L;
	public final static long ID_VAI_TRO_THU_TIEN = 3L;
	public final static long ID_VAI_TRO_GHI_THU = 4L;
	public final static long ID_VAI_TRO_CAP_NHAT_TT = 5L;
	
	public final static long ID_HINH_THUC_THANH_TOAN_TAI_GIAO_DICH = 1L;
	public final static long ID_HINH_THUC_THANH_TOAN_CHUYEN_KHOAN = 2L;
	public final static long ID_HINH_THUC_THANH_TOAN_TAI_NHA = 3L;
	
	public final static String NAMESPACE = "http://tempuri.org/";
	
	public final static long ID_KIEU_CAP_NHAT_KHACH_HANG = 1L;
	public final static long ID_KIEU_PHAT_HANH_HOA_DON = 2L;
	public final static long ID_KIEU_HUY_PHAT_HANH_HOA_DON = 3L;
	public final static long ID_KIEU_THANH_TOAN_HOA_DON = 4L;
	public final static long ID_KIEU_HUY_THANH_TOAN_HOA_DON = 5L;
	
	public final static long ID_MENU_THANH_TOAN_GIAO_DICH = 24L;

	public final static long ID_THAM_SO_PATTERN_LD = 35L;
	public final static long ID_THAM_SO_SERIAL_LD = 36L;
	public final static long ID_THAM_SO_TEN_HANG_HOA_DICH_VU_LD = 37L;
	public final static long ID_THAM_SO_DON_VI_TINH_LD = 38L;
	public final static long ID_THAM_SO_THUE_SUAT_LD = 39L;

	public static Long layIdNguoiDung() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetailsImp) {
			return ((UserDetailsImp) principal).getId();
		}
		return 0L;
	}
	public static NguoiDung layNguoiDung() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetailsImp) {
			return ((UserDetailsImp) principal).getNguoiDung();
		}
		return null;
	}

	public static String layHoTenNguoiDung() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetailsImp) {
			return ((UserDetailsImp) principal).getFullname();
		}
		return "";
	}

	public static ThangDto layThangLamViec(HttpSession httpSession) {
		ThangDto thangLamViecCanLay = (ThangDto) httpSession.getAttribute("thang");
		return thangLamViecCanLay;
	}
	
	public static KhuVucDto layKhuVucLamViec(HttpSession httpSession) {
		KhuVucDto khuVucLamViecCanLay = (KhuVucDto) httpSession.getAttribute("khuVuc");
		return khuVucLamViecCanLay;
	}
	
	public static Long layIdDonViLamViec(HttpSession httpSession) {
		Long idDonViLamViecCanLay = (Long) httpSession.getAttribute("idDonVi");
		return idDonViLamViecCanLay;
	}
	
	public static String getMessage_SQLException(JpaSystemException jpaSystemException, String message) {
		Object object = jpaSystemException.getCause().getCause();
		
		if (object instanceof SQLException) {
			SQLException sQLException = (SQLException) object;

			if (sQLException.getSQLState().equals("45000")) {
				return sQLException.getMessage();
			}
		}
		
		return message;
	}
	
	public static long layThangTruoc(long idThang) {
		String thangTruoc;
		String namThang = ((Long) idThang).toString();
		String nam = namThang.substring(0, 4);
		String thang = namThang.substring(4);
		if (thang.equals("01")) {
			nam = ((Long) (Long.parseLong(nam) - 1)).toString();
			thangTruoc = nam + "12";
		} else {
			thang = ((Long) (Long.parseLong(thang) - 1)).toString();
			if (thang.length() == 1) {
				thang = "0" + thang;
			}
			thangTruoc = nam + thang;
		}
		return Long.parseLong(thangTruoc);
	}

	public static String layThangHienThi(long idThang) {
		String thangHienThi;
		String namThang = ((Long) idThang).toString();
		String nam = namThang.substring(0, 4);
		String thang = namThang.substring(4);
		thangHienThi = thang + "/" + nam;
		return thangHienThi;
	}
	
	public static long layThangSau(long idThang) {
		String thangSau;
		String namThang = ((Long) idThang).toString();
		String nam = namThang.substring(0, 4);
		String thang = namThang.substring(4);
		if (thang.equals("12")) {
			nam = ((Long) (Long.parseLong(nam) + 1)).toString();
			thangSau = nam + "01";
		} else {
			thang = ((Long) (Long.parseLong(thang) + 1)).toString();
			if (thang.length() == 1) {
				thang = "0" + thang;
			}
			thangSau = nam + thang;
		}
		return Long.parseLong(thangSau);
	}
	
	public static boolean kiemTraSoNguyen(String soNguyen) {
		try {
			Integer.parseInt(soNguyen);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
