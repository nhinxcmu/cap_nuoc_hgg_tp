package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThanhToan;

public interface ThanhToanRepository extends CrudRepository<ThanhToan, Long> {

	List<ThanhToan> findByPhieuThanhToan_IdPhieuThanhToan(Long idPhieuThanhToan);

	@EntityGraph(attributePaths = { "hinhThucThanhToan", "hoaDon.thang", "nguoiDung" })
	Page<ThanhToan> findByThang_IdThangAndHoaDon_KhachHang_ThongTinKhachHangs_IdThongTinKhachHangOrderByIdThanhToanDesc(
			Long idThangThanhToan, Long idThongTinKhachHang, Pageable pageable);

	ThanhToan findByHoaDon_IdHoaDonAndHuyThanhToanFalse(Long idHoaDon);
}
