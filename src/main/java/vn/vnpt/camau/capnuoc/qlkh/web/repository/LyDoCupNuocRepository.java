package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.LyDoCupNuoc;

public interface LyDoCupNuocRepository extends CrudRepository<LyDoCupNuoc, Long> {

	void deleteByHoaDon_IdHoaDon(Long idHoaDon);
	
}
