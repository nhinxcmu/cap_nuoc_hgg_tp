package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.LichSuApiDto;

public interface LichSuApiService {

	LichSuApiDto luu(LichSuApiDto idHoaDon);

}
