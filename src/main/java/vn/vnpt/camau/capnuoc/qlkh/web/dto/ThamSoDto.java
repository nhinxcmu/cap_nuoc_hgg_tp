package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class ThamSoDto {

	@Getter
	@Setter
	@NotNull
	private Long idThamSo;
	@Getter
	@Setter
	@Mapping("nguoiDung.idNguoiDung")
	private Long idNguoiCapNhat;
	@Getter
	@Setter
	@Mapping("nguoiDung.idNguoiDung")
	private String tenNguoiCapNhat;
	@Getter
	@Setter
	@NotNull
	@Size(max = 250)
	private String tenThamSo;
	@Getter
	@Setter
	@NotNull
	@Size(max = 100)
	private String giaTri;
	@Getter
	@Setter
	@NotNull
	private boolean suDung;
	@Getter
	@Setter
	@NotNull
	@Size(max = 19)
	private Date ngayGioCapNhat;
}
