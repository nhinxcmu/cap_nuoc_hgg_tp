package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.TrangThaiChiSoConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.TrangThaiChiSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiChiSo;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.TrangThaiChiSoRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.TrangThaiChiSoService;

@Service
public class TrangThaiChiSoServiceImp implements TrangThaiChiSoService {

	@Autowired
	private TrangThaiChiSoRepository repository;
	@Autowired
	private TrangThaiChiSoConverter converter;
	
	@Override
	public List<TrangThaiChiSoDto> layDsTrangThaiChiSo() {
		List<TrangThaiChiSo> dsTrangThaiChiSoCanLay = repository.findAll();
		List<TrangThaiChiSoDto> dsTrangThaiChiSoDaChuyenDoi = dsTrangThaiChiSoCanLay.stream().map(converter::convertToDto).collect(Collectors.toList());
		return dsTrangThaiChiSoDaChuyenDoi;
	}

}
