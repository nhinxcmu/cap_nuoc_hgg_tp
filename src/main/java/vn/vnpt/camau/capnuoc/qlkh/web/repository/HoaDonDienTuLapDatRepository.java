package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDonDienTuLapDat;

public interface HoaDonDienTuLapDatRepository extends CrudRepository<HoaDonDienTuLapDat, Long> {
	
	@EntityGraph(attributePaths = { "hoaDon" })
	List<HoaDonDienTuLapDat> findByHuyIsFalseAndPhatHanhThanhCongIsTrueAndThanhToanIsFalseAndHoaDonLapDat_IdHoaDonIn(List<Long> dsIdHoaDon);

	List<HoaDonDienTuLapDat> findByHuyIsFalseAndPhatHanhThanhCongIsTrueAndThanhToanIsTrueAndHoaDonLapDat_IdHoaDonIn(List<Long> dsIdHoaDon);

	HoaDonDienTuLapDat findByHuyIsFalseAndPhatHanhThanhCongIsTrueAndHoaDonLapDat_IdHoaDon(Long idHoaDon);

	List<HoaDonDienTuLapDat> findByHoaDonLapDat_TrangThaiHoaDon_IdTrangThaiHoaDonAndHoaDonLapDat_IdHoaDonIn(Long idTrangThai, List<Long> dsIdHoaDon);

	HoaDonDienTuLapDat findByIdHoaDonDienTu(Long idHoaDonDienTu);
	
	@EntityGraph(attributePaths = { "nguoiDung" })
	List<HoaDonDienTuLapDat> findByHoaDonLapDat_IdHoaDon(Long idHoaDon);
}
