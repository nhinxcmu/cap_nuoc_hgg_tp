package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.DongHoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHoKhachHangThang;

@Component
public class DongHoConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public DongHoDto convertToDto(DongHo entity) {
		if (entity == null) {
			return new DongHoDto();
		}
		DongHoDto dto = mapper.map(entity, DongHoDto.class);
		setIdKhachHang(entity, dto);
		return dto;
	}

	private void setIdKhachHang(DongHo entity, DongHoDto dto) {
		if (!entity.getDongHoKhachHangThangs().isEmpty()) {
			DongHoKhachHangThang dongHoKhachHangThang = entity.getDongHoKhachHangThangs().iterator().next();
			dto.setIdKhachHang(dongHoKhachHangThang.getId().getIdKhachHang());
		}
	}

}
