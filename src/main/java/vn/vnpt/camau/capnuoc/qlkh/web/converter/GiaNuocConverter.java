package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.GiaNuocDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.GiaNuoc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Huyen;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ChiTietGiaNuoc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuong;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Duong;

@Component
public class GiaNuocConverter {

	@Autowired
	private DozerBeanMapper mapper;
	@Autowired
	private DuongConverter duongConverter;
	@Autowired
	private ChiTietGiaNuocConverter chiTietGiaNuocConverter;
	
	public GiaNuocDto convertToDto(GiaNuoc entity) {
		if (entity == null) {
			return new GiaNuocDto();
		}
		GiaNuocDto dto = mapper.map(entity, GiaNuocDto.class);
		return dto;
	}
	
	public GiaNuoc convertToEntity(GiaNuocDto dto) {
		if (dto == null) {
			return new GiaNuoc();
		}
		GiaNuoc entity = mapper.map(dto, GiaNuoc.class);
		setDoiTuong(dto, entity);
		setHuyen(dto, entity);
		setNguoiDung(dto, entity);
		setDuongs(dto, entity);
		setChiTietGiaNuocs(dto, entity);
		return entity;
	}

	private void setDoiTuong(GiaNuocDto dto, GiaNuoc entity) {
		if (dto.getIdDoiTuong() != null) {
			DoiTuong doiTuong = new DoiTuong();
			doiTuong.setIdDoiTuong(dto.getIdDoiTuong());
			entity.setDoiTuong(doiTuong);
		}
	}

	private void setHuyen(GiaNuocDto dto, GiaNuoc entity) {
		if (dto.getIdHuyen() != null) {
			Huyen huyen = new Huyen();
			huyen.setIdHuyen(dto.getIdHuyen());
			entity.setHuyen(huyen);
		}
	}

	private void setNguoiDung(GiaNuocDto dto, GiaNuoc entity) {
		if (dto.getIdNguoiCapNhat() != null) {
			NguoiDung nguoiDung = new NguoiDung();
			nguoiDung.setIdNguoiDung(dto.getIdNguoiCapNhat());
			entity.setNguoiDung(nguoiDung);
		}
	}

	private void setDuongs(GiaNuocDto dto, GiaNuoc entity) {
		Set<Duong> duongs = dto.getDuongDtos().stream().map(duongConverter::convertToEntity).collect(Collectors.toSet());
		Set<GiaNuoc> giaNuocs = new HashSet<GiaNuoc>();
		giaNuocs.add(entity);
		duongs.forEach(d -> d.setGiaNuocs(giaNuocs));
		entity.setDuongs(duongs);
	}

	private void setChiTietGiaNuocs(GiaNuocDto dto, GiaNuoc entity) {
		Set<ChiTietGiaNuoc> chiTietGiaNuocs = dto.getChiTietGiaNuocDtos().stream().map(chiTietGiaNuocConverter::convertToEntity).collect(Collectors.toSet());
		chiTietGiaNuocs.forEach(c -> c.setGiaNuoc(entity));
		entity.setChiTietGiaNuocs(chiTietGiaNuocs);
	}

}
