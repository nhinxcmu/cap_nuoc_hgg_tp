package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import java.util.Iterator;
import java.util.Set;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonNganHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuong;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuongCupNuoc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHoKhachHangThang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuCupNuoc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LyDoCupNuoc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.SoGhi;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThanhToan;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThongTinKhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThanhToanRepository;

@Component
public class HoaDonConverter {

	@Autowired
	private DozerBeanMapper mapper;
	@Autowired
	private ThanhToanRepository thanhToanRepository;

	public HoaDonDto convertToDtoIncludeThongTinKhachHang(HoaDon entity) {
		if (entity == null) {
			return new HoaDonDto();
		}
		HoaDonDto dto = mapper.map(entity, HoaDonDto.class);
		Set<ThongTinKhachHang> dsThongTinKhachHang = entity.getKhachHang().getThongTinKhachHangs();
		if (dsThongTinKhachHang.size() > 0) {
			ThongTinKhachHang thongTinKhachHang = dsThongTinKhachHang.iterator().next();
			DoiTuong doiTuong = thongTinKhachHang.getDoiTuong();
			SoGhi soGhi = thongTinKhachHang.getSoGhi();
			setThongTinKhachHang(thongTinKhachHang, dto);
			setDoiTuong(doiTuong, dto);
			setSoGhi(soGhi, dto);
		}
		dto.setSoLanIn(entity.getLichSuIns().size());
		ThanhToan thanhToan = thanhToanRepository.findByHoaDon_IdHoaDonAndHuyThanhToanFalse(entity.getIdHoaDon());
		if (thanhToan != null)
			dto.setNgayThanhToan(thanhToan.getNgayGioCapNhat());
		return dto;
	}

	private void setThongTinKhachHang(ThongTinKhachHang thongTinKhachHang, HoaDonDto dto) {
		dto.setIdThongTinKhachHang(thongTinKhachHang.getIdThongTinKhachHang());
		dto.setMaKhachHang(thongTinKhachHang.getMaKhachHang());
		dto.setThuTu(thongTinKhachHang.getThuTu());
		dto.setTenKhachHang(thongTinKhachHang.getTenKhachHang());
		dto.setDiaChiSuDung(thongTinKhachHang.getDiaChiSuDung());
		dto.setThuPbvmt(thongTinKhachHang.isThuPbvmt());
		dto.setSoDienThoai(thongTinKhachHang.getSoDienThoai());
	}

	private void setThongTinKhachHang(ThongTinKhachHang thongTinKhachHang, HoaDonNganHangDto dto) {
		dto.setIdThongTinKhachHang(thongTinKhachHang.getIdThongTinKhachHang());
		dto.setMaKhachHang(thongTinKhachHang.getMaKhachHang());
		dto.setTenKhachHang(thongTinKhachHang.getTenKhachHang());
		dto.setDiaChiSuDung(thongTinKhachHang.getDiaChiSuDung());
		dto.setSoDienThoai(thongTinKhachHang.getSoDienThoai());
	}

	private void setDoiTuong(DoiTuong doiTuong, HoaDonDto dto) {
		if (doiTuong != null) {
			dto.setIdDoiTuong(doiTuong.getIdDoiTuong());
			dto.setTenDoiTuong(doiTuong.getTenDoiTuong());
			dto.setMucSuDung(doiTuong.getMucSuDung());
		}
	}

	private void setSoGhi(SoGhi soGhi, HoaDonDto dto) {
		if (soGhi != null) {
			dto.setIdSoGhi(soGhi.getIdSoGhi());
			dto.setTenSoGhi(soGhi.getTenSoGhi());
		}
	}

	public HoaDonDto convertToDtoIncludeLyDoCupNuoc(HoaDon entity) {
		HoaDonDto dto = convertToDtoIncludeThongTinKhachHang(entity);
		Set<LyDoCupNuoc> dsLyDoCupNuoc = entity.getLyDoCupNuocs();
		if (dsLyDoCupNuoc.size() > 0) {
			LyDoCupNuoc lyDoCupNuoc = dsLyDoCupNuoc.iterator().next();
			dto.setLyDoCupNuoc(lyDoCupNuoc.getLyDo());
			dto.setIdDoiTuongCupNuoc(lyDoCupNuoc.getDoiTuongCupNuoc().getIdDoiTuongCupNuoc());
			dto.setTenDoiTuongCupNuoc(lyDoCupNuoc.getDoiTuongCupNuoc().getTenDoiTuongCupNuoc());
		}
		return dto;
	}

	public HoaDonDto convertToDtoIncludeLichSuCupNuoc(HoaDon entity) {
		HoaDonDto dto = convertToDtoIncludeThongTinKhachHang(entity);
		Set<LichSuCupNuoc> dsLsCupNuoc = entity.getLichSuCupNuocs();
		if (dsLsCupNuoc.size() > 0) {
			LichSuCupNuoc logCupNuoc = dsLsCupNuoc.iterator().next();
			dto.setIdHoaDon(logCupNuoc.getHoaDon().getIdHoaDon());
		}
		return dto;
	}

	public HoaDonDto convertToDto(HoaDon entity) {
		if (entity == null) {
			return new HoaDonDto();
		}
		HoaDonDto dto = mapper.map(entity, HoaDonDto.class);
		return dto;
	}

	public HoaDonNganHangDto convertToNganHangDto(HoaDon entity) {
		if (entity == null) {
			return new HoaDonNganHangDto();
		}
		HoaDonNganHangDto dto = mapper.map(entity, HoaDonNganHangDto.class);
		return dto;
	}

	public HoaDonNganHangDto convertToNganHangDtoIncludeThongTinKhachHang(HoaDon entity) {
		if (entity == null) {
			return new HoaDonNganHangDto();
		}
		HoaDonNganHangDto dto = mapper.map(entity, HoaDonNganHangDto.class);
		Set<ThongTinKhachHang> dsThongTinKhachHang = entity.getKhachHang().getThongTinKhachHangs();
		if (dsThongTinKhachHang.size() > 0) {
			Iterator<ThongTinKhachHang> it = dsThongTinKhachHang.iterator();
			ThongTinKhachHang thongTinKhachHang = it.next();
			while (dto.getIdThang() != thongTinKhachHang.getThang().getIdThang() && it.hasNext()) {
				thongTinKhachHang = it.next();
			}
			setThongTinKhachHang(thongTinKhachHang, dto);
			setMaDongHo(thongTinKhachHang, dto);
		}
		ThanhToan thanhToan = thanhToanRepository.findByHoaDon_IdHoaDonAndHuyThanhToanFalse(entity.getIdHoaDon());
		if (thanhToan != null)
			dto.setNgayThanhToan(thanhToan.getNgayGioCapNhat());
		
		return dto;
	}
	private void setMaDongHo(ThongTinKhachHang entity, HoaDonNganHangDto dto) {
		if (!entity.getKhachHang().getDongHoKhachHangThangs().isEmpty()) {
			Set<DongHoKhachHangThang> dsDongHoKhachHangThang = entity.getKhachHang().getDongHoKhachHangThangs();
			Iterator<DongHoKhachHangThang> it = dsDongHoKhachHangThang.iterator();
			DongHoKhachHangThang dongHoKhachHangThang = it.next();
			while (dto.getIdThang() != dongHoKhachHangThang.getThang().getIdThang() && it.hasNext()) {
				dongHoKhachHangThang = it.next();
			}
			DongHo dongHo = dongHoKhachHangThang.getDongHo();
			dto.setMaDongHo(dongHo.getMaDongHo());
			dto.setIdDongHo(dongHo.getIdDongHo());
			dto.setSoSeri(dongHo.getSoSeri());
		}
	}

	public Long convertToIdHoaDon(HoaDon entity) {
		return entity.getIdHoaDon();
	}

}
