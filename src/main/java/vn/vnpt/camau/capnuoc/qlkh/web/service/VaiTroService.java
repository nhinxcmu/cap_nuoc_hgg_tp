package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.CayDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.VaiTroDto;

public interface VaiTroService {

	List<VaiTroDto> layDsVaiTro();
	
	List<VaiTroDto> layDsVaiTroGhiThu();

	List<Long> layDsIdVaiTroGhiThu();
	
	Page<VaiTroDto> layDsVaiTro(Pageable pageable);
	
	VaiTroDto layVaiTro(long id);
	
	List<CayDto> layDsCayMenu();
	
	VaiTroDto luuVaiTro(VaiTroDto dto) throws Exception;
	
	void xoaVaiTro(long id) throws Exception;
}
