package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuongBaoCao;

public interface DoiTuongBaoCaoRepository extends CrudRepository<DoiTuongBaoCao, Long> {
	
	Page<DoiTuongBaoCao> findByOrderByIdDoiTuongBaoCaoDesc(Pageable pageable);

}
