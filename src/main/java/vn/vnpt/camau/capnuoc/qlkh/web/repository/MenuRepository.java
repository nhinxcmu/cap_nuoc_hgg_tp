package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.repository.Repository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.Menu;

public interface MenuRepository extends Repository<Menu, Long> {
	
	Set<Menu> findByVaiTros_NguoiDungs_IdNguoiDungAndMenu_IdMenuOrderByOrderMenu(Long idNguoiDung, Long idMenu);
	
	List<Menu> findByMenu_IdMenuIsNullOrderByOrderMenu();
	
	List<Menu> findByMenu_IdMenuOrderByOrderMenu(Long idMenu);

	List<Menu> findByMenu_IdMenuIsNotNullOrderByOrderMenu();

}
