package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.KeHoach;

public interface KeHoachRepository extends CrudRepository<KeHoach, Long> {

	@EntityGraph(attributePaths = { "duong", "thang" })
	KeHoach findByIdKeHoach(Long idKeHoach);

	@EntityGraph(attributePaths = { "duong", "thang" })
	KeHoach findByDuong_IdDuongAndThang_IdThang(Long idDuong, Long idThang);

	@EntityGraph(attributePaths = { "duong", "thang" })
	@Query("select kh from KeHoach kh where kh.duong.khuVuc.idKhuVuc = ?1 and substring(kh.thang.idThang, 1, 4) = ?2 order by kh.thang.idThang desc, kh.duong.tenDuong asc")
	List<KeHoach> layDanhSach(Long idKhuVuc, String nam);
	
	@EntityGraph(attributePaths = { "duong", "thang" })
	@Query("select kh from KeHoach kh where kh.duong.khuVuc.idKhuVuc = ?1 and kh.thang.idThang = ?2 order by kh.thang.idThang desc, kh.duong.tenDuong asc")
	List<KeHoach> layDanhSach(Long idKhuVuc, Long idThang);
}
