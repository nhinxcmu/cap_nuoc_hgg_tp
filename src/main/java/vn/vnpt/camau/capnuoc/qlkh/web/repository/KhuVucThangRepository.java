package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhuVucThang;

public interface KhuVucThangRepository extends CrudRepository<KhuVucThang, Long> {

	KhuVucThang findTopByKhuVuc_IdKhuVucOrderByThang_IdThangDesc(Long idKhuVuc);
	
	void deleteByKhuVuc_IdKhuVuc(Long idKhuVuc);

	@EntityGraph(attributePaths = { "thang", "trangThaiThang", "nguoiDung" })
	Page<KhuVucThang> findByKhuVuc_IdKhuVucOrderByThang_IdThangDesc(Long idKhuVuc, Pageable pageable);
	
	@EntityGraph(attributePaths = { "khuVuc" })
	KhuVucThang findByThang_IdThangAndKhuVuc_IdKhuVuc(Long idThang, Long idKhuVuc);
	
	@Procedure(procedureName = "ketChuyenDuLieu")
	void ketChuyenDuLieu(Long idThang, Long idKhuVuc, Long idNguoiDung);
}
