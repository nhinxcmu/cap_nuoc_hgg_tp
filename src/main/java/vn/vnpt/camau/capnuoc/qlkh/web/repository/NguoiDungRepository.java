package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;

public interface NguoiDungRepository extends CrudRepository<NguoiDung, Long> {

	NguoiDung findByTenNguoiDung(String tenNguoiDung);

	@EntityGraph(attributePaths = { "donVi", "vaiTro" })
	NguoiDung findByIdNguoiDung(Long idNguoiDung);

	@EntityGraph(attributePaths = { "donVi", "vaiTro" })
	Page<NguoiDung> findByOrderByIdNguoiDungDesc(Pageable pageable);

	@EntityGraph(attributePaths = { "donVi", "vaiTro" })
	Page<NguoiDung> findByDonVi_IdDonViOrderByIdNguoiDungDesc(Long idDonVi, Pageable pageable);

	@EntityGraph(attributePaths = { "donVi", "vaiTro" })
	Page<NguoiDung> findByTenNguoiDungContainsOrHoTenContainsOrderByIdNguoiDungDesc(String tenNguoiDung, String hoTen,
			Pageable pageable);

	@Query("select nd from NguoiDung nd " 
			+ "where donVi.idDonVi = ?1 and (tenNguoiDung like %?2% or hoTen like %?3%) "
			+ "order by idNguoiDung desc")
	@EntityGraph(attributePaths = { "donVi", "vaiTro" })
	Page<NguoiDung> findByDonVi_IdDonViAndTenNguoiDungContainsOrHoTenContainsOrderByIdNguoiDungDesc(Long idDonVi,
			String tenNguoiDung, String hoTen, Pageable pageable);

	List<NguoiDung> findByDonVi_XiNghiepIsTrueOrderByHoTen();

	List<NguoiDung> findByDonVi_IdDonViAndVaiTro_IdVaiTroOrderByHoTen(Long idDonVi, Long idVaiTro);

	List<NguoiDung> findByDonVi_IdDonViAndVaiTro_IdVaiTroInAndLichGhiThusForIdNguoiDuocPhanCong_Thangs_IdThangOrderByHoTen(
			Long idDonVi, List<Long> idVaiTro, Long idThang);

	@Query("select nd from NguoiDung nd join nd.vaiTro vt left join vt.menus m "
			+ "where nd.tenNguoiDung = ?1 and ((m.idMenu != null and m.menu.idMenu != null) or m.idMenu = null)")
	@EntityGraph(attributePaths = { "vaiTro.menus" })
	NguoiDung findByTenNguoiDungAndVaiTro_Menus_Menu_IdMenuIsNotNull(String tenNguoiDung);

	@Query("select nd from NguoiDung nd join nd.vaiTro vt left join vt.menus m "
			+ "where nd.idNguoiDung = ?1 and ((m.idMenu != null and m.menu.idMenu != null) or m.idMenu = null)")
	@EntityGraph(attributePaths = { "vaiTro.menus" })
	NguoiDung findByIdNguoiDungAndVaiTro_Menus_Menu_IdMenuIsNotNull(Long idNguoiDung);

	List<NguoiDung> findByDonVi_IdDonViAndVaiTro_Menus_IdMenuOrderByHoTen(Long idDonVi, Long idMenu);

}
