package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.CayDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.LichGhiThuDto;

public interface LichGhiThuService {

	Page<LichGhiThuDto> layDsLichGhiThu(long idVaiTro, String thongTinCanTim, Pageable pageable);
	
	LichGhiThuDto layLichGhiThu(long id);
	
	List<CayDto> layDsCaySoGhi();
	
	LichGhiThuDto luuLichGhiThu(LichGhiThuDto dto) throws Exception;
	
	void xoaLichGhiThu(long id) throws Exception;

	Page<LichGhiThuDto> layDsLichGhiThuTheoMenuThanhToanGiaoDich(String thongTinCanTim, Pageable pageable);
}
