package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import vn.vnpt.camau.capnuoc.qlkh.web.ResponseUtil;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.TinNhanConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SmsBrandnameDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.TinNhanDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThamSo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TinNhan;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HoaDonRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThamSoRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.TinNhanRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.TinNhanService;

@Service
public class TinNhanServiceImp implements TinNhanService {

	@Autowired
	private TinNhanRepository repository;
	@Autowired
	private TinNhanConverter converter;
	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	private ThamSoRepository thamSoRepository;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private HoaDonRepository hoaDonRepository;
	
	@Override
	public List<TinNhanDto> layDsTinNhan(long idHoaDon) {
		List<TinNhan> dsTinNhanCanLay = repository.findByHoaDon_IdHoaDonOrderByIdTinNhanDesc(idHoaDon);
		List<TinNhanDto> dsTinNhanDaChuyenDoi = dsTinNhanCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsTinNhanDaChuyenDoi;
	}

	@Override
	@Transactional
	public int themTinNhan(List<Long> dsIdHoaDon) throws Exception {
		// gọi store lấy ds số điện thoại và params
		List<SmsBrandnameDto> dsSmsBrandnameDto = layDsSmsBrandnameDto(dsIdHoaDon);
		// gọi web service nhắn tin
		int soLuongTinNhanThanhCong = goiWebServiceNhanTin(dsSmsBrandnameDto);
		return soLuongTinNhanThanhCong;
	}

	private List<SmsBrandnameDto> layDsSmsBrandnameDto(List<Long> dsIdHoaDon) {
		// TODO Auto-generated method stub
		String stringDsIdHoaDon = dsIdHoaDon.stream().map(Object::toString).collect(Collectors.joining(","));
		StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("layDsSmsBrandnameDto")
				.registerStoredProcedureParameter("dsIdHoaDon", String.class, ParameterMode.IN)
				.setParameter("dsIdHoaDon", stringDsIdHoaDon);
		@SuppressWarnings("rawtypes")
		List list = storedProcedure.getResultList();
		List<SmsBrandnameDto> dsSmsBrandnameDto = thietLapDsSmsBrandnameDto(list);
		return dsSmsBrandnameDto;
	}

	private List<SmsBrandnameDto> thietLapDsSmsBrandnameDto(@SuppressWarnings("rawtypes") List list) {
		List<SmsBrandnameDto> dsSmsBrandnameDto = new ArrayList<SmsBrandnameDto>();
		for (Object object : list) {
			SmsBrandnameDto smsBrandnameDto = new SmsBrandnameDto();
			Object[] rs = (Object[]) object;
			smsBrandnameDto.setIdHoaDon(Long.parseLong(rs[0].toString()));
			smsBrandnameDto.setSoDienThoai(rs[1].toString());
			smsBrandnameDto.setParams(rs[2].toString());
			dsSmsBrandnameDto.add(smsBrandnameDto);
		}
		return dsSmsBrandnameDto;
	}

	private int goiWebServiceNhanTin(List<SmsBrandnameDto> dsSmsBrandnameDto) throws Exception {
		// TODO Auto-generated method stub
		int soLuongTinNhanThanhCong = 0;
		List<TinNhan> dsTinNhan = new ArrayList<TinNhan>();
		List<HoaDon> dsHoaDon = new ArrayList<HoaDon>();
		Thang thangLamViec = layThangLamViec();
		// lấy tham số nhắn tin
		String REQID = "1";
		String LABELID = layLABELID();
		String TEMPLATEID = layTEMPLATEID();
		String ISTELCOSUB = "0";
		String CONTRACTTYPEID = "1";
		String SCHEDULETIME = "";
		String AGENTID = layAGENTID();
		String APIUSER = layAPIUSER();
		String APIPASS = layAPIPASS();
		String USERNAME = layUSERNAME();
		String CONTRACTID = layCONTRACTID();
		for (SmsBrandnameDto smsBrandnameDto : dsSmsBrandnameDto) {
			String MOBILELIST = "84" + smsBrandnameDto.getSoDienThoai().replace(" ","").replace(".","").replace(",","").substring(1);
			String[] PARAMS = smsBrandnameDto.getParams().split("-");
			String request = buildSendByListReq(REQID, LABELID, TEMPLATEID, ISTELCOSUB, CONTRACTTYPEID, SCHEDULETIME,
					MOBILELIST, AGENTID, APIUSER, APIPASS, USERNAME, CONTRACTID, PARAMS);
			try {
				boolean loi = true;
				String response = ResponseUtil.getResponse(request);
				// response.replaceAll("\\<.*?>","");
				DocumentBuilder builder = DocumentBuilderFactory
		                .newInstance().newDocumentBuilder();
		        InputSource src = new InputSource();
		        src.setCharacterStream(new StringReader(response));

		        Document doc = builder.parse(src);
		        String maLoi = doc.getElementsByTagName("ERROR").item(0).getTextContent();
		        String tenLoi = doc.getElementsByTagName("ERROR_DESC").item(0).getTextContent();
				if (laNhanTinThanhCong(maLoi)) {
					soLuongTinNhanThanhCong++;
					loi = false;
				}
				TinNhan tinNhan = taoTinNhan(smsBrandnameDto, maLoi, tenLoi, thangLamViec, loi);
				dsTinNhan.add(tinNhan);
				HoaDon hoaDon = timHoaDonCanCapNhat(smsBrandnameDto.getIdHoaDon(), loi);
				dsHoaDon.add(hoaDon);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// thêm tin nhắn
		themTinNhan2(dsTinNhan);
		capNhatHoaDon(dsHoaDon);
		return soLuongTinNhanThanhCong;
	}

	private Thang layThangLamViec() {
		Thang thangLamViec = new Thang();
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		thangLamViec.setIdThang(idThangLamViec);
		return thangLamViec;
	}

	private String layLABELID() throws Exception {
		ThamSo thamSo = thamSoRepository.findOne(Utilities.ID_THAM_SO_LABELID);
		if (thamSo == null) {
			throw new Exception("Không thể lấy tham số LABELID.");
		}
		return thamSo.getGiaTri();
	}

	private String layTEMPLATEID() throws Exception {
		ThamSo thamSo = thamSoRepository.findOne(Utilities.ID_THAM_SO_TEMPLATEID);
		if (thamSo == null) {
			throw new Exception("Không thể lấy tham số TEMPLATEID.");
		}
		return thamSo.getGiaTri();
	}

	private String layAGENTID() throws Exception {
		ThamSo thamSo = thamSoRepository.findOne(Utilities.ID_THAM_SO_AGENTID);
		if (thamSo == null) {
			throw new Exception("Không thể lấy tham số AGENTID.");
		}
		return thamSo.getGiaTri();
	}

	private String layAPIUSER() throws Exception {
		ThamSo thamSo = thamSoRepository.findOne(Utilities.ID_THAM_SO_APIUSER);
		if (thamSo == null) {
			throw new Exception("Không thể lấy tham số APIUSER.");
		}
		return thamSo.getGiaTri();
	}

	private String layAPIPASS() throws Exception {
		ThamSo thamSo = thamSoRepository.findOne(Utilities.ID_THAM_SO_APIPASS);
		if (thamSo == null) {
			throw new Exception("Không thể lấy tham số APIPASS.");
		}
		return thamSo.getGiaTri();
	}

	private String layUSERNAME() throws Exception {
		ThamSo thamSo = thamSoRepository.findOne(Utilities.ID_THAM_SO_USERNAME_SMS);
		if (thamSo == null) {
			throw new Exception("Không thể lấy tham số USERNAME.");
		}
		return thamSo.getGiaTri();
	}

	private String layCONTRACTID() throws Exception {
		ThamSo thamSo = thamSoRepository.findOne(Utilities.ID_THAM_SO_CONTRACTID);
		if (thamSo == null) {
			throw new Exception("Không thể lấy tham số CONTRACTID.");
		}
		return thamSo.getGiaTri();
	}

	private String buildSendByListReq(String REQID, String LABELID, String TEMPLATEID, String ISTELCOSUB,
			String CONTRACTTYPEID, String SCHEDULETIME, String MOBILELIST, String AGENTID, String APIUSER,
			String APIPASS, String USERNAME, String CONTRACTID, String[] PARAMS) {
		StringBuilder sb = new StringBuilder();
		sb.append("<RQST name=\"send_sms_list\">");
		sb.append("<REQID>").append(REQID).append("</REQID>");
		sb.append("<LABELID>").append(LABELID).append("</LABELID>");
		sb.append("<TEMPLATEID>").append(TEMPLATEID).append("</TEMPLATEID>");
		sb.append("<ISTELCOSUB>").append(ISTELCOSUB).append("</ISTELCOSUB>");
		sb.append("<CONTRACTTYPEID>").append(CONTRACTTYPEID).append("</CONTRACTTYPEID>");
		sb.append("<SCHEDULETIME>").append(SCHEDULETIME).append("</SCHEDULETIME>");
		sb.append("<MOBILELIST>").append(MOBILELIST).append("</MOBILELIST>");
		sb.append("<AGENTID>").append(AGENTID).append("</AGENTID>");
		sb.append("<APIUSER>").append(APIUSER).append("</APIUSER>");
		sb.append("<APIPASS>").append(APIPASS).append("</APIPASS>");
		sb.append("<USERNAME>").append(USERNAME).append("</USERNAME>");
		sb.append("<CONTRACTID>").append(CONTRACTID).append("</CONTRACTID>");
		if (PARAMS != null && PARAMS.length > 0) {
			for (int j = 0; j < PARAMS.length; j++) {
				sb.append("<PARAMS>");
				sb.append("<NUM>").append(j + 1).append("</NUM><CONTENT>").append(PARAMS[j]).append("</CONTENT>");
				sb.append("</PARAMS>");
			}
		}
		sb.append("</RQST>");
		return sb.toString();
	}

	private boolean laNhanTinThanhCong(String maLoi) {
		return maLoi.equals("0");
	}

	private TinNhan taoTinNhan(SmsBrandnameDto smsBrandnameDto, String maLoi, String tenLoi, Thang thang, boolean loi) throws Exception {
		TinNhan tinNhan = new TinNhan();
		HoaDon hoaDon = new HoaDon();
		hoaDon.setIdHoaDon(smsBrandnameDto.getIdHoaDon());
		tinNhan.setHoaDon(hoaDon);
		tinNhan.setThang(thang);
		tinNhan.setSoDienThoai(smsBrandnameDto.getSoDienThoai());
		tinNhan.setNoiDung(layTEMPLATEID() + "," + smsBrandnameDto.getParams());
		tinNhan.setThoiGianGui(new Date());
		tinNhan.setMaLoi(maLoi);
		tinNhan.setTenLoi(tenLoi);
		tinNhan.setLoi(loi);
		return tinNhan;
	}

	private HoaDon timHoaDonCanCapNhat(Long idHoaDon, boolean loi) {
		HoaDon hoaDon = hoaDonRepository.findOne(idHoaDon);
		int nhanTin = (loi ? 2 : 1);
		hoaDon.setNhanTin(nhanTin);
		return hoaDon;
	}

	private void themTinNhan2(List<TinNhan> dsTinNhan) throws Exception {
		try {
			repository.save(dsTinNhan);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể thêm danh sách tin nhắn.");
		}
	}

	private void capNhatHoaDon(List<HoaDon> dsHoaDon) throws Exception {
		try {
			hoaDonRepository.save(dsHoaDon);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể cập nhật danh sách hóa đơn.");
		}
	}

}
