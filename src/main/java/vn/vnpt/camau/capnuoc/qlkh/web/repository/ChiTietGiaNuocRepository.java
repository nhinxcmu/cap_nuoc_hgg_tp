package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ChiTietGiaNuoc;

public interface ChiTietGiaNuocRepository extends CrudRepository<ChiTietGiaNuoc, Long> {

	@EntityGraph(attributePaths = { "doiTuongBaoCao" })
	List<ChiTietGiaNuoc> findByGiaNuoc_IdGiaNuocOrderByThuTu(Long idGiaNuoc);
	
}
