package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import java.util.Set;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.DoiTuongCupNuocDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuongCupNuoc;

@Component
public class DoiTuongCupNuocConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public DoiTuongCupNuocDto convertToDto(DoiTuongCupNuoc entity) {
		if (entity == null) {
			return new DoiTuongCupNuocDto();
		}
		DoiTuongCupNuocDto dto = mapper.map(entity, DoiTuongCupNuocDto.class);
		return dto;
	}

}
