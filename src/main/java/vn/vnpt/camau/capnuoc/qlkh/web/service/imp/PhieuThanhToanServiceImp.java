package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.BienNhanThuTienNuocDtoConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.HoaDonConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.HoaDonDienTuConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.PhieuThanhToanConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.BienNhanThuTienNuocDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.PhieuThanhToanDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDonDienTu;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KetQuaHddt;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KieuKetQuaHddt;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.PhieuThanhToan;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThanhToan;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HoaDonDienTuRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HoaDonRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.KetQuaHddtRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.PhieuThanhToanRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThanhToanRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.PhieuThanhToanService;
import vn.vnpt.camau.capnuoc.qlkh.web.webservice.BusinessServiceClient;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.UnConfirmPaymentFkeyResponse;

@Service
public class PhieuThanhToanServiceImp implements PhieuThanhToanService {

	@Autowired
	private PhieuThanhToanRepository repository;
	@Autowired
	private PhieuThanhToanConverter converter;
	@Autowired
	private BienNhanThuTienNuocDtoConverter bienNhanThuTienNuocDtoConverter;
	@Autowired
	private ThangRepository thangRepository;
	@Autowired
	private HoaDonRepository hoaDonRepository;
	@Autowired
	private ThanhToanRepository thanhToanRepository;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private HoaDonDienTuRepository hoaDonDienTuRepository;
	@Autowired
	private HoaDonConverter hoaDonConverter;
	@Autowired
	private HoaDonDienTuConverter hoaDonDienTuConverter;
	@Autowired
	private BusinessServiceClient businessServiceClient;
	@Autowired
	private KetQuaHddtRepository ketQuaHddtRepository;
	@Autowired
	private KhuVucService khuVucService;

	@Override
	public List<PhieuThanhToanDto> layDsPhieuThanhToanTheoNguoiDung(long idNguoiDung, String thongTinCanTim)
			throws Exception {
		long idThangPhanCongGhiThuMoiNhat = layIdThangPhanCongGhiThuMoiNhat(idNguoiDung);
		List<PhieuThanhToan> dsPhieuThanhToanCanLay;
		if (thongTinCanTim.equals("")) {
			dsPhieuThanhToanCanLay = repository.layDsPhieuThanhToanTheoNguoiDung(idNguoiDung,
					idThangPhanCongGhiThuMoiNhat);
		} else {
			dsPhieuThanhToanCanLay = repository.layDsPhieuThanhToanTheoNguoiDungVaThongTinCanTim(idNguoiDung,
					idThangPhanCongGhiThuMoiNhat, thongTinCanTim);
		}
		List<PhieuThanhToanDto> dsPhieuThanhToanDaChuyenDoi = dsPhieuThanhToanCanLay.stream()
				.map(converter::convertToDto).collect(Collectors.toList());
		return dsPhieuThanhToanDaChuyenDoi;
	}

	// private Page<PhieuThanhToanDto> layDsPhieuThanhToan(long idNguoiDung, String
	// thongTinCanTim, long idThang,
	// Pageable pageable) {
	// Page<PhieuThanhToan> dsPhieuThanhToanCanLay;
	// if (thongTinCanTim.equals("")) {
	// dsPhieuThanhToanCanLay =
	// repository.layDsPhieuThanhToanTheoNguoiDung(idNguoiDung, idThang, pageable);
	// } else {
	// dsPhieuThanhToanCanLay =
	// repository.layDsPhieuThanhToanTheoNguoiDungVaThongTinCanTim(idNguoiDung,
	// idThang,
	// thongTinCanTim, pageable);
	// }
	// Page<PhieuThanhToanDto> dsPhieuThanhToanDaChuyenDoi =
	// dsPhieuThanhToanCanLay.map(converter::convertToDto);
	// return dsPhieuThanhToanDaChuyenDoi;
	// }

	private long layIdThangPhanCongGhiThuMoiNhat(long idNguoiDung) throws Exception {
		Thang thangPhanCongGhiThuMoiNhat = thangRepository
				.findTopByLichGhiThuThangs_NguoiDuocPhanCong_IdNguoiDungOrderByIdThangDesc(idNguoiDung);
		if (thangPhanCongGhiThuMoiNhat == null) {
			throw new Exception("Không thể lấy tháng thanh toán hóa đơn.");
		}
		return thangPhanCongGhiThuMoiNhat.getIdThang();
	}

	@Override
	public BienNhanThuTienNuocDto layBienNhanThuTienNuocDto(long idPhieuThanhToan) {
		PhieuThanhToan phieuThanhToanCanLay = repository.findByIdPhieuThanhToan(idPhieuThanhToan);
		BienNhanThuTienNuocDto bienNhanThuTienNuocDtoDaChuyenDoi = bienNhanThuTienNuocDtoConverter
				.convertToDto(phieuThanhToanCanLay);
		return bienNhanThuTienNuocDtoDaChuyenDoi;
	}

	@Override
	@Transactional
	public void huyPhieuThanhToan(long id, long idNguoiDung) throws Exception {
		huyPhieuThanhToan2(id, idNguoiDung);
	}

	private void huyPhieuThanhToan2(long id, long idNguoiDung) throws Exception {
		// tìm phiếu thanh toán
		NguoiDung nguoiCapNhat = taoNguoiCapNhat(idNguoiDung);
		PhieuThanhToan phieuThanhToanCanHuy = timPhieuThanhToan(id);
		// kiểm tra phiếu thanh toán
		kiemTraPhieuThanhToan(phieuThanhToanCanHuy, idNguoiDung);
		// cập nhật hóa đơn chưa thanh toán
		capNhatHoaDonChuaThanhToan(id, nguoiCapNhat);
		// hủy ds thanh toán
		huyDsThanhToan(id, nguoiCapNhat);
		// hủy phiếu thanh toán
		huyPhieuThanhToan(phieuThanhToanCanHuy, nguoiCapNhat);
	}

	private NguoiDung taoNguoiCapNhat(long idNguoiDung) {
		NguoiDung nguoiCapNhat = new NguoiDung();
		nguoiCapNhat.setIdNguoiDung(idNguoiDung);
		return nguoiCapNhat;
	}

	private PhieuThanhToan timPhieuThanhToan(long id) throws Exception {
		PhieuThanhToan phieuThanhToanCanTim = repository.findOne(id);
		if (phieuThanhToanCanTim == null) {
			throw new Exception("Không tìm thấy phiếu thanh toán.");
		}
		return phieuThanhToanCanTim;
	}

	private void kiemTraPhieuThanhToan(PhieuThanhToan phieuThanhToanCanHuy, long idNguoiDung) throws Exception {
		// kiểm tra phiếu thanh toán có tháng = tháng mới nhất ko
		/*
		 * bỏ qua vì trên web vẫn cho hủy tháng cũ long idThangPhanCongGhiThuMoiNhat =
		 * layIdThangPhanCongGhiThuMoiNhat(idNguoiDung); long idThangPhieuThanhToan =
		 * phieuThanhToanCanHuy.getThang().getIdThang(); if (idThangPhieuThanhToan !=
		 * idThangPhanCongGhiThuMoiNhat) { throw new
		 * Exception("Tháng hủy phiếu thanh toán không phù hợp."); }
		 */
		// kiểm tra phiếu thanh toán đã hủy chưa
		if (phieuThanhToanCanHuy.isHuyThanhToan()) {
			throw new Exception("Phiếu thanh toán đã hủy.");
		}
	}

	private void capNhatHoaDonChuaThanhToan(long id, NguoiDung nguoiCapNhat) throws Exception {
		List<HoaDon> dsHoaDonCanCapNhat = hoaDonRepository.findByThanhToans_PhieuThanhToan_IdPhieuThanhToan(id);
		huyThanhToanHoaDonDienTu(dsHoaDonCanCapNhat, nguoiCapNhat);
		capNhatHoaDonChuaThanhToan(dsHoaDonCanCapNhat, nguoiCapNhat);
	}

	private void huyThanhToanHoaDonDienTu(List<HoaDon> dsHoaDon, NguoiDung nguoiCapNhat) throws Exception {
		if (khuVucService.laKhuVucSuDungHDDT()) {
			List<HoaDonDienTu> dsHoaDonDienTu = layDsHoaDonDienTu(dsHoaDon);
			String ketQua = goiWebServiceHuyThanhToan(dsHoaDonDienTu);
			if (!ketQua.substring(0, 2).equals("OK")) {
				throw new Exception("Không thể hủy thanh toán hóa đơn điện tử.");
			}
			huyThanhToanHoaDonDienTu2(dsHoaDonDienTu, nguoiCapNhat);
		}
	}

	private List<HoaDonDienTu> layDsHoaDonDienTu(List<HoaDon> dsHoaDon) throws Exception {
		List<Long> dsIdHoaDon = dsHoaDon.stream().map(hoaDonConverter::convertToIdHoaDon).collect(Collectors.toList());
		List<HoaDonDienTu> dsHoaDonDienTu = hoaDonDienTuRepository
				.findByHuyIsFalseAndPhatHanhThanhCongIsTrueAndThanhToanIsTrueAndHoaDon_IdHoaDonIn(dsIdHoaDon);
		if (dsHoaDonDienTu.size() != dsHoaDon.size()) {
			throw new Exception("Số lượng hóa đơn điện tử không bằng số lượng hóa đơn.");
		}
		return dsHoaDonDienTu;
	}

	private String goiWebServiceHuyThanhToan(List<HoaDonDienTu> dsHoaDonDienTu) throws Exception {
		String dsFKey = dsHoaDonDienTu.stream().map(hoaDonDienTuConverter::convertToIdHoaDon).map(Object::toString)
				.collect(Collectors.joining("_"));
		String dsIdHoaDonDienTu = dsFKey.replace("_", ",");
		try {
			UnConfirmPaymentFkeyResponse response = businessServiceClient.unConfirmPaymentFkey(dsFKey);
			String ketQua = response.getUnConfirmPaymentFkeyResult();
			themKetQuaHDDT(dsIdHoaDonDienTu, ketQua);
			return ketQua;
		} catch (Exception e) {
			String ketQua = "Không thể gọi web service hủy thanh toán hóa đơn điện tử.";
			themKetQuaHDDT(dsIdHoaDonDienTu, ketQua);
			e.printStackTrace();
			throw new Exception("Không thể gọi web service hủy thanh toán hóa đơn điện tử.");
		}
	}

	private void themKetQuaHDDT(String dsIdHoaDonDienTu, String ketQua) throws Exception {
		try {
			KetQuaHddt ketQuaHddt = new KetQuaHddt();
			KieuKetQuaHddt kieuKetQuaHddt = new KieuKetQuaHddt();
			kieuKetQuaHddt.setIdKieuKetQuaHddt(Utilities.ID_KIEU_HUY_THANH_TOAN_HOA_DON);
			ketQuaHddt.setKieuKetQuaHddt(kieuKetQuaHddt);
			ketQuaHddt.setTrangThai(ketQua);
			ketQuaHddt.setChuoiKetQua(dsIdHoaDonDienTu);
			ketQuaHddt.setNgayGioCapNhat(new Date());
			ketQuaHddtRepository.save(ketQuaHddt);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể thêm kết quả hóa đơn điện tử.");
		}
	}

	private void huyThanhToanHoaDonDienTu2(List<HoaDonDienTu> dsHoaDonDienTu, NguoiDung nguoiDung) throws Exception {
		for (HoaDonDienTu hoaDonDienTu : dsHoaDonDienTu) {
			hoaDonDienTu.setNguoiDung(nguoiDung);
			hoaDonDienTu.setThanhToan(false);
			// hoaDonDienTu.setNgayGioCapNhat(new Date());
		}
		try {
			hoaDonDienTuRepository.save(dsHoaDonDienTu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể cập nhật chưa thanh toán hóa đơn điện tử.");
		}
	}

	private void capNhatHoaDonChuaThanhToan(List<HoaDon> dsHoaDon, NguoiDung nguoiCapNhat) throws Exception {
		for (HoaDon hoaDon : dsHoaDon) {
			hoaDon.setNguoiDung(nguoiCapNhat);
			hoaDon.setTienConLai(hoaDon.getTienThanhToan());
			hoaDon.setTienThanhToan(0);
			hoaDon.setNgayGioCapNhat(new Date());
		}
		try {
			hoaDonRepository.save(dsHoaDon);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể cập nhật hóa đơn hủy thanh toán.");
		}

	}

	private void huyDsThanhToan(long id, NguoiDung nguoiCapNhat) throws Exception {
		List<ThanhToan> dsThanhToanCanHuy = thanhToanRepository.findByPhieuThanhToan_IdPhieuThanhToan(id);
		for (ThanhToan thanhToan : dsThanhToanCanHuy) {
			thanhToan.setNguoiDung(nguoiCapNhat);
			thanhToan.setHuyThanhToan(true);
			thanhToan.setNgayGioCapNhat(new Date());
		}
		try {
			thanhToanRepository.save(dsThanhToanCanHuy);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể hủy thanh toán.");
		}
	}

	private void huyPhieuThanhToan(PhieuThanhToan phieuThanhToanCanHuy, NguoiDung nguoiCapNhat) throws Exception {
		phieuThanhToanCanHuy.setNguoiDung(nguoiCapNhat);
		phieuThanhToanCanHuy.setHuyThanhToan(true);
		phieuThanhToanCanHuy.setNgayGioCapNhat(new Date());
		try {
			repository.save(phieuThanhToanCanHuy);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể hủy phiếu thanh toán.");
		}
	}

	@Override
	public Page<PhieuThanhToanDto> layDsPhieuThanhToan(String thongTinCanTim, Long idHinhThucThanhToan,
			Pageable pageable) throws Exception {
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		Page<PhieuThanhToan> dsPhieuThanhToanCanLay;
		if (thongTinCanTim.equals("")) {
			if (idHinhThucThanhToan.equals(Utilities.ID_HINH_THUC_THANH_TOAN_CHUYEN_KHOAN)) {
				dsPhieuThanhToanCanLay = repository.layDsPhieuThanhToanChuyenKhoanTheoNguoiDung(idNguoiDungHienHanh,
						idThangLamViec, idKhuVucLamViec, pageable);
			} else {
				dsPhieuThanhToanCanLay = repository.layDsPhieuThanhToanTheoNguoiDung(idNguoiDungHienHanh,
						idThangLamViec, idKhuVucLamViec, pageable);
			}
		} else {
			if (idHinhThucThanhToan.equals(Utilities.ID_HINH_THUC_THANH_TOAN_CHUYEN_KHOAN)) {
				dsPhieuThanhToanCanLay = repository.layDsPhieuThanhToanChuyenKhoanTheoNguoiDungVaThongTinCanTim(
						idNguoiDungHienHanh, idThangLamViec, thongTinCanTim, idKhuVucLamViec, pageable);
			} else {
				dsPhieuThanhToanCanLay = repository.layDsPhieuThanhToanTheoNguoiDungVaThongTinCanTim(
						idNguoiDungHienHanh, idThangLamViec, thongTinCanTim, idKhuVucLamViec, pageable);
			}
		}
		Page<PhieuThanhToanDto> dsPhieuThanhToanDaChuyenDoi = dsPhieuThanhToanCanLay.map(converter::convertToDto);
		return dsPhieuThanhToanDaChuyenDoi;
	}

	@Override
	@Transactional
	public void huyPhieuThanhToan(long id) throws Exception {
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		huyPhieuThanhToan2(id, idNguoiDungHienHanh);
	}

}
