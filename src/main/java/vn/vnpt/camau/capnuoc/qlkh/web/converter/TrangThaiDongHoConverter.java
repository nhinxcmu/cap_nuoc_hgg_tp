package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.TrangThaiDongHoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiDongHo;

@Component
public class TrangThaiDongHoConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public TrangThaiDongHoDto convertToDto(TrangThaiDongHo entity) {
		if (entity == null) {
			return new TrangThaiDongHoDto();
		}
		TrangThaiDongHoDto dto = mapper.map(entity, TrangThaiDongHoDto.class);
		return dto;
	}

}
