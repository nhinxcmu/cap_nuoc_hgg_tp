package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.BienNhanThuTienNuocMobileDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ChiTietHoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThamSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DonVi;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHoKhachHangThang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDonDienTu;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhuVucThang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThongTinKhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DonViRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HoaDonDienTuRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.KhuVucThangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThongTinKhachHangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ChiTietHoaDonService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;

@Component
public class BienNhanThuTienNuocMobileDtoConverter {

	@Autowired
	private DozerBeanMapper mapper;
	@Autowired
	private ThamSoService thamSoService;
	@Autowired
	private DonViRepository donViRepository;
	@Autowired
	private ThongTinKhachHangRepository thongTinKhachHangRepository;
	@Autowired
	private HoaDonDienTuRepository hoaDonDienTuRepository;
	@Autowired
	private ChiTietHoaDonService chiTietHoaDonService;
	@Autowired
	private KhuVucThangRepository khuVucThangRepository;

	public BienNhanThuTienNuocMobileDto convertToDto(HoaDon entity) {
		if (entity == null) {
			return new BienNhanThuTienNuocMobileDto();
		}
		BienNhanThuTienNuocMobileDto dto = mapper.map(entity, BienNhanThuTienNuocMobileDto.class);
		dto.setSoLanIn(entity.getLichSuIns().size());
		thietLapThongTinCongTy(dto);
		thietLapThongTinDonVi(dto);
		thietLapThongTinKhachHang(dto, entity);
		thietLapHoaDonDienTu(dto);
		thietLapChiTietHoaDon(dto);
		thietLapThongTinIn(dto);
		return dto;
	}

	private void thietLapThongTinCongTy(BienNhanThuTienNuocMobileDto dto) {
		ThamSoDto thamSoDto = thamSoService.layThamSo(Utilities.ID_THAM_SO_TEN_CONG_TY);
		ThamSoDto thamSoDiaChiDto = thamSoService.layThamSo(Utilities.ID_THAM_SO_DIA_CHI_CONG_TY);
		ThamSoDto thamSoSoDienThoaiDto = thamSoService.layThamSo(Utilities.ID_THAM_SO_SO_DIEN_THOAI);
		dto.setTenCongTy(thamSoDto.getGiaTri());
		dto.setDiaChiCongTy(thamSoDiaChiDto.getGiaTri());
		dto.setSoDienThoaiCongTy(thamSoSoDienThoaiDto.getGiaTri());
	}

	private void thietLapThongTinDonVi(BienNhanThuTienNuocMobileDto dto) {
		DonVi donVi = donViRepository.findByNguoiDungs_IdNguoiDung(Utilities.layIdNguoiDung());
		dto.setTenDonVi(donVi.getTenDonVi());
	}

	private void thietLapThongTinKhachHang(BienNhanThuTienNuocMobileDto dto, HoaDon entity) {
		ThongTinKhachHang thongTinKhachHang = thongTinKhachHangRepository
				.layThongTinKhachHangTheoHoaDon(dto.getIdHoaDon(), entity.getThang().getIdThang());
		dto.setMaKhachHang(thongTinKhachHang.getMaKhachHang());
		dto.setTenKhachHang(thongTinKhachHang.getTenKhachHang());
		dto.setDiaChiSuDung(thongTinKhachHang.getDiaChiSuDung());
		dto.setSoDienThoai(thongTinKhachHang.getSoDienThoai());
		thietLapThongTinDongHo(dto, thongTinKhachHang);
		thietLapTuNgayDenNgay(dto, entity.getThang().getIdThang(),
				thongTinKhachHang.getSoGhi().getDuong().getKhuVuc().getIdKhuVuc());
	}

	private void thietLapThongTinDongHo(BienNhanThuTienNuocMobileDto dto, ThongTinKhachHang thongTinKhachHang) {
		Set<DongHoKhachHangThang> dsDongHoKhachHangThang = thongTinKhachHang.getKhachHang().getDongHoKhachHangThangs();
		if (!dsDongHoKhachHangThang.isEmpty()) {
			DongHoKhachHangThang dongHoKhachHangThang = dsDongHoKhachHangThang.iterator().next();
			dto.setMaDongHo(dongHoKhachHangThang.getDongHo().getMaDongHo());
		}
	}

	private void thietLapTuNgayDenNgay(BienNhanThuTienNuocMobileDto dto, Long idThang, Long idKhuVuc) {
		KhuVucThang khuVucThang = khuVucThangRepository.findByThang_IdThangAndKhuVuc_IdKhuVuc(idThang, idKhuVuc);
		dto.setTuNgay(khuVucThang.getTuNgay());
		dto.setDenNgay(khuVucThang.getDenNgay());
	}

	private void thietLapHoaDonDienTu(BienNhanThuTienNuocMobileDto dto) {
		HoaDonDienTu hoaDonDienTu = hoaDonDienTuRepository
				.findByHuyIsFalseAndPhatHanhThanhCongIsTrueAndHoaDon_IdHoaDon(dto.getIdHoaDon());
		if (hoaDonDienTu != null) {
			dto.setKyHieu(hoaDonDienTu.getKyHieu());
			dto.setSoHoaDon(hoaDonDienTu.getSoHoaDon());
		}
	}

	private void thietLapChiTietHoaDon(BienNhanThuTienNuocMobileDto dto) {
		List<ChiTietHoaDonDto> dsChiTietHoaDon = chiTietHoaDonService.layDsChiTietHoaDon(dto.getIdHoaDon());
		dto.setChiTietHoaDonDtos(dsChiTietHoaDon);
	}

	private void thietLapThongTinIn(BienNhanThuTienNuocMobileDto dto) {
		Date ngayHienHanh = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String ngayIn = format.format(ngayHienHanh);
		dto.setNgayIn(ngayIn);
		dto.setNhanVienThuNgan(Utilities.layHoTenNguoiDung());
	}

}
