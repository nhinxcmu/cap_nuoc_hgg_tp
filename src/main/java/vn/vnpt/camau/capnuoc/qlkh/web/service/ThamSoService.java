package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThamSoDto;

public interface ThamSoService {

	Page<ThamSoDto> layDsThamSo(String thongTinCanTim, Pageable pageable);

	ThamSoDto layThamSo(long id);

	ThamSoDto luuThamSo(ThamSoDto dto) throws Exception;

	List<ThamSoDto> layDsThamSoCongTy();
	
	boolean laCongTySuDungHDDT();

	// hoa don dien tu
	String layUriPublishService();

	String layUriBusinessService();

	String layUriPortalService();

	String layUsername();

	String layPassword();

	String layAccount();

	String layACpass();

	String layPattern();

	String laySerial();

	String layPatternLapDat();

	String laySerialLapDat();
}
