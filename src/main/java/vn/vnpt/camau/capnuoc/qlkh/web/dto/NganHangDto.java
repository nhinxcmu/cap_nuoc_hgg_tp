package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

public class NganHangDto implements Comparable<NganHangDto> {

	@Getter
	@Setter
	@NotNull
	private Long idNganHang;
	@Getter
	@Setter
	@NotNull
	@Size(max = 250)
	private String tenNganHang;
	
	@Override
	public int compareTo(NganHangDto object) {
		return this.tenNganHang.compareTo(object.getTenNganHang());
	}
	
}
