package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDonDienTu;

public interface HoaDonDienTuRepository extends CrudRepository<HoaDonDienTu, Long> {
	
	@EntityGraph(attributePaths = { "hoaDon" })
	List<HoaDonDienTu> findByHuyIsFalseAndPhatHanhThanhCongIsTrueAndThanhToanIsFalseAndHoaDon_IdHoaDonIn(List<Long> dsIdHoaDon);

	List<HoaDonDienTu> findByHuyIsFalseAndPhatHanhThanhCongIsTrueAndThanhToanIsTrueAndHoaDon_IdHoaDonIn(List<Long> dsIdHoaDon);

	HoaDonDienTu findByHuyIsFalseAndPhatHanhThanhCongIsTrueAndHoaDon_IdHoaDon(Long idHoaDon);
}
