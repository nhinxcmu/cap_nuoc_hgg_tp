package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoLieuChuyenDuLieuDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoLieuDuyetChiSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoLieuPhatHanhHoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoLieuPhatHanhHoaDonLapDatDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoLieuThanhToanChuyenKhoanDto;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ChiSoRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HoaDonRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.SoLieuService;

@Service
public class SoLieuServiceImp implements SoLieuService {

	@Autowired
	private HoaDonRepository hoaDonRepository;
	@Autowired
	private ChiSoRepository chiSoRepository;
	@Autowired
	private HttpSession httpSession;
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public SoLieuChuyenDuLieuDto thongKeChuyenDuLieu() {
		SoLieuChuyenDuLieuDto soLieuChuyenDuLieuDto = new SoLieuChuyenDuLieuDto();
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
		int tongKhachHang = hoaDonRepository
				.countByThang_IdThangAndKhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndKhachHang_ThongTinKhachHangs_Thangs_IdThangAndHuyIsFalse(
						idThangLamViec, idKhuVucLamViec, idThangLamViec);
		int tongDaChuyen = chiSoRepository
				.countByThang_IdThangAndHoaDon_KhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndHoaDon_KhachHang_ThongTinKhachHangs_Thangs_IdThangAndHuyIsFalse(
						idThangLamViec, idKhuVucLamViec, idThangLamViec);
		soLieuChuyenDuLieuDto.setTongKhachHang(tongKhachHang);
		soLieuChuyenDuLieuDto.setTongDaChuyen(tongDaChuyen);
		soLieuChuyenDuLieuDto.setTongChuaChuyen(tongKhachHang - tongDaChuyen);
		return soLieuChuyenDuLieuDto;
	}

	@Override
	public Page<SoLieuDuyetChiSoDto> thongKeDuyetChiSo(long idNhanVienGhiThu, Pageable pageable) throws Exception {
		try {
			Page<SoLieuDuyetChiSoDto> dsSoLieuDuyetChiSoDto;
			long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
			if (idNhanVienGhiThu == 0) {
				long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
				dsSoLieuDuyetChiSoDto = thongKeDuyetChiSoTheoKhuVuc(idThangLamViec, idKhuVucLamViec, pageable);
			} else {
				dsSoLieuDuyetChiSoDto = thongKeDuyetChiSoTheoNhanVienGhiThu(idThangLamViec, idNhanVienGhiThu, pageable);
			}
			return dsSoLieuDuyetChiSoDto;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể thống kê duyệt chỉ số.");
		}
	}

	private Page<SoLieuDuyetChiSoDto> thongKeDuyetChiSoTheoKhuVuc(long idThang, long idKhuVuc, Pageable pageable) {
		StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("thongKeDuyetChiSoTheoKhuVuc")
				.registerStoredProcedureParameter("idThang", Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter("idKhuVuc", Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter("pageNumber", Integer.class, ParameterMode.IN)
				.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN)
				.setParameter("idThang", idThang)
				.setParameter("idKhuVuc", idKhuVuc)
				.setParameter("pageNumber", pageable.getPageNumber())
				.setParameter("pageSize", pageable.getPageSize());
		@SuppressWarnings("rawtypes")
		List list = storedProcedure.getResultList();
		List<SoLieuDuyetChiSoDto> dsSoLieuDuyetChiSoDto = thietLapSoLieuDuyetChiSoDto(list);
		Integer total = chiSoRepository.laySoLuongDuyetChiSoTheoKhuVuc(idThang, idKhuVuc);
		Page<SoLieuDuyetChiSoDto> dsSoLieuDuyetChiSoDtoDaChuyenDoi = new PageImpl<>(dsSoLieuDuyetChiSoDto, pageable, total);
		return dsSoLieuDuyetChiSoDtoDaChuyenDoi;
	}

	private List<SoLieuDuyetChiSoDto> thietLapSoLieuDuyetChiSoDto(@SuppressWarnings("rawtypes") List list) {
		List<SoLieuDuyetChiSoDto> dsSoLieuDuyetChiSoDto = new ArrayList<SoLieuDuyetChiSoDto>();
		for (Object object : list) {
			SoLieuDuyetChiSoDto soLieuDuyetChiSoDto = new SoLieuDuyetChiSoDto();
			Object[] rs = (Object[]) object;
			soLieuDuyetChiSoDto.setIdNhanVienGhiThu(Integer.parseInt(rs[0].toString()));
			soLieuDuyetChiSoDto.setTenNhanVienGhiThu(rs[1].toString());
			soLieuDuyetChiSoDto.setIdSoGhi(Integer.parseInt(rs[2].toString()));
			soLieuDuyetChiSoDto.setTenSoGhi(rs[3].toString());
			soLieuDuyetChiSoDto.setTongKhachHang(Integer.parseInt(rs[4].toString()));
			soLieuDuyetChiSoDto.setTongCoTheDuyet(Integer.parseInt(rs[5].toString()));
			soLieuDuyetChiSoDto.setTongDaDuyet(Integer.parseInt(rs[6].toString()));
			soLieuDuyetChiSoDto.setTongChuaGhi(Integer.parseInt(rs[7].toString()));
			soLieuDuyetChiSoDto.setTongBinhThuong(Integer.parseInt(rs[8].toString()));
			soLieuDuyetChiSoDto.setTongKhongXai(Integer.parseInt(rs[9].toString()));
			soLieuDuyetChiSoDto.setTongKhoanTieuThu(Integer.parseInt(rs[10].toString()));
			soLieuDuyetChiSoDto.setTongCupNuoc(Integer.parseInt(rs[11].toString()));
			soLieuDuyetChiSoDto.setIdDuong(Integer.parseInt(rs[12].toString()));
			soLieuDuyetChiSoDto.setTenDuong(rs[13].toString());
			dsSoLieuDuyetChiSoDto.add(soLieuDuyetChiSoDto);
		}
		return dsSoLieuDuyetChiSoDto;
	}

	private Page<SoLieuDuyetChiSoDto> thongKeDuyetChiSoTheoNhanVienGhiThu(long idThang, long idNguoiDung, Pageable pageable) {
		StoredProcedureQuery storedProcedure = entityManager
				.createStoredProcedureQuery("thongKeDuyetChiSoTheoNhanVienGhiThu")
				.registerStoredProcedureParameter("idThang", Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter("idNguoiDung", Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter("pageNumber", Integer.class, ParameterMode.IN)
				.registerStoredProcedureParameter("pageSize", Integer.class, ParameterMode.IN)
				.setParameter("idThang", idThang)
				.setParameter("idNguoiDung", idNguoiDung)
				.setParameter("pageNumber", pageable.getPageNumber())
				.setParameter("pageSize", pageable.getPageSize());
		@SuppressWarnings("rawtypes")
		List list = storedProcedure.getResultList();
		List<SoLieuDuyetChiSoDto> dsSoLieuDuyetChiSoDto = thietLapSoLieuDuyetChiSoDto(list);
		Integer total = chiSoRepository.laySoLuongDuyetChiSoTheoNhanVienGhiThu(idThang, idNguoiDung);
		Page<SoLieuDuyetChiSoDto> dsSoLieuDuyetChiSoDtoDaChuyenDoi = new PageImpl<>(dsSoLieuDuyetChiSoDto, pageable, total);
		return dsSoLieuDuyetChiSoDtoDaChuyenDoi;
	}

	@Override
	public SoLieuPhatHanhHoaDonDto thongKePhatHanhHoaDon() throws Exception {
		try {
			long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
			long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
			StoredProcedureQuery storedProcedure = entityManager
					.createStoredProcedureQuery("thongKePhatHanhHoaDonTheoKhuVuc")
					.registerStoredProcedureParameter("idThang", Long.class, ParameterMode.IN)
					.registerStoredProcedureParameter("idKhuVuc", Long.class, ParameterMode.IN)
					.setParameter("idThang", idThangLamViec)
					.setParameter("idKhuVuc", idKhuVucLamViec);
			Object[] rs = (Object[]) storedProcedure.getSingleResult();
			SoLieuPhatHanhHoaDonDto soLieuPhatHanhHoaDonDto = thietLapSoLieuPhatHanhHoaDonDto(rs);
			return soLieuPhatHanhHoaDonDto;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể thống kê phát hành hóa đơn.");
		}
	}

	private SoLieuPhatHanhHoaDonDto thietLapSoLieuPhatHanhHoaDonDto(Object[] rs) {
		SoLieuPhatHanhHoaDonDto soLieuPhatHanhHoaDonDto = new SoLieuPhatHanhHoaDonDto();
		soLieuPhatHanhHoaDonDto.setTongKhachHang(Integer.parseInt(rs[0].toString()));
		soLieuPhatHanhHoaDonDto.setTongDaThanhToan(Integer.parseInt(rs[1].toString()));
		soLieuPhatHanhHoaDonDto.setTongDaPhatHanh(Integer.parseInt(rs[2].toString()));
		soLieuPhatHanhHoaDonDto.setTongCoThePhatHanh(Integer.parseInt(rs[3].toString()));
		soLieuPhatHanhHoaDonDto.setTongKhongPhatHanh(Integer.parseInt(rs[4].toString()));
		soLieuPhatHanhHoaDonDto.setTongChuaPhatHanh(Integer.parseInt(rs[5].toString()));
		return soLieuPhatHanhHoaDonDto;
	}
	private SoLieuPhatHanhHoaDonLapDatDto thietLapSoLieuPhatHanhHoaDonLapDatDto(Object[] rs) {
		SoLieuPhatHanhHoaDonLapDatDto soLieuPhatHanhHoaDonDto = new SoLieuPhatHanhHoaDonLapDatDto();
		soLieuPhatHanhHoaDonDto.setTongHoaDon(Integer.parseInt(rs[0].toString()));
		soLieuPhatHanhHoaDonDto.setTongDaPhatHanh(Integer.parseInt(rs[1].toString()));
		soLieuPhatHanhHoaDonDto.setTongKhongPhatHanh(Integer.parseInt(rs[2].toString()));
		soLieuPhatHanhHoaDonDto.setTongChuaPhatHanh(Integer.parseInt(rs[3].toString()));
		return soLieuPhatHanhHoaDonDto;
	}

	@Override
	public SoLieuThanhToanChuyenKhoanDto thongKeThanhToanChuyenKhoan() throws Exception {
		// TODO Auto-generated method stub
		try {
			long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
			long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
			StoredProcedureQuery storedProcedure = entityManager
					.createStoredProcedureQuery("thongKeThanhToanChuyenKhoanTheoKhuVuc")
					.registerStoredProcedureParameter("idThang", Long.class, ParameterMode.IN)
					.registerStoredProcedureParameter("idKhuVuc", Long.class, ParameterMode.IN)
					.setParameter("idThang", idThangLamViec)
					.setParameter("idKhuVuc", idKhuVucLamViec);
			Object[] rs = (Object[]) storedProcedure.getSingleResult();
			SoLieuThanhToanChuyenKhoanDto soLieuThanhToanChuyenKhoanDto = thietLapSoLieuThanhToanChuyenKhoanDto(rs);
			return soLieuThanhToanChuyenKhoanDto;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể thống kê thanh toán chuyển khoản.");
		}
	}
	
	@Override
	public SoLieuThanhToanChuyenKhoanDto thongKeThanhToanGiaoDich() throws Exception {
		// TODO Auto-generated method stub
		try {
			long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
			long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
			StoredProcedureQuery storedProcedure = entityManager
					.createStoredProcedureQuery("thongKeThanhToanGiaoDichTheoKhuVuc")
					.registerStoredProcedureParameter("idThang", Long.class, ParameterMode.IN)
					.registerStoredProcedureParameter("idKhuVuc", Long.class, ParameterMode.IN)
					.setParameter("idThang", idThangLamViec)
					.setParameter("idKhuVuc", idKhuVucLamViec);
			Object[] rs = (Object[]) storedProcedure.getSingleResult();
			SoLieuThanhToanChuyenKhoanDto soLieuThanhToanChuyenKhoanDto = thietLapSoLieuThanhToanChuyenKhoanDto(rs);
			return soLieuThanhToanChuyenKhoanDto;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể thống kê thanh toán giao dịch.");
		}
	}

	private SoLieuThanhToanChuyenKhoanDto thietLapSoLieuThanhToanChuyenKhoanDto(Object[] rs) {
		SoLieuThanhToanChuyenKhoanDto soLieuThanhToanChuyenKhoanDto = new SoLieuThanhToanChuyenKhoanDto();
		
		soLieuThanhToanChuyenKhoanDto.setTongHoaDon(Integer.parseInt(rs[0].toString()));
		soLieuThanhToanChuyenKhoanDto.setTongDaThanhToan(Integer.parseInt(rs[1].toString()));
		soLieuThanhToanChuyenKhoanDto.setTongChuaThanhToan(Integer.parseInt(rs[2].toString()));
		
		return soLieuThanhToanChuyenKhoanDto;
	}

	@Override
	public SoLieuPhatHanhHoaDonLapDatDto thongKePhatHanhHoaDonLapDat() throws Exception {
		try {
			long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
			long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
			StoredProcedureQuery storedProcedure = entityManager
					.createStoredProcedureQuery("thongKePhatHanhHoaDonLapDatTheoKhuVuc")
					.registerStoredProcedureParameter("idThang", Long.class, ParameterMode.IN)
					.registerStoredProcedureParameter("idKhuVuc", Long.class, ParameterMode.IN)
					.setParameter("idThang", idThangLamViec)
					.setParameter("idKhuVuc", idKhuVucLamViec);
			Object[] rs = (Object[]) storedProcedure.getSingleResult();
			SoLieuPhatHanhHoaDonLapDatDto soLieuPhatHanhHoaDonDto = thietLapSoLieuPhatHanhHoaDonLapDatDto(rs);
			return soLieuPhatHanhHoaDonDto;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể thống kê phát hành hóa đơn.");
		}
	}

}
