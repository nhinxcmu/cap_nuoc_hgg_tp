package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuKhachHang;

public interface LichSuKhachHangRepository extends CrudRepository<LichSuKhachHang, Long> {

	Page<LichSuKhachHang> findByIdKhachHangOrderByNgayGioCapNhatDesc(Long idKhachHang, Pageable pageable);

	LichSuKhachHang findByIdLichSuKhachHang(Long idLichSuKhachHang);

}
