package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;

@Controller
@RequestMapping("/sample")
public class SampleController {
	private ModelAttr modelAttr = new ModelAttr("Sample page", "sample",
			new String[] { "plugins/input-mask/jquery.inputmask.js",
					"plugins/input-mask/jquery.inputmask.date.extensions.js", "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js",
					"js/sample.js" },
			new String[] { "jdgrid/jdgrid.css", "css/sample.css" });

	@GetMapping
	public String showPage(Model model) throws Exception {
		model.addAttribute("MODEL", modelAttr);

		return "layout";
	}

}
