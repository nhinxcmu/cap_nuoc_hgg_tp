package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuong;

public interface DoiTuongRepository extends CrudRepository<DoiTuong, Long> {

	DoiTuong findByMaDoiTuong(String maDoiTuong);

	Page<DoiTuong> findByOrderByIdDoiTuongDesc(Pageable pageable);

	DoiTuong findByTenDoiTuong(String tenDoiTuong);
}
