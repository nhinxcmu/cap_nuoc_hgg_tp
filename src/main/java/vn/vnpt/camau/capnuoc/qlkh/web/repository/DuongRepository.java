package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Duong;

public interface DuongRepository extends CrudRepository<Duong, Long> {

	@EntityGraph(attributePaths = { "khuVuc" })
	Page<Duong> findByKhuVuc_IdKhuVucOrderByIdDuongDesc(Long idKhuVuc, Pageable pageable);

	@EntityGraph(attributePaths = { "khuVuc" })
	Page<Duong> findByKhuVuc_IdKhuVucAndTenDuongContainsOrderByIdDuongDesc(Long idKhuVuc, String tenDuong,
			Pageable pageable);

	@EntityGraph(attributePaths = { "khuVuc" })
	Page<Duong> findByTenDuongContainsOrderByIdDuongDesc(String tenDuong, Pageable pageable);

	@EntityGraph(attributePaths = { "khuVuc" })
	Page<Duong> findByOrderByIdDuongDesc(Pageable pageable);

	@EntityGraph(attributePaths = { "khuVuc.donVi" })
	Duong findByIdDuong(Long idDuong);

	List<Duong> findByKhuVuc_DonVi_Huyen_IdHuyenOrderByTenDuong(Long idHuyen);

	List<Duong> findByGiaNuocs_IdGiaNuocOrderByTenDuong(Long idGiaNuoc);

	List<Duong> findByKhuVuc_IdKhuVucOrderByTenDuong(Long idKhuVuc);

	List<Duong> findDistinctDuongBySoGhis_LichGhiThus_Thangs_IdThangAndSoGhis_LichGhiThus_NguoiDuocPhanCong_IdNguoiDungOrderByTenDuong(
			Long idThang, Long idNguoiDung);

	Long countByKhuVuc_IdKhuVuc(Long idKhuVuc);
}
