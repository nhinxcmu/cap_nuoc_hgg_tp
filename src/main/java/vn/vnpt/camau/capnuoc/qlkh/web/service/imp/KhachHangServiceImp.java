package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.vnpt.camau.capnuoc.qlkh.web.ReadNumber;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.HoaDonConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.KhachHangConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.LichSuKhachHangConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DongHoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhachHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThamSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThongTinKhachHangNganHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ChiSo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.FileBean;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDonLapDat;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KetQuaHddt;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KieuKetQuaHddt;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuKhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThongTinKhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiHoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ChiSoRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DoiTuongRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HoaDonLapDatRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.HoaDonRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.KetQuaHddtRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.KhachHangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.LichSuKhachHangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.NganHangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.SoGhiRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThongTinKhachHangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DongHoService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhachHangService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThangService;
import vn.vnpt.camau.capnuoc.qlkh.web.webservice.PublishServiceClient;
import vn.vnpt.camau.capnuoc.qlkh.web.wsdl.UpdateCusResponse;

@Service
public class KhachHangServiceImp implements KhachHangService {

	@Autowired
	private KhachHangConverter converter;
	@Autowired
	private HoaDonConverter hoaDonConverter;
	@Autowired
	private ThongTinKhachHangRepository thongTinKhachHangRepository;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private ThamSoService thamSoService;
	@Autowired
	private KhachHangRepository repository;
	@Autowired
	private ThangRepository thangRepository;
	@Autowired
	private HoaDonRepository hoaDonRepository;
	@Autowired
	private ChiSoRepository chiSoRepository;
	@Autowired
	private NganHangRepository nganHangRepository;
	@Autowired
	private SoGhiRepository soGhiRepository;
	@Autowired
	private DoiTuongRepository doiTuongRepository;
	@Autowired
	private ThangService thangService;
	@Autowired
	private PublishServiceClient publishServiceClient;
	@Autowired
	private KetQuaHddtRepository ketQuaHddtRepository;
	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	private KhuVucService khuVucService;
	@Autowired
	private DongHoService dongHoService;
	@Autowired
	private LichSuKhachHangRepository lichSuKhachHangRepository;
	@Autowired
	private LichSuKhachHangConverter lichSuKhachHangConverter;

	@Autowired
	private HoaDonLapDatRepository hoaDonLdRepo;

	@Override
	public KhachHangDto layKhachHang(long id) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		ThongTinKhachHang thongTinKhachHangCanLay = thongTinKhachHangRepository.layThongTinKhachHang(id,
				idThangLamViec);
		KhachHangDto khachHangDaChuyenDoi = converter.convertToDtoIncludeMaDongHo(thongTinKhachHangCanLay);
		return khachHangDaChuyenDoi;
	}

	@Override
	@Transactional
	public KhachHangDto luuKhachHang1(KhachHangDto dto) throws Exception {
		ThongTinKhachHang thongTinKhachHangCanLuu;
		if (dto.getIdThongTinKhachHang() == null) {
			thongTinKhachHangCanLuu = themKhachHang(dto);
		} else {
			thongTinKhachHangCanLuu = capNhatKhachHang(dto);
		}
		luuHoaDonLapDat(thongTinKhachHangCanLuu);
		KhachHangDto khachHangDaChuyenDoi = converter.convertToDto(thongTinKhachHangCanLuu);
		LichSuKhachHang lichSuKhachHang = lichSuKhachHangConverter.convertToEntity(khachHangDaChuyenDoi);
		lichSuKhachHangRepository.save(lichSuKhachHang);
		return khachHangDaChuyenDoi;
	}

	public void luuHoaDonLapDat(ThongTinKhachHang ttkh) {

		String tenHangHoa = thamSoService.layThamSo(Utilities.ID_THAM_SO_TEN_HANG_HOA_DICH_VU_LD).getGiaTri();
		// Long thueSuat =
		// Long.parseLong(thamSoService.layThamSo(Utilities.ID_THAM_SO_THUE_SUAT_LD).getGiaTri());
		Long tienThue = 0l;
		Long tongTien = 0l;

		Thang thang = new Thang();
		thang.setIdThang(Utilities.layThangLamViec(httpSession).getIdThang());
		HoaDonLapDat hoaDonLapDat = hoaDonLdRepo.findByThang_IdThangAndThongTinKhachHang_IdThongTinKhachHang(
				Utilities.layThangLamViec(httpSession).getIdThang(), ttkh.getIdThongTinKhachHang());
		if (hoaDonLapDat == null) {
			hoaDonLapDat = new HoaDonLapDat();
			hoaDonLapDat.setThongTinKhachHang(ttkh);
			hoaDonLapDat.setNgayGioCapNhat(new Date());
			hoaDonLapDat.setNguoiDung(Utilities.layNguoiDung());
			hoaDonLapDat.setTienDichVu(0l);
			hoaDonLapDat.setTienThue(tienThue);
			hoaDonLapDat.setTongTien(tongTien);
			hoaDonLapDat.setBangChu(ReadNumber.numberToString(BigDecimal.valueOf(tongTien)));
			hoaDonLapDat.setTrangThaiHoaDon(new TrangThaiHoaDon(5L));
			hoaDonLapDat.setTenHangHoa(tenHangHoa);
			hoaDonLapDat.setThang(thang);
			hoaDonLapDat = hoaDonLdRepo.save(hoaDonLapDat);
		}
	}

	private KhachHangDto luuKhachHangImport(KhachHangDto dto) throws Exception {
		ThongTinKhachHang thongTinKhachHangCanLuu;
		if (dto.getIdThongTinKhachHang() == null) {
			thongTinKhachHangCanLuu = themKhachHang(dto);
		} else {
			thongTinKhachHangCanLuu = capNhatKhachHang(dto);
		}
		KhachHangDto khachHangDaChuyenDoi = converter.convertToDto(thongTinKhachHangCanLuu);
		return khachHangDaChuyenDoi;
	}

	private ThongTinKhachHang themKhachHang(KhachHangDto dto) throws Exception {
		// kiểm tra mã khách hàng đã tồn tại
		if (laMaKhachHangDaTonTai(dto.getMaKhachHang())) {
			throw new Exception("Mã khách hàng đã tồn tại.");
		}
		try {
			Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
			long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();

			dto.setIdNguoiCapNhat(idNguoiDungHienHanh);

			ObjectMapper mapper = new ObjectMapper();
			String giaTri = mapper.setSerializationInclusion(Include.NON_NULL).writeValueAsString(dto);

			StoredProcedureQuery storedProcedure = entityManager
					.createStoredProcedureQuery("themKhachHang", ThongTinKhachHang.class)
					.registerStoredProcedureParameter("giaTri", String.class, ParameterMode.IN)
					.registerStoredProcedureParameter("idThang", Long.class, ParameterMode.IN)
					.setParameter("giaTri", giaTri).setParameter("idThang", idThangLamViec);
			ThongTinKhachHang thongTinKhachHangCanThem = (ThongTinKhachHang) storedProcedure.getSingleResult();

			// gọi webservice cập nhật khách hàng
			if (khuVucService.laKhuVucSuDungHDDT())
				goiWebServiceCapNhatKhachHang(thongTinKhachHangCanThem);
			// trả thông tin khách hàng
			return thongTinKhachHangCanThem;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể thêm thông tin khách hàng.");
		}
	}

	private boolean laMaKhachHangDaTonTai(String maKhachHang) {
		ThamSoDto thamSoCanLay = thamSoService.layThamSo(Utilities.ID_THAM_SO_MA_KHACH_HANG_DUY_NHAT);
		if (thamSoCanLay == null) {
			return false;
		}
		boolean laMaKhachHangDuyNhat = Boolean.parseBoolean(thamSoCanLay.getGiaTri());
		if (laMaKhachHangDuyNhat) {
			if (maKhachHang.equals("")) {
				return false;
			} else {
				Long soLuongKhachHangCanKiemTra = thongTinKhachHangRepository.countByMaKhachHang(maKhachHang);
				return soLuongKhachHangCanKiemTra > 0;
			}
		}
		return false;
	}

	private void goiWebServiceCapNhatKhachHang(ThongTinKhachHang thongTinKhachHang) throws Exception {
		try {
			String xmlCusData = converter.convertToXml(thongTinKhachHang);
			UpdateCusResponse response = publishServiceClient.updateCus(xmlCusData, 0);
			Integer ketQua = response.getUpdateCusResult();
			themKetQuaHDDT(thongTinKhachHang.getIdThongTinKhachHang(), ketQua.toString(),
					Utilities.ID_KIEU_CAP_NHAT_KHACH_HANG);
		} catch (Exception e) {
			String ketQua = "Không thể gọi web service cập nhật khách hàng.";
			themKetQuaHDDT(thongTinKhachHang.getIdThongTinKhachHang(), ketQua, Utilities.ID_KIEU_THANH_TOAN_HOA_DON);
			e.printStackTrace();
			throw new Exception("Không thể gọi web service cập nhật khách hàng.");
		}
	}

	private void themKetQuaHDDT(Long idThongTinKhachHang, String ketQua, long idKieuKetQuaHddt) throws Exception {
		try {
			KetQuaHddt ketQuaHddt = new KetQuaHddt();
			KieuKetQuaHddt kieuKetQuaHddt = new KieuKetQuaHddt();
			kieuKetQuaHddt.setIdKieuKetQuaHddt(idKieuKetQuaHddt);
			ketQuaHddt.setKieuKetQuaHddt(kieuKetQuaHddt);
			ketQuaHddt.setTrangThai(ketQua);
			ketQuaHddt.setChuoiKetQua(idThongTinKhachHang.toString());
			ketQuaHddt.setNgayGioCapNhat(new Date());
			ketQuaHddtRepository.save(ketQuaHddt);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể thêm kết quả hóa đơn điện tử.");
		}
	}

	private ThongTinKhachHang capNhatKhachHang(KhachHangDto dto) throws Exception {
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();

		// kiểm tra chuyển khu vực phải thanh toán hết nợ
		long idThongTinKhachHang = dto.getIdThongTinKhachHang();
		ThongTinKhachHang thongTinKhachHangTruoc = thongTinKhachHangRepository.layThongTinKhachHang(idThongTinKhachHang,
				idThangLamViec);
		if (dto.getIdSoGhi() != thongTinKhachHangTruoc.getSoGhi().getIdSoGhi()) {
			List<HoaDon> dsHoaDonChuaThanhToan = hoaDonRepository
					.findByKhachHang_ThongTinKhachHangs_IdThongTinKhachHangAndTrangThaiHoaDon_IdTrangThaiHoaDonAndTienThanhToanAndTienConLaiGreaterThanAndThang_IdThangLessThanEqual(
							idThongTinKhachHang, Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON, 0l, 0l, idThangLamViec);
			if (dsHoaDonChuaThanhToan.size() > 0)
				throw new Exception("Phải thanh toán hết các hóa đơn trước khi chuyển khu vực.");
		}

		// kiểm tra thông tin khách hàng
		ThongTinKhachHang thongTinKhachHangCanKiemTra = kiemTraKhachHang(dto);

		// cập nhật thông tin khách hàng
		try {
			dto.setIdNguoiCapNhat(idNguoiDungHienHanh);
			dto.setIdKhachHang(thongTinKhachHangCanKiemTra.getKhachHang().getIdKhachHang());

			ObjectMapper mapper = new ObjectMapper();
			String giaTri = mapper.setSerializationInclusion(Include.NON_NULL).writeValueAsString(dto);

			StoredProcedureQuery storedProcedure = entityManager
					.createStoredProcedureQuery("capNhatKhachHang", ThongTinKhachHang.class)
					.registerStoredProcedureParameter("giaTri", String.class, ParameterMode.IN)
					.registerStoredProcedureParameter("idThang", Long.class, ParameterMode.IN)
					.setParameter("giaTri", giaTri).setParameter("idThang", idThangLamViec);
			ThongTinKhachHang thongTinKhachHangDaCapNhat = (ThongTinKhachHang) storedProcedure.getSingleResult();

			// gọi webservice cập nhật khách hàng
			if (khuVucService.laKhuVucSuDungHDDT())
				goiWebServiceCapNhatKhachHang(thongTinKhachHangDaCapNhat);
			// trả thông tin khách hàng
			return thongTinKhachHangDaCapNhat;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể cập nhật thông tin khách hàng.");
		}
	}

	private ThongTinKhachHang kiemTraKhachHang(KhachHangDto dto) throws Exception {
		// kiểm tra khách hàng cần cập nhật
		ThongTinKhachHang thongTinKhachHangCanKiemTra = thongTinKhachHangRepository
				.findOne(dto.getIdThongTinKhachHang());
		if (thongTinKhachHangCanKiemTra == null) {
			throw new Exception("Không tìm thấy khách hàng.");
		}
		// kiểm tra mã khách hàng đã tồn tại
		if (laMaKhachHangDaTonTai(dto.getMaKhachHang(), thongTinKhachHangCanKiemTra.getKhachHang().getIdKhachHang())) {
			throw new Exception("Mã khách hàng đã tồn tại.");
		}
		// kiểm tra thông tin khách hàng tháng làm việc
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		long idThangCanKiemTra = thongTinKhachHangCanKiemTra.getThang().getIdThang();
		if (idThangCanKiemTra != idThangLamViec) {
			dto.setIdThongTinKhachHang(null);
		}
		return thongTinKhachHangCanKiemTra;
	}

	private boolean laMaKhachHangDaTonTai(String maKhachHang, long idKhachHang) {
		ThamSoDto thamSoCanLay = thamSoService.layThamSo(Utilities.ID_THAM_SO_MA_KHACH_HANG_DUY_NHAT);
		if (thamSoCanLay == null) {
			return false;
		}
		boolean laMaKhachHangDuyNhat = Boolean.parseBoolean(thamSoCanLay.getGiaTri());
		if (laMaKhachHangDuyNhat) {
			if (maKhachHang.equals("")) {
				return false;
			} else {
				Long soLuongKhachHangCanKiemTra = thongTinKhachHangRepository
						.countByMaKhachHangAndKhachHang_IdKhachHangNot(maKhachHang, idKhachHang);
				return soLuongKhachHangCanKiemTra > 0;
			}
		}
		return false;
	}

	@Override
	@Transactional
	public void xoaKhachHang(long id) throws Exception {
		// kiểm tra thông tin khách hàng
		kiemTraThongTinKhachHang(id);
		// kiểm tra hóa đơn
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		ThongTinKhachHang thongTinKhachHang = thongTinKhachHangRepository.findOne(id);
		KhachHang khachHang = thongTinKhachHang.getKhachHang();
		HoaDon hoaDonDaKiemTra = kiemTraHoaDon(khachHang.getIdKhachHang(), idThangLamViec);
		// người cập nhật
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		NguoiDung nguoiCapNhat = new NguoiDung();
		nguoiCapNhat.setIdNguoiDung(idNguoiDungHienHanh);
		// hủy chỉ số
		huyChiSo(hoaDonDaKiemTra.getIdHoaDon(), idThangLamViec, nguoiCapNhat);
		// hủy hóa đơn
		huyHoaDon(hoaDonDaKiemTra, nguoiCapNhat);
		// hủy khách hàng
		huyKhachHang(khachHang, idNguoiDungHienHanh);
	}

	private void kiemTraThongTinKhachHang(long id) throws Exception {
		// lấy số lượng thông tin khách hàng tháng
		Long soLuongThang = thangRepository.countByThongTinKhachHangThangs_IdThongTinKhachHang(id);
		if (soLuongThang == 0) {
			throw new Exception("Không tìm thấy khách hàng.");
		}
		if (soLuongThang > 1) {
			throw new Exception("Thông tin khách hàng đã áp dụng cho nhiều tháng.");
		}
	}

	private HoaDon kiemTraHoaDon(long idKhachHang, long idThang) throws Exception {
		// lấy hóa đơn để kiểm tra đã phát hành chưa
		HoaDon hoaDon = hoaDonRepository.findByKhachHang_IdKhachHangAndThang_IdThang(idKhachHang, idThang);
		TrangThaiHoaDon trangThaiHoaDon = hoaDon.getTrangThaiHoaDon();
		if (trangThaiHoaDon.getIdTrangThaiHoaDon() == Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON) {
			throw new Exception("Hóa đơn đã phát hành.");
		}
		return hoaDon;
	}

	private void huyChiSo(Long idHoaDon, long idThang, NguoiDung nguoiCapNhat) throws Exception {
		ChiSo chiSo = chiSoRepository.findByHoaDon_IdHoaDonAndThang_IdThang(idHoaDon, idThang);
		if (chiSo != null) {
			chiSo.setNguoiDungByIdNguoiCapNhat(nguoiCapNhat);
			chiSo.setNgayGioCapNhat(new Date());
			chiSo.setHuy(true);
			try {
				chiSoRepository.save(chiSo);
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Không thể hủy chỉ số.");
			}
		}
	}

	private void huyHoaDon(HoaDon hoaDon, NguoiDung nguoiCapNhat) throws Exception {
		hoaDon.setHuy(true);
		hoaDon.setNguoiDung(nguoiCapNhat);
		hoaDon.setNgayGioCapNhat(new Date());
		try {
			hoaDonRepository.save(hoaDon);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể hủy hóa đơn.");
		}
	}

	private void huyKhachHang(KhachHang khachHang, Long idNguoiHuy) throws Exception {
		khachHang.setHuy(true);
		khachHang.setIdNguoiHuy(idNguoiHuy);
		khachHang.setNgayGioHuy(new Date());
		try {
			repository.save(khachHang);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể hủy khách hàng.");
		}
	}

	@Override
	public List<KhachHangDto> layDsKhachHang(long idSoGhi, String thongTinCanTim, long idNguoiDung) throws Exception {
		Thang thangPhanCongGhiThuMoiNhat = thangService.layThangPhanCongGhiThuMoiNhat(idNguoiDung);
		if (thangPhanCongGhiThuMoiNhat == null) {
			return new ArrayList<KhachHangDto>();
		}
		long idThangPhanCongGhiThuMoiNhat = thangPhanCongGhiThuMoiNhat.getIdThang();
		List<ThongTinKhachHang> dsThongTinKhachHangCanLay;
		if (thongTinCanTim.equals("")) {
			dsThongTinKhachHangCanLay = thongTinKhachHangRepository.layDsThongTinKhachHangTheoSoGhi(idSoGhi,
					idThangPhanCongGhiThuMoiNhat);
		} else {
			dsThongTinKhachHangCanLay = thongTinKhachHangRepository.layDsThongTinKhachHangTheoSoGhiVaThongTinCanTim(
					idSoGhi, thongTinCanTim, idThangPhanCongGhiThuMoiNhat);
		}
		List<KhachHangDto> dsKhachHangDaChuyenDoi = dsThongTinKhachHangCanLay.stream()
				.map(converter::convertToDtoIncludeMaDongHo).collect(Collectors.toList());
		return dsKhachHangDaChuyenDoi;
	}

	@Override
	public KhachHangDto layKhachHang(String thongTinCanTim) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		ThongTinKhachHang thongTinKhachHangCanLay = thongTinKhachHangRepository
				.layThongTinKhachHangTheoMaKhachHang(thongTinCanTim, idThangLamViec);
		KhachHangDto khachHangDaChuyenDoi = converter.convertToDtoIncludeMaDongHo(thongTinKhachHangCanLay);
		return khachHangDaChuyenDoi;
	}

	@Override
	public Page<KhachHangDto> layDsKhachHangTheoNhanVienThanhToanGiaoDich(long idDuong, long idSoGhi,
			String thongTinCanTim, Pageable pageable) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		Long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		Page<ThongTinKhachHang> dsThongTinKhachHangCanLay = null;
		if (idDuong == 0) {
			if (thongTinCanTim.isEmpty()) {
				dsThongTinKhachHangCanLay = thongTinKhachHangRepository
						.layDsThongTinKhachHangTheoNhanVienThanhToanGiaoDich(idThangLamViec, idNguoiDungHienHanh,
								pageable);
			} else {
				dsThongTinKhachHangCanLay = thongTinKhachHangRepository
						.layDsThongTinKhachHangTheoNhanVienThanhToanGiaoDich(idThangLamViec, idNguoiDungHienHanh,
								thongTinCanTim, pageable);
			}
		} else {
			if (idSoGhi == 0) {
				if (thongTinCanTim.isEmpty()) {
					dsThongTinKhachHangCanLay = thongTinKhachHangRepository
							.layDsThongTinKhachHangTheoDuongVaNhanVienThanhToanGiaoDich(idDuong, idThangLamViec,
									idNguoiDungHienHanh, pageable);
				} else {
					dsThongTinKhachHangCanLay = thongTinKhachHangRepository
							.layDsThongTinKhachHangTheoDuongVaNhanVienThanhToanGiaoDich(idDuong, idThangLamViec,
									idNguoiDungHienHanh, thongTinCanTim, pageable);
				}
			} else {
				if (thongTinCanTim.isEmpty()) {
					dsThongTinKhachHangCanLay = thongTinKhachHangRepository
							.layDsThongTinKhachHangTheoSoGhiVaNhanVienThanhToanGiaoDich(idSoGhi, idThangLamViec,
									idNguoiDungHienHanh, pageable);
				} else {
					dsThongTinKhachHangCanLay = thongTinKhachHangRepository
							.layDsThongTinKhachHangTheoSoGhiVaNhanVienThanhToanGiaoDich(idSoGhi, idThangLamViec,
									idNguoiDungHienHanh, thongTinCanTim, pageable);
				}
			}
		}
		return dsThongTinKhachHangCanLay.map(converter::convertToDtoIncludeMaDongHo);
	}

	@Override
	public Page<KhachHangDto> layDsKhachHang(long idKhuVuc, long idDuong, long idSoGhi, String thongTinCanTim,
			Pageable pageable) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		Page<ThongTinKhachHang> dsThongTinKhachHangCanLay;
		if (thongTinCanTim.equals("")) {
			dsThongTinKhachHangCanLay = thongTinKhachHangRepository.layDsThongTinKhachHang(idKhuVuc, idDuong, idSoGhi,
					idThangLamViec, pageable);
		} else {
			dsThongTinKhachHangCanLay = thongTinKhachHangRepository.layDsThongTinKhachHang(idKhuVuc, idDuong, idSoGhi,
					idThangLamViec, thongTinCanTim, pageable);
		}
		Page<KhachHangDto> dsKhachHangDaChuyenDoi = dsThongTinKhachHangCanLay
				.map(converter::convertToDtoIncludeMaDongHo);
		return dsKhachHangDaChuyenDoi;
	}

	@Override
	public List<KhachHangDto> layDsKhachHangFull(long idKhuVuc, long idDuong, long idSoGhi, String thongTinCanTim) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		List<ThongTinKhachHang> dsThongTinKhachHangCanLay;
		if (thongTinCanTim.equals("")) {
			dsThongTinKhachHangCanLay = thongTinKhachHangRepository.layDsThongTinKhachHang(idKhuVuc, idDuong, idSoGhi,
					idThangLamViec);
		} else {
			dsThongTinKhachHangCanLay = thongTinKhachHangRepository.layDsThongTinKhachHang(idKhuVuc, idDuong, idSoGhi,
					idThangLamViec, thongTinCanTim);
		}
		List<KhachHangDto> dsKhachHangDaChuyenDoi = dsThongTinKhachHangCanLay.stream()
				.map(converter::convertToDtoIncludeMaDongHo).collect(Collectors.toList());
		return dsKhachHangDaChuyenDoi;
	}

	@Override
	public void capNhatTaiKhoanHDDTHangLoat() throws Exception {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		List<ThongTinKhachHang> dsThongTinKhachHangCanCapNhatTaiKhoan = thongTinKhachHangRepository
				.findByThangs_IdThang(idThangLamViec);
		for (ThongTinKhachHang thongTinKhachHang : dsThongTinKhachHangCanCapNhatTaiKhoan) {
			if (khuVucService.laKhuVucSuDungHDDT())
				goiWebServiceCapNhatKhachHang(thongTinKhachHang);
		}
	}

	// @Override
	// public Page<KhachHangDto> layDsKhachHang(String thongTinCanTim, Pageable
	// pageable) {
	// long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
	// long idKhuVucLamViec = Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
	// Page<ThongTinKhachHang> dsThongTinKhachHangCanLay;
	// if (thongTinCanTim.equals("")) {
	// dsThongTinKhachHangCanLay = thongTinKhachHangRepository
	// .findBySoGhi_Duong_KhuVuc_IdKhuVucAndThangs_IdThangAndKhachHang_HuyIsFalseOrderByThuTu(
	// idKhuVucLamViec, idThangLamViec, pageable);
	// } else {
	// dsThongTinKhachHangCanLay = thongTinKhachHangRepository
	// .findBySoGhi_Duong_KhuVuc_IdKhuVucAndThongTinCanTimContainsAndThangs_IdThangAndKhachHang_HuyIsFalseOrderByThuTu(
	// idKhuVucLamViec, thongTinCanTim, idThangLamViec, pageable);
	// }
	// Page<KhachHangDto> dsKhachHangDaChuyenDoi =
	// dsThongTinKhachHangCanLay.map(converter::convertToDtoIncludeMaDongHo);
	// return dsKhachHangDaChuyenDoi;
	// }

	@Override
	public int layMaKhachHangMoi() {
		return thongTinKhachHangRepository.layMaKhachHangMoi(Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc());
	}

	@Override
	public Page<HoaDonDto> layDsHoaDonTheoIdKhachHang(long idKhachHang, Pageable pageable) {
		Page<HoaDon> dsHoaDonCanLay = hoaDonRepository.findByKhachHang_IdKhachHangOrderByThang_IdThangDesc(idKhachHang,
				pageable);
		Page<HoaDonDto> dsHoaDonDaChuyenDoi = dsHoaDonCanLay.map(hoaDonConverter::convertToDtoIncludeThongTinKhachHang);
		return dsHoaDonDaChuyenDoi;
	}

	@Override
	public void capNhatToaDo(long idThongTinKhachHang, Double viDo, Double kinhDo) throws Exception {
		ThongTinKhachHang thongTinKhachHangCanLuu = thongTinKhachHangRepository.findOne(idThongTinKhachHang);
		thongTinKhachHangCanLuu.setViDo(viDo);
		thongTinKhachHangCanLuu.setKinhDo(kinhDo);
		thongTinKhachHangRepository.save(thongTinKhachHangCanLuu);
	}

	@SuppressWarnings("resource")
	@Override
	@Transactional
	public String importKhachHang(FileBean fileBean) {

		ByteArrayInputStream bis = new ByteArrayInputStream(fileBean.getFileData().getBytes());
		Workbook workbook;
		int i = 2;
		try {
			if (fileBean.getFileData().getOriginalFilename().endsWith("xls")) {
				workbook = new HSSFWorkbook(bis);
			} else if (fileBean.getFileData().getOriginalFilename().endsWith("xlsx")) {
				workbook = new XSSFWorkbook(bis);
			} else {
				return new Response(-1, "Vui lòng chọn file Excel!").toString();
			}

			for (Row row : workbook.getSheetAt(0)) {
				if (row.getRowNum() > 0) {
					DateFormat sourceFormat = new SimpleDateFormat("dd/MM/yyyy");
					String maKhachHang = row.getCell(0) == null ? "" : row.getCell(0).getStringCellValue();
					int thuTu = row.getCell(1) == null ? 0 : (int) row.getCell(1).getNumericCellValue();
					String ho = row.getCell(2) == null ? "" : row.getCell(2).getStringCellValue();
					String ten = row.getCell(3) == null ? "" : row.getCell(3).getStringCellValue();
					String diaChiSuDung = row.getCell(4) == null ? "" : row.getCell(4).getStringCellValue();
					String diaChiThanhToan = row.getCell(5) == null ? "" : row.getCell(5).getStringCellValue();
					String soDienThoai = row.getCell(6) == null ? "" : row.getCell(6).getStringCellValue();
					String soCmnd = row.getCell(7) == null ? "" : row.getCell(7).getStringCellValue();
					String maSoThue = row.getCell(8) == null ? "" : row.getCell(8).getStringCellValue();
					String soTaiKhoan = row.getCell(9) == null ? "" : row.getCell(9).getStringCellValue();
					String tenTaiKhoan = row.getCell(10) == null ? "" : row.getCell(10).getStringCellValue();
					String tenNganHang = row.getCell(11) == null ? "" : row.getCell(11).getStringCellValue();
					String tenSoGhi = row.getCell(12) == null ? "" : row.getCell(12).getStringCellValue();
					String soHopDong = row.getCell(13) == null ? "" : row.getCell(13).getStringCellValue();
					int thuPbvmt = row.getCell(14) == null ? 0 : (int) row.getCell(14).getNumericCellValue();
					String sNgayLapHopDong = row.getCell(15) == null ? "" : row.getCell(15).getStringCellValue();
					String sNgayDangKy = row.getCell(16) == null ? "" : row.getCell(16).getStringCellValue();
					String sNgayLapDat = row.getCell(17) == null ? "" : row.getCell(17).getStringCellValue();
					String tenDoiTuong = row.getCell(18) == null ? "" : row.getCell(18).getStringCellValue();
					String maDongHo = row.getCell(19) == null ? "" : row.getCell(19).getStringCellValue();
					String sNgayKiemDinh = row.getCell(20) == null ? "" : row.getCell(20).getStringCellValue();
					Long donGiaLapDat = row.getCell(21) == null ? 0 : (long) row.getCell(21).getNumericCellValue();
					String tenHangHoa = row.getCell(22) == null ? "" : row.getCell(22).getStringCellValue();

					KhachHangDto khachHangDto = new KhachHangDto();
					khachHangDto.setMaKhachHang(maKhachHang);
					khachHangDto.setThuTu(thuTu);
					khachHangDto.setHo(ho);
					khachHangDto.setTen(ten);
					khachHangDto.setTenKhachHang(ho + " " + ten);
					khachHangDto.setDiaChiSuDung(diaChiSuDung);
					khachHangDto.setDiaChiThanhToan(diaChiThanhToan);
					khachHangDto.setSoDienThoai(soDienThoai);
					khachHangDto.setSoCmnd(soCmnd);
					khachHangDto.setMaSoThue(maSoThue);
					khachHangDto.setSoTaiKhoan(soTaiKhoan);
					khachHangDto.setTenTaiKhoan(tenTaiKhoan);
					if (!tenNganHang.equals(""))
						khachHangDto.setIdNganHang(nganHangRepository.findByTenNganHang(tenNganHang).getIdNganHang());
					if (!tenSoGhi.equals(""))
						khachHangDto.setIdSoGhi(soGhiRepository.findByTenSoGhi(tenSoGhi).getIdSoGhi());
					khachHangDto.setSoHopDong(soHopDong);

					if (thuPbvmt == 1)
						khachHangDto.setThuPbvmt(true);
					else
						khachHangDto.setThuPbvmt(false);

					if (!sNgayLapHopDong.equals(""))
						khachHangDto.setNgayLapHopDong(sourceFormat.parse(sNgayLapHopDong));
					if (!sNgayDangKy.equals(""))
						khachHangDto.setNgayDangKy(sourceFormat.parse(sNgayDangKy));
					if (!sNgayLapDat.equals(""))
						khachHangDto.setNgayLapDat(sourceFormat.parse(sNgayLapDat));
					if (!tenDoiTuong.equals(""))
						khachHangDto.setIdDoiTuong(doiTuongRepository.findByTenDoiTuong(tenDoiTuong).getIdDoiTuong());

					KhachHangDto dto = luuKhachHangImport(khachHangDto);
					ThongTinKhachHang ttkh = thongTinKhachHangRepository
							.findByIdThongTinKhachHang(dto.getIdThongTinKhachHang());

					if (!maDongHo.equals("") || maDongHo.equals("0")) {
						
						DongHoDto dongHoDto = new DongHoDto();
						dongHoDto.setIdTrangThaiDongHo(1L);
						maDongHo = maDongHo.equals("0") ? "THIEU-" + ttkh.getKhachHang().getIdKhachHang() : maDongHo;
						dongHoDto.setMaDongHo(ttkh.getMaKhachHang().trim() + "_" + maDongHo);
						dongHoDto.setChungLoai("");
						dongHoDto.setSoSeri(maDongHo);
						dongHoDto.setKichCo("");
						dongHoDto.setNhaSanXuat("");
						if (!sNgayKiemDinh.equals(""))
						{
							DateFormat monthYearFormat = new SimpleDateFormat("MM/yyyy");
							DateFormat yearFormat = new SimpleDateFormat("yyyy");
							Date ngayKiemDinh;
							if(sNgayKiemDinh.length() ==0)
							{
								ngayKiemDinh=new Date();
							}
							if(sNgayKiemDinh.length() ==6||sNgayKiemDinh.length() ==7)
							{
								ngayKiemDinh = monthYearFormat.parse(sNgayKiemDinh);
							}
							else if (sNgayKiemDinh.length() == 4) {
								ngayKiemDinh = yearFormat.parse(sNgayKiemDinh);
							} else {
								ngayKiemDinh = sourceFormat.parse(sNgayKiemDinh);
							}
							dongHoDto.setNgayKiemDinh(ngayKiemDinh);
						}
						dongHoDto.setGhiChuSuaChuaLanCuoi("");
						dongHoDto.setIdKhachHang(ttkh.getKhachHang().getIdKhachHang());

						dongHoService.luuDongHo(dongHoDto);
					}

					if (donGiaLapDat > 0) {
						tenHangHoa = tenHangHoa != "" ? tenHangHoa
								: thamSoService.layThamSo(Utilities.ID_THAM_SO_TEN_HANG_HOA_DICH_VU_LD).getGiaTri();
						Long thueSuat = Long
								.parseLong(thamSoService.layThamSo(Utilities.ID_THAM_SO_THUE_SUAT_LD).getGiaTri());
						Long tienThue = donGiaLapDat * thueSuat / 100;
						Long tongTien = donGiaLapDat + tienThue;

						Thang thang = new Thang();
						thang.setIdThang(Utilities.layThangLamViec(httpSession).getIdThang());
						HoaDonLapDat hoaDonLapDat = new HoaDonLapDat();
						hoaDonLapDat.setThongTinKhachHang(ttkh);
						hoaDonLapDat.setNgayGioCapNhat(new Date());
						hoaDonLapDat.setNguoiDung(Utilities.layNguoiDung());
						hoaDonLapDat.setTienDichVu(donGiaLapDat);
						hoaDonLapDat.setTienThue(tienThue);
						hoaDonLapDat.setTongTien(tongTien);
						hoaDonLapDat.setBangChu(ReadNumber.numberToString(BigDecimal.valueOf(tongTien)));
						hoaDonLapDat.setTrangThaiHoaDon(new TrangThaiHoaDon(5L));
						hoaDonLapDat.setTenHangHoa(tenHangHoa);
						hoaDonLapDat.setThang(thang);
						hoaDonLapDat = hoaDonLdRepo.save(hoaDonLapDat);
					}

					i++;
				}
			}

			return new Response(1, "Success").toString();

		} catch (IOException e) {
			System.out.println(e.getMessage());
			return new Response(-1, "Lỗi dữ liệu tại dòng " + i).toString();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new Response(-1, "Lỗi dữ liệu tại dòng " + i).toString();
		}
	}

	@SuppressWarnings("resource")
	@Override
	@Transactional
	public String capNhatDongHo(FileBean fileBean) {

		ByteArrayInputStream bis = new ByteArrayInputStream(fileBean.getFileData().getBytes());
		Workbook workbook;
		int i = 2;
		try {
			if (fileBean.getFileData().getOriginalFilename().endsWith("xls")) {
				workbook = new HSSFWorkbook(bis);
			} else if (fileBean.getFileData().getOriginalFilename().endsWith("xlsx")) {
				workbook = new XSSFWorkbook(bis);
			} else {
				return new Response(-1, "Vui lòng chọn file Excel!").toString();
			}

			for (Row row : workbook.getSheetAt(0)) {
				if (row.getRowNum() > 0) {
					DateFormat sourceFormat = new SimpleDateFormat("dd/MM/yyyy");
					DateFormat monthYearFormat = new SimpleDateFormat("MM/yyyy");
					DateFormat yearFormat = new SimpleDateFormat("yyyy");
					row.getCell(1).setCellType(CellType.STRING);
					String maKhachHang = row.getCell(1) == null ? "" : row.getCell(1).getStringCellValue();
					row.getCell(5).setCellType(CellType.STRING);
					String maDongHo = row.getCell(5) == null ? "" : row.getCell(5).getStringCellValue();
					row.getCell(6).setCellType(CellType.STRING);
					String sNgayKiemDinh = row.getCell(6) == null ? "" : row.getCell(6).getStringCellValue();

					long idThangHienTai = Utilities.layThangLamViec(httpSession).getIdThang();
					ThongTinKhachHang ttkh = thongTinKhachHangRepository.findByMaKhachHangAndThangs_IdThang(maKhachHang,
							idThangHienTai);

					if (!maDongHo.equals("")) {
						Date ngayKiemDinh;
						if(sNgayKiemDinh.length() ==0)
						{
							ngayKiemDinh=new Date();
						}
						else if(sNgayKiemDinh.length() ==6||sNgayKiemDinh.length() ==7)
						{
							ngayKiemDinh = monthYearFormat.parse(sNgayKiemDinh);
						}
						else if (sNgayKiemDinh.length() == 4) {
							ngayKiemDinh = yearFormat.parse(sNgayKiemDinh);
						} else {
							ngayKiemDinh = sourceFormat.parse(sNgayKiemDinh);
						}
						String soSeri = maDongHo.equals("0") ? "THIEU-" + maKhachHang : maDongHo;
						DongHoDto dongHoDto = new DongHoDto();
						dongHoDto.setIdTrangThaiDongHo(1L);
						dongHoDto.setMaDongHo(ttkh.getMaKhachHang().trim() + "_" + maDongHo);
						dongHoDto.setChungLoai("");
						dongHoDto.setSoSeri(soSeri);
						dongHoDto.setKichCo("");
						dongHoDto.setNhaSanXuat("");
						if (!sNgayKiemDinh.equals(""))
							dongHoDto.setNgayKiemDinh(ngayKiemDinh);
						dongHoDto.setGhiChuSuaChuaLanCuoi("");
						dongHoDto.setIdKhachHang(ttkh.getKhachHang().getIdKhachHang());

						dongHoService.luuDongHo(dongHoDto);
					} else {
						dongHoService.xoaDongHoKhachHangThangTheoThang(ttkh.getKhachHang().getIdKhachHang(),
								idThangHienTai);
					}

					i++;
				}
			}

			return new Response(1, "Success").toString();

		} catch (IOException e) {
			System.out.println(e.getMessage());
			return new Response(-1, "Lỗi dữ liệu tại dòng " + i).toString();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new Response(-1, "Lỗi dữ liệu tại dòng " + i).toString();
		}
	}

	@SuppressWarnings("resource")
	@Override
	public String chuyenKhuVuc(FileBean fileBean) {

		ByteArrayInputStream bis = new ByteArrayInputStream(fileBean.getFileData().getBytes());
		Workbook workbook;
		int i = 2;
		try {
			if (fileBean.getFileData().getOriginalFilename().endsWith("xls")) {
				workbook = new HSSFWorkbook(bis);
			} else if (fileBean.getFileData().getOriginalFilename().endsWith("xlsx")) {
				workbook = new XSSFWorkbook(bis);
			} else {
				return new Response(-1, "Vui lòng chọn file Excel!").toString();
			}

			long idThangHienTai = Utilities.layThangLamViec(httpSession).getIdThang();
			long idThangTruoc = Utilities.layThangTruoc(idThangHienTai);

			for (Row row : workbook.getSheetAt(0)) {
				if (row.getRowNum() > 0) {
					String maKhachHang = row.getCell(0) == null ? "" : row.getCell(0).getStringCellValue();
					String tenSoGhi = row.getCell(1) == null ? "" : row.getCell(1).getStringCellValue();
					int thuTu = row.getCell(2) == null ? 0 : (int) row.getCell(2).getNumericCellValue();
					int thuPbvmt = row.getCell(3) == null ? 0 : (int) row.getCell(3).getNumericCellValue();
					String tenDoiTuong = row.getCell(4) == null ? "" : row.getCell(4).getStringCellValue();

					ThongTinKhachHang thongTinKhachHangHienTai = thongTinKhachHangRepository
							.findByMaKhachHangAndThangs_IdThang(maKhachHang, idThangHienTai);
					ThongTinKhachHang thongTinKhachHangTruoc = thongTinKhachHangRepository
							.findByMaKhachHangAndThangs_IdThang(maKhachHang, idThangTruoc);

					if (thongTinKhachHangHienTai == null)
						continue;

					if (thuTu != -1)
						thongTinKhachHangHienTai.setThuTu(thuTu);

					if (!tenSoGhi.equals(""))
						thongTinKhachHangHienTai.setSoGhi(soGhiRepository.findByTenSoGhi(tenSoGhi));

					if (!tenDoiTuong.equals(""))
						thongTinKhachHangHienTai.setDoiTuong(doiTuongRepository.findByTenDoiTuong(tenDoiTuong));

					if (thuPbvmt != -1) {
						if (thuPbvmt == 1)
							thongTinKhachHangHienTai.setThuPbvmt(true);
						else
							thongTinKhachHangHienTai.setThuPbvmt(false);
					}

					if (thongTinKhachHangTruoc == null
							|| thongTinKhachHangHienTai.getIdThongTinKhachHang() != thongTinKhachHangTruoc
									.getIdThongTinKhachHang()
							|| (tenSoGhi.equals("") && thuPbvmt == -1 && tenDoiTuong.equals(""))) {
						thongTinKhachHangRepository.save(thongTinKhachHangHienTai);
					} else {
						Set<Thang> thangs = thongTinKhachHangHienTai.getThangs();
						for (Thang thang : thangs) {
							if (thang.getIdThang() == idThangHienTai)
								thangs.remove(thang);
						}
						thongTinKhachHangHienTai.setThangs(thangs);
						thongTinKhachHangRepository.save(thongTinKhachHangHienTai);

						ThongTinKhachHang thongTinKhachHangMoi = new ThongTinKhachHang();
						thongTinKhachHangMoi.setDoiTuong(thongTinKhachHangHienTai.getDoiTuong());
						thongTinKhachHangMoi.setKhachHang(thongTinKhachHangHienTai.getKhachHang());
						thongTinKhachHangMoi.setNganHang(thongTinKhachHangHienTai.getNganHang());
						thongTinKhachHangMoi.setNguoiDung(thongTinKhachHangHienTai.getNguoiDung());
						thongTinKhachHangMoi.setSoGhi(thongTinKhachHangHienTai.getSoGhi());
						thongTinKhachHangMoi.setMaKhachHang(thongTinKhachHangHienTai.getMaKhachHang());
						thongTinKhachHangMoi.setThuTu(thongTinKhachHangHienTai.getThuTu());
						thongTinKhachHangMoi.setTenKhachHang(thongTinKhachHangHienTai.getTenKhachHang());
						thongTinKhachHangMoi.setHo(thongTinKhachHangHienTai.getHo());
						thongTinKhachHangMoi.setTen(thongTinKhachHangHienTai.getTen());
						thongTinKhachHangMoi.setDiaChiSuDung(thongTinKhachHangHienTai.getDiaChiSuDung());
						thongTinKhachHangMoi.setDiaChiThanhToan(thongTinKhachHangHienTai.getDiaChiThanhToan());
						thongTinKhachHangMoi.setMaSoThue(thongTinKhachHangHienTai.getMaSoThue());
						thongTinKhachHangMoi.setSoCmnd(thongTinKhachHangHienTai.getSoCmnd());
						thongTinKhachHangMoi.setSoDienThoai(thongTinKhachHangHienTai.getSoDienThoai());
						thongTinKhachHangMoi.setSoHopDong(thongTinKhachHangHienTai.getSoHopDong());
						thongTinKhachHangMoi.setThuPbvmt(thongTinKhachHangHienTai.isThuPbvmt());
						thongTinKhachHangMoi.setNgayDangKy(thongTinKhachHangHienTai.getNgayDangKy());
						thongTinKhachHangMoi.setNgayLapDat(thongTinKhachHangHienTai.getNgayLapDat());
						thongTinKhachHangMoi.setNgayLapHopDong(thongTinKhachHangHienTai.getNgayLapHopDong());
						thongTinKhachHangMoi.setSoTaiKhoan(thongTinKhachHangHienTai.getSoTaiKhoan());
						thongTinKhachHangMoi.setTenTaiKhoan(thongTinKhachHangHienTai.getTenTaiKhoan());
						thongTinKhachHangMoi.setNgayGioCapNhat(thongTinKhachHangHienTai.getNgayGioCapNhat());
						thongTinKhachHangMoi.setViDo(thongTinKhachHangHienTai.getViDo());
						thongTinKhachHangMoi.setKinhDo(thongTinKhachHangHienTai.getKinhDo());
						thongTinKhachHangMoi.setDoiThiCong(thongTinKhachHangHienTai.isDoiThiCong());

						Thang thangThemThongTinKhachHang = new Thang();
						thangThemThongTinKhachHang.setIdThang(idThangHienTai);
						Set<Thang> dsThangSuDungThongTinKhachHang = new HashSet<Thang>();
						dsThangSuDungThongTinKhachHang.add(thangThemThongTinKhachHang);
						thongTinKhachHangMoi.setThang(thangThemThongTinKhachHang);
						thongTinKhachHangMoi.setThangs(dsThangSuDungThongTinKhachHang);
						thongTinKhachHangRepository.save(thongTinKhachHangMoi);
					}

					i++;
				}
			}

			return new Response(1, "Success").toString();

		} catch (IOException e) {
			System.out.println(e.getMessage());
			return new Response(-1, "Lỗi dữ liệu tại dòng " + i).toString();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new Response(-1, "Lỗi dữ liệu tại dòng " + i).toString();
		}
	}

	@Override
	public ThongTinKhachHangNganHangDto layKhachHangTheoMaKhachHang(String maKhachHang) {
		ThongTinKhachHang tt = thongTinKhachHangRepository.layThongTinThangGanNhatTheoMaKhachHang(maKhachHang);
		KhachHangDto khachHang = converter.convertToDtoIncludeMaDongHo(tt);
		return new ThongTinKhachHangNganHangDto(khachHang);
	}
}
