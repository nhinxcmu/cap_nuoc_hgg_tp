package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhuVuc;

public interface KhuVucRepository extends CrudRepository<KhuVuc, Long> {

	KhuVuc findTopByDonVi_IdDonViOrderByTenKhuVuc(Long idDonVi);

	List<KhuVuc> findByDonVi_IdDonViOrderByTenKhuVuc(Long idDonVi);

	List<KhuVuc> findByOrderByTenKhuVuc();

	@EntityGraph(attributePaths = { "donVi" })
	Page<KhuVuc> findByDonVi_IdDonViOrderByIdKhuVucDesc(Long idDonVi, Pageable pageable);

	@EntityGraph(attributePaths = { "donVi" })
	Page<KhuVuc> findByOrderByIdKhuVucDesc(Pageable pageable);

	@EntityGraph(attributePaths = { "donVi" })
	KhuVuc findByIdKhuVuc(Long idKhuVuc);

}
