package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DuongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.PhieuSuaSeriDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoGhiDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.FileBean;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.SearchForm;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DuongService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.PhieuSuaSeriService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.SoGhiService;

@Controller
@RequestMapping("/phieu-sua-seri")
public class PhieuSuaSeriController {
    private ModelAttr modelAttr = new ModelAttr("Phiếu sửa seri", "phieusuaseri",
            new String[] { "fancytree-2.23.0/jquery.fancytree-all-deps.min.js",
			"fancytree-2.23.0/jquery.fancytree.dnd.js", "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js",
			"bower_components/select2/dist/js/select2.min.js", "js/controllers/PhieuSuaSeriController.js" }, new String[] {});

    @Autowired
    private PhieuSuaSeriService service;
	@Autowired
	private KhuVucService khuVucServ;
	@Autowired
	private DuongService duongServ;
	@Autowired
	private SoGhiService soGhiServ;

    @GetMapping
    public String showPage(Model model) {
        model.addAttribute("MODEL", modelAttr);
        return "layout";
    }

    @RequestMapping("/lay-ds")
    public @ResponseBody Page<PhieuSuaSeriDto> layDs(@RequestBody SearchForm searchForm) {
        return service.layDs(Utilities.layIdNguoiDung(), searchForm.trangThai, searchForm.tuNgay,
                searchForm.denNgay, new PageRequest(searchForm.page, searchForm.pageSize));
    }

    @RequestMapping(value = "/luu")
	@ResponseBody
	public Response luu(@RequestBody PhieuSuaSeriDto phieuSuaSeri,Boolean gui) {
		try {
			phieuSuaSeri = service.luu(phieuSuaSeri);
			if(gui)
			{
				guiDuyet(phieuSuaSeri);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

    @RequestMapping(value = "/gui-duyet")
	@ResponseBody
	public Response guiDuyet(@RequestBody PhieuSuaSeriDto phieuSuaSeri) {
		try {
			service.guiDuyet(phieuSuaSeri);
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}
    
    @RequestMapping(value = "/xoa")
	@ResponseBody
	public Response xoa(@RequestBody PhieuSuaSeriDto phieuSuaSeri) {
		try {
			service.xoa(phieuSuaSeri);
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage());
		}
		return new Response(1, "Thành công");
	}

	@RequestMapping("/lay-ds-phuong")
	public @ResponseBody List<DuongDto> layDsDuong(HttpSession httpSession) {
		return duongServ.layDsDuong(Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc());
	}

	@RequestMapping("/lay-ds-to")
	public @ResponseBody List<SoGhiDto> layDsSoGhi(long idDuong) {
		return soGhiServ.layDsSoGhi(idDuong);
	}

	@GetMapping("/lay-ds-khu-vuc")
	public @ResponseBody List<KhuVucDto> layDsKhuVuc(HttpSession httpSession) {
		return khuVucServ.layDsKhuVuc(Utilities.layIdDonViLamViec(httpSession));
	}

	@RequestMapping("/xu-ly")
	public @ResponseBody String upload(FileBean uploadItem, BindingResult result) {
		return service.importChiSo(uploadItem);
	}
}
