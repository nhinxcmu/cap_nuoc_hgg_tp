package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;

@Component
public class ThangConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public ThangDto convertToDto(Thang entity) {
		if (entity == null) {
			return new ThangDto();
		}
		ThangDto dto = mapper.map(entity, ThangDto.class);
		return dto;
	}

	public Thang convertToEntity(ThangDto dto) {
		if (dto == null) {
			return new Thang();
		}
		Thang entity = mapper.map(dto, Thang.class);
		return entity;
	}

}
