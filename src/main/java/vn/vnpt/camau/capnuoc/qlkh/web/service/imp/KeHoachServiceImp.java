package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.KeHoachConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KeHoachDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Duong;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KeHoach;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DuongRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.KeHoachRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KeHoachService;

@Service
public class KeHoachServiceImp implements KeHoachService {

	@Autowired
	private KeHoachRepository repository;
	@Autowired
	private KeHoachConverter converter;
	@Autowired
	private DuongRepository duongRepository;
	@Autowired
	private ThangRepository thangRepository;

	@Override
	public KeHoachDto layKeHoach(long idKeHoach) {
		KeHoach keHoach = repository.findByIdKeHoach(idKeHoach);
		KeHoachDto keHoachDto = converter.convertToDto(keHoach);
		return keHoachDto;
	}

	@Override
	public KeHoachDto luuKeHoach(KeHoachDto dto) throws Exception {
		KeHoach keHoachCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(keHoachCanLuu);
			KeHoachDto keHoachDaChuyenDoi = converter.convertToDto(keHoachCanLuu);
			return keHoachDaChuyenDoi;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu kế hoạch.");
		}
	}

	private long layThangTruoc(long idThang) {
		String namTruoc, thangTruoc;
		long namHienTai = idThang / 100;
		long thangHienTai = idThang % 100;

		if (thangHienTai == 1) {
			thangTruoc = "12";
			namTruoc = "" + (namHienTai - 1);
		} else {
			if (thangHienTai - 1 < 10)
				thangTruoc = "0" + (thangHienTai - 1);
			else
				thangTruoc = "" + (thangHienTai - 1);

			namTruoc = "" + namHienTai;
		}

		return Long.parseLong(namTruoc + thangTruoc);
	}

	@Override
	public List<KeHoachDto> layDanhSach(long idKhuVuc, String nam, long idThang) {
		List<KeHoach> keHoachs;
		if (idThang == 0l) {
			List<Duong> duongs = duongRepository.findByKhuVuc_IdKhuVucOrderByTenDuong(idKhuVuc);
			for (Duong duong : duongs) {
				List<Thang> thangs = thangRepository.layDanhSachThangTheoNam(nam);
				for (Thang thang : thangs) {
					if (repository.findByDuong_IdDuongAndThang_IdThang(duong.getIdDuong(),
							thang.getIdThang()) == null) {
						KeHoach keHoachTruoc = repository.findByDuong_IdDuongAndThang_IdThang(duong.getIdDuong(),
								layThangTruoc(thang.getIdThang()));
						KeHoach keHoach = new KeHoach();
						keHoach.setThang(thang);
						keHoach.setDuong(duong);

						if (keHoachTruoc == null)
							keHoach.setChiSoCu(0);
						else
							keHoach.setChiSoCu(keHoachTruoc.getChiSoMoi());

						keHoach.setChiSoMoi(0);
						keHoach.setSanXuat(0);
						keHoach.setTieuThu(0);
						keHoach.setDoanhThu(0);
						repository.save(keHoach);
					}
				}
			}
			keHoachs = repository.layDanhSach(idKhuVuc, nam);
		} else {
			Thang thang = new Thang();
			thang.setIdThang(idThang);
			List<Duong> duongs = duongRepository.findByKhuVuc_IdKhuVucOrderByTenDuong(idKhuVuc);
			for (Duong duong : duongs) {
				if (repository.findByDuong_IdDuongAndThang_IdThang(duong.getIdDuong(), idThang) == null) {
					KeHoach keHoachTruoc = repository.findByDuong_IdDuongAndThang_IdThang(duong.getIdDuong(),
							layThangTruoc(idThang));
					KeHoach keHoach = new KeHoach();
					keHoach.setThang(thang);
					keHoach.setDuong(duong);

					if (keHoachTruoc == null)
						keHoach.setChiSoCu(0);
					else
						keHoach.setChiSoCu(keHoachTruoc.getChiSoMoi());

					keHoach.setChiSoMoi(0);
					keHoach.setSanXuat(0);
					keHoach.setTieuThu(0);
					keHoach.setDoanhThu(0);
					repository.save(keHoach);
				}
			}
			keHoachs = repository.layDanhSach(idKhuVuc, idThang);
		}

		List<KeHoachDto> ds = keHoachs.stream().map(converter::convertToDto).collect(Collectors.toList());

		return ds;
	}
}
