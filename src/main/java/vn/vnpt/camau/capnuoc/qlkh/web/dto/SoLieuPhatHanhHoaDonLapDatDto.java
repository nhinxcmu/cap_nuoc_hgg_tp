package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import lombok.Getter;
import lombok.Setter;

public class SoLieuPhatHanhHoaDonLapDatDto {

	@Getter
	@Setter
	private int tongHoaDon;
	@Getter
	@Setter
	private int tongDaPhatHanh;
	@Getter
	@Setter
	private int tongKhongPhatHanh;
	@Getter
	@Setter
	private int tongChuaPhatHanh;

}
