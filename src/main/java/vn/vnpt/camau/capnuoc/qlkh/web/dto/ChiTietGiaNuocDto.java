package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class ChiTietGiaNuocDto {

	@Getter
	@Setter
	@NotNull
	private Long idChiTietGiaNuoc;
	@Getter
	@Setter
	@Mapping("doiTuongBaoCao.idDoiTuongBaoCao")
	private Long idDoiTuongBaoCao;
	@Getter
	@Setter
	@Mapping("doiTuongBaoCao.tenDoiTuongBaoCao")
	private String tenDoiTuongBaoCao;
	@Getter
	@Setter
	@Mapping("giaNuoc.idGiaNuoc")
	private Long idGiaNuoc;
	@Getter
	@Setter
	private Integer tieuThuTu;
	@Getter
	@Setter
	private Integer tieuThuDen;
	@Getter
	@Setter
	private Integer phanTramTieuThu;
	@Getter
	@Setter
	@NotNull
	@Digits(integer = 13, fraction = 0)
	private BigDecimal mucGia;
	@Getter
	@Setter
	@NotNull
	private int thuTu;
	@Getter
	@Setter
	private int phiBvmt;
	@Getter
	@Setter
	private int phiBvr;
	
}
