package vn.vnpt.camau.capnuoc.qlkh.web.entity;

import java.util.Date;

public class SearchForm {
    public String searchString;
    public Long idTrangThaiHoaDon;
    public Integer page;
    public Integer pageSize;
    public Long idDuong;
    public Long idSoGhi;
    public Date tuNgay;
    public Date denNgay;
    public int trangThai;
}