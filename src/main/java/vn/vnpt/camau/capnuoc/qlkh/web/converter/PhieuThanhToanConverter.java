package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.PhieuThanhToanDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.PhieuThanhToan;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThongTinKhachHang;

@Component
public class PhieuThanhToanConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public PhieuThanhToanDto convertToDto(PhieuThanhToan entity) {
		if (entity == null) {
			return new PhieuThanhToanDto();
		}
		PhieuThanhToanDto dto = mapper.map(entity, PhieuThanhToanDto.class);
		setThongTinKhachHang(entity, dto);
		setMaDongHo(entity, dto);
		return dto;
	}

	private void setThongTinKhachHang(PhieuThanhToan entity, PhieuThanhToanDto dto) {
		if (!entity.getKhachHang().getThongTinKhachHangs().isEmpty()) {
			ThongTinKhachHang thongTinKhachHang = entity.getKhachHang().getThongTinKhachHangs().iterator().next();
			dto.setMaKhachHang(thongTinKhachHang.getMaKhachHang());
			dto.setTenKhachHang(thongTinKhachHang.getTenKhachHang());
			dto.setDiaChiSuDung(thongTinKhachHang.getDiaChiSuDung());
			dto.setDiaChiThanhToan(thongTinKhachHang.getDiaChiThanhToan());
			dto.setSoDienThoai(thongTinKhachHang.getSoDienThoai());
		}
	
	}

	private void setMaDongHo(PhieuThanhToan entity, PhieuThanhToanDto dto) {
		if (!entity.getKhachHang().getDongHoKhachHangThangs().isEmpty()) {
			DongHo dongHo = entity.getKhachHang().getDongHoKhachHangThangs().iterator().next().getDongHo();
			dto.setMaDongHo(dongHo.getMaDongHo());
		}
	}

}
