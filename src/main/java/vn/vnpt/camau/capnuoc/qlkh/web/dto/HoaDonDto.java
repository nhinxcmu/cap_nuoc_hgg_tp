package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class HoaDonDto {

	@Getter
	@Setter
	private Long idHoaDon;
	@Getter
	@Setter
	private Long idThongTinKhachHang;
	@Getter
	@Setter
	private String maKhachHang;
	@Getter
	@Setter
	private int thuTu;
	@Getter
	@Setter
	private String tenKhachHang;
	@Getter
	@Setter
	private String diaChiSuDung; // diaChiThanhToan
	@Getter
	@Setter
	private String soDienThoai;
	@Getter
	@Setter
	private boolean thuPbvmt;
	@Getter
	@Setter
	private Long idDoiTuong;
	@Getter
	@Setter
	private String tenDoiTuong;
	@Getter
	@Setter
	private int mucSuDung;
	@Getter
	@Setter
	private Long idSoGhi;
	@Getter
	@Setter
	private String tenSoGhi;
	@Getter
	@Setter
	@Mapping("nguoiDung.idNguoiDung")
	private Long idNguoiCapNhat;
	@Getter
	@Setter
	@Mapping("nguoiDung.tenNguoiDung")
	private String tenNguoiCapNhat;
	@Getter
	@Setter
	@Mapping("trangThaiChiSo.idTrangThaiChiSo")
	private long idTrangThaiChiSo;
	@Getter
	@Setter
	@Mapping("trangThaiChiSo.tenTrangThaiChiSo")
	private String tenTrangThaiChiSo;
	@Getter
	@Setter
	@Mapping("trangThaiHoaDon.idTrangThaiHoaDon")
	private Long idTrangThaiHoaDon;
	@Getter
	@Setter
	@Mapping("trangThaiHoaDon.tenTrangThaiHoaDon")
	private String tenTrangThaiHoaDon;
	@Getter
	@Setter
	private int chiSoCu;
	@Getter
	@Setter
	private int chiSoMoi;
	@Getter
	@Setter
	private int tieuThu;
	@Getter
	@Setter
	private long tienNuoc;
	@Getter
	@Setter
	private long thue;
	@Getter
	@Setter
	private long phiBvmt;
	@Getter
	@Setter
	private long tongTien;
	@Getter
	@Setter
	private String bangChu;
	@Getter
	@Setter
	private long tienThanhToan;
	@Getter
	@Setter
	private long tienConLai;
	@Getter
	@Setter
	private String ghiChu;
	@Getter
	@Setter
	private Date ngayGioCapNhat;
	@Getter
	@Setter
	private String lyDoCupNuoc;
	@Getter
	@Setter
	@Mapping("thang.idThang")
	private long idThang;
	@Getter
	@Setter
	@Mapping("thang.tenThang")
	private String tenThang;
	@Getter
	@Setter
	private long phiBvr;
	@Getter
	@Setter
	private Boolean hoaDonDau;
	@Getter
	@Setter
	private Boolean hoaDonCuoi;
	@Getter
	@Setter
	private boolean daIn;
	@Getter
	@Setter
	private Date ngayThanhToan;
	@Getter
	@Setter
	private int soLanIn;
	@Getter
	@Setter
	private long idDoiTuongCupNuoc;
	@Getter
	@Setter
	private String tenDoiTuongCupNuoc;
	@Getter
	@Setter
	private String lichSuCupNuoc;

	private boolean daThanhToan;

	public boolean isDaThanhToan() {
		return tienThanhToan > 0;
	}

	public void setDaThanhToan(boolean daThanhToan) {
		this.daThanhToan = daThanhToan;
	}

}
