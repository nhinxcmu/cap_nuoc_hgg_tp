package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThamSo;

public interface ThamSoRepository extends CrudRepository<ThamSo, Long> {

	Page<ThamSo> findByHienThiIsTrue(Pageable pageable);
	
	Page<ThamSo> findByHienThiIsTrueAndTenThamSoContains(String tenThamSo, Pageable pageable);

	List<ThamSo> findByIdThamSoInAndHienThiIsTrueOrderByIdThamSo(List<Long> dsIdThamSo);
	
}
