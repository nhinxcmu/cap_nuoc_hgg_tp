package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ChiSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuong;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHoKhachHangThang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDon;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LyDoCupNuoc;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ChiSo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.SoGhi;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThongTinKhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DongHoRepository;

@Component
public class ChiSoConverter {

	@Autowired
	private DozerBeanMapper mapper;
	@Autowired
	private DongHoRepository dHoRepository;
	@Autowired
	HttpSession httpSession;

	public ChiSoDto convertToDto(ChiSo entity) {
		if (entity == null) {
			return new ChiSoDto();
		}
		ChiSoDto dto = mapper.map(entity, ChiSoDto.class);
		Set<ThongTinKhachHang> dsThongTinKhachHang = entity.getHoaDon().getKhachHang().getThongTinKhachHangs();
		if (dsThongTinKhachHang.size() > 0) {
			ThongTinKhachHang thongTinKhachHang = dsThongTinKhachHang.iterator().next();
			DoiTuong doiTuong = thongTinKhachHang.getDoiTuong();
			SoGhi soGhi = thongTinKhachHang.getSoGhi();
			Set<DongHoKhachHangThang> dongHos = thongTinKhachHang.getKhachHang().getDongHoKhachHangThangs();
			Iterator<DongHoKhachHangThang> i = dongHos.iterator();
			while (i.hasNext() && dto.getIdDongHo() == null) {
				DongHoKhachHangThang dhkht = i.next();
				Thang thang = dhkht.getThang();
				if (thang.getIdThang() == Utilities.layThangLamViec(httpSession).getIdThang()) {
					setDongHo(dhkht.getDongHo(), dto);
				}
			}
			// DongHo dongHo =
			// dHoRepository.findByDongHoKhachHangThangs_Id_IdKhachHangAndDongHoKhachHangThangs_Id_IdThang(
			// thongTinKhachHang.getKhachHang().getIdKhachHang(),
			// Utilities.layThangLamViec(httpSession).getIdThang());
			// setDongHo(dongHo, dto);
			setThongTinKhachHang(thongTinKhachHang, dto);
			setDoiTuong(doiTuong, dto);
			setSoGhi(soGhi, dto);

			if (entity.getHoaDon().getLyDoCupNuocs().size() > 0) {
				setLyDoCupNuoc(entity.getHoaDon().getLyDoCupNuocs().iterator().next(), dto);
			}
		}
		return dto;
	}

	private void setThongTinKhachHang(ThongTinKhachHang thongTinKhachHang, ChiSoDto dto) {
		dto.setMaKhachHang(thongTinKhachHang.getMaKhachHang());
		dto.setThuTu(thongTinKhachHang.getThuTu());
		dto.setTenKhachHang(thongTinKhachHang.getTenKhachHang());
		dto.setDiaChiSuDung(thongTinKhachHang.getDiaChiSuDung());
	}

	private void setDoiTuong(DoiTuong doiTuong, ChiSoDto dto) {
		if (doiTuong != null) {
			dto.setIdDoiTuong(doiTuong.getIdDoiTuong());
			dto.setTenDoiTuong(doiTuong.getTenDoiTuong());
			dto.setMucSuDung(doiTuong.getMucSuDung());
		}
	}

	private void setSoGhi(SoGhi soGhi, ChiSoDto dto) {
		if (soGhi != null) {
			dto.setIdSoGhi(soGhi.getIdSoGhi());
			dto.setTenSoGhi(soGhi.getTenSoGhi());
		}
	}

	public ChiSo convertHoaDonToChiSo(HoaDon hoaDon) {
		if (hoaDon == null) {
			return new ChiSo();
		}
		ChiSo chiSo = new ChiSo();
		if (hoaDon.getChiSos().size() > 0) {
			chiSo = hoaDon.getChiSos().iterator().next();
		}
		chiSo.setTrangThaiChiSo(hoaDon.getTrangThaiChiSo());
		chiSo.setHoaDon(hoaDon);
		chiSo.setThang(hoaDon.getThang());
		chiSo.setChiSoCu(hoaDon.getChiSoCu());
		chiSo.setChiSoMoi(hoaDon.getChiSoMoi());
		chiSo.setNgayGioCapNhat(new Date());
		chiSo.setGhiChu("");
		setNguoiCapNhat(chiSo);
		return chiSo;
	}

	private void setNguoiCapNhat(ChiSo chiSo) {
		NguoiDung nguoiCapNhat = new NguoiDung();
		long idNguoiDungHienHanh = Utilities.layIdNguoiDung();
		nguoiCapNhat.setIdNguoiDung(idNguoiDungHienHanh);
		chiSo.setNguoiDungByIdNguoiCapNhat(nguoiCapNhat);
	}

	public Long convertToIdChiSo(ChiSoDto dto) {
		return dto.getIdChiSo();
	}

	void setDongHo(DongHo dongHo, ChiSoDto chiSo) {
		if (dongHo != null) {
			chiSo.setIdDongHo(dongHo.getIdDongHo());
			chiSo.setSoSeri(dongHo.getSoSeri());
		}
	}

	void setLyDoCupNuoc(LyDoCupNuoc lyDo, ChiSoDto chiSo) {
		chiSo.setLyDoCupNuoc(lyDo.getLyDo());
		chiSo.setTenDoiTuongCupNuoc(lyDo.getDoiTuongCupNuoc().getTenDoiTuongCupNuoc());
	}

}
