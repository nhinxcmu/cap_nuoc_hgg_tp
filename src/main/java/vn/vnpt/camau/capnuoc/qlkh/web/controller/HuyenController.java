package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HuyenDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HuyenService;


@Controller
@RequestMapping("/huyen")
public class HuyenController {
	private ModelAttr modelAttr = new ModelAttr("Danh mục huyện/thành phố", "huyen", new String[]{"jdgrid/jdgrid-v3.js","jdpage/jdpage.js","js/huyen.js"},
			new String[]{"jdgrid/jdgrid.css"});
	@Autowired
	private HuyenService huyenServ;
	
	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
	    return "layout";
	}
	
	@GetMapping("/lay-ds")
	public @ResponseBody Page<HuyenDto> layDanhSach(Pageable page){
		return huyenServ.layDsHuyen(page);
	}
	
	@GetMapping("/lay-ct")
	public @ResponseBody HuyenDto layChiTiet(long id){
		return huyenServ.layHuyen(id);
	}
	
	@PostMapping("/luu")
	public @ResponseBody String luu(HuyenDto dto){
		try {
			huyenServ.luuHuyen(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
	
	@PostMapping("/xoa")
	public @ResponseBody String xoa(long id){
		try {
			huyenServ.xoaHuyen(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
//			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
