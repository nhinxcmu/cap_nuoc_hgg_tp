package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import vn.vnpt.camau.capnuoc.qlkh.web.service.CallReportService;

@Service
public class CallReportServiceImp implements CallReportService {
	@Value("${report.path}")
	private String reportPath;

	@Override
	public String viewReport(File reportFile, Map<String, Object> parameters, DataSource dataSource,
			HttpSession httpSession) throws Exception {
		Connection conn = dataSource.getConnection();
		JasperPrint print = JasperFillManager.fillReport(reportFile.getPath(), parameters, conn);
		conn.close();
		File directory = new File(reportPath + httpSession.getId());
		if (!directory.exists()) {
			directory.mkdir();
		}
		String fileName = reportFile.getName().substring(0, reportFile.getName().length() - 6).replaceAll("\\d", "");
		String exportDir = reportPath + httpSession.getId();
		String exportPath = exportDir + "/" + fileName + "pdf";
		JRPdfExporter exporter = new JRPdfExporter();
		exporter.setExporterInput(new SimpleExporterInput(print));
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(exportPath));
		exporter.exportReport();
		return "report/" + httpSession.getId() + "/" + fileName + "pdf";
	}

	@Override
	public String viewListReport(List<JasperPrint> jasperPrintList, File reportFile, HttpSession httpSession)
			throws Exception {
		File directory = new File(reportPath + httpSession.getId());
		if (!directory.exists()) {
			directory.mkdir();
		}
		String fileName = reportFile.getName().substring(0, reportFile.getName().length() - 6).replaceAll("\\d", "");
		String exportDir = reportPath + httpSession.getId();
		String exportPath = exportDir + "/" + fileName + "pdf";
		JRPdfExporter exporter = new JRPdfExporter();
		exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(exportPath));
		exporter.exportReport();
		return "report/" + httpSession.getId() + "/" + fileName + "pdf";
	}

	@Override
	public void printReport(String type, File reportFile, Map<String, Object> parameters, DataSource dataSource,
			HttpSession httpSession, HttpServletResponse response) throws Exception {
		Connection conn = dataSource.getConnection();
		JasperPrint print = JasperFillManager.fillReport(reportFile.getPath(), parameters, conn);
		conn.close();
		File directory = new File(reportPath + httpSession.getId());
		if (!directory.exists()) {
			directory.mkdir();
		}
		String fileName = reportFile.getName().substring(0, reportFile.getName().length() - 6).replaceAll("\\d", "");
		File file = new File("/" + fileName + type);
		if (type.equalsIgnoreCase("pdf")) {
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(print));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));
			exporter.exportReport();
			if (file.exists()) {
				response.setContentType("application/pdf");
				response.setContentLength(new Long(file.length()).intValue());
				response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
				FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
			}
		}
		if (type.equalsIgnoreCase("xls")) {
			JRXlsExporter exporter = new JRXlsExporter();
			exporter.setExporterInput(new SimpleExporterInput(print));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));
			exporter.exportReport();
			if (file.exists()) {
				response.setContentType("application/xls");
				response.setContentLength(new Long(file.length()).intValue());
				response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
				FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
			}
		}
		if (type.equalsIgnoreCase("xlsx")) {
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(print));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));
			exporter.exportReport();
			if (file.exists()) {
				response.setContentType("application/xlsx");
				response.setContentLength(new Long(file.length()).intValue());
				response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
				FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
			}
		}
		if (type.equalsIgnoreCase("rtf")) {
			JRRtfExporter exporter = new JRRtfExporter();
			exporter.setExporterInput(new SimpleExporterInput(print));
			exporter.setExporterOutput(new SimpleWriterExporterOutput(file));
			exporter.exportReport();
			if (file.exists()) {
				response.setContentType("application/rtf");
				response.setContentLength(new Long(file.length()).intValue());
				response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
				FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
			}
		}
	}

	@Override
	public void printListReport(List<JasperPrint> jasperPrintList, String type, File reportFile,
			HttpSession httpSession, HttpServletResponse response) throws Exception {
		File directory = new File(reportPath + httpSession.getId());
		if (!directory.exists()) {
			directory.mkdir();
		}
		String fileName = reportFile.getName().substring(0, reportFile.getName().length() - 6).replaceAll("\\d", "");
		File file = new File("/" + fileName + type);
		if (type.equalsIgnoreCase("pdf")) {
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));
			exporter.exportReport();
			if (file.exists()) {
				response.setContentType("application/pdf");
				response.setContentLength(new Long(file.length()).intValue());
				response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
				FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
			}
		}
		if (type.equalsIgnoreCase("xls")) {
			JRXlsExporter exporter = new JRXlsExporter();
			exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));
			exporter.exportReport();
			if (file.exists()) {
				response.setContentType("application/xls");
				response.setContentLength(new Long(file.length()).intValue());
				response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
				FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
			}
		}
		if (type.equalsIgnoreCase("xlsx")) {
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));
			exporter.exportReport();
			if (file.exists()) {
				response.setContentType("application/xlsx");
				response.setContentLength(new Long(file.length()).intValue());
				response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
				FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
			}
		}
		if (type.equalsIgnoreCase("rtf")) {
			JRRtfExporter exporter = new JRRtfExporter();
			exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
			exporter.setExporterOutput(new SimpleWriterExporterOutput(file));
			exporter.exportReport();
			if (file.exists()) {
				response.setContentType("application/rtf");
				response.setContentLength(new Long(file.length()).intValue());
				response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
				FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
			}
		}
	}
}
