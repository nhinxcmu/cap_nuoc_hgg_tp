package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.BienNhanThuTienNuocMobileDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDeNhanTinDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonNganHangDto;

public interface HoaDonService {

	HoaDonDto themHoaDon(long id) throws Exception;

	void xoaHoaDon(long id) throws Exception;

	List<Long> layDsIdHoaDon(long idDuong, long idSoGhi, String thongTinCanTim, long idTrangThaiChiSo,
			long idTrangThaiHoaDon, int daThanhToan);

	Page<HoaDonDto> layDsHoaDon(long idKhuVuc, long idDuong, long idSoGhi, String thongTinCanTim, long idTrangThaiChiSo,
			long idTrangThaiHoaDon, int daThanhToan, Pageable pageable);

	HoaDonDto layHoaDon(long id);

	HoaDonDto luuChiSoMoi(long id, int chiSoMoi) throws Exception;

	HoaDonDto khoanTieuThu(long id, int tieuThu) throws Exception;

	HoaDonDto luuKhongXai(long id) throws Exception;

	HoaDonDto luuDaIn(long id) throws Exception;

	HoaDonDto luuChiSoCu(long id, int chiSoCu) throws Exception;

	HoaDonDto luuChuaGhiChiSo(long id) throws Exception;

	HoaDonDto luuCupNuoc(long id, String lyDoCup, long idDoiTuong, String logCupNuoc) throws Exception;

	HoaDonDto luuMoNuoc(long id, String logMoNuoc) throws Exception;

	void phatHanhHoaDon(long id) throws Exception;

	void ngungPhatHanhHoaDon(long id) throws Exception;

	void capNhatChiSoNuocBangFileExcel(List<HoaDonDto> dsHoaDon) throws Exception;

	void phatHanhHoaDonHangLoat() throws Exception;

	void ngungPhatHanhHoaDonHangLoat() throws Exception;

	void ngungPhatHanhHoaDonHangLoatTheoFKey() throws Exception;

	List<HoaDonDto> layDsHoaDon(long idThongTinKhachHang);

	long thanhToanHoaDonTaiGiaoDich(List<Long> dsIdHoaDon) throws Exception;

	// lấy ds hóa đơn: 0 - chưa gửi tin; 1 - gửi thành công; 2 - gửi bị lỗi
	List<HoaDonDeNhanTinDto> layDsHoaDonDeNhanTin(String thongTinCanTim, int nhanTin);

	String layDsIdHoaDonTheoPhieuThanhToan(long idPhieuThanhToan);

	void thanhToanHoaDonChuyenKhoan() throws Exception;

	void thanhToanHoaDonGiaoDich(boolean tatCa) throws Exception;

	// mobile
	List<HoaDonDto> layDsHoaDon(long idThongTinKhachHang, long idNguoiDung) throws Exception;

	List<HoaDonDto> layDsHoaDonChuaThanhToan(long idThongTinKhachHang, long idNguoiDung) throws Exception;

	long thanhToanHoaDonTaiNha(List<Long> dsIdHoaDon, long idNguoiDung) throws Exception;

	List<BienNhanThuTienNuocMobileDto> layDSBienNhanThuTienNuocMobileDto(long idPhieuThanhToan, boolean xem);

	HoaDonDto luuChiSoMoi(String maKhachHang, int chiSoMoi) throws Exception;

	List<HoaDonNganHangDto> layDsHoaDonTheoMaKhachHang(String maKhachHang);

	long thanhToanHoaDonTrucTuyenNganHang(List<Long> dsIdHoaDon) throws Exception;

	long thanhToanHoaDonTrucTuyenViDienTu(List<Long> dsIdHoaDon) throws Exception;
}
