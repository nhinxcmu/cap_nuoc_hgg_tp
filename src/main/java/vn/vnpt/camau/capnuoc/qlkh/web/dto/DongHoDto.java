package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class DongHoDto {

	@Getter
	@Setter
	private Long idDongHo;
	@Getter
	@Setter
	@Mapping("trangThaiDongHo.idTrangThaiDongHo")
	private Long idTrangThaiDongHo;
	@Getter
	@Setter
	@Mapping("trangThaiDongHo.tenTrangThaiDongHo")
	private String tenTrangThaiDongHo;
	@Getter
	@Setter
	private String maDongHo;
	@Getter
	@Setter
	private String chungLoai;
	@Getter
	@Setter
	private String soSeri;
	@Getter
	@Setter
	private String kichCo;
	@Getter
	@Setter
	private Integer heSoNhan;
	@Getter
	@Setter
	private Integer chiSoCucDai;
	@Getter
	@Setter
	private String nhaSanXuat;
	@Getter
	@Setter
	private Date ngaySanXuat;
	@Getter
	@Setter
	private Date ngayDuaVaoSuDung;
	@Getter
	@Setter
	private Date ngayHetHanBaoHanh;
	@Getter
	@Setter
	private Date ngayKiemDinh;
	@Getter
	@Setter
	private Date ngaySuaChuaLanCuoi;
	@Getter
	@Setter
	private String ghiChuSuaChuaLanCuoi;
	@Getter
	@Setter
	private long idKhachHang;
	
}
