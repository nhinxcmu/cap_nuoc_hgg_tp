package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ChiSo;

public interface ChiSoRepository extends CrudRepository<ChiSo, Long> {

	ChiSo findByHoaDon_IdHoaDonAndThang_IdThang(Long idHoaDon, Long idThang);

	@Query("select cs from ChiSo cs join cs.hoaDon hd join hd.khachHang kh join kh.thongTinKhachHangs ttkh "
				+ "join ttkh.thangs ttkht join ttkh.soGhi sg join sg.lichGhiThus lgt join lgt.thangs lgtt "
			+ "where cs.thang.idThang = ?1 and ttkht.idThang = ?1 and lgtt.idThang = ?1 and lgt.nguoiDuocPhanCong.idNguoiDung = ?2 "
				+ "and cs.huy = 0")
	@EntityGraph(attributePaths = { "hoaDon.khachHang.thongTinKhachHangs.doiTuong", "trangThaiChiSo",
			"nguoiDungByIdNguoiDuyet", "nguoiDungByIdNguoiCapNhat", "hoaDon.khachHang.thongTinKhachHangs.soGhi", "nguoiDungByIdNguoiDongBo","hoaDon.lyDoCupNuocs.doiTuongCupNuoc"})
	List<ChiSo> layDsChiSoTheoNguoiDung(Long idThang, Long idNguoiDung);

	Integer countByThang_IdThangAndHoaDon_KhachHang_ThongTinKhachHangs_SoGhi_Duong_KhuVuc_IdKhuVucAndHoaDon_KhachHang_ThongTinKhachHangs_Thangs_IdThangAndHuyIsFalse(
			Long idThangChiSo, Long idKhuVuc, Long idThangKhachHang);

	@EntityGraph(attributePaths = { "hoaDon", "nguoiDungByIdNguoiDuyet", "thang" })
	List<ChiSo> findByThang_IdThangAndIdChiSoIn(Long idThang, List<Long> dsIdChiSo);

	@Query("select cs from ChiSo cs join cs.hoaDon hd join hd.khachHang kh join kh.thongTinKhachHangs ttkh "
				+ "join ttkh.thangs ttkht join ttkh.soGhi sg join sg.lichGhiThus lgt join lgt.thangs lgtt "
			+ "where cs.thang.idThang = ?1 and ttkht.idThang = ?1 and lgtt.idThang = ?1 and lgt.nguoiDuocPhanCong.idNguoiDung = ?2 "
				+ "and cs.huy = 0 "
			+ "order by ttkh.thuTu")
	@EntityGraph(attributePaths = { "hoaDon.khachHang.thongTinKhachHangs.doiTuong", "trangThaiChiSo",
			"nguoiDungByIdNguoiDuyet", "nguoiDungByIdNguoiCapNhat", "hoaDon.khachHang.thongTinKhachHangs.soGhi" })
	Page<ChiSo> layDsChiSoTheoNguoiDung(Long idThang, Long idNguoiDung, Pageable pageable);

	@Query("select cs from ChiSo cs join cs.hoaDon hd join hd.khachHang kh join kh.thongTinKhachHangs ttkh "
				+ "join ttkh.thangs ttkht join ttkh.soGhi sg join sg.lichGhiThus lgt join lgt.thangs lgtt "
			+ "where cs.thang.idThang = ?1 and ttkht.idThang = ?1 and lgtt.idThang = ?1 and lgt.nguoiDuocPhanCong.idNguoiDung = ?2 "
				+ "and sg.idSoGhi = ?3 and cs.huy = 0 "
			+ "order by ttkh.thuTu")
	@EntityGraph(attributePaths = { "hoaDon.khachHang.thongTinKhachHangs.doiTuong", "trangThaiChiSo",
			"nguoiDungByIdNguoiDuyet", "nguoiDungByIdNguoiCapNhat", "hoaDon.khachHang.thongTinKhachHangs.soGhi" })
	Page<ChiSo> layDsChiSoTheoNguoiDungVaSoGhi(Long idThang, Long idNguoiDung, Long idSoGhi, Pageable pageable);

	@Query("select cs from ChiSo cs join cs.hoaDon hd join hd.khachHang kh join kh.thongTinKhachHangs ttkh "
				+ "join ttkh.thangs ttkht join ttkh.soGhi sg join sg.lichGhiThus lgt join lgt.thangs lgtt "
			+ "where cs.thang.idThang = ?1 and ttkht.idThang = ?1 and lgtt.idThang = ?1 and lgt.nguoiDuocPhanCong.idNguoiDung = ?2 "
				+ "and cs.trangThaiChiSo.idTrangThaiChiSo in ?3 and cs.huy = 0 and cs.duyetChiSo = 0 "
			+ "order by ttkh.thuTu")
	@EntityGraph(attributePaths = { "hoaDon.khachHang.thongTinKhachHangs.doiTuong", "trangThaiChiSo",
			"nguoiDungByIdNguoiDuyet", "nguoiDungByIdNguoiCapNhat", "hoaDon.khachHang.thongTinKhachHangs.soGhi" })
	List<ChiSo> layDsChiSoTheoNguoiDung(Long idThang, Long idNguoiDung, List<Long> dsIdTrangThaiChiSo);

	@Query("select cs from ChiSo cs join cs.hoaDon hd join hd.khachHang kh join kh.thongTinKhachHangs ttkh "
				+ "join ttkh.thangs ttkht join ttkh.soGhi sg join sg.lichGhiThus lgt join lgt.thangs lgtt "
			+ "where cs.thang.idThang = ?1 and ttkht.idThang = ?1 and lgtt.idThang = ?1 and lgt.nguoiDuocPhanCong.idNguoiDung = ?2 "
				+ "and sg.idSoGhi = ?3 and cs.trangThaiChiSo.idTrangThaiChiSo in ?4 and cs.huy = 0 and cs.duyetChiSo = 0 "
			+ "order by ttkh.thuTu")
	@EntityGraph(attributePaths = { "hoaDon.khachHang.thongTinKhachHangs.doiTuong", "trangThaiChiSo",
			"nguoiDungByIdNguoiDuyet", "nguoiDungByIdNguoiCapNhat", "hoaDon.khachHang.thongTinKhachHangs.soGhi" })
	List<ChiSo> layDsChiSoTheoNguoiDung(Long idThang, Long idNguoiDung, Long idSoGhi, List<Long> dsIdTrangThaiChiSo);

	@Query("select cs from ChiSo cs join cs.hoaDon hd join hd.khachHang kh join kh.thongTinKhachHangs ttkh "
				+ "join ttkh.thangs ttkht join ttkh.soGhi sg join sg.lichGhiThus lgt join lgt.thangs lgtt "
			+ "where cs.thang.idThang = ?1 and ttkht.idThang = ?1 and lgtt.idThang = ?1 and lgt.nguoiDuocPhanCong.idNguoiDung = ?2 "
				+ "and cs.trangThaiChiSo.idTrangThaiChiSo in ?3 and cs.huy = 0 "
			+ "order by ttkh.thuTu")
	@EntityGraph(attributePaths = { "hoaDon", "trangThaiChiSo" })
	List<ChiSo> layDsChiSoTheoNguoiDungDeKiemDuyet(Long idThang, Long idNguoiDung, List<Long> dsIdTrangThaiChiSo);

	@Query("select cs from ChiSo cs join cs.hoaDon hd join hd.khachHang kh join kh.thongTinKhachHangs ttkh "
				+ "join ttkh.thangs ttkht join ttkh.soGhi sg join sg.lichGhiThus lgt join lgt.thangs lgtt "
			+ "where cs.thang.idThang = ?1 and ttkht.idThang = ?1 and lgtt.idThang = ?1 and lgt.nguoiDuocPhanCong.idNguoiDung = ?2 "
				+ "and sg.idSoGhi = ?3 and cs.trangThaiChiSo.idTrangThaiChiSo in ?4 and cs.huy = 0 "
			+ "order by ttkh.thuTu")
	@EntityGraph(attributePaths = { "hoaDon", "trangThaiChiSo" })
	List<ChiSo> layDsChiSoTheoNguoiDungDeKiemDuyet(Long idThang, Long idNguoiDung, Long idSoGhi, List<Long> dsIdTrangThaiChiSo);

	@Query("select cs from ChiSo cs join cs.hoaDon hd join hd.khachHang kh join kh.thongTinKhachHangs ttkh "
				+ "join ttkh.thangs ttkht join ttkh.soGhi sg join sg.lichGhiThus lgt join lgt.thangs lgtt "
			+ "where cs.thang.idThang = ?1 and ttkht.idThang = ?1 and lgtt.idThang = ?1 and cs.idChiSo in ?2 "
				+ "and cs.trangThaiChiSo.idTrangThaiChiSo in ?3 and cs.huy = 0 "
			+ "order by ttkh.thuTu")
	@EntityGraph(attributePaths = { "hoaDon", "trangThaiChiSo" })
	List<ChiSo> layDsChiSoTheoDsIdChiSoDeKiemDuyet(Long idThang, List<Long> dsIdChiSo, List<Long> dsIdTrangThaiChiSo);

	@Procedure(procedureName = "soLuongDuyetChiSoTheoKhuVuc")
	Integer laySoLuongDuyetChiSoTheoKhuVuc(Long idThang, Long idKhuVuc);

	@Procedure(procedureName = "soLuongDuyetChiSoTheoNhanVienGhiThu")
	Integer laySoLuongDuyetChiSoTheoNhanVienGhiThu(Long idThang, Long idNguoiDung);

}
