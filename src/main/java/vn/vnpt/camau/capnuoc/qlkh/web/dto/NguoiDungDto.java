package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class NguoiDungDto {

	@Getter
	@Setter
	@NotNull
	private Long idNguoiDung;
	@Getter
	@Setter
	@Mapping("donVi.idDonVi")
	private Long idDonVi;
	@Getter
	@Setter
	@Mapping("donVi.tenDonVi")
	private String tenDonVi;
	@Getter
	@Setter
	@Mapping("vaiTro.idVaiTro")
	private Long idVaiTro;
	@Getter
	@Setter
	@Mapping("vaiTro.tenVaiTro")
	private String tenVaiTro;
	@Getter
	@Setter
	@NotNull
	@Size(max = 50)
	private String tenNguoiDung;
	@Getter
	@Setter
	@NotNull
	@Size(max = 250)
	private String hoTen;
	@Getter
	@Setter
	@NotNull
	private boolean khoa;

}
