package vn.vnpt.camau.capnuoc.qlkh.web.config;

import java.io.File;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class HttpSessionListenerImp implements HttpSessionListener {

	@Value("${report.path}")	
	private String reportPath;
	private final Logger logger = LoggerFactory.getLogger(HttpSessionListenerImp.class);
	
	@Override
	public void sessionCreated(HttpSessionEvent sessionEvent) {
		String idSession = sessionEvent.getSession().getId();
		
		logger.info(String.format("Session [%s] created.", idSession));
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		String idSession = sessionEvent.getSession().getId();
		File directory = new File(reportPath + idSession);
		deleteDirectory(directory);
		logger.info(String.format("Session [%s] destroyed.", idSession));
	}

	private boolean deleteDirectory(File directory) {
		if(directory.exists()){
	        File[] files = directory.listFiles();
	        if(null!=files){
	            for(int i=0; i<files.length; i++) {
	                if(files[i].isDirectory()) {
	                    deleteDirectory(files[i]);
	                }
	                else {
	                    files[i].delete();
	                }
	            }
	        }
	        return(directory.delete());
	    }
	    return false;
	}

}
