package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.DongHoConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DongHoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHoKhachHangThang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHoKhachHangThangId;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.KhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.TrangThaiDongHo;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DongHoKhachHangThangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DongHoRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DongHoService;

@Service
public class DongHoServiceImp implements DongHoService {

	@Autowired
	private DongHoRepository repository;
	@Autowired
	private DongHoConverter converter;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private DongHoKhachHangThangRepository dongHoKhachHangThangRepository;

	@Override
	@Transactional
	public DongHoDto luuDongHo(DongHoDto dto) throws Exception {
		long idKhachHang = dto.getIdKhachHang();
		DongHo dongHoCanLay = layDongHo2(dto.getMaDongHo(), idKhachHang);
		thietLapDongHo(dongHoCanLay, dto);
		luuDongHo(dongHoCanLay);
		luuDongHoKhachHangThang(idKhachHang, dongHoCanLay.getIdDongHo());
		DongHoDto dongHoDaChuyenDoi = converter.convertToDto(dongHoCanLay);
		return dongHoDaChuyenDoi;
	}

	private void thietLapDongHo(DongHo dongHo, DongHoDto dto) {
		dongHo.setChiSoCucDai(dto.getChiSoCucDai());
		dongHo.setChungLoai(dto.getChungLoai());
		dongHo.setGhiChuSuaChuaLanCuoi(dto.getGhiChuSuaChuaLanCuoi());
		dongHo.setHeSoNhan(dto.getHeSoNhan());
		dongHo.setKichCo(dto.getKichCo());
		dongHo.setMaDongHo(dto.getMaDongHo());
		dongHo.setNgayDuaVaoSuDung(dto.getNgayDuaVaoSuDung());
		dongHo.setNgayHetHanBaoHanh(dto.getNgayHetHanBaoHanh());
		dongHo.setNgayKiemDinh(dto.getNgayKiemDinh());
		dongHo.setNgaySanXuat(dto.getNgaySanXuat());
		dongHo.setNgaySuaChuaLanCuoi(dto.getNgaySuaChuaLanCuoi());
		dongHo.setNhaSanXuat(dto.getNhaSanXuat());
		dongHo.setSoSeri(dto.getSoSeri());
		thietLapTrangThaiDongHo(dongHo, dto.getIdTrangThaiDongHo());
	}

	private void thietLapTrangThaiDongHo(DongHo dongHo, Long idTrangThaiDongHo) {
		TrangThaiDongHo trangThaiDongHo = new TrangThaiDongHo();
		trangThaiDongHo.setIdTrangThaiDongHo(idTrangThaiDongHo);
		dongHo.setTrangThaiDongHo(trangThaiDongHo);
	}

	private void luuDongHo(DongHo dongHoCanLay) throws Exception {
		try {
			repository.save(dongHoCanLay);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu đồng hồ.");
		}
	}

	private void luuDongHoKhachHangThang(long idKhachHang, long idDongHo) throws Exception {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		DongHoKhachHangThang dongHoKhachHangThang = dongHoKhachHangThangRepository
				.findByDongHo_IdDongHoAndId_IdThang(idDongHo, idThangLamViec);
		if (dongHoKhachHangThang == null) {
			dongHoKhachHangThang = new DongHoKhachHangThang();
			DongHo dongHo = new DongHo();
			dongHo.setIdDongHo(idDongHo);
			dongHoKhachHangThang.setDongHo(dongHo);
			DongHoKhachHangThangId id = new DongHoKhachHangThangId();
			id.setIdKhachHang(idKhachHang);
			id.setIdThang(idThangLamViec);
			dongHoKhachHangThang.setId(id);
			Thang thang = new Thang();
			thang.setIdThang(idThangLamViec);
			dongHoKhachHangThang.setThang(thang);
		}
		thietLapKhachHang(dongHoKhachHangThang, idKhachHang);
		try {
			dongHoKhachHangThangRepository.save(dongHoKhachHangThang);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu đồng hồ khách hàng tháng.");
		}
	}

	private void thietLapKhachHang(DongHoKhachHangThang dongHoKhachHangThang, long idKhachHang) {
		KhachHang khachHang = new KhachHang();
		khachHang.setIdKhachHang(idKhachHang);
		dongHoKhachHangThang.setKhachHang(khachHang);
	}

	@Override
	@Transactional
	public void xoaDongHo(long idDongHo, long idKhachHang) throws Exception {
		DongHo dongHoCanKiemTra = repository.findByIdDongHoAndDongHoKhachHangThangs_Id_IdKhachHangNot(idDongHo,
				idKhachHang);
		if (dongHoCanKiemTra != null) {
			throw new Exception("Không thể xóa đồng hồ vì có khách hàng khác sử dụng đồng hồ.");
		}
		xoaDongHoKhachHangThang(idDongHo, idKhachHang);
		xoaDongHo(idDongHo);
	}

	private void xoaDongHoKhachHangThang(long idDongHo, long idKhachHang) throws Exception {
		try {
			dongHoKhachHangThangRepository.deleteByDongHo_IdDongHoAndId_IdKhachHang(idDongHo, idKhachHang);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể xóa đồng hồ khách hàng tháng.");
		}
	}

	private void xoaDongHo(long idDongHo) throws Exception {
		try {
			repository.delete(idDongHo);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể xóa đồng hồ.");
		}
	}

	@Override
	public DongHoDto layDongHoTheoKhachHang(long idKhachHang) {
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		DongHo dongHoCanLay = repository.findByDongHoKhachHangThangs_Id_IdKhachHangAndDongHoKhachHangThangs_Id_IdThang(
				idKhachHang, idThangLamViec);
		DongHoDto dongHoDaChuyenDoi = converter.convertToDto(dongHoCanLay);
		return dongHoDaChuyenDoi;
	}

	@Override
	public DongHoDto layDongHo(String maDongHo, long idKhachHang) throws Exception {
		DongHo dongHoCanLay = layDongHo2(maDongHo, idKhachHang);
		DongHoDto dongHoDaChuyenDoi = converter.convertToDto(dongHoCanLay);
		return dongHoDaChuyenDoi;
	}

	private DongHo layDongHo2(String maDongHo, long idKhachHang) throws Exception {
		DongHo dongHoCanLay = repository.findByMaDongHo(maDongHo);
		if (dongHoCanLay == null) {
			return new DongHo();
		}
		long idThangLamViec = Utilities.layThangLamViec(httpSession).getIdThang();
		DongHoKhachHangThang dongHoKhachHangThang = dongHoCanLay.getDongHoKhachHangThangs().stream()
				.filter(d -> d.getId().getIdThang() == idThangLamViec).findFirst().orElse(null);
		if (dongHoKhachHangThang != null && dongHoKhachHangThang.getId().getIdKhachHang() != idKhachHang) {
			throw new Exception("Đồng hồ đang thuộc khách hàng khác.");
		}
		return dongHoCanLay;
	}

	@Override
	public Page<DongHoDto> layDsDongHoTheoKhachHang(long idKhachHang, Pageable pageable) {
		Page<DongHo> dsDongHoCanLay = repository
				.findDistinctByDongHoKhachHangThangs_Id_IdKhachHangOrderByIdDongHoDesc(idKhachHang, pageable);
		Page<DongHoDto> dsDongHoDaChuyenDoi = dsDongHoCanLay.map(converter::convertToDto);
		return dsDongHoDaChuyenDoi;
	}

	@Override
	public void xoaDongHoKhachHangThangTheoThang(long idKhachHang, long idThang) {
		DongHoKhachHangThang dongHoKhachHangThang = dongHoKhachHangThangRepository
				.findByKhachHang_IdKhachHangAndId_IdThang(idKhachHang, idThang);
		if (dongHoKhachHangThang != null)
			dongHoKhachHangThangRepository.delete(dongHoKhachHangThang);
	}

}
