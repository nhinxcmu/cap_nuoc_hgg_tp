package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.FileBean;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HoaDonService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ImportService;

@Service
public class ImportServiceImp implements ImportService {

	@Autowired
	private HoaDonService hoaDonServ;

	@SuppressWarnings("resource")
	@Override
	public String importChiSo(FileBean fileBean) {

		ByteArrayInputStream bis = new ByteArrayInputStream(fileBean.getFileData().getBytes());
		Workbook workbook;
		int i = 2;
		try {
			if (fileBean.getFileData().getOriginalFilename().endsWith("xls")) {
				workbook = new HSSFWorkbook(bis);
			} else if (fileBean.getFileData().getOriginalFilename().endsWith("xlsx")) {
				workbook = new XSSFWorkbook(bis);
			} else {
				return new Response(-1, "Vui lòng chọn file Excel!").toString();
			}

			List<HoaDonDto> dsHoaDon = new ArrayList<HoaDonDto>();

			for (Row row : workbook.getSheetAt(0)) {
				if (row.getRowNum() > 0 && !row.getCell(0).getStringCellValue().equals("")) {
					// Cell cell0 = row.getCell(0);
					// Cell cell6 = row.getCell(6);
					// Cell cell7 = row.getCell(7);
					// System.out.println(cell0.getStringCellValue());
					// System.out.println((int) cell6.getNumericCellValue());
					// System.out.println((int) cell7.getNumericCellValue());
					HoaDonDto hoaDonDto = new HoaDonDto();
					hoaDonDto.setMaKhachHang(row.getCell(0).getStringCellValue());
					// hoaDonDto.setChiSoCu(Integer.parseInt(row.getCell(6).getStringCellValue()));
					row.getCell(6).setCellType(CellType.STRING);
					hoaDonDto.setChiSoCu( Integer.parseInt(row.getCell(6).getStringCellValue()));
					row.getCell(7).setCellType(CellType.STRING);
					hoaDonDto.setChiSoMoi( Integer.parseInt(row.getCell(7).getStringCellValue()));
					dsHoaDon.add(hoaDonDto);
					// Iterator<Cell> cellIterator = row.cellIterator();
					// while (cellIterator.hasNext()) {
					// Cell cell = cellIterator.next();
					// System.out.println(cell.getNumericCellValue());
					// }
				}

				if (row.getRowNum() > 0)
					i++;
			}

			hoaDonServ.capNhatChiSoNuocBangFileExcel(dsHoaDon);

			return new Response(1, "Success").toString();

		} catch (IOException e) {
			e.printStackTrace();
			return new Response(-1, "Lỗi dữ liệu tại dòng " + i).toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, "Lỗi dữ liệu tại dòng " + i).toString();
		}
	}

}
