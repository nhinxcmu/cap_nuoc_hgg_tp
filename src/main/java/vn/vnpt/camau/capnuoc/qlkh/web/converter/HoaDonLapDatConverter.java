package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonLapDatDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.HoaDonLapDat;

@Component
public class HoaDonLapDatConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public HoaDonLapDatDto convertToDto(HoaDonLapDat entity) {
		if (entity == null) {
			return new HoaDonLapDatDto();
		}
		HoaDonLapDatDto dto = mapper.map(entity, HoaDonLapDatDto.class);
		return dto;
	}

	public HoaDonLapDat convertToEntity(HoaDonLapDatDto dto) {
		if (dto == null) {
			return new HoaDonLapDat();
		}
		HoaDonLapDat entity = mapper.map(dto, HoaDonLapDat.class);
		return entity;
	}
	public Long convertToIdHoaDon(HoaDonLapDat entity) {
		return entity.getIdHoaDon();
	}

}
