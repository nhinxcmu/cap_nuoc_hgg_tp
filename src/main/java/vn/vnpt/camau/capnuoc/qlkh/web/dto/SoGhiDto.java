package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class SoGhiDto implements Comparable<SoGhiDto> {

	@Getter
	@Setter
	@NotNull
	private Long idSoGhi;
	@Getter
	@Setter
	@Mapping("duong.idDuong")
	private Long idDuong;
	@Getter
	@Setter
	@Mapping("duong.tenDuong")
	private String tenDuong;
	@Getter
	@Setter
	@Mapping("duong.khuVuc.idKhuVuc")
	private Long idKhuVuc;
	@Getter
	@Setter
	@Mapping("duong.khuVuc.tenKhuVuc")
	private String tenKhuVuc;
	@Getter
	@Setter
	@Mapping("duong.khuVuc.donVi.idDonVi")
	private Long idDonVi;
	@Getter
	@Setter
	@Mapping("duong.khuVuc.donVi.tenDonVi")
	private String tenDonVi;
	@Getter
	@Setter
	@NotNull
	@Size(max = 250)
	private String tenSoGhi;
	
	@Override
	public int compareTo(SoGhiDto object) {
		return this.tenSoGhi.compareTo(object.tenSoGhi);
	}
	
}
