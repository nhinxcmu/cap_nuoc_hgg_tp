package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.FileBean;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhachHangService;

@Controller
@RequestMapping("/chuyen-khu-vuc")
public class ChuyenKhuVucController {
	private ModelAttr modelAttr = new ModelAttr("Chuyển khu vực", "chuyenkhuvuc",
			new String[] { "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js", "js/chuyenkhuvuc.js" },
			new String[] { "jdgrid/jdgrid.css" });
	@Autowired
	private KhachHangService khachHangServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@PostMapping("/xu-ly")
	public @ResponseBody String upload(FileBean uploadItem, BindingResult result) {
		return khachHangServ.chuyenKhuVuc(uploadItem);
	}
}
