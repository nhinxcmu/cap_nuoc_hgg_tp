package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

public class ChiTietHoaDonDto {

	@Getter
	@Setter
	private Long idChiTietHoaDon;
	@Getter
	@Setter
	private BigDecimal tieuThu;
	@Getter
	@Setter
	private BigDecimal donGia;
	@Getter
	@Setter
	private long thanhTien;
	@Getter
	@Setter
	private long phiBvmt;
	@Getter
	@Setter
	private long phiBvr;
	
}
