package vn.vnpt.camau.capnuoc.qlkh.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import vn.vnpt.camau.capnuoc.qlkh.web.entity.PhieuThanhToan;

public interface PhieuThanhToanRepository extends CrudRepository<PhieuThanhToan, Long> {

	@Query("select ptt from PhieuThanhToan ptt join ptt.khachHang kh join kh.thongTinKhachHangs ttkh join ttkh.thangs ttkht left join kh.dongHoKhachHangThangs dhkht "
			+ "where ptt.nguoiDung.idNguoiDung = ?1 and ptt.thang.idThang = ?2 and ttkht.idThang = ?2 "
				+ " and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null) "
			+ "order by ptt.idPhieuThanhToan desc")
	@EntityGraph(attributePaths = { "nguoiDung", "khachHang.thongTinKhachHangs", "khachHang.dongHoKhachHangThangs.dongHo" })
	List<PhieuThanhToan> layDsPhieuThanhToanTheoNguoiDung(Long idNguoiDung, Long idThang);

	@Query("select ptt from PhieuThanhToan ptt join ptt.khachHang kh join kh.thongTinKhachHangs ttkh join ttkh.thangs ttkht left join kh.dongHoKhachHangThangs dhkht left join dhkht.dongHo dh "
			+ "where ptt.nguoiDung.idNguoiDung = ?1 and ptt.thang.idThang = ?2 and ttkht.idThang = ?2 "
				+ "and (ttkh.maKhachHang like %?3% or ttkh.tenKhachHang like %?3% or ttkh.diaChiSuDung like %?3% or ttkh.diaChiThanhToan like %?3% or ttkh.soDienThoai like %?3% or dh.maDongHo like %?3%)"
				+ " and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null) "
			+ "order by ptt.idPhieuThanhToan desc")
	@EntityGraph(attributePaths = { "nguoiDung", "khachHang.thongTinKhachHangs", "khachHang.dongHoKhachHangThangs.dongHo" })
	List<PhieuThanhToan> layDsPhieuThanhToanTheoNguoiDungVaThongTinCanTim(Long idNguoiDung, Long idThang,
			String thongTinCanTim);

	@Query("select ptt from PhieuThanhToan ptt join ptt.khachHang kh join kh.thongTinKhachHangs ttkh join ttkh.thangs ttkht "
			+ "where ptt.idPhieuThanhToan = ?1 and ptt.thang.idThang = ttkht.idThang")
	@EntityGraph(attributePaths = { "thanhToans.hoaDon.thang", "nguoiDung", "khachHang.thongTinKhachHangs" })
	PhieuThanhToan findByIdPhieuThanhToan(Long id);

	@Query("select ptt from PhieuThanhToan ptt join ptt.khachHang kh join kh.thongTinKhachHangs ttkh join ttkh.thangs ttkht left join kh.dongHoKhachHangThangs dhkht "
				+ "join ttkh.soGhi sg join sg.duong d join d.khuVuc kv "
			+ "where ptt.nguoiDung.idNguoiDung = ?1 and ptt.thang.idThang = ?2 and ttkht.idThang = ?2 "
				+ " and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null) and ttkh.nganHang.idNganHang <> null and kv.idKhuVuc = ?3 "
			+ "order by ptt.idPhieuThanhToan desc")
	@EntityGraph(attributePaths = { "nguoiDung", "khachHang.thongTinKhachHangs", "khachHang.dongHoKhachHangThangs.dongHo" })
	Page<PhieuThanhToan> layDsPhieuThanhToanChuyenKhoanTheoNguoiDung(Long idNguoiDung, Long idThang, Long idKhucVuc,
			Pageable pageable);

	@Query("select ptt from PhieuThanhToan ptt join ptt.khachHang kh join kh.thongTinKhachHangs ttkh join ttkh.thangs ttkht left join kh.dongHoKhachHangThangs dhkht "
				+ "join ttkh.soGhi sg join sg.duong d join d.khuVuc kv "
			+ "where ptt.nguoiDung.idNguoiDung = ?1 and ptt.thang.idThang = ?2 and ttkht.idThang = ?2 "
				+ " and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null) and ttkh.nganHang.idNganHang = null and kv.idKhuVuc = ?3 "
			+ "order by ptt.idPhieuThanhToan desc")
	@EntityGraph(attributePaths = { "nguoiDung", "khachHang.thongTinKhachHangs", "khachHang.dongHoKhachHangThangs.dongHo" })
	Page<PhieuThanhToan> layDsPhieuThanhToanTheoNguoiDung(Long idNguoiDung, Long idThang, Long idKhucVuc,
			Pageable pageable);

	@Query("select ptt from PhieuThanhToan ptt join ptt.khachHang kh join kh.thongTinKhachHangs ttkh join ttkh.thangs ttkht left join kh.dongHoKhachHangThangs dhkht left join dhkht.dongHo dh "
				+ "join ttkh.soGhi sg join sg.duong d join d.khuVuc kv "
			+ "where ptt.nguoiDung.idNguoiDung = ?1 and ptt.thang.idThang = ?2 and ttkht.idThang = ?2 "
				+ "and (ttkh.maKhachHang like %?3% or ttkh.tenKhachHang like %?3% or ttkh.diaChiSuDung like %?3% or ttkh.diaChiThanhToan like %?3% or ttkh.soDienThoai like %?3% or dh.maDongHo like %?3%)"
				+ " and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null) and ttkh.nganHang.idNganHang <> null and kv.idKhuVuc = ?4 "
			+ "order by ptt.idPhieuThanhToan desc")
	@EntityGraph(attributePaths = { "nguoiDung", "khachHang.thongTinKhachHangs", "khachHang.dongHoKhachHangThangs.dongHo" })
	Page<PhieuThanhToan> layDsPhieuThanhToanChuyenKhoanTheoNguoiDungVaThongTinCanTim(Long idNguoiDung, Long idThang,
			String thongTinCanTim, Long idKhucVuc, Pageable pageable);

	@Query("select ptt from PhieuThanhToan ptt join ptt.khachHang kh join kh.thongTinKhachHangs ttkh join ttkh.thangs ttkht left join kh.dongHoKhachHangThangs dhkht left join dhkht.dongHo dh "
				+ "join ttkh.soGhi sg join sg.duong d join d.khuVuc kv "
			+ "where ptt.nguoiDung.idNguoiDung = ?1 and ptt.thang.idThang = ?2 and ttkht.idThang = ?2 "
				+ "and (ttkh.maKhachHang like %?3% or ttkh.tenKhachHang like %?3% or ttkh.diaChiSuDung like %?3% or ttkh.diaChiThanhToan like %?3% or ttkh.soDienThoai like %?3% or dh.maDongHo like %?3%)"
				+ " and (dhkht.thang.idThang = ?2 or dhkht.thang.idThang = null) and ttkh.nganHang.idNganHang = null and kv.idKhuVuc = ?4 "
			+ "order by ptt.idPhieuThanhToan desc")
	@EntityGraph(attributePaths = { "nguoiDung", "khachHang.thongTinKhachHangs", "khachHang.dongHoKhachHangThangs.dongHo" })
	Page<PhieuThanhToan> layDsPhieuThanhToanTheoNguoiDungVaThongTinCanTim(Long idNguoiDung, Long idThang,
			String thongTinCanTim, Long idKhucVuc, Pageable pageable);

}
