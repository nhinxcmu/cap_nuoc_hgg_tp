package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.jasperreports.engine.JRParameter;
import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.NguoiDungDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.CallReportService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NguoiDungService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThangService;

@Controller
@RequestMapping("/nhan-vien-thu-tien")
public class NhanVienThuTienController {
	private ModelAttr modelAttr = new ModelAttr("Báo cáo nhân viên thu tiền", "nhanvienthutien",
			new String[] { "plugins/input-mask/jquery.inputmask.js",
					"plugins/input-mask/jquery.inputmask.date.extensions.js",
					"bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
					"bower_components/select2/dist/js/select2.min.js", "js/nhanvienthutien.js", "js/pdfobject.js" },
			new String[] { "bower_components/bootstrap-datepicker/css/bootstrap-datepicker.min.css",
					"bower_components/select2/dist/css/select2.min.css" });
	@Autowired
	DataSource dataSource;
	@Autowired
	private CallReportService callReportServ;
	@Autowired
	private NguoiDungService nguoiDungServ;
	@Autowired
	private ThamSoService thamSoServ;
	@Autowired
	private ThangService thangServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/lay-ds-nguoi-dung")
	public @ResponseBody List<NguoiDungDto> layDsNguoiDung() {
		return nguoiDungServ.layDsNguoiDungThuocXiNghiep();
	}

	@RequestMapping(value = "/xem-bao-cao", method = RequestMethod.GET)
	public @ResponseBody String xemBaoCao(HttpSession httpSession, long idNguoiDung, String hoTen, String tungay,
			String denngay, long idthang) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/nhanvienthutien.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("nhanvienthutien", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String ngaythu;
		if (tungay.equals(denngay))
			ngaythu = "Ngày " + tungay;
		else
			ngaythu = "Từ ngày " + tungay + " đến ngày " + denngay;

		String[] arrTuNgay = tungay.split("/");
		String[] arrDenNgay = denngay.split("/");
		tungay = arrTuNgay[2] + "-" + arrTuNgay[1] + "-" + arrTuNgay[0];
		denngay = arrDenNgay[2] + "-" + arrDenNgay[1] + "-" + arrDenNgay[0];

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ngaythu", ngaythu);
		parameters.put("tungay", tungay);
		parameters.put("denngay", denngay);
		// parameters.put("idthang",
		// Utilities.layThangLamViec(httpSession).getIdThang());
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("idnguoidung", idNguoiDung);
		parameters.put("idthang", idthang);
		parameters.put("hoten", hoTen);
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		return callReportServ.viewReport(reportFile, parameters, dataSource, httpSession);
	}

	@RequestMapping(value = "/xuat-file-pdf", method = RequestMethod.GET)
	public @ResponseBody void xuatFilePDF(HttpServletResponse response, HttpSession httpSession, long idNguoiDung,
			String hoTen, String tungay, String denngay, long idthang) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/nhanvienthutien.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("nhanvienthutien", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String ngaythu;
		if (tungay.equals(denngay))
			ngaythu = "Ngày " + tungay;
		else
			ngaythu = "Từ ngày " + tungay + " đến ngày " + denngay;

		String[] arrTuNgay = tungay.split("/");
		String[] arrDenNgay = denngay.split("/");
		tungay = arrTuNgay[2] + "-" + arrTuNgay[1] + "-" + arrTuNgay[0];
		denngay = arrDenNgay[2] + "-" + arrDenNgay[1] + "-" + arrDenNgay[0];

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ngaythu", ngaythu);
		parameters.put("tungay", tungay);
		parameters.put("denngay", denngay);
		// parameters.put("idthang",
		// Utilities.layThangLamViec(httpSession).getIdThang());
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("idnguoidung", idNguoiDung);
		parameters.put("idthang", idthang);
		parameters.put("hoten", hoTen);
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("pdf", reportFile, parameters, dataSource, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-xls", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLS(HttpServletResponse response, HttpSession httpSession, long idNguoiDung,
			String hoTen, String tungay, String denngay, long idthang) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/nhanvienthutien.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("nhanvienthutien", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String ngaythu;
		if (tungay.equals(denngay))
			ngaythu = "Ngày " + tungay;
		else
			ngaythu = "Từ ngày " + tungay + " đến ngày " + denngay;

		String[] arrTuNgay = tungay.split("/");
		String[] arrDenNgay = denngay.split("/");
		tungay = arrTuNgay[2] + "-" + arrTuNgay[1] + "-" + arrTuNgay[0];
		denngay = arrDenNgay[2] + "-" + arrDenNgay[1] + "-" + arrDenNgay[0];

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ngaythu", ngaythu);
		parameters.put("tungay", tungay);
		parameters.put("denngay", denngay);
		// parameters.put("idthang",
		// Utilities.layThangLamViec(httpSession).getIdThang());
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("idnguoidung", idNguoiDung);
		parameters.put("idthang", idthang);
		parameters.put("hoten", hoTen);
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("xls", reportFile, parameters, dataSource, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-xlsx", method = RequestMethod.GET)
	public @ResponseBody void xuatFileXLSX(HttpServletResponse response, HttpSession httpSession, long idNguoiDung,
			String hoTen, String tungay, String denngay, long idthang) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/nhanvienthutien.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("nhanvienthutien", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String ngaythu;
		if (tungay.equals(denngay))
			ngaythu = "Ngày " + tungay;
		else
			ngaythu = "Từ ngày " + tungay + " đến ngày " + denngay;

		String[] arrTuNgay = tungay.split("/");
		String[] arrDenNgay = denngay.split("/");
		tungay = arrTuNgay[2] + "-" + arrTuNgay[1] + "-" + arrTuNgay[0];
		denngay = arrDenNgay[2] + "-" + arrDenNgay[1] + "-" + arrDenNgay[0];

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ngaythu", ngaythu);
		parameters.put("tungay", tungay);
		parameters.put("denngay", denngay);
		// parameters.put("idthang",
		// Utilities.layThangLamViec(httpSession).getIdThang());
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("idnguoidung", idNguoiDung);
		parameters.put("idthang", idthang);
		parameters.put("hoten", hoTen);
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("xlsx", reportFile, parameters, dataSource, httpSession, response);
	}

	@RequestMapping(value = "/xuat-file-rtf", method = RequestMethod.GET)
	public @ResponseBody void xuatFileRTF(HttpServletResponse response, HttpSession httpSession, long idNguoiDung,
			String hoTen, String tungay, String denngay, long idthang) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		classPathResource = new ClassPathResource("/static/report/nhanvienthutien.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("nhanvienthutien", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		String ngaythu;
		if (tungay.equals(denngay))
			ngaythu = "Ngày " + tungay;
		else
			ngaythu = "Từ ngày " + tungay + " đến ngày " + denngay;

		String[] arrTuNgay = tungay.split("/");
		String[] arrDenNgay = denngay.split("/");
		tungay = arrTuNgay[2] + "-" + arrTuNgay[1] + "-" + arrTuNgay[0];
		denngay = arrDenNgay[2] + "-" + arrDenNgay[1] + "-" + arrDenNgay[0];

		KhuVucDto khuVucDto = Utilities.layKhuVucLamViec(httpSession);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ngaythu", ngaythu);
		parameters.put("tungay", tungay);
		parameters.put("denngay", denngay);
		// parameters.put("idthang",
		// Utilities.layThangLamViec(httpSession).getIdThang());
		parameters.put("idkhuvuc", khuVucDto.getIdKhuVuc());
		parameters.put("idnguoidung", idNguoiDung);
		parameters.put("idthang", idthang);
		parameters.put("hoten", hoTen);
		parameters.put("nguoiin", nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung()).getHoTen());
		parameters.put("congty", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		callReportServ.printReport("rtf", reportFile, parameters, dataSource, httpSession, response);
	}

	@GetMapping("/lay-ds-thang")
	public @ResponseBody List<ThangDto> layDanhSachThang() {
		return thangServ.layDsThang();
	}
}
