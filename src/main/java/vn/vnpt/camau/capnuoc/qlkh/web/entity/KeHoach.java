package vn.vnpt.camau.capnuoc.qlkh.web.entity;
// Generated Aug 12, 2019 9:10:55 AM by Hibernate Tools 5.1.7.Final

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * KeHoach generated by hbm2java
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "KE_HOACH", uniqueConstraints = @UniqueConstraint(columnNames = { "ID_THANG", "ID_DUONG" }))
public class KeHoach implements java.io.Serializable {

	private Long idKeHoach;
	private Thang thang;
	private Duong duong;
	private int chiSoCu;
	private int chiSoMoi;
	private int sanXuat;
	private int tieuThu;
	private long doanhThu;

	public KeHoach() {
	}

	public KeHoach(Thang thang, Duong duong, int chiSoCu, int chiSoMoi, int sanXuat, int tieuThu, long doanhThu) {
		this.thang = thang;
		this.duong = duong;
		this.chiSoCu = chiSoCu;
		this.chiSoMoi = chiSoMoi;
		this.sanXuat = sanXuat;
		this.tieuThu = tieuThu;
		this.doanhThu = doanhThu;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID_KE_HOACH", unique = true, nullable = false)
	public Long getIdKeHoach() {
		return this.idKeHoach;
	}

	public void setIdKeHoach(Long idKeHoach) {
		this.idKeHoach = idKeHoach;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_THANG", nullable = false)
	public Thang getThang() {
		return this.thang;
	}

	public void setThang(Thang thang) {
		this.thang = thang;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_DUONG", nullable = false)
	public Duong getDuong() {
		return this.duong;
	}

	public void setDuong(Duong duong) {
		this.duong = duong;
	}

	@Column(name = "CHI_SO_CU", nullable = false)
	public int getChiSoCu() {
		return this.chiSoCu;
	}

	public void setChiSoCu(int chiSoCu) {
		this.chiSoCu = chiSoCu;
	}

	@Column(name = "CHI_SO_MOI", nullable = false)
	public int getChiSoMoi() {
		return this.chiSoMoi;
	}

	public void setChiSoMoi(int chiSoMoi) {
		this.chiSoMoi = chiSoMoi;
	}

	@Column(name = "SAN_XUAT", nullable = false)
	public int getSanXuat() {
		return this.sanXuat;
	}

	public void setSanXuat(int sanXuat) {
		this.sanXuat = sanXuat;
	}

	@Column(name = "TIEU_THU", nullable = false)
	public int getTieuThu() {
		return this.tieuThu;
	}

	public void setTieuThu(int tieuThu) {
		this.tieuThu = tieuThu;
	}

	@Column(name = "DOANH_THU", nullable = false)
	public long getDoanhThu() {
		return this.doanhThu;
	}

	public void setDoanhThu(long doanhThu) {
		this.doanhThu = doanhThu;
	}

}
