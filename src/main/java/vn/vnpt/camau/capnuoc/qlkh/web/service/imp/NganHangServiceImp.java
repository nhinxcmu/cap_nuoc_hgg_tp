package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.NganHangConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.NganHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NganHang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.NganHangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NganHangService;

@Service
public class NganHangServiceImp implements NganHangService {

	@Autowired
	private NganHangConverter converter;
	@Autowired
	private NganHangRepository repository;

	@Override
	public Page<NganHangDto> layDsNganHang(Pageable pageable) {
		Page<NganHang> dsNganHangCanLay = repository.findByOrderByIdNganHangDesc(pageable);
		Page<NganHangDto> dsNganHangDaChuyenDoi = dsNganHangCanLay.map(converter::convertToDto);
		return dsNganHangDaChuyenDoi;
	}

	@Override
	public List<NganHangDto> layDsNganHang() {
		Pageable pageable = null;
		List<NganHangDto> dsNganHangCanLay = layDsNganHang(pageable).getContent().stream().sorted()
				.collect(Collectors.toList());
		return dsNganHangCanLay;
	}

	@Override
	public NganHangDto layNganHang(long id) {
		NganHang nganHangCanLay = repository.findOne(id);
		NganHangDto nganHangDaChuyenDoi = converter.convertToDto(nganHangCanLay);
		return nganHangDaChuyenDoi;
	}

	@Override
	public NganHangDto luuNganHang(NganHangDto dto) throws Exception {
		kiemTraNganHang(dto.getIdNganHang());
		NganHang nganHangCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(nganHangCanLuu);
			NganHangDto nganHangDaChuyenDoi = converter.convertToDto(nganHangCanLuu);
			return nganHangDaChuyenDoi;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu ngân hàng.");
		}
	}

	private void kiemTraNganHang(Long idNganHang) throws Exception {
		if (idNganHang != null) {
			NganHang nganHangCanKiemTra = repository.findOne(idNganHang);
			if (nganHangCanKiemTra == null) {
				throw new Exception("Không tìm thấy ngân hàng.");
			}
		}
	}

	@Override
	public void xoaNganHang(long id) throws Exception {
		kiemTraNganHang(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa ngân hàng.");
			}
			throw new Exception(e.getMessage());
		}
	}

}
