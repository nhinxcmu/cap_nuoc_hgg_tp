package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChiSoDto {

	@Getter
	@Setter
	private Long idChiSo;
	@Getter
	@Setter
	private String maKhachHang;
	@Getter
	@Setter
	private int thuTu;
	@Getter
	@Setter
	private String tenKhachHang;
	@Getter
	@Setter
	private String diaChiSuDung;
	@Getter
	@Setter
	private Long idDoiTuong;
	@Getter
	@Setter
	private String tenDoiTuong;
	@Getter
	@Setter
	private int mucSuDung;
	@Getter
	@Setter
	private Long idSoGhi;
	@Getter
	@Setter
	private String tenSoGhi;
	@Getter
	@Setter
	@Mapping("nguoiDungByIdNguoiDuyet.idNguoiDung")
	private Long idNguoiDuyet;
	@Getter
	@Setter
	@Mapping("nguoiDungByIdNguoiDuyet.hoTen")
	private String tenNguoiDuyet;
	@Getter
	@Setter
	@Mapping("nguoiDungByIdNguoiCapNhat.idNguoiDung")
	private Long idNguoiCapNhat;
	@Getter
	@Setter
	@Mapping("nguoiDungByIdNguoiCapNhat.hoTen")
	private String tenNguoiCapNhat;
	@Getter
	@Setter
	@Mapping("nguoiDungByIdNguoiDongBo.idNguoiDung")
	private Long idNguoiDongBo;
	@Getter
	@Setter
	@Mapping("nguoiDungByIdNguoiDongBo.hoTen")
	private String tenNguoiDongBo;
	@Getter
	@Setter
	@Mapping("trangThaiChiSo.idTrangThaiChiSo")
	private long idTrangThaiChiSo;
	@Getter
	@Setter
	@Mapping("trangThaiChiSo.tenTrangThaiChiSo")
	private String tenTrangThaiChiSo;
	@Getter
	@Setter
	private int chiSoCu;
	@Getter
	@Setter
	private int chiSoMoi;
	@Getter
	@Setter
	private int tieuThu;
	@Getter
	@Setter
	private int chiSoCuThangTruoc;
	@Getter
	@Setter
	private int tieuThuThangTruoc;
	@Getter
	@Setter
	private Date ngayGioCapNhat;
	@Getter
	@Setter
	private boolean duyetChiSo;
	@Getter
	@Setter
	private Date ngayGioDuyet;
	@Getter
	@Setter
	private String ghiChu;
	@Getter
	@Setter
	private Date ngayGioDongBo;
	@Getter
	@Setter
	private Long idDongHo;
	@Getter
	@Setter
	private String soSeri;
	@Getter
	@Setter
	private String lyDoCupNuoc;
	@Getter
	@Setter
	private String tenDoiTuongCupNuoc;
	
}
