package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;

@Controller
public class ForbiddenController {

	@GetMapping("/forbidden")
	public String forbidden(Model model) {
		ModelAttr modelAttr = new ModelAttr("Forbidden", "forbidden");
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}
}
