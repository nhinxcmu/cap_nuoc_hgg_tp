package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HoaDonService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.PhieuThanhToanService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.SoLieuService;

@Controller
@RequestMapping("/thanh-toan-chuyen-khoan")
public class ThanhToanChuyenKhoanController {
	private ModelAttr modelAttr = new ModelAttr("Thanh toán chuyển khoản", "thanhtoanchuyenkhoan",
			new String[] { "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js", "js/thanhtoanchuyenkhoan.js" },
			new String[] { "jdgrid/jdgrid.css" });
	@Autowired
	private SoLieuService soLieuServ;
	@Autowired
	private HoaDonService hoaDonServ;
	@Autowired
	private PhieuThanhToanService phieuThanhToanServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/lay-so-luong")
	public @ResponseBody String thongKeThanhToanChuyenKhoan() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(soLieuServ.thongKeThanhToanChuyenKhoan());
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/luu")
	public @ResponseBody String thanhToanHoaDonChuyenKhoan() {
		try {
			hoaDonServ.thanhToanHoaDonChuyenKhoan();
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@GetMapping("/lay-ds-da-thanh-toan")
	public @ResponseBody String layDsPhieuThanhToan(String thongTinCanTim, Pageable page) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(phieuThanhToanServ.layDsPhieuThanhToan(thongTinCanTim,
					Utilities.ID_HINH_THUC_THANH_TOAN_CHUYEN_KHOAN, page));
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/huy-thanh-toan")
	public @ResponseBody String huyPhieuThanhToan(long id) {
		try {
			phieuThanhToanServ.huyPhieuThanhToan(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
