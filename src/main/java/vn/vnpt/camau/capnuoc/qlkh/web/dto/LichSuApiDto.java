package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Data;

@Data
public class LichSuApiDto {
	private long idLichSu;
    @Mapping("nguoiDung.idNguoiDung")
	private Long idNguoiDung;
	@Mapping("nguoiDung.tenNguoiDung")
	private String tenNguoiDung;
	private Date thoiDiemPhatSinh;
	private String ip;
	private String parameters;
	private String duLieuBanDau;
	private String duLieuKetQua;
}
