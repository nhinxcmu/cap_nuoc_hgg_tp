package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhachHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuong;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NganHang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.NguoiDung;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.SoGhi;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThongTinKhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HoaDonService;

@Component
public class KhachHangConverter {

	@Autowired
	private DozerBeanMapper mapper;
	@Autowired
	private HoaDonService hoaDonServ;

	public KhachHangDto convertToDto(ThongTinKhachHang entity) {
		if (entity == null) {
			return new KhachHangDto();
		}
		KhachHangDto dto = mapper.map(entity, KhachHangDto.class);
		return dto;
	}

	public ThongTinKhachHang convertToEntity(KhachHangDto dto) {
		if (dto == null) {
			return new ThongTinKhachHang();
		}
		ThongTinKhachHang entity = mapper.map(dto, ThongTinKhachHang.class);
		setDoiTuong(dto, entity);
		setNganHang(dto, entity);
		setNguoiDung(dto, entity);
		setSoGhi(dto, entity);
		return entity;
	}

	private void setDoiTuong(KhachHangDto dto, ThongTinKhachHang entity) {
		if (dto.getIdDoiTuong() != null) {
			DoiTuong doiTuong = new DoiTuong();
			doiTuong.setIdDoiTuong(dto.getIdDoiTuong());
			entity.setDoiTuong(doiTuong);
		}
	}

	private void setNganHang(KhachHangDto dto, ThongTinKhachHang entity) {
		if (dto.getIdNganHang() != null) {
			NganHang nganHang = new NganHang();
			nganHang.setIdNganHang(dto.getIdNganHang());
			entity.setNganHang(nganHang);
		} else {
			entity.setNganHang(null);
		}
	}

	private void setNguoiDung(KhachHangDto dto, ThongTinKhachHang entity) {
		if (dto.getIdNguoiCapNhat() != null) {
			NguoiDung nguoiDung = new NguoiDung();
			nguoiDung.setIdNguoiDung(dto.getIdNguoiCapNhat());
			entity.setNguoiDung(nguoiDung);
		}
	}

	private void setSoGhi(KhachHangDto dto, ThongTinKhachHang entity) {
		if (dto.getIdSoGhi() != null) {
			SoGhi soGhi = new SoGhi();
			soGhi.setIdSoGhi(dto.getIdSoGhi());
			entity.setSoGhi(soGhi);
		}
	}

	public String convertToXml(ThongTinKhachHang thongTinKhachHang) {
		NganHang nganHang = thongTinKhachHang.getNganHang();
		String tenNganHang = "";
		if (nganHang != null) {
			tenNganHang = nganHang.getTenNganHang();
		}
		String customerXml = "<Customers>";
		customerXml += "<Customer>";
		customerXml += String.format("<Name><![CDATA[%s]]></Name>", thongTinKhachHang.getTenKhachHang());
		customerXml += String.format("<Code>%s</Code>", thongTinKhachHang.getMaKhachHang());
		customerXml += String.format("<TaxCode>%s</TaxCode>", thongTinKhachHang.getMaSoThue());
		customerXml += String.format("<Address><![CDATA[%s]]></Address>", thongTinKhachHang.getDiaChiSuDung());
		customerXml += String.format("<BankAccountName>%s</BankAccountName>", thongTinKhachHang.getTenTaiKhoan());
		customerXml += String.format("<BankName><![CDATA[%s]]></BankName>", tenNganHang);
		customerXml += String.format("<BankNumber>%s</BankNumber>", thongTinKhachHang.getSoTaiKhoan());
		customerXml += String.format("<Email>%s</Email>", "");
		customerXml += String.format("<Fax>%s</Fax>", "");
		customerXml += String.format("<Phone>%s</Phone>", thongTinKhachHang.getSoDienThoai());
		customerXml += String.format("<ContactPerson>%s</ContactPerson>", "");
		customerXml += String.format("<RepresentPerson>%s</RepresentPerson>", "");
		customerXml += String.format("<CusType>%s</CusType>", 0);
		customerXml += "</Customer>";

		customerXml += "</Customers>";

		return customerXml;
	}

	public KhachHangDto convertToDtoIncludeMaDongHo(ThongTinKhachHang entity) {
		if (entity == null) {
			return new KhachHangDto();
		}
		KhachHangDto dto = mapper.map(entity, KhachHangDto.class);
		dto.setIdSoGhi(entity.getSoGhi().getIdSoGhi());
		setMaDongHo(entity, dto);
		try {
			dto.setSoLuongHoaDon(hoaDonServ
					.layDsHoaDonChuaThanhToan(entity.getIdThongTinKhachHang(), Utilities.layIdNguoiDung()).size());
		} catch (Exception e) {
			dto.setSoLuongHoaDon(0);
			// e.printStackTrace();
		}
		return dto;
	}

	private void setMaDongHo(ThongTinKhachHang entity, KhachHangDto dto) {
		if (!entity.getKhachHang().getDongHoKhachHangThangs().isEmpty()) {
			DongHo dongHo = entity.getKhachHang().getDongHoKhachHangThangs().iterator().next().getDongHo();
			dto.setMaDongHo(dongHo.getMaDongHo());
			dto.setIdDongHo(dongHo.getIdDongHo());
			dto.setSoSeri(dongHo.getSoSeri());
		}
	}

}
