package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhachHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThongTinKhachHangNganHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.FileBean;

public interface KhachHangService {

	void capNhatTaiKhoanHDDTHangLoat() throws Exception;

	Page<KhachHangDto> layDsKhachHang(long idKhuVuc, long idDuong, long idSoGhi, String thongTinCanTim,
			Pageable pageable);

	List<KhachHangDto> layDsKhachHangFull(long idKhuVuc, long idDuong, long idSoGhi, String thongTinCanTim);

	Page<KhachHangDto> layDsKhachHangTheoNhanVienThanhToanGiaoDich(long idDuong, long idSoGhi, String thongTinCanTim,
			Pageable pageable);

	KhachHangDto layKhachHang(long id);

	KhachHangDto luuKhachHang1(KhachHangDto dto) throws Exception;

	void xoaKhachHang(long id) throws Exception;

	KhachHangDto layKhachHang(String thongTinCanTim);

	// mobile
	List<KhachHangDto> layDsKhachHang(long idSoGhi, String thongTinCanTim, long idNguoiDung) throws Exception;

	int layMaKhachHangMoi();

	Page<HoaDonDto> layDsHoaDonTheoIdKhachHang(long idKhachHang, Pageable pageable);

	void capNhatToaDo(long idThongTinKhachHang, Double viDo, Double kinhDo) throws Exception;

	String importKhachHang(FileBean fileBean);

	String capNhatDongHo(FileBean fileBean);

	String chuyenKhuVuc(FileBean fileBean);

	ThongTinKhachHangNganHangDto layKhachHangTheoMaKhachHang(String maKhachHang);
}
