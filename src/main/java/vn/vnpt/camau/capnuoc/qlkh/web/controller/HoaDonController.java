package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.jasperreports.engine.JRParameter;
import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ChiTietHoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DoiTuongCupNuocDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DuongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.HoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhuVucThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.LichSuInDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.SoGhiDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.TrangThaiChiSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.TrangThaiHoaDonDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.CallReportService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ChiTietHoaDonService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DoiTuongCupNuocService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DuongService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.HoaDonService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.KhuVucThangService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.LichSuInService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.SoGhiService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ThamSoService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.TrangThaiChiSoService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.TrangThaiHoaDonService;

@Controller
@RequestMapping("/hoa-don")
public class HoaDonController {
	private ModelAttr modelAttr = new ModelAttr("Quản lý chỉ số nước", "hoadon",
			new String[] { "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js", "bower_components/select2/dist/js/select2.min.js",
					"js/hoadon.js", "js/pdfobject.js" },
			new String[] { "bower_components/select2/dist/css/select2.min.css", "jdgrid/jdgrid.css" });
	@Autowired
	private DuongService duongServ;
	@Autowired
	private HttpSession httpSession;
	@Autowired
	private SoGhiService soGhiServ;
	@Autowired
	private HoaDonService hoaDonServ;
	@Autowired
	private ChiTietHoaDonService chiTietHoaDonServ;
	@Autowired
	private TrangThaiChiSoService trangThaiChiSoService;
	@Autowired
	private TrangThaiHoaDonService trangThaiHoaDonService;
	@Autowired
	private CallReportService callReportServ;
	@Autowired
	DataSource dataSource;
	@Autowired
	private ThamSoService thamSoServ;
	@Autowired
	private KhuVucThangService khuVucThangServ;
	@Autowired
	private KhuVucService khuVucServ;
	@Autowired
	private LichSuInService lichSuInServ;
	@Autowired
	private DoiTuongCupNuocService doiTuongCupNuocServ;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
	}

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/lay-khu-vuc-hien-tai")
	public @ResponseBody long layKhuVucHienTai() {
		return Utilities.layKhuVucLamViec(httpSession).getIdKhuVuc();
	}

	@GetMapping("/lay-ds-tt-chi-so")
	public @ResponseBody List<TrangThaiChiSoDto> layDsTrangThaiChiSo() {
		return trangThaiChiSoService.layDsTrangThaiChiSo();
	}

	@GetMapping("/lay-ds-tt-hoa-don")
	public @ResponseBody List<TrangThaiHoaDonDto> layDsTrangThaiHoaDon() {
		return trangThaiHoaDonService.layDsTrangThaiHoaDon();
	}

	@GetMapping("/lay-ds-gia-nuoc")
	public @ResponseBody List<ChiTietHoaDonDto> layDsChiTietHoaDon(long id) {
		return chiTietHoaDonServ.layDsChiTietHoaDon(id);
	}

	@GetMapping("/lay-ds-khu-vuc")
	public @ResponseBody List<KhuVucDto> layDsKhuVuc() {
		return khuVucServ.layDsKhuVuc(Utilities.layIdDonViLamViec(httpSession));
	}

	@GetMapping("/lay-ds-phuong")
	public @ResponseBody List<DuongDto> layDsDuong(long id) {
		return duongServ.layDsDuong(id);
	}

	@GetMapping("/lay-ds-to")
	public @ResponseBody List<SoGhiDto> layDsSoGhi(long id) {
		return soGhiServ.layDsSoGhi(id);
	}

	@GetMapping("/lay-ds")
	public @ResponseBody Page<HoaDonDto> layDsHoaDon(long idKhuVuc, long idDuong, long idSoGhi, String thongTinCanTim,
			long idTrangThaiChiSo, long idTrangThaiHoaDon, int daThanhToan, Pageable page) {
		return hoaDonServ.layDsHoaDon(idKhuVuc, idDuong, idSoGhi, thongTinCanTim, idTrangThaiChiSo, idTrangThaiHoaDon,
				daThanhToan, page);
	}
	
	@GetMapping("/lay-ds-doi-tuong-cup-nuoc")
	public @ResponseBody List<DoiTuongCupNuocDto> layDsDoiTuongCupNuoc() {
		return doiTuongCupNuocServ.layDsDoiTuongCupNuoc();
	}

	@GetMapping("/lay-ct")
	public @ResponseBody HoaDonDto layHoaDon(long id) {
		return hoaDonServ.layHoaDon(id);
	}

	@GetMapping("/lay-lich-su")
	public @ResponseBody List<LichSuInDto> layDsLichSuIn(long id) {
		return lichSuInServ.layDsLichSuIn(id);
	}

	@PostMapping("/luu-chi-so-moi")
	public @ResponseBody String luuChiSoMoi(long id, int chiSoMoi) {
		try {
			hoaDonServ.luuChiSoMoi(id, chiSoMoi);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/khoan-tieu-thu")
	public @ResponseBody String khoanTieuThu(long id, int tieuThu) {
		try {
			hoaDonServ.khoanTieuThu(id, tieuThu);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/luu-khong-xai")
	public @ResponseBody String luuKhongXai(long id) {
		try {
			hoaDonServ.luuKhongXai(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/luu-da-in")
	public @ResponseBody String luuDaIn(long id) {
		try {
			hoaDonServ.luuDaIn(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/luu-chi-so-cu")
	public @ResponseBody String luuChiSoCu(long id, int chiSoCu) {
		try {
			hoaDonServ.luuChiSoCu(id, chiSoCu);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/luu-chua-ghi-chi-so")
	public @ResponseBody String luuChuaGhiChiSo(long id) {
		try {
			hoaDonServ.luuChuaGhiChiSo(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/luu-cup-nuoc")
	public @ResponseBody String luuCupNuoc(long id, String lyDoCup, long idDoiTuong, String logCupNuoc) {
		try {
			HoaDonDto hoaDon = hoaDonServ.layHoaDon(id);
			if(hoaDon.getIdTrangThaiHoaDon() == Utilities.ID_TRANG_THAI_DA_PHAT_HANH_HOA_DON)
			{
				hoaDonServ.ngungPhatHanhHoaDon(id);
			}
			hoaDonServ.luuCupNuoc(id, lyDoCup, idDoiTuong, logCupNuoc);
			return new Response(1, "Success").toString();
		} catch (JpaSystemException e) {
			return new Response(-1, Utilities.getMessage_SQLException(e, "Không thể cập nhật cúp nước.")).toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/luu-mo-nuoc")
	public @ResponseBody String luuMoNuoc(long id, String logMoNuoc) {
		try {
			hoaDonServ.luuMoNuoc(id, logMoNuoc);
			return new Response(1, "Success").toString();
		} catch (JpaSystemException e) {
			return new Response(-1, Utilities.getMessage_SQLException(e, "Không thể cập nhật mở nước.")).toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/phat-hanh")
	public @ResponseBody String phatHanhHoaDon(long id) {
		try {
			hoaDonServ.phatHanhHoaDon(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/ngung-phat-hanh")
	public @ResponseBody String ngungPhatHanhHoaDon(long id) {
		try {
			hoaDonServ.ngungPhatHanhHoaDon(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@GetMapping("/lay-ds-full")
	public @ResponseBody List<Long> layKhachHangTruoc(long idDuong, long idSoGhi, String thongTinCanTim,
			long idTrangThaiChiSo, long idTrangThaiHoaDon, int daThanhToan) {
		return hoaDonServ.layDsIdHoaDon(idDuong, idSoGhi, thongTinCanTim, idTrangThaiChiSo, idTrangThaiHoaDon,
				daThanhToan);
	}

	@RequestMapping(value = "/xem-bao-cao", method = RequestMethod.GET)
	public @ResponseBody String xemBaoCao(HttpSession httpSession, long id) throws Exception {
		ClassPathResource classPathResource;
		InputStream inputStream;
		File reportFile;
		/*
		long idThang = Utilities.layThangLamViec(httpSession).getIdThang();

		if (thamSoServ.layThamSo(3).getGiaTri().equals("1500354922")) {
			classPathResource = new ClassPathResource("/static/report/inhoadonkhachhanghpg.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("inhoadonkhachhanghpg", ".jasper");
		} else if (idThang == 202011l || idThang == 202012l) {
			classPathResource = new ClassPathResource("/static/report/inhoadonkhachhanghoantra.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("inhoadonkhachhanghoantra", ".jasper");
		} else {
			classPathResource = new ClassPathResource("/static/report/inhoadonkhachhang.jasper");
			inputStream = classPathResource.getInputStream();
			reportFile = File.createTempFile("inhoadonkhachhang", ".jasper");
		}
		*/
		classPathResource = new ClassPathResource("/static/report/inhoadonkhachhang.jasper");
		inputStream = classPathResource.getInputStream();
		reportFile = File.createTempFile("inhoadonkhachhang", ".jasper");

		try {
			FileUtils.copyInputStreamToFile(inputStream, reportFile);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

		KhuVucThangDto khuVucThangDto = khuVucThangServ.layKhuVucThangLamViec();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", id);
		parameters.put("idthang", Utilities.layThangLamViec(httpSession).getIdThang());
		parameters.put("tuNgay", khuVucThangDto.getTuNgay());
		parameters.put("denNgay", khuVucThangDto.getDenNgay());
		parameters.put("logo",
				"iVBORw0KGgoAAAANSUhEUgAAAGoAAABdCAYAAABNXvOCAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAlwSFlzAAAXEQAAFxEByibzPwAAJb5JREFUeF7tnQd4lGW2x1HBXXtZdcWydu+ze+/q3bW7Xl17ZRXcq6KuumtdFdeGomJBUarYUAQ7CtgoglKkSe9FQFroJYQUQkgggUDO/f/eL+/MN5NJMpNMArt3v+eZZyaZb75y/u8553/K+34NGvx7++eRgFmZ7bovXZrboteYtXmbLdxQbFOzNtv3KzdZn0X59u7cXHtjdo69OkOvmdnu9Zb+/mBenvXW9wOXbrRRqzbZ1HVF+m2JrS3cannFpbaldLvtKNuV77/MIiNp1wTJrHTHDlsjgU6WcJfkl9imrdttyIpN9tLU9fbkhHX2xPh11laf3xMYAwTE2LWF9lNesWUWbbOibdvd77eVv4J7DAAHGL7zr/B3u6IsdjGgAiEiWLZF+cXSiFx7Zdp6679ko60o2Oq0YG1R8L4AQAq3lWuD17rgvVAgrSgocfsUCly2jI0lNiGzyMbzWltks7O3WK40ig3gNus3W7fvsO0x2hWryTsLxF0AqECwCGqBTNlwmbF10ob5EvCLU9bbQ2My3ftXi/OlVZtlqrbFILJNgs2Qps1cv9kJemNJqfVemG8dZ2Tbhz/lOU1cpON+qd+3HJ+p46219tOy7dvlBTY3Z4s7zwiZw64/BgMCEzpz/RZnEtFIXlFt23mg7XSgEO4U+Znu8i9dpT0IPV9CQsATMzfbOJmyDGkWJspv+QIDzei1IN9em5Vjny7YYHMkdDTktZk59sjYTHtvbp5N03FXb9pqK6WJ0/W5n7TynTm51lP7o1F5W0qdKZ2oY3XT/9Fe9lkk/5WlwTJ0RYHAy7Z39d3wlYW2QsfavsObz/oFbScBFdzkpq2lTkAt5WdektZ8L98zRVoDSYgdxea0DIAY4e/MybPHx2Xac5OynEnEdy2TWfspt9gBgrYg8DFrCm2xjoU58xuAb9WrWP9DG/15yqTRJfobjSzQdfF/fjdFAwZgX9UAeFm+8G1p3ghpPWSmPv1aPQNlVlK6w2Zp5JeIaaEtD49da0+IFDw3OcsJYeyaItvg/IYcvgSKOfx4/gZnztAOtAeNwYw9PXGdtZ+ebR/IxI0VKAXlvijWW6U68uP3D44GqJnyjZMEHAwSC9AvY6MtlraXhUhKXfmwegWKm3rrxxz7ZmmBZW/e6rQBao3vmZu7xTYLPL9BEmByz0xa50Y04KEdo1cXOrM1avUmm6D3ZTJrxQK/rgQUe9zoEMBkMuAG6l5goctEXALA6uZVb0Bh0tAA4pwi3WQm7E2veEeN1uwo22FfatSiOW9qf7RsqIQBSPFkIhBd3Qin6uN60MqcWcZEQmi8NUj3NaUFqO3EKRF7Hys0fAr2/31pB0B9J/8xaNlG+1xALJbT5jtuCpv/iUzcd8sKbL0+w8DwQbx3VuCKFq4SiAGp2BnAVHXOADT82/KNWy1ny7Y4il/76601UBACGFIiAeLMMQ85EvwL8kH4IQQPbWYUem3IFeXuLDoNoQAk6DSsb7nMCccOm8RdD6SKIBTLhAcEpvYA+WPUAihzQiQWgS2FL4qAlSwBGQMET5oHf7JUzCxbo807343yO+yL2WglQkEMNVvHgxYD1q4Qv6RT2LU5Vg2BMkeHf1hVaFsiAWEwesibETQ+IVYGE2NkQZ+JhfA9foNK95SpI45if4DqJAZHXASw8eDX5ib/FX5bA6DMacZXizeW0+GoegPSZwpCAYmEKPQbak0MMkuO1vujyQown5G/GiaCgPMdJcBn6HsCzSjFTp/Z+H8IlDm/0X1erkt6xjIuc8nTNlOyrLVIAO8kTJ+ZmOWoNxukY6TSNU9Le9Ccddof00mQGpCRKJP6VxBu9feQPGNNQaPM+ZvXJeBZ2ZtjQMLP5Mn3oB0EoGQaMGVdtC8mzm+YwNaKi54WkG/ou9dmqRQhEkH6Z7szi3WtRaGxEDco0Hbyjby85sdnR9I5kMoi95scWEkDBc2mttNvSUEFTSKpCRHYIHraQUARoM4U2yOD7bUOn4M2EsCiaU8JSILf1S6WSu5iUwcyqqH4SmgzIQE+cIRydxCePsp4EBYQPnRXfrCHXnzmf4QQAxRKjJRpJm+In83VMUhBxW6pD7CSbaXWedhCW1/gB3LVx0gKKEhAr4UbrJNGfyB8f1CVDnTxmDdMGnEOtSDoaXg0EuyihUvk20iKcuOLpF0BYUg3SMERAWalrofkLZlzsuMvKzTALKPtDBZ8Kekr6lr8r5X8pnuV/4/vSFXxzv+eLQ8v3tY9fK30EekkAvTUzbbZ5pJtdvzL39tVPSbqc1BqqWogJgGU2Q/KCHAzUOfwAQtKtrvMAd/9KA0qFogzVCIYpODUlwfIhrdRfDRPfipbptPHRtVdWGraEwUH80pQDTDPS7CuuKjXUwIAbQYoBE5Ml8qL3/BbjsGxPHj4YgYfmROYcDjLX/k9mK3ILrTDXhhiDR7qa3f1mWk7qhm01QAVkAduiHJ2fLmabDejkiCW9MlnMnnP6mYoc7NR04H9MZKXSoDDZB7J3y1Xfi41ICozCwFAFP9+UCiAwBCoB6amoCQLoAcPLfSJZdoByEXiBqJb/PWbDZ6XaQ2fHGgNHh9gDR7pZ+2+X1ilVlUJVPF2Sgq5bgQxUmO1wGychMN3lMUZWY+p9IC/8humEOAAsv309c4vQRxqr03BEdZJQ79VygkC48yXrgVwkhV0uvfzZpVrwU2gZdmqeVUEzOyxAXMcQA2e+MaBtadAGyLwKpNNFUCZy1A/MSHTPpqfF8OEoNkwPIJV2BssD7o9PRQrUf6mTEEwC4BkKXDItdckc6N1sMwbmopQyCHWxJylGyh/PK6Fa8IPttMgokJAHtD77bzCYjup/Qhr8Fj/AChej/a3E14Zbpn5sYy62hQSqR40AY2ZF6HYgQovFGEgXwfJIFnad0m+y0j4g5Idp36EzxqvoLdvRr5LHdVOkyjX73A5QPKCAIS/qCthp+u4APaY3AMDmZwnW9exSxwwEZA8WA/3s7/1npFQTgk1Cl9EhfRRaQJlCYp9HgQaPz4TA8Sk0TyyUi8qoiRfMZVsJGNfkClEq2BF9CWghTXXJnNEhKIh5pPBky5B1vVxYIsvShbj5Leo4q/ZsNmOk+bEaJMHquUA26PlN/bd3IomMCFQ0E0CW0BC0GEBw+awvwgMcAoFEhVWimckWymoQSC66EWq6VOBSoxV8745c36NZhW0iAFS18JNx/HxV1zvW2KfyMFtGuS39ZpuDaQ5FbQpZALPeP0H2yL6HpZ7Yh8lE0MGO+gLcGeIeRGX4Lswe5SkyduRtWBDqL5ETmkDR1+k4K5G2qQbG6XQAHA4ZjoEWNfHwD+hRZyHMCUa9Jt1GrkoMHnSnEqBKvdX709YFiP7CkDRQEKJgRipQJ/DqR18RKn0F3/DCHdBoPwE3adBSsRc/EQMA8GAqtJnUBOQMJV0tvr4p64FnI7jM5iQCYOXREA46O8xfpk1kllzdNxrT2XvAvPUV0dVDhSdObROkVpBm8g2BKWJQKMonUOH2Xjvoqw4iVi/kV4BXHogvhKBiEbsqaVYAInGkWAg7PqmzjM8WqnJhIS7npDNq6MWByCFWV51YGnfraEUXEij1JkqTYKK0w1EXxwnjZo+c61cRPtoySIRBBw8VBmazkYGo78YIP1yfAfZSLXhA80cpIHiUzbpGOl1dQyft2wvdkwwH1Dw6FaggfvA17MDc/dYEpoUBk8x1pB56yLyjwGKHBwmjdwdJifIILAFGvGNBEjuKwhqy1yNiQQmQJFxJhkL0aCBEj8G20vV7AH2rhYXxQPtASJ8occjJyaoDSQ2ZVmunf3mWGvwj77V+6RE2iU/dkevKFWPAWqtWrRo4XpcYGBnw7ERvorYCSbTSzHSDCVZEWifRRvchWHmyPuhcaSJACy1Ni5zwCKUnZldqEr7wgBh+j2BitEitcG1GTLf9n/6W7E7QCoPaKszdQm+P7zNUMsuCDI5EaB8nENiEdMHqQhrA6AhfC6WjlESrXSrjpTK+22qTKNPKQV1qGR9k7nzUQneFdmd90Feg6LBewgiWZQvZ66x3786OtCiVPxRZSA+0t++mrk6Fqigr0HNJ/ItFfN6QXvve6rsolE+RiA9EsRZQVGRZheICI2WqXbhUDbwtLaufEqqx/Ua1A4Ttzw+bxcFafiCLLv83QmBL6oqRkpVq5RZf7Dvj7FA0dxIoYwaDmYsUdW1UPEQZg2wiI/QPr+R5+srYZONILcVgJ2MRpmADTL0uwrD8wORXCIlk0Q+iPv+YXG2Xf/RFNsN81ZTX1QVeAL9zNfGWJlYcMT0DZBvotERZsfsCnxPmLHRdAIIxFfQcoCEQAR+zGySGCIjj2Qp6SM0MxmgqN98KKa5K6SFGCgMQioBPkQJ+x//eXQ5QA2fUJlCoz6p2ChVbWJ/McX9W39n6zduiQIFYyOTwBQTyulkvcOCpmSB6YPhMTmMKi5919Sr2CgMMhIhInQeBZPRqtMocwleBLSzs98MFHwReUysSjhY9QCNWrTemn402fZAiHUJkAeV4FivyUtzokC9qdwe+btnJDSKgWhYWND4ILIQ/eWD+D99BwiXOUNslDQomMH8gv5rtqqBQiMxtztTm7yZizTtuBbr2G1MRrY1+3BK/QEUStLi8z6btioKFLGR7xWgyLfWtYNFBU0w92J5gZCyB4Bwk/RBsNGkgl+CXPwosxi0IVcNFM0mEJLWKZbFUyUFle3PAEGbB4pq05odHVzBp8nLc615z2nWqL40KJF5lO/roCaYmDgKuwwFhQzEawSNKPimx5XDw8miTbyC1uOgKbOHYi9oeXeZz+iUysrL6GjlzqLjVIMJN4KKc6wWLVU/w/1fzba9nxpUPyauKv8loB7q92MUKHwFwqUKS38DxCG+rZhMxYfyP65jx6Xwc8onMpe5OKidNI6yPAKoHqgyNy+3vs0egwvCQFtYNGANgNomK/DWmKV29ItDA4DSEQvVhESEf6Og+fqPp4Z8lEwZhICkKkIepjJHdP5SrFYQ2MLs6C33mkfylpoUAAaJ26rNHmSkq4DGJ6bLlFV3HF+CILMSDKToNm/NRru6+8QgDkpUfa2twGv6e13L+a8rFeU3Rlg7TSwm11aiSi2lY9+K7E0D7cczy7tkAWu5zB0M0DM8hE+pnPiqugldNH1gZuszdsLcwWaj/YQBUL2mrbQjXpAW1UUsVFOA/O+k1Wd0UrbDbwSrLTS1H/8De0NjyAiHHSzZcHJ5TKEBPEwk6aaNiq2cn9LfTKGZJBDplahcqwKfxjnqi5ZjYslVRvOXujzV1toOXWB7QIN3JS0Kg6vr+p+wRpE+Yj0HOmd4R4DMsAiTCtJCdB211k0jaLLtkAFPKKhmMsu9g3wUtFuSqAQsc5MD6it+4hppDaDP0FuHMoH0+Ddzg5atVEsQtdWSVH4f76MAhOw5WuSrlNGuz2gL8xD5LoJa+ilggeyLufOADlNPN9/TN1E5oTDXsFkfWXJvWvG/YXbXZsiCAKRkKq6pCDbd+8oc/yPM+vAv3iyQBvINKUGmOArUqk1oXpbrUAIkTGW40T9HoxbfQx9FdCZHxU7R+gIKbfoiJsti9sWM1TJ3SZbF0y34VI8noDqG4yhA+nRBnlvsgjQRk4YhBdxkfJV2hcCinQx/hY8qjZkaqnZdAU3PRGD+ErG/oAmmrk0fGku4EJ59vyK3yI5pO0w+qYpOoFSFWVf70wQjFtpreigzgelitiDCZ6YgAqYD6IXyCdDxAqd+RU5sgPJ9AIufClrCgtoSlWLIQnhSdfgYZDE4V12SCadNitXC53Wl8XSWIlIBKdUiYiTXlxubmWBqDBpCTg/B93Ol92AKZ/hmMZO0QlGRJZtOcNxTGfBo2sncLEICSyrGibRqvSqhLsNRR3EUA4CBwCIifpu3Jt8OUDZ6p/gl19wywHZnYkCy4Iqa7xefPedmyHfRecTMBIgCiUpGJcQgOucp0BoyGHw3SMEtQXI3AYw59I2WvBOztFb/hS8uhgGDaJAvrKuAl74PGCoxoScRrQbN2znaBEgiLs3em2THtf0++R4Kaf5Zr/2ghHeFhRUD5kc9xldyGfHYeb+Qkxd2qTIRdCw9rNiLYBlQSeaG96Nr9jWB/J5ygLFr4AXTLwGyrnJ9HDeqzVqsY8tW+22nkfWfFiI+Uzrqpp5Tbd6qfDseoJJlmvpdi76znWLENWAKBgmQdi3K0H6mBG250V61KANcp7YxZt9hHpmxQS0rPj+4Rn6MHgvmL3kK70c4PYR1BRSaGg0blAlfmmt7tlKSNVU/kayZYj+cPyCQI6Sx5aGv7WCZrtbf/mTFslarRWSOYPJaskCpZ6LvrDWJgApAoLeOAJgqL4KkEAiAJDFjJyIHFV5ilE9UeMT05ctnBb3WUUCZ2Aa54JhhsCAaTMdJN6Hw/ilYTi7YeqhFuNZJVt/pipYQg5FyEhgNWpS/ZKooyx/y/GA7760x9vzg+TZ3LT462ObIRx74rHxkdS3N5YOjsZLDFbqQ4h0+PglQuFlM1FbZelqME8VGpI5o4cUXARK+xy8B6rWHub20oIX72fFjboZGmvvKAQrzzYKKfnvuu59qniZCQwBFr0YiA7Rx0W10ZY9JdnufGfa4JqW9MmKRfTJlhY1UFZgQYEf5zJbowNQslyU51rCVyEQyQGmfO0NTcCqdyIYWIESCWVqV8Te0KUMy+BwGln2YMU/WIkO/IwXFNNHYcryaWAQ6xwincuZQwq8HoJ4YoHRRqvk8BCpw9lGP3nUfTLa3xy21iSqLZ6nXLtpuHBkLCT6EY0izvgq0ndlLxvxKY0equ8kDXSlQOP+PRRYwfSzTie8hrqJTFs2KD4LRPIhHR1VsX5gcTBWlYzZWU80tJOKWc3PdpUEbGr4tnb4qYvpcp26wPa+myJSAIv8nYJuLBMxYGTSZVr5V1xsSMOUO0rpkY7jdBCj1sWqBYgd8Ej6E3B3NL5TrWdCDGX9FFUrt5tZ0pWjoVxKj1B6skBw7skjezlcl2PdWkKXguOn0VWQl6IzypveTySuS91ESEuX310ZnxGGTDCCV7/NXmbLIvN2qSIpIxPldx0VA4h6qnRUPMyOnB7Mjx+e0hv7y0JzUMHFA8yiHMBGbetUYac9PeRUnWENY0Cr2x1exjkU6fRUaSgU5UkuTI9/nGbUZJ8O45JO6aAZGdKsdQBynUIPWddFWl6mXuW0oEL+fH50gkARQwZIzBLUu0apRD2BE/GgbhCHRyisQBqZCugV5BRLNmgHjq3jD4YUV6QtMVwCMRjGgAn8alNkv7Ta++hEtkC5R12twXWy1BSk4zqxVG2w/Bkp1RELM8d4vZlY4dxILggQXSiBLbo+MxEcqwGFWyKyzvEGQ9IzeFOBSXKQ/sLc0pY18VgcFw7mRWQ+JE7V02wJUukwgA2tCpo/fzPVxVznjr5x+w97SB1IAVOeRizVIEkywDptAEYhTO4+yvML4pSKqNX2xAqXZEr9CyzMxEBSeeVT4pWBCW6wvgpLj4+gTpCxCGb+yJU29iaKFOF096IBOZsQ/SQCm1uT9SQHVTuQjBFQj0efJ6uNLJ1AEu2e9MaZqHymQDnt+iM2KEJdY2SetUZgCRjx+Ci2isQUNoQoMEMRIFRdDNLdQFOX5wWqyZyYi5hCz6IGJNy0UK4nbyIpU16ySzPf4qoFaeMqfLyNrU5BvS5RBF1B0wQ6ZG+sfamf+zAbMXmO7V+UbBdJBymCMCNHx+HMmDZT/IaYEYHh0AtrSS2wQ8DprWqRf6DdesxidmE4PFJPfcPTRTqDY0YOmsjpmOmbBu/5DaVawGEmwjc3IscM1el2aJ16zRMkfVQCbHo0yKxbpOvdNaVNlMZy0+wgF0CMXrK/ynCkDBQjQ6Y8kSLqWWEkZQVDRpT+djlo0yE++9qCxdBpVXdZUwlQSj6GZvnc9UWYE/5YOsDCBxHUE136boNzfiaz3QP9e2MFr5B8qEBdnxS93VxNSYdaW6mwi34SG6dxnyiTOVqtadQOjRkD5g5LnY5UW/NNI0XCoOK3MZCk+V56w4rKjgSlkLhRm6xH5LSbEsVRpoiXhtim9zxIJmK/artLCMQCL7ly/Lc0pUtP/lECQ5O58xkCfL3p7nBWoZladABObxeBX/WXy9iJlFDZ7fJYmN9JMkEf6z7ENbtUAtqoHQg2BCg7KdFGKh8xvwoz59X5YUJ5sOg0wfFfRFJa5yW5+qikL0XeTjwuKfBVpMQwS81rbCdh+GTgIyxa3Gg2nUwZGwfB/dVQJBJKBOSwXJpPTluVEWWN1woxeu/oyNPsQvxNJBJfnC0nanvPmOOs3x/vB6kFKKo6q6uKg4SRpyVRQdscc8oQaQMC8YbbwY5RDKo6aYI1Zshm0mJH9AAgmaUfXq/U3oeXoREJYt8KXXpIhEon28SuAcc5wdj2fks3Ypa5Q54AiI37/V3b0S8PsvYnLrSgyQyWikAk/sKLlkypQRia3uQx7XwcajLPL2OW2PFJFSA6kWgPlRxDxFNqBKWNeL4EmwSu+i6k8rALDRvBZce2JYKI2y/VA9ekrRJj0YrDMW3iUQjLojnKUv3yKak0B89oJw4z6ST0BTucYIcdOixYxjcskPPCVW62y1cB5Nnxhlq3RCmCFWgKnWDMwi/SeqSTt8IVKtalP8NDnBluDe75wrPJgfb6i+wTFUIvsm5+y3aMrcsubVZPTzqg5rJXpC6eOEDa5QPJ8aIefGPakPpOZYFopGYvP1Ped2MQFq4eRwaAhBQ0lA8JnNwU1tGY6T2SjwwmQfCW6JoD5uVFcM7lMVvgs2e5NrwaQrvnH1fn24aQVdr8qrZfJFJ7fdaxdrgxH8w8n222fTLUblVW/SHk50kOndRlt//vxFGszbIENmrvWVucVWZb89zhZmdE8xaeGIKVJo2KDXOZLuT52ZSKY5U4FmNWaKYtDPmiKAUQa9WMLjNFCI9pHKYTWauIztBNKP1FxWJBjDDYepQdgbunuWqzb5wHz/SG0d88SaFHBRq0cdab8omKtElakulOhrcortGxN3dxcEjtLsURuYcTqIi3otcFNt61tSipNGhULFtq1UXETBAM2RymDGfOUPaDtrIAM2SDG4aFcrK8U1s6wueMzWRACbGpc0HuYIJPlMKUIgJVk6CVkQCDsyGKLKXY4RRZELF9qDhNLVxZ+k/ZtMisMxMzNpZYn7cjX+XnlFuvpPfpfhnohZ8iEf62aGz76Sy3iD8ut6J9Tp/p1AFSsZqxUsyYz+tAo/AstaTNEkbmRp9UphK+AhQEIQNJjEQ9UdDyXOWbJDBKAY0lssvswSwQCoNBv/E6wAk2wCqUHLpU1ZgEtvNCvW81Zx2IgcBzMMk0/JKvpCeFvv/IzE/oCtuu31IGpdWYiVSeINjGVFNPFjUDF8Qm8uHlvstAIBI0pg5jwrNygEhzPjMJkq8wts0Ymn8Qw7zwrhDQUM0xgpFSdWU+QiQsI06+wTBXAr0PL/7xP9Z9591Nl2Q9ShB8MHpOUY8x5RoPROvZjf0w+kx+izyCpPUBe3nWoURXNWdAwk+9GIiMeUFgqAV/FI/IQNKkmQIWQ4OfCExAo7ccuV52IIcdqtINZ5pHeQ9gd+UpIT19pOK0G7iGYMm/0APrFJPmbUACCgRljYh7MlUVB8F8A7zS23LTS/MNcsoqTKv7pgAoLr8yZKd93QSzG32Q3ABKCAHDvSFie0ZG5wI/RYEPwy+rH0Hl8U6I2tlj4KgIX/z3H4JmLrN3kHhGrwYJ20y2MaaX1gIVQYK9oD4PoFX2m64prrS1RSMZK1ZNGxY+sqO3GZOG/qF1hPgAHjaOZkxGLiSIvyOgGSB6dR/WYTifMDvlC37aMf6N/kAQsfoyiphciHbMMBvKR/J9lF/jbf89nNI0HTRIM86xe/BTn/ocemAn5eXFqlmudI7kcnVEZb5rTp0VhAHcSULE3Q6MM03UwS8RNrCCDwyZtBENEMJgt/4hXCAWkBJBYMhXfQ+MNz01kaivP4iXYxg9+qokPjHqe+sbjkGCOTHulU4pYrYc0hTZuUmFkP2i8gRw8Kl8KccDEseI0syhjH+laPwDVs49KZpRFDRLmB+G6Mn/5Cpyke3hmFSC6bD1PzSnXQNaqgEKjWfyWABmAYX1kSwASbWFCAxrJ8gxoLVqCxuKHmPeFJpKx58Eq7vkhLNkaac5JH4NLxtTVO+uryUXF+xCcNKaJMgmL5rOqDEB8pGCSpbthh6y9hFbxxBrMJtqI6WTmPZ28AASlJ03FNFEy/B/q90wa5xphi7QahBeSTye9rpkc0p5CSkZjartPPMOLJQmYtkVKBlP+9y/8lC9OUo7BfJLwhVk6EhJJS8UTjtpea/p/v0v4qNqOttisRnKUPX3nTD8oia7tXwyo+hHazgD530ClpW+v7gdIFUAlMiHV/S9RcFnZ/5I1UbEpo4rBbHXfVx/wJh8gR0sg0dRWPBuMv57Kzh+fuansd8F+lQK1Y8d22769VNNHghefy8p41FDwcuovZ+y/D/sJMuTBPtFYw/22/Bjs649fWqrHdev/kdsNH1OfOVaF70PX4M/vH+7oro/jxT3c0v9/u87HOcOJX9YadMfx91V+ffxdui18fQFQsfemutVWshPB/bp7j9xPLEjbdN4tW6I9G+FrKC0tjfsuKrtKgFKLkw52+21/sQv/+Ee79k9/siuvuMIuvfhi6/v113bLzc3tphtvtNWrV9sava5v1sya33SjrVwZdJcuX77MbtLfl192mX34wQcRAN7t1s0uuegi6673zLVr3DGuufpqu/SSS9y+995zj82aNdNycrLt1ltusWZNm9qyZUvti88/d98/+MADtrmoyNaty7Sm111nnTt1stzcHLvrzjt1jU1swfz5tnFjvt16663WTN8vXuSfcEYBcKs988zTdtWVV7p74TqaXnettX3pJcvJ1vKskydZk2uutr/ecYcVFYnu5+Xan3VfnTt1tD69e7v933//PXcvb77xhl126aU2auQI9/fTTz9ld999lxUX69GA2evdffH92127Ru49P3+DtW/3il144YV21pln2t/+9lebPn2a+37DBr5rZxdfdLGdecYZ9hdd//Dh38cM8ko1aqtGyBtvvO4E+fOf/cz+8ze/sWefbW3fffutHXH44XZE48a2ePFiy8hYbIf84hfu7/nzf3IHf+P1122vn//c9mzUyM44/XTbtCkowz/y8MOorz2q9yVLMuyXhx1mxx17rHXr9o7dfddd7rubbrzBfXfsMcfYQQceZHPmzLEOHTrYz/bc0/Zs2MieaNnSFi5cYAcddKAT6loB/ptf/9r223dfmzJlsgP5GPfbAyOC4NzcDyDtsfvudt9991pvCf/0009z52zzwvM2+LtvbZ+99rKTTzrJgZ2ZudZ+cfDBdsftd9jYMWNs3332sUsuudgNknPPOcf97qEWLWz1qlV2zK9+Zffde6+7xw/ef9/dOzI79ZRTLDcnx01oe+ihFtZwjz3cAHvpxTZuH657SUaGu/dGDRva1VddZe3bt7PGjY+ww3/5Sxs2bGgMWFX6qP79+tn+++1nt0m72FatWmnHH3ecA4aRedVVV7qD/sfJJ1vG4kVWUlJiF1xwvt3cvLk08jY78IAD7JsBA9xvWz35pLvAp/SOpnCcE084wVq0eNAJkRvu3buXrVmz2t3EUUceZfPmzbMur75qjTU4fv+73zkAWrZ83J3vXgkHgTIYDj3kELv00kusSZNr7MgjjrBfHX20zZzpV+EPgLq+aTM7YP/9rXnzm+y1Ll3sHAmcQcZAQSiHHnqo/fepp1pBgZ5LkrXOHeNOaSum6vLLLnfXy7XwziA75+yzrWPHDu7aRo0a6XKGaBIW6L777nPn+rxPHwfuiScc734zdeoUJ4uhQ4bYl19+YcOGDnXnQYb+uztuv90Bd4+0NNiq8VHs0rtXL3fCm2Xu2LyAAerGG25was6FMhJXyOSNHj3KjUQAxBztq5GOGXNAtWpVAaiTTjzRnnqqlQP1qCOPdOZpsQBHg6NAdXY30qNHd/uf885zYHHTf5cwPFCHScjNmjV1Zo/jHJ0IKJkyfvuHc8+1o486yl1LiwcfdNc2aNBAd90AhY/ZJLDY57a/BAO0k0zgwQcdZCccf7ydp2tAmzgnoHE8wJws88n/AKvZdU1tn733thtlIbif4489zslp1syZ7niY6WnTptnoUaPcwGp8eGObPXuW++6O229zQOEKkgbqs08/dWp8ww1/dr9ZunSJ0yZG//LlywXOcifEY489xpmku+++2w7R6L7u2j85NWdfvp87d44DZPfddrOWjz3mjsMFAvDHH39kT5WD+DtpzcwZM9z/uem5c+daJ43a/WR68AnYdcyiG3H33O1M3ym//a0T4gxpEGbrRIHPb2fMmF5+o4FGNbnmGmeOuaeen3xie8vUnX3WWZa9Xs9enDbV/QYNaydfwrVy31gBtlk6Nte7h67/YZlujs3fXEfr1q3dPg88cL+zINeW37vXFAbv3/9+nzN9+B9cCjLhupHZrbfe4o5zi97Rbv+74d/H+qkqTd/gwZrdrRHEhbNBHtAWbhrysEqvK2QCm8rxjx8/zr2jOX7jJs6WiYBUdJVzxZG+KR+GeWvSpIn98YILnAk5V6PylltutrFjx1jWunWOSEAgMmTDe/b8xO03Qcdn69+/n533hz/Yy23bOueNmb1YRGeu/BkkgGu4TL9dsGB+5DogE48+8ojzLwMHDnSOH205+6yzHXBsX3/9lbsvgD5F/uVZXTvHZ9taUmwPPviAOy+OHpZ4jwYl5AAtYYBcd+218kUPRc758stt7Uzd7zvvvGOFhYXWoX17O/200+xkmW0siNegfP22o/ww+2LSm990k02YMD5Gm2LIRASxf3/YZSXwf/w8pblhZkEIAAAAAElFTkSuQmCC");
		parameters.put("tenCongTy", thamSoServ.layThamSo(1).getGiaTri());
		parameters.put("diaChi", thamSoServ.layThamSo(2).getGiaTri());
		parameters.put("taiKhoan", thamSoServ.layThamSo(34).getGiaTri());
		parameters.put("maSoThue", thamSoServ.layThamSo(3).getGiaTri());
		parameters.put("soDienThoai", thamSoServ.layThamSo(4).getGiaTri());
		parameters.put("tyLeGTGT", thamSoServ.layThamSo(5).getGiaTri());
		parameters.put(JRParameter.REPORT_LOCALE, Locale.ITALIAN);
		return callReportServ.viewReport(reportFile, parameters, dataSource, httpSession);
	}

	@PostMapping("/huy-hoa-don")
	public @ResponseBody String huyHoaDon(long id) {
		try {
			hoaDonServ.xoaHoaDon(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/them-hoa-don")
	public @ResponseBody String themHoaDon(long id) {
		try {
			long idHoaDonMoi = hoaDonServ.themHoaDon(id).getIdHoaDon();
			return new Response(1, Long.toString(idHoaDonMoi)).toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
