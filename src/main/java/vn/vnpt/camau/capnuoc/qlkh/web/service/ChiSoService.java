package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.ChiSoDto;

public interface ChiSoService {

	// mobile
	List<ChiSoDto> layDsChiSoTheoNguoiDung(long idNguoiDung);
	
	void chuyenDuLieuChiSo(boolean chuyenTatCa) throws Exception;
	
	void dongBoChiSo(List<ChiSoDto> dsChiSo, boolean boQua) throws Exception;

	Page<ChiSoDto> layDsChiSoTheoNguoiDung(long idNguoiDung, long idSoGhi, Pageable pageable);

	List<ChiSoDto> layDsChiSoCoTheKiemDuyetTheoNguoiDung(long idNguoiDung, long idSoGhi);
	
	void kiemDuyetChiSo(long idNguoiDung, long idSoGhi) throws Exception;
	
	void kiemDuyetChiSo(List<Long> dsIdChiSo) throws Exception;
	
}
