package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.DongHoConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.KhachHangConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.converter.PhieuSuaSeriConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DongHoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.KhachHangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.PhieuSuaSeriDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ThangDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DongHo;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.FileBean;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.PhieuSuaSeri;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.ThongTinKhachHang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DongHoRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.PhieuSuaSeriRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThongTinKhachHangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DongHoService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.PhieuSuaSeriService;

@Service
public class PhieuSuaSeriServiceImp implements PhieuSuaSeriService {
    @Autowired
    private PhieuSuaSeriConverter converter;
    @Autowired
    private PhieuSuaSeriRepository repo;
    @Autowired
    private HttpSession httpSession;
    @Autowired
    private ThongTinKhachHangRepository ttkhRepo;
    @Autowired
    private KhachHangConverter khConverter;
    @Autowired
    private DongHoRepository dongHoRepo;
    @Autowired
    private DongHoService dongHoService;
    @Autowired
    private DongHoConverter dhConverter;

    @Override
    public Page<PhieuSuaSeriDto> layDs(Long idNguoiDung, int trangThai, Date tuNgay, Date denNgay, Pageable pageable) {
        return repo.layDs(idNguoiDung, trangThai, tuNgay, denNgay, pageable).map(converter::convertToDto);
    }

    @Override
    public List<PhieuSuaSeriDto> layDs(Long idNguoiDung, int trangThai, Date tuNgay, Date denNgay) {
        if (idNguoiDung == 1) {
            if (trangThai !=1) {
                idNguoiDung = -1l;
            }
        }
        return repo.layDs(idNguoiDung, trangThai, tuNgay, denNgay).stream().map(converter::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public PhieuSuaSeriDto luu(PhieuSuaSeriDto phieuSuaSeri) throws Exception {
        if (phieuSuaSeri.getIdPhieuSuaSeri() != null && phieuSuaSeri.getTrangThai() != 1
                && phieuSuaSeri.getTrangThai() != 3)
            throw new Exception("Chỉ có thể sửa phiếu có trạng thái \"Khởi tạo\"");

        DongHoDto dongHo = kiemTraDongHoKhachHang(phieuSuaSeri.getIdKhachHang());
        phieuSuaSeri.setIdDongHo(dongHo.getIdDongHo());
        PhieuSuaSeri phieuMoi = converter.convertToEntity(phieuSuaSeri);
        phieuMoi.setNgayLap(new Date());
        phieuMoi.setNgayCapNhat(new Date());
        phieuMoi.setNguoiDung(Utilities.layNguoiDung());
        phieuMoi.setNguoiCapNhat(Utilities.layNguoiDung());
        ThangDto thang = Utilities.layThangLamViec(httpSession);
        phieuMoi.setThang(new Thang(thang.getIdThang(), thang.getTenThang()));
        if (phieuSuaSeri.getIdPhieuSuaSeri() == null)
            phieuMoi.setTrangThai(1);
        ThongTinKhachHang ttkh = ttkhRepo.findByIdThongTinKhachHang(phieuSuaSeri.getIdThongTinKhachHang());
        phieuMoi.setThongTinKhachHang(ttkh);
        phieuMoi = repo.save(phieuMoi);
        if (phieuSuaSeri.getSoSeriCu() == null) {
            phieuMoi.setSoSeriCu(phieuMoi.getDongHo().getSoSeri());
            phieuMoi = repo.save(phieuMoi);
        }
        return converter.convertToDto(phieuMoi);
    }

    @Override
    public PhieuSuaSeriDto guiDuyet(PhieuSuaSeriDto phieuSuaSeri) throws Exception {
        PhieuSuaSeri phieuGuiDuyet = repo.findOne(phieuSuaSeri.getIdPhieuSuaSeri());
        if (phieuGuiDuyet.getTrangThai() != 1 && phieuGuiDuyet.getTrangThai() != 3)
            throw new Exception("Phiếu đã được gửi duyệt!");
        phieuGuiDuyet.setTrangThai(2);
        phieuGuiDuyet.setNgayCapNhat(new Date());
        phieuGuiDuyet.setNguoiCapNhat(Utilities.layNguoiDung());
        phieuGuiDuyet = repo.save(phieuGuiDuyet);
        return converter.convertToDto(phieuGuiDuyet);
    }

    @Override
    public PhieuSuaSeriDto tuChoiDuyet(PhieuSuaSeriDto phieuSuaSeri) throws Exception {
        PhieuSuaSeri phieuTuChoiDuyet = repo.findOne(phieuSuaSeri.getIdPhieuSuaSeri());
        if (phieuTuChoiDuyet.getTrangThai() != 2)
            throw new Exception("Phiếu đã được duyệt/từ chối duyệt!");
        phieuTuChoiDuyet.setTrangThai(3);
        phieuTuChoiDuyet.setNgayCapNhat(new Date());
        phieuTuChoiDuyet.setNguoiCapNhat(Utilities.layNguoiDung());
        phieuTuChoiDuyet.setLyDoTuChoi(phieuSuaSeri.getLyDoTuChoi());
        phieuTuChoiDuyet = repo.save(phieuTuChoiDuyet);
        return converter.convertToDto(phieuTuChoiDuyet);
    }

    @Override
    public PhieuSuaSeriDto duyet(PhieuSuaSeriDto phieuSuaSeri) throws Exception {
        PhieuSuaSeri phieuDuyet = repo.findOne(phieuSuaSeri.getIdPhieuSuaSeri());
        if (phieuDuyet.getTrangThai() != 2)
            throw new Exception("Phiếu đã được duyệt/từ chối duyệt!");
        phieuDuyet.setTrangThai(4);
        phieuDuyet.setNgayCapNhat(new Date());
        phieuDuyet.setNguoiCapNhat(Utilities.layNguoiDung());
        phieuDuyet.getDongHo().setSoSeri(phieuDuyet.getSoSeriMoi());
        phieuDuyet = repo.save(phieuDuyet);
        return converter.convertToDto(phieuDuyet);
    }

    @Override
    public int xoa(PhieuSuaSeriDto phieuSuaSeri) throws Exception {
        PhieuSuaSeri phieuXoa = repo.findOne(phieuSuaSeri.getIdPhieuSuaSeri());
        if (phieuXoa.getTrangThai() != 1 && phieuXoa.getTrangThai() != 3)
            throw new Exception("Không thể xóa phiếu có trạng thái khác \"Đã ghi\" và \"Bị từ chôi\"");
        repo.delete(phieuXoa.getIdPhieuSuaSeri());
        return 1;
    }

    @Override
    public String importChiSo(FileBean fileBean) {
        ByteArrayInputStream bis = new ByteArrayInputStream(fileBean.getFileData().getBytes());
        Workbook workbook;
        int i = 2;
        try {
            if (fileBean.getFileData().getOriginalFilename().endsWith("xls")) {
                workbook = new HSSFWorkbook(bis);
            } else if (fileBean.getFileData().getOriginalFilename().endsWith("xlsx")) {
                workbook = new XSSFWorkbook(bis);
            } else {
                return new Response(-1, "Vui lòng chọn file Excel!").toString();
            }
            Sheet sheet = workbook.getSheetAt(0);
            for (Row row : workbook.getSheetAt(0)) {
                if (row.getRowNum() > 0 && !row.getCell(0).getStringCellValue().equals("")) {
                    String maKhachHang = row.getCell(0).getStringCellValue();
                    ThongTinKhachHang tt = ttkhRepo.findByMaKhachHangAndThangs_IdThang(maKhachHang,
                            Utilities.layThangLamViec(httpSession).getIdThang());
                    if (tt == null)
                        throw new Exception("Không tìm thấy khách hàng có mã: " + maKhachHang);
                    KhachHangDto khachHang = khConverter.convertToDtoIncludeMaDongHo(tt);

                    String soSeriMoi = row.getCell(1).getStringCellValue();
                    if (soSeriMoi.equals("") || soSeriMoi == null)
                        throw new Exception("Số seri mới không được để trống");

                    PhieuSuaSeriDto phieuSuaSeri = new PhieuSuaSeriDto();
                    phieuSuaSeri.setIdDongHo(khachHang.getIdDongHo());
                    phieuSuaSeri.setIdKhachHang(khachHang.getIdKhachHang());
                    phieuSuaSeri.setIdThongTinKhachHang(khachHang.getIdThongTinKhachHang());
                    phieuSuaSeri.setMaKhachHang(khachHang.getMaKhachHang());
                    phieuSuaSeri.setSoSeri(khachHang.getSoSeri());
                    phieuSuaSeri.setSoSeriMoi(soSeriMoi);
                    phieuSuaSeri = luu(phieuSuaSeri);
                }
                if (row.getRowNum() > 0)
                    i++;
            }

            return new Response(1, "Success").toString();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            return new Response(-1, "Lỗi dữ liệu tại dòng " + i).toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new Response(-1, "Lỗi dữ liệu tại dòng " + i).toString();
        }
    }

    DongHoDto kiemTraDongHoKhachHang(Long idKhachHang) throws Exception {
        DongHo dongHo = dongHoRepo.findByDongHoKhachHangThangs_Id_IdKhachHangAndDongHoKhachHangThangs_Id_IdThang(
                idKhachHang, Utilities.layThangLamViec(httpSession).getIdThang());
        if (dongHo == null) {
            DongHoDto dongHoDto = new DongHoDto();
            dongHoDto.setIdTrangThaiDongHo(1L);
            dongHoDto.setMaDongHo("");
            dongHoDto.setChungLoai("");
            dongHoDto.setSoSeri("");
            dongHoDto.setKichCo("");
            dongHoDto.setNhaSanXuat("");
            dongHoDto.setGhiChuSuaChuaLanCuoi("");
            dongHoDto.setIdKhachHang(idKhachHang);
            dongHoDto = dongHoService.luuDongHo(dongHoDto);
            return dongHoDto;
        }
        return dhConverter.convertToDto(dongHo);
    }

    @Override
    public String nhacViecNhanVien() {
        int soPhieuChoDuyet = repo.demPhieuTheoTrangThai(Utilities.layIdNguoiDung(), 2);
        int soPhieuTuChoiDuyet = repo.demPhieuTheoTrangThai(Utilities.layIdNguoiDung(), 3);
        return "{\"choDuyet\":" + soPhieuChoDuyet + ", \"tuChoiDuyet\":" + soPhieuTuChoiDuyet + "}";
    }

    @Override
    public String nhacViecPhongKeHoach() {
        int soPhieuChoDuyet = repo.demPhieuTheoTrangThai(-1l, 2);
        return "{\"choDuyet\":" + soPhieuChoDuyet + "}";
    }

    @Override
    public PhieuSuaSeriDto luu(String maKhachHang, String soSeriMoi, String lyDoSua) throws Exception {
        ThongTinKhachHang ttkh = ttkhRepo.findByMaKhachHangAndThangs_IdThang(maKhachHang,
                Utilities.layThangLamViec(httpSession).getIdThang());
        if (ttkh == null) {
            throw new Exception("Không tìm thấy thông tin khách hàng");
        }
        DongHo dh = dongHoRepo.findByDongHoKhachHangThangs_Id_IdKhachHangAndDongHoKhachHangThangs_Id_IdThang(
                ttkh.getKhachHang().getIdKhachHang(), Utilities.layThangLamViec(httpSession).getIdThang());
        if (dh == null) {
            throw new Exception("Khách không có đồng hồ");
        }
        PhieuSuaSeri phieuSuaSeri = new PhieuSuaSeri();
        phieuSuaSeri.setDongHo(dh);
        phieuSuaSeri.setLyDo(lyDoSua);
        phieuSuaSeri.setNgayCapNhat(new Date());
        phieuSuaSeri.setNgayLap(new Date());
        phieuSuaSeri.setNguoiDung(Utilities.layNguoiDung());
        phieuSuaSeri.setSoSeriCu(dh.getSoSeri());
        phieuSuaSeri.setSoSeriMoi(soSeriMoi);
        Thang thang = new Thang();
        thang.setIdThang(Utilities.layThangLamViec(httpSession).getIdThang());
        phieuSuaSeri.setThang(thang);
        phieuSuaSeri.setThongTinKhachHang(ttkh);
        phieuSuaSeri.setTrangThai(1);
        return converter.convertToDto(repo.save(phieuSuaSeri));
    }

    @Override
    public PhieuSuaSeriDto layPhieuSuaSeri(Long idPhieuSuaSeri) {
        return converter.convertToDto(repo.findOne(idPhieuSuaSeri));
    }

    @Override
    public PhieuSuaSeriDto luu(Long idPhieuSuaSeri, String soSeriMoi, String lyDo) throws Exception {
        PhieuSuaSeri phieu = repo.findOne(idPhieuSuaSeri);
        phieu.setLyDo(lyDo);
        phieu.setSoSeriMoi(soSeriMoi);
        return converter.convertToDto(repo.save(phieu));
    }

}
