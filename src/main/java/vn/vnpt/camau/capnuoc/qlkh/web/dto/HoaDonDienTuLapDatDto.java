package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class HoaDonDienTuLapDatDto {

	private Long idHoaDonDienTu;
	@Mapping("hoaDonLapDat.idHoaDon")
	private Integer idHoaDon;
	@Mapping("hoaDonLapDat.tongTien")
	private Double tongTien;

	@Mapping("thang.idThang")
	private Long idThang;
	@Mapping("thang.tenThang")
	private String tenThang;

	@Mapping("nguoiDung.idNguoiDung")
	private String idNguoiDung;
	@Mapping("nguoiDung.tenNguoiDung")
	private String tenNguoiDung;

	private String kyHieu;
	private String soHoaDon;
	private Date ngayGioCapNhat;
	private boolean huy;
	private boolean phatHanhThanhCong;
	private boolean thanhToan;

}
