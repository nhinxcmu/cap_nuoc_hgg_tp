package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.DoiTuongCupNuocConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DoiTuongCupNuocDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.DoiTuongCupNuoc;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DoiTuongCupNuocRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DoiTuongCupNuocService;

@Service
public class DoiTuongCupNuocServiceImp implements DoiTuongCupNuocService {

	@Autowired
	private DoiTuongCupNuocRepository repository;
	@Autowired
	private DoiTuongCupNuocConverter converter;
	
	@Override
	public List<DoiTuongCupNuocDto> layDsDoiTuongCupNuoc() {
		List<DoiTuongCupNuoc> dsDoiTuongCupNuocCanLay = repository.findAll();
		List<DoiTuongCupNuocDto> dsDoiTuongCupNuocDaChuyenDoi = dsDoiTuongCupNuocCanLay.stream().map(converter::convertToDto).collect(Collectors.toList());
		return dsDoiTuongCupNuocDaChuyenDoi;
	}	
	
}
