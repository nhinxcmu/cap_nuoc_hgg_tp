package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import java.util.Date;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class KhachHangDto {

	@Getter
	@Setter
	private Long idThongTinKhachHang;
	@Getter
	@Setter
	@Mapping("doiTuong.idDoiTuong")
	private Long idDoiTuong;
	@Getter
	@Setter
	@Mapping("doiTuong.tenDoiTuong")
	private String tenDoiTuong;
	@Getter
	@Setter
	@Mapping("nganHang.idNganHang")
	private Long idNganHang;
	@Getter
	@Setter
	@Mapping("nganHang.tenNganHang")
	private String tenNganHang;
	@Getter
	@Setter
	@Mapping("nguoiDung.idNguoiDung")
	private Long idNguoiCapNhat;
	@Getter
	@Setter
	@Mapping("nguoiDung.tenNguoiDung")
	private String tenNguoiCapNhat;
	@Getter
	@Setter
	@Mapping("soGhi.idSoGhi")
	private Long idSoGhi;
	@Getter
	@Setter
	@Mapping("soGhi.tenSoGhi")
	private String tenSoGhi;
	@Getter
	@Setter
	@Mapping("soGhi.duong.idDuong")
	private Long idDuong;
	@Getter
	@Setter
	@Mapping("soGhi.duong.tenDuong")
	private String tenDuong;
	@Getter
	@Setter
	@Mapping("soGhi.duong.khuVuc.idKhuVuc")
	private Long idKhuVuc;
	@Getter
	@Setter
	@Mapping("soGhi.duong.khuVuc.tenKhuVuc")
	private String tenKhuVuc;
	@Getter
	@Setter
	private String maKhachHang;
	@Getter
	@Setter
	private int thuTu;
	@Getter
	@Setter
	private String tenKhachHang;
	@Getter
	@Setter
	private String ho;
	@Getter
	@Setter
	private String ten;
	@Getter
	@Setter
	private String diaChiSuDung;
	@Getter
	@Setter
	private String diaChiThanhToan;
	@Getter
	@Setter
	private String maSoThue;
	@Getter
	@Setter
	private String soCmnd;
	@Getter
	@Setter
	private String soDienThoai;
	@Getter
	@Setter
	private String soHopDong;
	@Getter
	@Setter
	private boolean thuPbvmt;
	@Getter
	@Setter
	private Date ngayDangKy;
	@Getter
	@Setter
	private Date ngayLapDat;
	@Getter
	@Setter
	private Date ngayLapHopDong;
	@Getter
	@Setter
	private String soTaiKhoan;
	@Getter
	@Setter
	private String tenTaiKhoan;
	@Getter
	@Setter
	private Date ngayGioCapNhat;
	@Getter
	@Setter
	private Long idDongHo;
	@Getter
	@Setter
	private String maDongHo;
	@Getter
	@Setter
	private String soSeri;
	@Getter
	@Setter
	private Double viDo;
	@Getter
	@Setter
	private Double kinhDo;
	@Getter
	@Setter
	private boolean doiThiCong;
	@Getter
	@Setter
	@Mapping("khachHang.idKhachHang")
	private Long idKhachHang;
	@Getter
	@Setter
	private int soLuongHoaDon;
}
