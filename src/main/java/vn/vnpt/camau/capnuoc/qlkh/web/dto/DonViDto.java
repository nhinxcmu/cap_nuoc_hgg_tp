package vn.vnpt.camau.capnuoc.qlkh.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.dozer.Mapping;

import lombok.Getter;
import lombok.Setter;

public class DonViDto implements Comparable<DonViDto> {

	@Getter
	@Setter
	@NotNull
	private Long idDonVi;
	@Getter
	@Setter
	@Mapping("huyen.idHuyen")
	private Long idHuyen;
	@Getter
	@Setter
	@Mapping("huyen.tenHuyen")
	private String tenHuyen;
	@Getter
	@Setter
	@NotNull
	@Size(max = 250)
	private String tenDonVi;
	@Getter
	@Setter
	@NotNull
	private boolean xiNghiep;
	
	@Override
	public int compareTo(DonViDto object) {
		return this.tenDonVi.compareTo(object.getTenDonVi());
	}
}
