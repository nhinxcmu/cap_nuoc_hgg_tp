package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.ChiSoDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.NguoiDungDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.ChiSoService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NguoiDungService;
import vn.vnpt.camau.capnuoc.qlkh.web.service.SoLieuService;

@Controller
@RequestMapping("/kiem-duyet-chi-so")
public class KiemDuyetChiSoController {
	private ModelAttr modelAttr = new ModelAttr("Kiểm duyệt chỉ số nước", "kiemduyetchiso",
			new String[] { "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js", "bower_components/select2/dist/js/select2.min.js",
					"js/kiemduyetchiso.js" },
			new String[] { "bower_components/select2/dist/css/select2.min.css", "jdgrid/jdgrid.css" });
	@Autowired
	private NguoiDungService nguoiDungServ;
	@Autowired
	private SoLieuService soLieuServ;
	@Autowired
	private ChiSoService chiSoServ;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
	}

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/lay-ds-nhan-vien")
	public @ResponseBody List<NguoiDungDto> layDsNhanVienGhiThu() {
		return nguoiDungServ.layDsNhanVienGhiThu();
	}

	@GetMapping("/lay-ds")
	public @ResponseBody String layThongKeDuyetChiSo(long id, Pageable page) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(soLieuServ.thongKeDuyetChiSo(id, page));
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@GetMapping("/lay-ds-chi-so")
	public @ResponseBody Page<ChiSoDto> layDsChiSoTheoNguoiDung(long idNguoiDung, long idSoGhi, Pageable page) {
		return chiSoServ.layDsChiSoTheoNguoiDung(idNguoiDung, idSoGhi, page);
	}

	@GetMapping("/lay-ds-kiem-duyet")
	public @ResponseBody List<ChiSoDto> layDsChiSoCoTheKiemDuyetTheoNguoiDung(long idNguoiDung, long idSoGhi) {
		return chiSoServ.layDsChiSoCoTheKiemDuyetTheoNguoiDung(idNguoiDung, idSoGhi);
	}

	@PostMapping("/kiem-duyet-chi-so")
	public @ResponseBody String kiemDuyetChiSo(long idNguoiDung, long idSoGhi) {
		try {
			chiSoServ.kiemDuyetChiSo(idNguoiDung, idSoGhi);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/kiem-duyet-chi-so-khach-hang")
	public @ResponseBody String kiemDuyetChiSo(String strDsChiSo) {
		try {
			List<Long> dsIdChiSo = new ArrayList<Long>();
			for (String s : strDsChiSo.split(","))
				dsIdChiSo.add(Long.parseLong(s));
			chiSoServ.kiemDuyetChiSo(dsIdChiSo);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
