package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.LichSuInDto;

public interface LichSuInService {

	List<LichSuInDto> layDsLichSuIn(long idHoaDon);

}
