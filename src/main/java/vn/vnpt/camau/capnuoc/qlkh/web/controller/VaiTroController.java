package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import java.beans.PropertyEditorSupport;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.CayDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.MenuDto;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.VaiTroDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.VaiTroService;

@Controller
@RequestMapping("/vai-tro")
public class VaiTroController {
	private ModelAttr modelAttr = new ModelAttr("Vai trò", "vaitro",
			new String[] { "fancytree-2.23.0/jquery.fancytree-all-deps.min.js",
					"fancytree-2.23.0/jquery.fancytree.dnd.js", "jdgrid/jdgrid-v3.js", "jdpage/jdpage.js",
					"js/vaitro.js" },
			new String[] { "fancytree-2.23.0/skin-win8/ui.fancytree.min.css", "jdgrid/jdgrid.css" });
	@Autowired
	private VaiTroService vaiTroServ;

	@InitBinder("vaiTroDto")
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Set.class, "menuDtos", new CustomMenuEditor());
	}

	private class CustomMenuEditor extends PropertyEditorSupport {

		private Set<MenuDto> set = new HashSet<MenuDto>();

		@Override
		public void setValue(Object value) {
			set = new HashSet<MenuDto>();
			String[] a = (String[]) value;
			for (int i = 0; i < a.length; i++) {
				MenuDto d = new MenuDto();
				d.setIdMenu(Long.valueOf(a[i]));
				set.add(d);
			}
		}

		@Override
		public Set<MenuDto> getValue() {
			return set;
		}

		@Override
		public String getAsText() {
			// TODO Auto-generated method stub
			return super.getAsText();
		}

		@Override
		public void setAsText(String text) throws IllegalArgumentException {
			set = new HashSet<MenuDto>();
			String[] a = text.split(",");
			for (String s : a) {
				MenuDto d = new MenuDto();
				d.setIdMenu(Long.valueOf(s));
				set.add(d);
			}
		}
	}

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@GetMapping("/du-lieu-cay")
	public @ResponseBody List<CayDto> layDsCayMenu() {
		return vaiTroServ.layDsCayMenu();
	}

	@GetMapping("/lay-ds")
	public @ResponseBody Page<VaiTroDto> layDsVaiTro(Pageable page) {
		return vaiTroServ.layDsVaiTro(page);
	}

	@GetMapping("/lay-ct")
	public @ResponseBody VaiTroDto layVaiTro(long id) {
		return vaiTroServ.layVaiTro(id);
	}

	@PostMapping("/luu")
	public @ResponseBody String luu(VaiTroDto dto) {
		try {
			vaiTroServ.luuVaiTro(dto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			// e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/xoa")
	public @ResponseBody String xoa(long id) {
		try {
			vaiTroServ.xoaVaiTro(id);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			// e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}
}
