package vn.vnpt.camau.capnuoc.qlkh.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.vnpt.camau.capnuoc.qlkh.web.ModelAttr;
import vn.vnpt.camau.capnuoc.qlkh.web.Response;
import vn.vnpt.camau.capnuoc.qlkh.web.Utilities;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.NguoiDungDto;
import vn.vnpt.camau.capnuoc.qlkh.web.service.NguoiDungService;

@Controller
@RequestMapping("/thong-tin-nguoi-dung")
public class ThongTinNguoiDungController {

	private ModelAttr modelAttr = new ModelAttr("Thông tin người dùng", "thongtinnguoidung",
			new String[] { "js/thongtinnguoidung.js" });
	@Autowired
	private NguoiDungService nguoiDungServ;

	@GetMapping
	public String showPage(Model model) {
		model.addAttribute("MODEL", modelAttr);
		return "layout";
	}

	@PostMapping("/luu-nguoi-dung")
	public @ResponseBody String luuNguoiDung(NguoiDungDto nguoiDungDto) {
		try {
			nguoiDungServ.capNhatThongTinNguoiDung(nguoiDungDto);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@PostMapping("/doi-mat-khau")
	public @ResponseBody String DoiMatKhau(String matKhauCu, String matKhauMoi) {
		try {
			nguoiDungServ.doiMatKhauCuaNguoiDung(matKhauCu, matKhauMoi);
			return new Response(1, "Success").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new Response(-1, e.getMessage()).toString();
		}
	}

	@GetMapping("/lay-nguoi-dung")
	public @ResponseBody NguoiDungDto layNguoiDung() {
		return nguoiDungServ.layNguoiDung(Utilities.layIdNguoiDung());
	}

}
