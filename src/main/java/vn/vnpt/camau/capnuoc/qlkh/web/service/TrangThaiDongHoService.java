package vn.vnpt.camau.capnuoc.qlkh.web.service;

import java.util.List;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.TrangThaiDongHoDto;

public interface TrangThaiDongHoService {

	List<TrangThaiDongHoDto> layDsTrangThaiDongHo();
	
}
