package vn.vnpt.camau.capnuoc.qlkh.web.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.vnpt.camau.capnuoc.qlkh.web.converter.DuongConverter;
import vn.vnpt.camau.capnuoc.qlkh.web.dto.DuongDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Duong;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.Thang;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.DuongRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.repository.ThangRepository;
import vn.vnpt.camau.capnuoc.qlkh.web.service.DuongService;

@Service
public class DuongServiceImp implements DuongService {

	@Autowired
	private DuongConverter converter;
	@Autowired
	private DuongRepository repository;
	@Autowired
	private ThangRepository thangRepository;

	@Override
	public Page<DuongDto> layDsDuong(long idKhuVuc, String thongTinCanTim, Pageable pageable) {
		Page<Duong> dsDuongCanLay;
		if (idKhuVuc == 0) {
			if (thongTinCanTim.equals("")) {
				dsDuongCanLay = repository.findByOrderByIdDuongDesc(pageable);
			} else {
				dsDuongCanLay = repository.findByTenDuongContainsOrderByIdDuongDesc(thongTinCanTim, pageable);
			}
		} else {
			if (thongTinCanTim.equals("")) {
				dsDuongCanLay = repository.findByKhuVuc_IdKhuVucOrderByIdDuongDesc(idKhuVuc, pageable);
			} else {
				dsDuongCanLay = repository.findByKhuVuc_IdKhuVucAndTenDuongContainsOrderByIdDuongDesc(idKhuVuc,
						thongTinCanTim, pageable);
			}
		}
		Page<DuongDto> dsDuongDaChuyenDoi = dsDuongCanLay.map(converter::convertToDto);
		return dsDuongDaChuyenDoi;
	}

	@Override
	public List<DuongDto> layDsDuong(long idKhuVuc) {
		String thongTinCanTim = "";
		Pageable pageable = null;
		List<DuongDto> dsDuongCanLay = layDsDuong(idKhuVuc, thongTinCanTim, pageable).getContent().stream().sorted()
				.collect(Collectors.toList());
		return dsDuongCanLay;
	}

	@Override
	public DuongDto layDuong(long id) {
		Duong duongCanLay = repository.findByIdDuong(id);
		DuongDto duongDaChuyenDoi = converter.convertToDto(duongCanLay);
		return duongDaChuyenDoi;
	}

	@Override
	public DuongDto luuDuong(DuongDto dto) throws Exception {
		kiemTraDuong(dto.getIdDuong());
		Duong duongCanLuu = converter.convertToEntity(dto);
		try {
			repository.save(duongCanLuu);
			DuongDto duongDaChuyenDoi = converter.convertToDto(duongCanLuu);
			return duongDaChuyenDoi;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Không thể lưu phường.");
		}
	}

	private void kiemTraDuong(Long idDuong) throws Exception {
		if (idDuong != null) {
			Duong duongCanKiemTra = repository.findOne(idDuong);
			if (duongCanKiemTra == null) {
				throw new Exception("Không tìm thấy phường.");
			}
		}
	}

	@Override
	public void xoaDuong(long id) throws Exception {
		kiemTraDuong(id);
		try {
			repository.delete(id);
		} catch (Exception e) {
			if (e.getCause() instanceof ConstraintViolationException) {
				throw new Exception("Không thể xóa phường.");
			}
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public List<DuongDto> layDsDuongTheoHuyen(long idHuyen) {
		List<Duong> dsDuongCanLay = repository.findByKhuVuc_DonVi_Huyen_IdHuyenOrderByTenDuong(idHuyen);
		List<DuongDto> dsDuongDaChuyenDoi = dsDuongCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsDuongDaChuyenDoi;
	}

	@Override
	public List<DuongDto> layDsDuongTheoNguoiDung(long idNguoiDung) {
		Thang thangPhanCongGhiThuMoiNhat = thangRepository
				.findTopByLichGhiThuThangs_NguoiDuocPhanCong_IdNguoiDungOrderByIdThangDesc(idNguoiDung);
		if (thangPhanCongGhiThuMoiNhat == null) {
			return new ArrayList<DuongDto>();
		}
		long idThangPhanCongGhiThuMoiNhat = thangPhanCongGhiThuMoiNhat.getIdThang();
		List<Duong> dsDuongCanLay = repository
				.findDistinctDuongBySoGhis_LichGhiThus_Thangs_IdThangAndSoGhis_LichGhiThus_NguoiDuocPhanCong_IdNguoiDungOrderByTenDuong(
						idThangPhanCongGhiThuMoiNhat, idNguoiDung);
		List<DuongDto> dsDuongDaChuyenDoi = dsDuongCanLay.stream().map(converter::convertToDto)
				.collect(Collectors.toList());
		return dsDuongDaChuyenDoi;
	}

}
