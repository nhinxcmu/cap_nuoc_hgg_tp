package vn.vnpt.camau.capnuoc.qlkh.web.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.vnpt.camau.capnuoc.qlkh.web.dto.LichSuApiDto;
import vn.vnpt.camau.capnuoc.qlkh.web.entity.LichSuApi;

@Component
public class LichSuApiConverter {

	@Autowired
	private DozerBeanMapper mapper;

	public LichSuApiDto convertToDto(LichSuApi entity) {
		if (entity == null) {
			return new LichSuApiDto();
		}
		LichSuApiDto dto = mapper.map(entity, LichSuApiDto.class);
		return dto;
	}

	public LichSuApi convertToEntity(LichSuApiDto dto) {
		if (dto == null) {
			return new LichSuApi();
		}
		LichSuApi entity = mapper.map(dto, LichSuApi.class);
		return entity;
	}

}
